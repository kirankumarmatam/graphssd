#include "../../GraphSSD_library/graphFiltering.h"
//#include "../../backup/GraphSSD_library/graphFiltering.h"
#define BFS_DEBUG 0

extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

#if(MEASURE_TIME == 1)
vector<Timer> auxiliary_memory_timer;
vector< map<REQUEST_UNIQUE_ID_TYPE,Timer> > reqid_to_start_time;
vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, bool > > > Cache_response_queue_time;
double hit_elapsed_time_sum = 0;
double miss_elapsed_time_sum = 0;
extern unsigned int cache_hits, cache_misses;
vector< mutex_wrapper > map_lck;
#endif//MEASURE_TIME

void application_initialization()
{
	NumberOfInFlightRequests.reserve(number_of_application_response_queues);
#if(MEASURE_TIME == 1)
	auxiliary_memory_timer.resize(NumOfRequestingThreads);
	for (unsigned int i=0; i<NumOfRequestingThreads; i++)
	{
		mutex_wrapper tmp_mtx;
		map_lck.push_back(tmp_mtx);
	}
	reqid_to_start_time.resize(NumOfRequestingThreads);
	Cache_response_queue_time.resize(NumOfRequestingThreads);
#endif//MEASURE_TIME
}
struct timespec bfs_sort_time_start, bfs_sort_time_end;
double bfs_sort_time_elapsed = 0;

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();
	VERTEX_TYPE root = atoi(argv[8]), reqElement = atoi(argv[6]);//which argument number should be this one
	unsigned int MaxIterationNumber = atoi(argv[7]), currentIteration = 0;
	std::vector< std::vector<VERTEX_TYPE> >active_queue;
	active_queue.resize(2);
	std::vector<bool> Visited;
	Visited.resize(NumNodes, 0);
	bool oddOrEven = 0;
	active_queue[oddOrEven].push_back(root);
	unsigned int bfs_iteration_complete = 1;
	mutex bfs_visited_lock; // same lock for both active_queue and visited vector
	mutex bfs_iteration_lock;
	mutex bfs_barrier_lock;
	unsigned int bfs_barrier = 0;
	unsigned int active_queue_size = active_queue[oddOrEven].size();

	start_application_time();

#pragma omp parallel num_threads(NumOfRequestingThreads + NumberOfResponseThreads) 
	{
		unsigned int tid = omp_get_thread_num();
		if(tid < NumOfRequestingThreads)
		{
			while(active_queue[oddOrEven].size() > 0)
			{
				NumberOfInFlightRequests[tid] = 0;
				//For requesting threads
				unsigned int start_vertex_index = floor(active_queue_size * ((tid * 1.0) / NumOfRequestingThreads)), end_vertex_index = floor(active_queue_size * (((tid + 1)*1.0) / NumOfRequestingThreads));
#if(BFS_DEBUG == 1)
				application_output_file[tid] << "tid = " << tid <<  " start_vertex_index = " << start_vertex_index << " end_vertex_index = " << end_vertex_index << endl;
#endif//BFS_DEBUG
				while(start_vertex_index < end_vertex_index)
				{
					if(active_queue[oddOrEven][start_vertex_index] == reqElement)
					{
						printf("Element found\n");
						Finalize();
						exit(0);
					}
					APP_REQUEST_TYPE App_to_cache_request = {active_queue[oddOrEven][start_vertex_index], tid, active_queue[oddOrEven][start_vertex_index]};
#if(MEASURE_TIME == 1)
					map_lck[tid].lock();
					(reqid_to_start_time[tid])[active_queue[oddOrEven][start_vertex_index]];
					(reqid_to_start_time[tid])[active_queue[oddOrEven][start_vertex_index]].start_timer();
					map_lck[tid].unlock();
#endif//MEASURE_TIME
					while(1)
					{
						Cache_response_queue_lock[tid].lock();
						if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
						{
							NumberOfInFlightRequests[tid]++;
							Cache_response_queue_lock[tid].unlock();
							break;
						}
						Cache_response_queue_lock[tid].unlock();
					}
					while(1)
					{
						App_to_cache_request_queue_lock.lock();
						if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
						{
							App_to_cache_request_queue.push(App_to_cache_request);
#if(BFS_DEBUG == 1)
							application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//BFS_DEBUG
							App_to_cache_request_queue_lock.unlock();
							break;
						}
						App_to_cache_request_queue_lock.unlock();
					}
					start_vertex_index++;
				}
				while(1)
				{
					bfs_iteration_lock.lock();
					if(bfs_iteration_complete == 0){
						bfs_barrier_lock.lock();
						bfs_barrier++;
						if(bfs_barrier == NumOfRequestingThreads)
						{
clock_gettime(CLOCK_MONOTONIC, &bfs_sort_time_start);
							sort(active_queue[oddOrEven].begin(), active_queue[oddOrEven].end());
clock_gettime(CLOCK_MONOTONIC, &bfs_sort_time_end);
bfs_sort_time_elapsed += ((double)bfs_sort_time_end.tv_sec - (double)bfs_sort_time_start.tv_sec) * 1000000000;
bfs_sort_time_elapsed += ((double)bfs_sort_time_end.tv_nsec - (double)bfs_sort_time_start.tv_nsec);
							currentIteration++;
							bfs_iteration_complete = active_queue_size;
							stat_increment_iteration();
						}
						bfs_barrier_lock.unlock();
						bfs_iteration_lock.unlock();
#if(MINIMAL_PRINT == 1)
						cout << "currentIteration = " << currentIteration << endl;
#endif//MINIMAL_PRINT
						break;
					}
					bfs_iteration_lock.unlock();
				}
#if(BFS_DEBUG == 1)
				application_output_file[tid] << "bfs_barrier = " << bfs_barrier << endl;
#endif//BFS_DEBUG
				while(1)
				{
					bfs_barrier_lock.lock();
					if(bfs_barrier == NumOfRequestingThreads)
					{
						bfs_barrier_lock.unlock();
						break;
					}
					bfs_barrier_lock.unlock();
				}
				if(currentIteration == MaxIterationNumber)
				{
					cout << "tid = " << tid  << " breaking after iterations = " << currentIteration << endl;
					break;
				}
			}
			if(tid == 0)
			{
				printf("currentIteration = %u\n", currentIteration);
				printf("Element not found\n");
				if(active_queue[oddOrEven].size() > 0)
				{
					printf("One vertex in next level = %u\n", active_queue[oddOrEven].front());
				}
				else {
					printf("Next level is empty\n");
				}
				Finalize();
#if(MEASURE_TIME == 1)
				for(unsigned int i=0; i < NumOfRequestingThreads; i++)
				{
					cout << i << " ";
					auxiliary_memory_timer[i].print_timer("auxiliary mmeory update time ");
				}
#endif//MEASURE_TIME
				exit(0);
			}
		} else {
			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
#if(MEASURE_TIME == 1)
			std::pair<REQUEST_UNIQUE_ID_TYPE, bool > cache_to_app_response_hit;
#endif//MEASURE_TIME
			while(1) {
				while(1)
				{
					Cache_response_queue_lock[tid-NumOfRequestingThreads].lock();
					if(Cache_response_queue[tid-NumOfRequestingThreads].empty() == false)
					{
						cache_to_app_response = Cache_response_queue[tid-NumOfRequestingThreads].front();
						Cache_response_queue[tid-NumOfRequestingThreads].pop();
						NumberOfInFlightRequests[tid-NumOfRequestingThreads]--;
#if(MEASURE_TIME == 1)
						cache_to_app_response_hit = Cache_response_queue_time[tid-NumOfRequestingThreads].front();
						Cache_response_queue_time[tid-NumOfRequestingThreads].pop();
#endif//MEASURE_TIME
						Cache_response_queue_lock[tid-NumOfRequestingThreads].unlock();
#if(MEASURE_TIME == 1)
						map_lck[tid-NumOfRequestingThreads].lock();
						if(cache_to_app_response_hit.second == 1)
						{
							double time = (reqid_to_start_time[tid-NumOfRequestingThreads])[cache_to_app_response_hit.first].get_timer();
							hit_elapsed_time_sum += time;
						}
						else {
							double time = (reqid_to_start_time[tid-NumOfRequestingThreads])[cache_to_app_response_hit.first].get_timer();
							miss_elapsed_time_sum += time;
						}
						(reqid_to_start_time[tid-NumOfRequestingThreads]).erase(cache_to_app_response_hit.first);
						map_lck[tid-NumOfRequestingThreads].unlock();
#endif//MEASURE_TIME

					break;
				}
				Cache_response_queue_lock[tid-NumOfRequestingThreads].unlock();
				}
#if(BFS_DEBUG == 1)
				application_output_file[tid] << "popping request id = " << cache_to_app_response.first << " adj. list size = " << (cache_to_app_response.second)->size() << endl;
#endif//BFS_DEBUG

#if(MEASURE_TIME == 1)
				auxiliary_memory_timer[tid-NumOfRequestingThreads].start_timer();
#endif//MEASURE_TIME
				for(unsigned int i = 0; i < (cache_to_app_response.second)->size(); i++)
				{
					bfs_visited_lock.lock();
					if(Visited[(*(cache_to_app_response.second))[i]] == 0)
					{
						active_queue[!oddOrEven].push_back((*(cache_to_app_response.second))[i]);
						Visited[(*(cache_to_app_response.second))[i]] = 1;
					}
					bfs_visited_lock.unlock();
				}
				(*(cache_to_app_response.second)).clear();
#if(MEASURE_TIME == 1)
				auxiliary_memory_timer[tid-NumOfRequestingThreads].end_timer();
#endif//MEASURE_TIME
				bfs_iteration_lock.lock();
				bfs_iteration_complete--;
#if(BFS_DEBUG == 1)
				application_output_file[tid] << "bfs_iteration_complete = " << bfs_iteration_complete << endl;
#endif//BFS_DEBUG
				if(bfs_iteration_complete == 0)
				{
					active_queue[oddOrEven].clear();
					oddOrEven = !oddOrEven;
					active_queue_size = active_queue[oddOrEven].size();
//					bfs_iteration_complete = active_queue[oddOrEven].size();
#if(BFS_DEBUG == 1)
				application_output_file[tid] << "bfs_iteration_complete = " << bfs_iteration_complete << " active_queue_size = " << active_queue_size << endl;
#endif//BFS_DEBUG
					bfs_barrier_lock.lock();
					bfs_barrier = 0;
					bfs_barrier_lock.unlock();
				}
				bfs_iteration_lock.unlock();
			}
		}
	}
	return;
}

void SSDInterface::application_print_info()
{
	cout << "BFS_sort_time: " << (double)bfs_sort_time_elapsed / 1000000000 << " (sec)" << endl;
}
