#include "../../GraphSSD_library/graphFiltering.h"
//#include "../../backup/GraphSSD_library/graphFiltering.h"
#define BFS_DEBUG 0

extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

void application_initialization()
{
	NumberOfInFlightRequests.reserve(number_of_application_response_queues);
}

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();
	VERTEX_TYPE root = 0;
	unsigned int MaxIterationNumber = atoi(argv[7]), currentIteration = 0;
	std::vector< std::vector<VERTEX_TYPE> >active_queue;
	active_queue.resize(2);
	std::vector<bool> Visited;
	Visited.resize(NumNodes, 0);
	Visited[0] = 1;
	unsigned int bfs_iteration_complete = 1;
	mutex bfs_visited_lock; // same lock for both active_queue and visited vector
	mutex bfs_iteration_lock;
	mutex bfs_barrier_lock;
	unsigned int bfs_barrier = 0;
	bool oddOrEven = 0;
	active_queue[oddOrEven].push_back(root);
	unsigned int active_queue_size = 1;
	unsigned int max_levels = 0, max_levels_start_vertex = 0;

	unsigned int activeVertexIndex = 0;

	start_application_time();

#pragma omp parallel num_threads(NumOfRequestingThreads + NumberOfResponseThreads) 
	{
		unsigned int tid = omp_get_thread_num();
		if(tid < NumOfRequestingThreads)
		{
			while(activeVertexIndex < NumNodes)
			{
				while(active_queue[oddOrEven].size() > 0)
				{
					NumberOfInFlightRequests[tid] = 0;
					//For requesting threads
					unsigned int start_vertex_index = floor(active_queue_size * ((tid * 1.0) / NumOfRequestingThreads)), end_vertex_index = floor(active_queue_size * (((tid + 1)*1.0) / NumOfRequestingThreads));
#if(BFS_DEBUG == 1)
					application_output_file[tid] << "tid = " << tid <<  " start_vertex_index = " << start_vertex_index << " end_vertex_index = " << end_vertex_index << endl;
#endif//BFS_DEBUG
					while(start_vertex_index < end_vertex_index)
					{
						APP_REQUEST_TYPE App_to_cache_request = {active_queue[oddOrEven][start_vertex_index], tid, active_queue[oddOrEven][start_vertex_index]};
						while(1)
						{
							Cache_response_queue_lock[tid].lock();
							if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
							{
								NumberOfInFlightRequests[tid]++;
								Cache_response_queue_lock[tid].unlock();
								break;
							}
							Cache_response_queue_lock[tid].unlock();
						}
						while(1)
						{
							App_to_cache_request_queue_lock.lock();
							if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
							{
								App_to_cache_request_queue.push(App_to_cache_request);
#if(BFS_DEBUG == 1)
								application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//BFS_DEBUG
								App_to_cache_request_queue_lock.unlock();
								break;
							}
							App_to_cache_request_queue_lock.unlock();
						}
						start_vertex_index++;
					}
					while(1)
					{
						bfs_iteration_lock.lock();
						if(bfs_iteration_complete == 0){
							bfs_barrier_lock.lock();
#if(MINIMAL_PRINT == 1)
							application_output_file[tid] << "currentIteration = " << currentIteration << endl;
#endif//MINIMAL_PRINT
							bfs_barrier++;
							if(bfs_barrier == NumOfRequestingThreads)
							{
								currentIteration++;
								if(active_queue_size == 0)
								{
									if(currentIteration > max_levels)
									{
										max_levels_start_vertex = activeVertexIndex;
										max_levels = currentIteration;
									}
									currentIteration = 0;
									while((++activeVertexIndex) < NumNodes)
									{
										if(Visited[activeVertexIndex] == 0)
										{
											active_queue[oddOrEven].push_back(activeVertexIndex);
											active_queue_size = active_queue[oddOrEven].size();
											Visited[activeVertexIndex] = 1;
											break;
										}
									}
								}
								bfs_iteration_complete = active_queue_size;
							}
							bfs_barrier_lock.unlock();
							bfs_iteration_lock.unlock();
							break;
						}
						bfs_iteration_lock.unlock();
					}
#if(BFS_DEBUG == 1)
					application_output_file[tid] << "bfs_barrier = " << bfs_barrier << endl;
#endif//BFS_DEBUG
					while(1)
					{
						bfs_barrier_lock.lock();
						if(bfs_barrier == NumOfRequestingThreads)
						{
							bfs_barrier_lock.unlock();
							break;
						}
						bfs_barrier_lock.unlock();
					}
					if(currentIteration == MaxIterationNumber)
					{
						cout << "tid = " << tid  << " breaking after iterations = " << currentIteration << endl;
						break;
					}
				}
			}
			if(tid == 0)
			{
				printf("Connected components: max_levels = %u max_levels_start_vertex = %u\n", max_levels, max_levels_start_vertex);
				Finalize();
				exit(0);
			}
		} else {
			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
			while(1) {
				while(1)
				{
					Cache_response_queue_lock[tid-NumOfRequestingThreads].lock();
					if(Cache_response_queue[tid-NumOfRequestingThreads].empty() == false)
					{
						cache_to_app_response = Cache_response_queue[tid-NumOfRequestingThreads].front();
						Cache_response_queue[tid-NumOfRequestingThreads].pop();
						NumberOfInFlightRequests[tid-NumOfRequestingThreads]--;
						Cache_response_queue_lock[tid-NumOfRequestingThreads].unlock();
						break;
					}
					Cache_response_queue_lock[tid-NumOfRequestingThreads].unlock();
				}
#if(BFS_DEBUG == 1)
				application_output_file[tid] << "popping request id = " << cache_to_app_response.first << " adj. list size = " << (cache_to_app_response.second)->size() << endl;
#endif//BFS_DEBUG

				for(unsigned int i = 0; i < (cache_to_app_response.second)->size(); i++)
				{
					bfs_visited_lock.lock();
					if(Visited[(*(cache_to_app_response.second))[i]] == 0)
					{
						active_queue[!oddOrEven].push_back((*(cache_to_app_response.second))[i]);
						Visited[(*(cache_to_app_response.second))[i]] = 1;
					}
					bfs_visited_lock.unlock();
				}
				(*(cache_to_app_response.second)).clear();
				bfs_iteration_lock.lock();
				bfs_iteration_complete--;
#if(BFS_DEBUG == 1)
				application_output_file[tid] << "bfs_iteration_complete = " << bfs_iteration_complete << endl;
#endif//BFS_DEBUG
				if(bfs_iteration_complete == 0)
				{
					active_queue[oddOrEven].clear();
					oddOrEven = !oddOrEven;
					active_queue_size = active_queue[oddOrEven].size();
//					bfs_iteration_complete = active_queue[oddOrEven].size();
#if(BFS_DEBUG == 1)
				application_output_file[tid] << "bfs_iteration_complete = " << bfs_iteration_complete << " active_queue_size = " << active_queue_size << endl;
#endif//BFS_DEBUG
					bfs_barrier_lock.lock();
					bfs_barrier = 0;
					bfs_barrier_lock.unlock();
				}
				bfs_iteration_lock.unlock();
			}
		}
	}
	return;
}

void SSDInterface::application_print_info()
{
}
