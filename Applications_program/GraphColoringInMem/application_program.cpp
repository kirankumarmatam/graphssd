#include "../../GraphSSD_library/graphFiltering.h"
#define GC_DEBUG 0
typedef unsigned short StatusType;
extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

#define INS 3
#define TENTAINS 2
#define NOTINS 1
#define UNKNOWN 0
#define COLORED 4
#define PRINTRESULT 0

unsigned int *valid_degree;
StatusType *status;
unsigned int *id;
VERTEX_TYPE TotalColor;
VERTEX_TYPE step;
bool *Done;
unsigned int *totalReq, *totalRes;
bool converged;
bool HasUnknown;
bool fired;
unsigned int coloredVertices = 0;

vector< shared_mutex_wrapper > Done_lock;

#if(MEASURE_TIME == 1)
vector<Timer> auxiliary_memory_timer;
vector< map<REQUEST_UNIQUE_ID_TYPE,Timer> > reqid_to_start_time;
vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, bool > > > Cache_response_queue_time;
double hit_elapsed_time_sum = 0;
double miss_elapsed_time_sum = 0;
extern unsigned int cache_hits, cache_misses;
vector< mutex_wrapper > map_lck;
#endif//MEASURE_TIME

void application_initialization()
{
	NumberOfInFlightRequests.reserve(number_of_application_response_queues);
	Done = (bool*)calloc(NumOfRequestingThreads, sizeof(bool));
	valid_degree=(unsigned int *)calloc(NumNodes,sizeof(unsigned int));
	id=(unsigned int *)calloc(NumNodes,sizeof(unsigned int));
	memset(id,-1,NumNodes*sizeof(unsigned int)/sizeof(char));
	status=(StatusType *)calloc(NumNodes*2,sizeof(StatusType));
	TotalColor=0;
	step=1;

	totalReq=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
	totalRes=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
	for(unsigned int i=0; i < NumOfRequestingThreads; i++)
	{
		shared_mutex_wrapper tmp_mtx;
		Done_lock.push_back(tmp_mtx);
	}
#if(MEASURE_TIME == 1)
	auxiliary_memory_timer.resize(NumOfRequestingThreads);
	for (unsigned int i=0; i<NumOfRequestingThreads; i++)
	{
		mutex_wrapper tmp_mtx;
		map_lck.push_back(tmp_mtx);
	}
	reqid_to_start_time.resize(NumOfRequestingThreads);
	Cache_response_queue_time.resize(NumOfRequestingThreads);
#endif//MEASURE_TIME
}

void ReqFunc()
{
#pragma omp parallel num_threads(NumOfRequestingThreads)
	{
		unsigned int tid = omp_get_thread_num();
#pragma omp for schedule(dynamic,10)
		for (int i=0; i<NumNodes; i++)
		{	
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "Node i = " << i << " step " << step <<  " status = " << status[i+NumNodes] << endl;
#endif//GTL_TRANSLATION_CHECKING
			if(step == 1)
			{
				if(status[i+NumNodes]!=UNKNOWN)
				{
#if(GTL_TRANSLATION_CHECKING == 1)
					application_output_file[tid] << "Node i = " << i << " status = " << status[i+NumNodes] << endl;
#endif//GTL_TRANSLATION_CHECKING
					continue;
				}
			} else if(step == 2)
			{
				if(status[i] != TENTAINS)
				{
					continue;
				}
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "Node i = " << i << endl;
#endif//GTL_TRANSLATION_CHECKING
			//reserve space in the response queue
			while (1)
			{
				Cache_response_queue_lock[tid].lock();
				if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
				{
					NumberOfInFlightRequests[tid]++;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
//					sched_yield();
				}
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING

			//initialize before push so don't need to lock, using vid as the request id
			APP_REQUEST_TYPE App_to_cache_request = {i, tid, i};
#if(MEASURE_TIME == 1)
			map_lck[tid].lock();
			(reqid_to_start_time[tid])[i];
			(reqid_to_start_time[tid])[i].start_timer();
			map_lck[tid].unlock();
#endif//MEASURE_TIME
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << " App_to_cache_request = " << i << " " << i <<  endl;
#endif//GTL_TRANSLATION_CHECKING
			//push the request
			while (1)
			{
				App_to_cache_request_queue_lock.lock();
				if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
				{
					App_to_cache_request_queue.push(App_to_cache_request);
#if(GC_DEBUG == 1)
					application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//TC_DEBUG
					App_to_cache_request_queue_lock.unlock();
					break;
				}
				else
				{
					App_to_cache_request_queue_lock.unlock();
				}
			}
			totalReq[tid]++;
		}
	Done_lock[tid].lock();
	Done[tid]=1;
#if(GTL_TRANSLATION_CHECKING == 1)
	application_output_file[tid] << "Request threads done, tid = " << tid << " totalReq = " << totalReq[tid] << " Done = " << Done[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING
	Done_lock[tid].unlock();
}
}

void ResFunc()
{
#pragma omp parallel num_threads(NumberOfResponseThreads)
	{
		unsigned int tid = omp_get_thread_num();
		bool exit;
		while (1)
		{
			//try to get the response
			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
#if(MEASURE_TIME == 1)
			std::pair<REQUEST_UNIQUE_ID_TYPE, bool > cache_to_app_response_hit;
#endif//MEASURE_TIME
			exit = 0;
			while(1)
			{
				Cache_response_queue_lock[tid].lock();
				if(Cache_response_queue[tid].empty() == false)
				{
					cache_to_app_response = Cache_response_queue[tid].front();
					Cache_response_queue[tid].pop();
					NumberOfInFlightRequests[tid]--;
#if(MEASURE_TIME == 1)
					cache_to_app_response_hit = Cache_response_queue_time[tid].front();
					Cache_response_queue_time[tid].pop();
#endif//MEASURE_TIME
					Cache_response_queue_lock[tid].unlock();
#if(MEASURE_TIME == 1)
					map_lck[tid].lock();
					if(cache_to_app_response_hit.second == 1)
					{
						double time = (reqid_to_start_time[tid])[cache_to_app_response_hit.first].get_timer();
						hit_elapsed_time_sum += time;
					}
					else {	
						double time = (reqid_to_start_time[tid])[cache_to_app_response_hit.first].get_timer();
						miss_elapsed_time_sum += time;
					}
					(reqid_to_start_time[tid]).erase(cache_to_app_response_hit.first);
					map_lck[tid].unlock();
#endif//MEASURE_TIME
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
				Done_lock[tid].lock_shared();
				if (Done[tid])
				{
#if(GTL_TRANSLATION_CHECKING == 1)
					application_output_file[NumOfRequestingThreads + tid] << "Response tid = " << tid << " total requests =  " << totalReq[tid] << "  total response = " << totalRes[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING
					if (totalReq[tid]==totalRes[tid])
					{
						exit = 1;
					}
				}
				Done_lock[tid].unlock_shared();
				if(exit == 1)
					break;

			}
			if(exit == 1) break;
			else {
#if(MEASURE_TIME == 1)
				auxiliary_memory_timer[tid].start_timer();
#endif//MEASURE_TIME

#if(GTL_TRANSLATION_CHECKING == 1)
				Done_lock[tid].lock_shared();
				application_output_file[NumOfRequestingThreads+tid] << "cache response = totalRes = " << totalRes[tid] << " Done = " << Done[tid] << endl;
				Done_lock[tid].unlock_shared();
#endif//GTL_TRANSLATION_CHECKING
				VERTEX_TYPE vid = cache_to_app_response.first;

				if(step == 1)
				{
					for(unsigned int loop_1=0; loop_1 < (cache_to_app_response.second)->size(); loop_1++)
					{
						VERTEX_TYPE nid=(*(cache_to_app_response.second))[loop_1];
						if(status[nid+NumNodes]==UNKNOWN)
						{
							valid_degree[vid]++;
						}
					}
					if ( (valid_degree[vid] == 0) || (valid_degree[vid] !=0 && (rand() % (2*valid_degree[vid]) == 1)))
					{
						status[vid]=TENTAINS;
						for(unsigned int loop_1=0; loop_1 < (cache_to_app_response.second)->size(); loop_1++)
						{
							VERTEX_TYPE nid=(*(cache_to_app_response.second))[loop_1];
							//TODO lock
							if (vid<id[nid] || id[nid]==-1)
							{
								id[nid]=vid;
							}
						}
						fired=1;
					}
				}
				else if(step == 2)
				{
					if(status[vid] == TENTAINS)
					{
						if (vid<id[vid] || id[vid]==-1)
						{
							//TODO lock
							status[vid]=INS;
							for(unsigned int loop_1=0; loop_1 < (cache_to_app_response.second)->size(); loop_1++)
							{
								VERTEX_TYPE nid=(*(cache_to_app_response.second))[loop_1];
								//TODO lock
								if (status[nid]<INS)
								{
									status[nid]=NOTINS;
								}
							}
						}
						else
						{
							status[vid]=UNKNOWN;// it's single thread so it's fine. if using multi thread, should make sure this vertex is not in NOINS state;
						}
					}
				}
				totalRes[tid]++;
				(*(cache_to_app_response.second)).clear();
				delete cache_to_app_response.second;

#if(MEASURE_TIME == 1)
				auxiliary_memory_timer[tid].end_timer();
#endif//MEASURE_TIME
			}
		}
	}
}

void SSDInterface::application_print_info()
{
}

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();
	omp_set_nested(1);
	int isMIS = atoi(argv[6]);
	start_application_time();
#if(MEASURE_TIME == 1)
	Timer Beside_iteration;
#endif//MEASURE_TIME

	unsigned int niters = atoi(argv[7]);
	for(unsigned int loop_1 = 0; loop_1 <= niters; loop_1++)
	{	
		converged = 1;
		fired=0;
		HasUnknown=0;
		
		cout << "Loop = " << loop_1 << " step = " << step << endl;
		if (loop_1 != niters)
		{
			switch(step)
			{
				case 1 :
				case 2 :
					{
#pragma omp parallel sections
						{
#pragma omp section
							{
								ReqFunc();
//								cout << "Req thread" << endl;
							}
#pragma omp section
							{
								ResFunc();
//								cout << "Res thread" << endl;
							}
						}
#pragma barrier
						converged = 0;
						break;
					}
				case 3 :
					{
						for(unsigned int loop_2 = 0; loop_2 < NumNodes; loop_2++)
						{
							if(status[loop_2]==UNKNOWN)
							{
								HasUnknown=1;
							}
							converged=0;
						}
						break;
					}
				case 4 :
					{
						for(unsigned int loop_2 = 0; loop_2 < NumNodes; loop_2++)
						{
							if(status[loop_2]==INS)
							{
								status[loop_2]=COLORED+TotalColor;
								coloredVertices++;
							}
							else if(status[loop_2]==NOTINS)
							{
								status[loop_2]=UNKNOWN;
								converged=0;
							}
						}
							break;
					}
			}
		}
		else
		{
#if(PRINTRESULT==1)
			cout << "v: " << loop_1 << " color is: " << status[loop_1]-COLORED << endl;
#endif
		}

		switch (step)
		{
			case 0:
				{
					step=1;
					break;
				}
			case 1:
				{
					if (fired)
					{
						step=2;
					}
					else
					{
						step=1;
					}
					break;
				}
			case 2:
				{
					step=3;
					break;
				}
			case 3:
				{
					if (HasUnknown)
					{
						step=1;
						memcpy(status+NumNodes,status,NumNodes*sizeof(StatusType)/sizeof(char));
					}
					else
					{
						step=4;
						if(isMIS == 1)
						{
							converged = 1;
							break;
						}
						else if(isMIS == 2)
						{
							end_application_time();
							print_info();
						}
					}
					memset(id,-1,NumNodes*sizeof(unsigned int)/sizeof(char));
					memset(valid_degree,0,NumNodes*sizeof(unsigned int)/sizeof(char));

					break;
				}
			case 4:
				{
					step=1;
					TotalColor++;
					memcpy(status+NumNodes,status,NumNodes*sizeof(StatusType)/sizeof(char));
					break;
				}
		}

		if (converged)
		{
#if(PRINTRESULT == 1)
			std::cout << "Total number of colors is " << TotalColor <<std::endl;
			for(unsigned int loop_2 = 0; loop_2 < NumNodes; loop_2++)
			{
				std::cout << "v: " << loop_2 << " color is " << (status[loop_2] - COLORED) << endl;
			}
#endif//PRINTRESULT

			break;
		}

		fill_n(Done, NumOfRequestingThreads, 0);
		fill_n(totalReq, NumOfRequestingThreads, 0);
		fill_n(totalRes, NumOfRequestingThreads, 0);
	}

	Finalize();
#if(MEASURE_TIME == 1)
	Beside_iteration.print_timer("Beside iteration timer: ");
	for(unsigned int i=0; i < NumOfRequestingThreads; i++)
	{
		cout << i << " ";
		auxiliary_memory_timer[i].print_timer("auxiliary mmeory update time ");
	}
#endif//MEASURE_TIME
	exit(0);
	return;
}
