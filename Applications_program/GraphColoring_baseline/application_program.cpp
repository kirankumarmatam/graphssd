#include "../../Baseline_library/graphFiltering.h"
#define GC_DEBUG 0

extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

unsigned int *Color;
bool *SelectedInThisIteration;
bool *Done;
unsigned int *totalReq, *totalRes;
extern unsigned int *degree;
unsigned int ColoredVertices = 0;
unsigned int CurrentColor = 1;

vector< shared_mutex_wrapper > Done_lock;

#if(MEASURE_TIME == 1)
vector<Timer> auxiliary_memory_timer;
vector< map<REQUEST_UNIQUE_ID_TYPE,Timer> > reqid_to_start_time;
vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, bool > > > Cache_response_queue_time;
double hit_elapsed_time_sum = 0;
double miss_elapsed_time_sum = 0;
extern unsigned int cache_hits, cache_misses;
vector< mutex_wrapper > map_lck;
#endif//MEASURE_TIME

void application_initialization()
{
	NumberOfInFlightRequests.reserve(number_of_application_response_queues);
	Done = (bool*)calloc(NumOfRequestingThreads, sizeof(bool));
	Color = (unsigned int*)calloc(NumNodes, sizeof(unsigned int));
	fill_n(Color,NumNodes,0);
	SelectedInThisIteration = (bool*)calloc(NumNodes, sizeof(bool));
	totalReq=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
	totalRes=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
	for(unsigned int i=0; i < NumOfRequestingThreads; i++)
	{
		shared_mutex_wrapper tmp_mtx;
		Done_lock.push_back(tmp_mtx);
	}
#if(MEASURE_TIME == 1)
	auxiliary_memory_timer.resize(NumOfRequestingThreads);
	for (unsigned int i=0; i<NumOfRequestingThreads; i++)
	{
		mutex_wrapper tmp_mtx;
		map_lck.push_back(tmp_mtx);
	}
	reqid_to_start_time.resize(NumOfRequestingThreads);
	Cache_response_queue_time.resize(NumOfRequestingThreads);
#endif//MEASURE_TIME
}

void ReqFunc()
{
#pragma omp parallel num_threads(NumOfRequestingThreads)
	{
		unsigned int tid = omp_get_thread_num();
#pragma omp for schedule(dynamic,10)
		for (int i=0; i<NumNodes; i++)
		{
			//check if its a dirty vertex (don't need a lock)
			if (SelectedInThisIteration[i]==0)
			{
				continue;
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "Node i = " << i << endl;
#endif//GTL_TRANSLATION_CHECKING
			//reserve space in the response queue
			while (1)
			{
				Cache_response_queue_lock[tid].lock();
				if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
				{
					NumberOfInFlightRequests[tid]++;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
//					sched_yield();
				}
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING

			//initialize before push so don't need to lock, using vid as the request id
			APP_REQUEST_TYPE App_to_cache_request = {i, tid, i};
#if(MEASURE_TIME == 1)
			map_lck[tid].lock();
			(reqid_to_start_time[tid])[i];
			(reqid_to_start_time[tid])[i].start_timer();
			map_lck[tid].unlock();
#endif//MEASURE_TIME
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << " App_to_cache_request = " << i << " " << i <<  endl;
#endif//GTL_TRANSLATION_CHECKING
			//push the request
			while (1)
			{
				App_to_cache_request_queue_lock.lock();
				if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
				{
					App_to_cache_request_queue.push(App_to_cache_request);
#if(GC_DEBUG == 1)
					application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//TC_DEBUG
					App_to_cache_request_queue_lock.unlock();
					break;
				}
				else
				{
					App_to_cache_request_queue_lock.unlock();
				}
			}
			totalReq[tid]++;
		}
	Done_lock[tid].lock();
	Done[tid]=1;
#if(GTL_TRANSLATION_CHECKING == 1)
	application_output_file[tid] << "Request threads done, tid = " << tid << " totalReq = " << totalReq[tid] << " Done = " << Done[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING
	Done_lock[tid].unlock();
}
}

void ResFunc()
{
#pragma omp parallel num_threads(NumberOfResponseThreads)
	{
		unsigned int tid = omp_get_thread_num();
bool exit;
		while (1)
		{
			//try to get the response
			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
#if(MEASURE_TIME == 1)
			std::pair<REQUEST_UNIQUE_ID_TYPE, bool > cache_to_app_response_hit;
#endif//MEASURE_TIME
			exit = 0;
			while(1)
			{
				Cache_response_queue_lock[tid].lock();
				if(Cache_response_queue[tid].empty() == false)
				{
					cache_to_app_response = Cache_response_queue[tid].front();
					Cache_response_queue[tid].pop();
					NumberOfInFlightRequests[tid]--;
#if(MEASURE_TIME == 1)
					cache_to_app_response_hit = Cache_response_queue_time[tid].front();
					Cache_response_queue_time[tid].pop();
#endif//MEASURE_TIME
					Cache_response_queue_lock[tid].unlock();
#if(MEASURE_TIME == 1)
//					application_output_file[NumOfRequestingThreads + tid] << "cache response time " << cache_to_app_response_hit.first << " cache response " << cache_to_app_response.first << endl;
					map_lck[tid].lock();
					if(cache_to_app_response_hit.second == 1)
					{
/*						cout << "resh " << cache_to_app_response_hit.first << endl;
						if((reqid_to_start_time[tid]).find(cache_to_app_response_hit.first) == (reqid_to_start_time[tid]).end())
						{
							cout << "not present" << endl;
						}
						else {
							cout << "present" << endl;
						}
*/						double time = (reqid_to_start_time[tid])[cache_to_app_response_hit.first].get_timer();
//						(reqid_to_start_time[tid])[cache_to_app_response_hit.first].print_timer("hte ");
						hit_elapsed_time_sum += time;
//						application_output_file[tid] << "h=" << time << endl;
					}
					else {	
/*						cout << "resm " << cache_to_app_response_hit.first << endl;
						if((reqid_to_start_time[tid]).find(cache_to_app_response_hit.first) == (reqid_to_start_time[tid]).end())
						{
							cout << "not present" << endl;
						}
						else {
							cout << "present" << endl;
						}
*/
						double time = (reqid_to_start_time[tid])[cache_to_app_response_hit.first].get_timer();
//						(reqid_to_start_time[tid])[cache_to_app_response_hit.first].print_timer("mte ");
						miss_elapsed_time_sum += time;
//						application_output_file[tid] << "m=" << time << endl;
					}
					(reqid_to_start_time[tid]).erase(cache_to_app_response_hit.first);
					map_lck[tid].unlock();
#endif//MEASURE_TIME

					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
				Done_lock[tid].lock_shared();
				if (Done[tid])
				{
#if(GTL_TRANSLATION_CHECKING == 1)
					application_output_file[NumOfRequestingThreads + tid] << "Response tid = " << tid << " total requests =  " << totalReq[tid] << "  total response = " << totalRes[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING
					if (totalReq[tid]==totalRes[tid])
					{
						exit = 1;
					}
				}
				Done_lock[tid].unlock_shared();
				if(exit == 1)
					break;

			}
			if(exit == 1) break;
			else {
#if(GTL_TRANSLATION_CHECKING == 1)
#pragma omp critical
				application_output_file[NumOfRequestingThreads+tid] << "cache response = req id = " <<  cache_to_app_response.first << " adjacency list size = " << (cache_to_app_response.second)->size() << " degree = " << degree[cache_to_app_response.first]<< endl;
#endif//GTL_TRANSLATION_CHECKING

#if(MEASURE_TIME == 1)
				auxiliary_memory_timer[tid].start_timer();
#endif//MEASURE_TIME
				VERTEX_TYPE min_vid = cache_to_app_response.first;
			for (unsigned int j=0; j<(cache_to_app_response.second)->size(); j++)
			{
				VERTEX_TYPE tmp_v=(*(cache_to_app_response.second))[j];
#pragma omp atomic
				degree[tmp_v]--;
				if(SelectedInThisIteration[tmp_v] == 1)
				{
					if(tmp_v < min_vid)
					{
						min_vid = tmp_v;
					}
				}
			}
			if(min_vid == cache_to_app_response.first)
			{
				Color[cache_to_app_response.first] = CurrentColor;
			}
			else {
				Color[cache_to_app_response.first] = CurrentColor + 1;
			}

(*(cache_to_app_response.second)).clear();
delete cache_to_app_response.second;

totalRes[tid]++;
#if(GTL_TRANSLATION_CHECKING == 1)
Done_lock[tid].lock_shared();
			application_output_file[NumOfRequestingThreads+tid] << "cache response = totalRes = " << totalRes[tid] << " Done = " << Done[tid] << endl;
Done_lock[tid].unlock_shared();
#endif//GTL_TRANSLATION_CHECKING

#if(MEASURE_TIME == 1)
auxiliary_memory_timer[tid].end_timer();
#endif//MEASURE_TIME
			}
}
}
}

void SSDInterface::application_print_info()
{
}

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();
	calculate_degree();
	omp_set_nested(1);
	start_application_time();
#if(MEASURE_TIME == 1)
	Timer Beside_iteration;
#endif//MEASURE_TIME

	unsigned int i=0;
	while(ColoredVertices < NumNodes)
	{
#if(MEASURE_TIME == 1)
		Beside_iteration.start_timer();
#endif//MEASURE_TIME
		cout << "Iteration number = " << i++ << " start" << " ColoredVertices = " << ColoredVertices << " NumNodes = " << NumNodes << endl;
		bool selectedVertex = 0;
#pragma omp parallel for
		for(unsigned int j = 0; j < NumNodes; j++)
		{
			SelectedInThisIteration[j] = 0;
			if(Color[j] == 0)
			{
				if(degree[j] == 0)
				{
					Color[j] = CurrentColor;
#pragma omp atomic
					ColoredVertices++;
				}
				else
				{
					unsigned int p=(unsigned int)rand()%(unsigned int)(degree[j]);
					if(p == 0) 
					{
						SelectedInThisIteration[j] = 1;
#pragma omp atomic
							ColoredVertices++;
//#pragma omp atomic
							selectedVertex = 1;
					}
				}
			}
		}

#if(MEASURE_TIME == 1)
		Beside_iteration.end_timer();
#endif//MEASURE_TIME

		if(selectedVertex == 1)
		{
#pragma omp parallel sections
		{
#pragma omp section
			{
				ReqFunc();
				cout << "Req thread" << endl;
			}
#pragma omp section
			{
				ResFunc();
				cout << "Res thread" << endl;
			}
		}
#pragma barrier
		}
#if(MEASURE_TIME == 1)
		Beside_iteration.start_timer();
#endif//MEASURE_TIME
		cout << "End - Active vertices list after the iteration" << endl;
		fill_n(Done, NumOfRequestingThreads, 0);
		fill_n(totalReq, NumOfRequestingThreads, 0);
		fill_n(totalRes, NumOfRequestingThreads, 0);
		CurrentColor += 2;
#if(MEASURE_TIME == 1)
		Beside_iteration.end_timer();
#endif//MEASURE_TIME
//		if(i >= 10)
//			break;
	}

	Finalize();
#if(MEASURE_TIME == 1)
	Beside_iteration.print_timer("Beside iteration timer: ");
	for(unsigned int i=0; i < NumOfRequestingThreads; i++)
	{
		cout << i << " ";
		auxiliary_memory_timer[i].print_timer("auxiliary mmeory update time ");
	}
#endif//MEASURE_TIME
	exit(0);
	return;
}
