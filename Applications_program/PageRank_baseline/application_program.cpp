#include "../../Baseline_library/graphFiltering.h"
#define PR_DEBUG 0

extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

//bool *dirty;
double *PageRank;
double alpha=0.85;
bool OddEven=0;
bool *Done;
unsigned int *totalReq, *totalRes;

void application_initialization()
{
	NumberOfInFlightRequests.reserve(number_of_application_response_queues);
	//	dirty = (bool*)calloc(NumNodes, sizeof(bool));
	Done = (bool*)calloc(NumNodes, sizeof(bool));
	//	fill_n(dirty, NumNodes, 1);
	PageRank = (double*)calloc(2*NumNodes, sizeof(double));
	fill_n(PageRank,NumNodes,1.0/NumNodes);
	totalReq=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
	totalRes=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
}

void ReqFunc()
{
#pragma omp parallel num_threads(NumOfRequestingThreads)
	{
		unsigned int tid = omp_get_thread_num();
#pragma omp for nowait
		for (int i=0; i<NumNodes; i++)
		{
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "Node i = " << i << endl;
#endif//GTL_TRANSLATION_CHECKING
			//check if its a dirty vertex (don't need a lock)
			/*			if (dirty[i]==0)
						{
						continue;
						}
			 */
			//reserve space in the response queue
			while (1)
			{
				Cache_response_queue_lock[tid].lock();
				if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
				{
					NumberOfInFlightRequests[tid]++;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING


			//initialize before push so don't need to lock, using vid as the request id
			APP_REQUEST_TYPE App_to_cache_request = {i, tid, i};
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << " App_to_cache_request = " << i << " " << i <<  endl;
#endif//GTL_TRANSLATION_CHECKING
			//push the request
			while (1)
			{
				App_to_cache_request_queue_lock.lock();
				if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
				{
					App_to_cache_request_queue.push(App_to_cache_request);
#if(PR_DEBUG == 1)
					application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//TC_DEBUG
					App_to_cache_request_queue_lock.unlock();
					break;
				}
				else
				{
					App_to_cache_request_queue_lock.unlock();
				}
			}
			totalReq[tid]++;
			/*
			//clean the dirty bit(need to lock now)
#pragma omp critical
{
dirty[i]=0;
}
			 */
			}
#pragma omp critical
{
	application_output_file[tid] << "Request threads done, tid = " << tid << endl;
	Done[tid]=1;
}
}
}

void ResFunc()
{
#pragma omp parallel num_threads(NumberOfResponseThreads)
	{
		unsigned int tid = omp_get_thread_num();

		while (1)
		{
			//try to get the response
			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
			while(1)
			{
				Cache_response_queue_lock[tid].lock();
				if(Cache_response_queue[tid].empty() == false)
				{
					cache_to_app_response = Cache_response_queue[tid].front();
					Cache_response_queue[tid].pop();
					NumberOfInFlightRequests[tid]--;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[NumOfRequestingThreads+tid] << "cache response = req id = " <<  cache_to_app_response.first << " adjacency list size = " << (cache_to_app_response.second)->size() << endl;
#endif//GTL_TRANSLATION_CHECKING
			PageRank[cache_to_app_response.first+(1-OddEven)*NumNodes]=0;
			for (unsigned int j=0; j<(cache_to_app_response.second)->size(); j++)
			{
				VERTEX_TYPE tmp_v=(*(cache_to_app_response.second))[j];
				//assert(tmp_v!=cache_to_app_response.first);
				//calculate the Page Rank
				PageRank[cache_to_app_response.first+(1-OddEven)*NumNodes]+=PageRank[tmp_v+OddEven*NumNodes];
				/*							//set the dirty bit(need to lock now)
#pragma omp critical
{
dirty[tmp_v]=1;
}
				 */
				}
if ((cache_to_app_response.second)->size()>0)
{
	PageRank[cache_to_app_response.first+(1-OddEven)*NumNodes]=alpha*PageRank[cache_to_app_response.first+(1-OddEven)*NumNodes]/((cache_to_app_response.second)->size())+(1-alpha)*NumNodes;
}

(*(cache_to_app_response.second)).clear();
delete cache_to_app_response.second;

totalRes[tid]++;

#pragma omp cirtical
if (Done[tid])
{
	application_output_file[NumOfRequestingThreads + tid] << "Response tid = " << tid << " total requests =  " << totalReq[tid] << "  total response = " << totalRes[tid] << endl;
	if (totalReq[tid]==totalRes[tid])
	{
		break;
	}
}
}
}
}

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();
	omp_set_nested(1);
	start_application_time();
	unsigned int MaxIterations = atoi(argv[6]);

	for (unsigned int i=0; i<MaxIterations; i++) //have not set the converge yet
	{
		cout << "Iteration number = " << i << " start" << endl;
#pragma omp parallel sections
		{
#pragma omp section
			{
				ReqFunc();
			}
#pragma omp section
			{
				ResFunc();
			}
		}
#pragma barrier
		OddEven=!OddEven;
		fill_n(Done, NumOfRequestingThreads, 0);
		fill_n(totalReq, NumOfRequestingThreads, 0);
		fill_n(totalRes, NumOfRequestingThreads, 0);
		cout << "Iteration number = " << i << " end " <<  endl;
	}
	Finalize();
	exit(0);
	return;
}
