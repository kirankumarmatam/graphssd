#include "../../GraphSSD_library/graphFiltering.h"
#define PR_DEBUG 0
typedef float ValueType;

#if(UseLockFreeQueues == 0)
extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;
#else//UseLockFreeQueues
extern boost::lockfree::queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
#endif//UseLockFreeQueues

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

bool *dirty;
bool *oddEven;
ValueType *PageRank;
ValueType alpha=0.85;
ValueType randomResetProb = 0.15;
ValueType Delta=0.4;
double Degree=1;
bool OddEven=0;
unsigned int iteration_num = 0;
bool *Done;
unsigned int *totalReq, *totalRes;
extern unsigned int *degree;

vector< shared_mutex_wrapper > Done_lock;

#if(MEASURE_TIME == 1)
vector<Timer> auxiliary_memory_timer;
vector< map<REQUEST_UNIQUE_ID_TYPE,Timer> > reqid_to_start_time;
vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, bool > > > Cache_response_queue_time;
double hit_elapsed_time_sum = 0;
double miss_elapsed_time_sum = 0;
extern unsigned int cache_hits, cache_misses;
vector< mutex_wrapper > map_lck;
#endif//MEASURE_TIME

void application_initialization()
{
	NumberOfInFlightRequests.reserve(number_of_application_response_queues);
	dirty = (bool*)calloc(2*NumNodes, sizeof(bool));
	fill_n(dirty, NumNodes, 1);
	Done = (bool*)calloc(NumOfRequestingThreads, sizeof(bool));
	PageRank = (ValueType*)calloc(2*NumNodes, sizeof(ValueType));
	fill_n(PageRank,NumNodes,randomResetProb);
	fill_n(PageRank+NumNodes, NumNodes, 1);
	oddEven = (bool*)calloc(NumNodes, sizeof(bool));
	fill_n(oddEven, NumNodes, 0);
	totalReq=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
	totalRes=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
	for(unsigned int i=0; i < NumOfRequestingThreads; i++)
	{
		shared_mutex_wrapper tmp_mtx;
		Done_lock.push_back(tmp_mtx);
	}
#if(MEASURE_TIME == 1)
	auxiliary_memory_timer.resize(NumOfRequestingThreads);
	for (unsigned int i=0; i<NumOfRequestingThreads; i++)
	{
		mutex_wrapper tmp_mtx;
		map_lck.push_back(tmp_mtx);
	}
	reqid_to_start_time.resize(NumOfRequestingThreads);
	Cache_response_queue_time.resize(NumOfRequestingThreads);
#endif//MEASURE_TIME
}

void ReqFunc()
{
#pragma omp parallel num_threads(NumOfRequestingThreads)
	{
		unsigned int tid = omp_get_thread_num();
#pragma omp for nowait
		for (int i=0; i<NumNodes; i++)
		{
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "Node i = " << i << endl;
#endif//GTL_TRANSLATION_CHECKING
			//check if its a dirty vertex (don't need a lock)
			if (dirty[i+OddEven*NumNodes]==0)
			{
				continue;
			}
			//reserve space in the response queue
			while (1)
			{
				Cache_response_queue_lock[tid].lock();
				if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
				{
					NumberOfInFlightRequests[tid]++;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING


			//initialize before push so don't need to lock, using vid as the request id
			APP_REQUEST_TYPE App_to_cache_request = {i, tid, i};
#if(MEASURE_TIME == 1)
			map_lck[tid].lock();
			(reqid_to_start_time[tid])[i];
			(reqid_to_start_time[tid])[i].start_timer();
			map_lck[tid].unlock();
#endif//MEASURE_TIME
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << " App_to_cache_request = " << i << " " << i <<  endl;
#endif//GTL_TRANSLATION_CHECKING
			//push the request
			while (1)
			{
#if(UseLockFreeQueues == 0)
				App_to_cache_request_queue_lock.lock();
				if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
				{
					App_to_cache_request_queue.push(App_to_cache_request);
#else//UseLockFreeQueues
					if(App_to_cache_request_queue.push(App_to_cache_request))
					{
#endif//UseLockFreeQueues
#if(PR_DEBUG == 1)
//					application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//TC_DEBUG
#if(UseLockFreeQueues == 0)
					App_to_cache_request_queue_lock.unlock();
					break;
				}
				else
				{
					App_to_cache_request_queue_lock.unlock();
				}
#else//UseLockFreeQueues
				break;
				}
#endif//UseLockFreeQueues
			}
			totalReq[tid]++;
			//clean the dirty bit(need to lock now)
//#pragma omp critical
//			{
//				dirty[i]=0;
//			}
		}
Done_lock[tid].lock();
Done[tid]=1;
#if(GTL_TRANSLATION_CHECKING == 1)
application_output_file[tid] << "Request threads done, tid = " << tid << " totalReq = " << totalReq[tid] << " Done = " << Done[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING
Done_lock[tid].unlock();
}
}

void ResFunc()
{
#pragma omp parallel num_threads(NumberOfResponseThreads)
	{
		unsigned int tid = omp_get_thread_num();
		bool exit;
		while (1)
		{
			//try to get the response
			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
#if(MEASURE_TIME == 1)
			std::pair<REQUEST_UNIQUE_ID_TYPE, bool > cache_to_app_response_hit;
#endif//MEASURE_TIME
			exit = 0;
			while(1)
			{
				Cache_response_queue_lock[tid].lock();
				if(Cache_response_queue[tid].empty() == false)
				{
					cache_to_app_response = Cache_response_queue[tid].front();
					Cache_response_queue[tid].pop();
					NumberOfInFlightRequests[tid]--;
#if(MEASURE_TIME == 1)
					cache_to_app_response_hit = Cache_response_queue_time[tid].front();
					Cache_response_queue_time[tid].pop();
#endif//MEASURE_TIME
					Cache_response_queue_lock[tid].unlock();
#if(MEASURE_TIME == 1)
					//                                      application_output_file[NumOfRequestingThreads + tid] << "cache response time " << cache_to_app_response_hit.first << " cache response " << cache_to_app_response.first << endl;
					map_lck[tid].lock();
					if(cache_to_app_response_hit.second == 1)
					{
						double time = (reqid_to_start_time[tid])[cache_to_app_response_hit.first].get_timer();
						hit_elapsed_time_sum += time;
					}
					else {
						double time = (reqid_to_start_time[tid])[cache_to_app_response_hit.first].get_timer();
						miss_elapsed_time_sum += time;
					}
					(reqid_to_start_time[tid]).erase(cache_to_app_response_hit.first);
					map_lck[tid].unlock();
#endif//MEASURE_TIME

					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
				Done_lock[tid].lock_shared();
				if (Done[tid])
				{
#if(GTL_TRANSLATION_CHECKING == 1)
					application_output_file[NumOfRequestingThreads + tid] << "Response tid = " << tid << " total requests =  " << totalReq[tid] << "  total response = " << totalRes[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING
					if (totalReq[tid]==totalRes[tid])
					{
						exit = 1;
					}
				}
				Done_lock[tid].unlock_shared();
				if(exit == 1)
					break;

			}
			if(exit == 1) break;
			else {
#if(GTL_TRANSLATION_CHECKING == 1)
#pragma omp critical
				application_output_file[NumOfRequestingThreads+tid] << "cache response = req id = " <<  cache_to_app_response.first << " adjacency list size = " << (cache_to_app_response.second)->size() << " degree = " << degree[cache_to_app_response.first]<< endl;
#endif//GTL_TRANSLATION_CHECKING
#if(MEASURE_TIME == 1)
				auxiliary_memory_timer[tid].start_timer();
#endif//MEASURE_TIME

			ValueType PageRank_new = 0;
			for (unsigned int j=0; j<(cache_to_app_response.second)->size(); j++)
			{
				VERTEX_TYPE tmp_v=(*(cache_to_app_response.second))[j];
				//assert(tmp_v!=cache_to_app_response.first);
				if(iteration_num == 0)
				{
				PageRank_new += 1.0 / (degree[tmp_v]);							//set the dirty bit(need to lock now)
				}else {
				PageRank_new += PageRank[tmp_v+oddEven[tmp_v]*NumNodes] / (degree[tmp_v]);							//set the dirty bit(need to lock now)
				}
#if(PR_DEBUG == 1)
				if(iteration_num == 0) {
				application_output_file[NumOfRequestingThreads+tid] << cache_to_app_response.first << " " << (cache_to_app_response.second)->size() << " " << tmp_v << " " << 1 << " " << degree[tmp_v] << " " << 1.0 / (degree[tmp_v]) << endl;}
				else {
				application_output_file[NumOfRequestingThreads+tid] << cache_to_app_response.first << " " << (cache_to_app_response.second)->size() << " " << tmp_v << " " << PageRank[tmp_v+oddEven[tmp_v]*NumNodes] << " " << degree[tmp_v] << " " << PageRank[tmp_v+oddEven[tmp_v]*NumNodes] / (degree[tmp_v]) << endl;}
#endif//PR_DEBUG
			}
			PageRank[cache_to_app_response.first+(1-OddEven)*NumNodes] = randomResetProb + alpha * PageRank_new;
#if(PR_DEBUG == 1)
			application_output_file[NumOfRequestingThreads+tid] << cache_to_app_response.first << " O = " << PageRank[cache_to_app_response.first+oddEven[cache_to_app_response.first]*NumNodes] << " N = " << PageRank[cache_to_app_response.first+(1-OddEven)*NumNodes] << endl;
#endif//PR_DEBUG
			if(fabs(PageRank[cache_to_app_response.first+(1-OddEven)*NumNodes] - PageRank[cache_to_app_response.first+oddEven[cache_to_app_response.first]*NumNodes]) > Delta)
			//set the dirty bit(need to lock now)
			{
				for (unsigned int j=0; j<(cache_to_app_response.second)->size(); j++)
				{
				VERTEX_TYPE tmp_v=(*(cache_to_app_response.second))[j];
#pragma omp critical
				{
						dirty[tmp_v + (1-OddEven)*NumNodes]=1;
				}
				}
			}

			(*(cache_to_app_response.second)).clear();
			delete cache_to_app_response.second;

			totalRes[tid]++;
#if(GTL_TRANSLATION_CHECKING == 1)
			Done_lock[tid].lock_shared();
			application_output_file[NumOfRequestingThreads+tid] << "cache response = totalRes = " << totalRes[tid] << " Done = " << Done[tid] << endl;
			Done_lock[tid].unlock_shared();
#endif//GTL_TRANSLATION_CHECKING

#if(MEASURE_TIME == 1)
			auxiliary_memory_timer[tid].end_timer();
#endif//MEASURE_TIME
			}
		}
}
}

void SSDInterface::application_print_info()
{
}

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();
	calculate_degree();
	omp_set_nested(1);
	start_application_time();
	unsigned int MaxIterations = atoi(argv[6]);

	for (unsigned int i=0; i<MaxIterations; i++) //have not set the converge yet
	{
		cout << "Iteration number = " << i << " start" << endl;
#pragma omp parallel sections
		{
#pragma omp section
			{
				ReqFunc();
			}
#pragma omp section
			{
				ResFunc();
			}
		}
#pragma barrier
		//use dirty bit to set the oddEven pointers, also unset the dirty bit
		unsigned int totalActiveVertices=0;
		application_output_file[0] << "Begin - Active vertices list after the iteration" << endl;
//#pragma omp parallel for reduction(+ : totalActiveVertices)
		for(unsigned int j = 0; j < NumNodes; j++)
		{
			if(dirty[j+OddEven*NumNodes] == 1)
			{
//				application_output_file[0] << j << endl;
				oddEven[j] = 1 - oddEven[j];
				dirty[j+OddEven*NumNodes] = 0;
				totalActiveVertices += 1;
			}
		}
		application_output_file[0] << "End - Active vertices list after the iteration" << endl;
		OddEven=!OddEven;
		fill_n(Done, NumOfRequestingThreads, 0);
		fill_n(totalReq, NumOfRequestingThreads, 0);
		fill_n(totalRes, NumOfRequestingThreads, 0);
		cout << "Iteration number = " << i << " totalActiveVertices = " << totalActiveVertices << " end " <<  endl;
		iteration_num++;
	}
	Finalize();
#if(MEASURE_TIME == 1)
	for(unsigned int i=0; i < NumOfRequestingThreads; i++)
	{
		cout << i << " ";
		auxiliary_memory_timer[i].print_timer("auxiliary mmeory update time ");
	}
#endif//MEASURE_TIME
	exit(0);
	return;
}
