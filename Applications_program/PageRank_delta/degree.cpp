#include "../../GraphSSD_library/graphFiltering.h"
#define DEGREE_DEBUG 0

#if(UseLockFreeQueues == 0)
extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;
#else//UseLockFreeQueues
extern boost::lockfree::queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
#endif//UseLockFreeQueues

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
extern unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
extern std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

unsigned int *degree;
bool *Done_degree;
unsigned int *totalReq_degree, *totalRes_degree;

#if(MEASURE_TIME == 1)
extern vector< map<REQUEST_UNIQUE_ID_TYPE,Timer> > reqid_to_start_time;
extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, bool > > > Cache_response_queue_time;
extern vector< mutex_wrapper > map_lck;
#endif//MEASURE_TIME

void ReqFunc_degree()
{
#pragma omp parallel num_threads(NumOfRequestingThreads)
	{
		unsigned int tid = omp_get_thread_num();
#pragma omp for nowait
		for (int i=0; i<NumNodes; i++)
		{
#if(DEGREE_DEBUG == 1)
			application_output_file[tid] << "Degree Node i = " << i << endl;
#endif//DEGREE_DEBUG
			//reserve space in the response queue
			while (1)
			{
				Cache_response_queue_lock[tid].lock();
				if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
				{
					NumberOfInFlightRequests[tid]++;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
			}
#if(DEGREE_DEBUG == 1)
			application_output_file[tid] << "NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//DEGREE_DEBUG

			//initialize before push so don't need to lock, using vid as the request id
			APP_REQUEST_TYPE App_to_cache_request = {i, tid, i};
#if(DEGREE_DEBUG == 1)
			application_output_file[tid] << " App_to_cache_request = " << i << " " << i <<  endl;
#endif//DEGREE_DEBUG
			//push the request
			while (1)
			{
#if(UseLockFreeQueues == 0)
				App_to_cache_request_queue_lock.lock();
				if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
				{
					App_to_cache_request_queue.push(App_to_cache_request);
#else//UseLockFreeQueues
					if(App_to_cache_request_queue.push(App_to_cache_request))
					{
#endif//UseLockFreeQueues
#if(PR_DEBUG == 1)
						application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//TC_DEBUG
#if(UseLockFreeQueues == 0)
						App_to_cache_request_queue_lock.unlock();
						break;
					}
					else
					{
						App_to_cache_request_queue_lock.unlock();
					}
#else//UseLockFreeQueues
					break;
				}
#endif//UseLockFreeQueues
			}
			totalReq_degree[tid]++;
		}
#pragma omp critical
{
#if(DEGREE_DEBUG == 1)
	application_output_file[tid] << "Request threads Done_degree, tid = " << tid << endl;
#endif//DEGREE_DEBUG
	Done_degree[tid]=1;
}
}
}

void ResFunc_degree()
{
#pragma omp parallel num_threads(NumberOfResponseThreads)
	{
		unsigned int tid = omp_get_thread_num();

		while (1)
		{
			//try to get the response
			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
			while(1)
			{
				Cache_response_queue_lock[tid].lock();
				if(Cache_response_queue[tid].empty() == false)
				{
					cache_to_app_response = Cache_response_queue[tid].front();
					Cache_response_queue[tid].pop();
#if(MEASURE_TIME == 1)
					Cache_response_queue_time[tid].pop();
#endif//MEASURE_TIME
					NumberOfInFlightRequests[tid]--;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
			}
#if(DEGREE_DEBUG == 1)
			application_output_file[NumOfRequestingThreads+tid] << "cache response = req id = " <<  cache_to_app_response.first << " adjacency list size = " << (cache_to_app_response.second)->size() << endl;
#endif//DEGREE_DEBUG
			degree[cache_to_app_response.first] = (cache_to_app_response.second)->size();

(*(cache_to_app_response.second)).clear();
delete cache_to_app_response.second;

totalRes_degree[tid]++;

bool exit=0;

#pragma omp critical
if (Done_degree[tid])
{
#if(DEGREE_DEBUG == 1)
	application_output_file[NumOfRequestingThreads + tid] << "Response tid = " << tid << " total requests =  " << totalReq_degree[tid] << "  total response = " << totalRes_degree[tid] << endl;
#endif//DEGREE_DEBUG
	if (totalReq_degree[tid]==totalRes_degree[tid])
	{
		exit = 1;
	}
}
if(exit == 1)
	break;
}
}
}

void SSDInterface::calculate_degree()
{
	omp_set_nested(1);
	degree = (unsigned int *) calloc(NumNodes, sizeof(unsigned int));
	Done_degree = (bool*)calloc(NumOfRequestingThreads, sizeof(bool));
	totalReq_degree=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
	totalRes_degree=(unsigned int *)calloc(NumOfRequestingThreads,sizeof(unsigned int));
#pragma omp parallel sections
		{
#pragma omp section
			{
				ReqFunc_degree();
			}
#pragma omp section
			{
				ResFunc_degree();
			}
		}
#pragma barrier
		free(Done_degree);
		free(totalReq_degree);
		free(totalRes_degree);
	return;
}
