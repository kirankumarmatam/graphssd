#include "../../Baseline_library/graphFiltering.h"
#define RW_DEBUG 0

extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

vector< map<VERTEX_TYPE,vector<VERTEX_TYPE> > > from_vec;
unsigned int *MaxCount;
VERTEX_TYPE *MaxVertex;
unsigned int *reqid;

#if(MEASURE_TIME == 1)
vector<Timer> auxiliary_memory_timer;
vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, bool > > > Cache_response_queue_time;
double hit_elapsed_time_sum = 0;
double miss_elapsed_time_sum = 0;
extern unsigned int cache_hits, cache_misses;
#endif//MEASURE_TIME

void application_initialization()
{
	NumberOfInFlightRequests.resize(number_of_application_response_queues);
	from_vec.resize(NumOfRequestingThreads*2);
	reqid = (unsigned int*)calloc(NumOfRequestingThreads, sizeof(unsigned int));
	MaxCount = (unsigned int*)calloc(NumOfRequestingThreads, sizeof(unsigned int));
	MaxVertex = (VERTEX_TYPE*)calloc(NumOfRequestingThreads, sizeof(VERTEX_TYPE));
#if(MEASURE_TIME == 1)
	auxiliary_memory_timer.resize(NumOfRequestingThreads);
	Cache_response_queue_time.resize(NumOfRequestingThreads);
#endif//MEASURE_TIME
}

void SSDInterface::application_print_info()
{                       
}

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();

	start_application_time();
	unsigned int MaxIteration = atoi(argv[6]), MaxStep = atoi(argv[7]);
	unsigned long long int MaxNodesToRun = 128;
	for (unsigned int i=0; i<MaxStep; i++)
	{
#if(MINIMAL_PRINT == 1)
		cout << "step number = " << i << endl;
#endif//MINIMAL_PRINT
#pragma omp parallel num_threads(NumOfRequestingThreads)
		{
#pragma omp for schedule(dynamic,10)
			for (unsigned int j=0; j<NumNodes; j++)
			{
				unsigned int tid = omp_get_thread_num();
				VERTEX_TYPE CurrentVid=j;
				if (i==0)
				{
					if (j%1000==0) // source
					{
						//don't need to reserve space in the response queue
						APP_REQUEST_TYPE App_to_cache_request = {CurrentVid, tid, reqid[tid]++};
#if(GTL_TRANSLATION_CHECKING == 1)
						application_output_file[tid] << " App_to_cache_request = " << CurrentVid << " " << (reqid[tid]-1) <<  endl;
#endif//GTL_TRANSLATION_CHECKING
						//push the request
						while (1)
						{
							App_to_cache_request_queue_lock.lock();
							if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
							{
								App_to_cache_request_queue.push(App_to_cache_request);
#if(RW_DEBUG == 1)
								application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//TC_DEBUG
								App_to_cache_request_queue_lock.unlock();
								break;
							}
							else
							{
								App_to_cache_request_queue_lock.unlock();
							}
						}

						//try to get the response
						std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
						while(1)
						{
							Cache_response_queue_lock[tid].lock();
							if(Cache_response_queue[tid].empty() == false)
							{
								cache_to_app_response = Cache_response_queue[tid].front();
								Cache_response_queue[tid].pop();
								//NumberOfInFlightRequests[tid]--;
#if(MEASURE_TIME == 1)
								Cache_response_queue_time[tid].pop();
#endif//MEASURE_TIME

								Cache_response_queue_lock[tid].unlock();
								break;
							}
							else
							{
								Cache_response_queue_lock[tid].unlock();
							}
						}
#if(GTL_TRANSLATION_CHECKING == 1)
						application_output_file[tid] << "cache response = req id = " <<  cache_to_app_response.first << " adjacency list size = " << (cache_to_app_response.second)->size() << endl;
#endif//GTL_TRANSLATION_CHECKING
#if(MEASURE_TIME == 1)
						auxiliary_memory_timer[tid].start_timer();
#endif//MEASURE_TIME
						if((cache_to_app_response.second)->size() == 0)
						{
							delete cache_to_app_response.second;
							continue;
						}
						for (unsigned k=0; k<MaxIteration; k++)
						{
							int randomV=rand()%((cache_to_app_response.second)->size());
							VERTEX_TYPE NextVid = (*(cache_to_app_response.second))[randomV];
							if ((from_vec[tid+(i%2)*NumOfRequestingThreads]).find(NextVid)==(from_vec[tid+(i%2)*NumOfRequestingThreads]).end())
							{
								vector<VERTEX_TYPE> tmp_vec;
								tmp_vec.push_back(CurrentVid);
								(from_vec[tid+(i%2)*NumOfRequestingThreads])[NextVid]=tmp_vec;
							}
							else
							{
								((from_vec[tid+(i%2)*NumOfRequestingThreads])[NextVid]).push_back(CurrentVid);
							}
							if ( ((from_vec[tid+(i%2)*NumOfRequestingThreads])[NextVid]).size()>MaxCount[tid] )
							{
								MaxCount[tid]=((from_vec[tid+(i%2)*NumOfRequestingThreads])[NextVid]).size();
								MaxVertex[tid]=NextVid;
							}
						}
						(*(cache_to_app_response.second)).clear();
						delete cache_to_app_response.second;
#if(MEASURE_TIME == 1)
						auxiliary_memory_timer[tid].end_timer();
#endif//MEASURE_TIME
					}
				}
				else
				{
					if ((from_vec[tid+((i+1)%2)*NumOfRequestingThreads]).find(CurrentVid)==(from_vec[tid+((i+1)%2)*NumOfRequestingThreads]).end()) //the vertex is not active
					{
						continue;
					}
					vector <VERTEX_TYPE> source_vec=(from_vec[tid+((i+1)%2)*NumOfRequestingThreads])[CurrentVid];
					if (source_vec.size()>0)
					{
						//don't need to reserve space in the response queue
						APP_REQUEST_TYPE App_to_cache_request = {CurrentVid, tid, reqid[tid]++};
#if(GTL_TRANSLATION_CHECKING == 1)
						application_output_file[tid] << " App_to_cache_request = " << CurrentVid << " " << (reqid[tid]-1) <<  endl;
#endif//GTL_TRANSLATION_CHECKING
						//push the request
						while (1)
						{
							App_to_cache_request_queue_lock.lock();
							if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
							{
								App_to_cache_request_queue.push(App_to_cache_request);
#if(RW_DEBUG == 1)
								application_output_file[tid] << "pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//TC_DEBUG
								App_to_cache_request_queue_lock.unlock();
								break;
							}
							else
							{
								App_to_cache_request_queue_lock.unlock();
							}
						}

						//try to get the response
						std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
						while(1)
						{
							Cache_response_queue_lock[tid].lock();
							if(Cache_response_queue[tid].empty() == false)
							{
								cache_to_app_response = Cache_response_queue[tid].front();
								Cache_response_queue[tid].pop();
								//NumberOfInFlightRequests[tid]--;
#if(MEASURE_TIME == 1)
								Cache_response_queue_time[tid].pop();
#endif//MEASURE_TIME
								Cache_response_queue_lock[tid].unlock();
								break;
							}
							else
							{
								Cache_response_queue_lock[tid].unlock();
							}
						}
#if(GTL_TRANSLATION_CHECKING == 1)
						application_output_file[tid] << "cache response = req id = " <<  cache_to_app_response.first << " adjacency list size = " << (cache_to_app_response.second)->size() << endl;
#endif//GTL_TRANSLATION_CHECKING
#if(MEASURE_TIME == 1)
						auxiliary_memory_timer[tid].start_timer();
#endif//MEASURE_TIME
						if((cache_to_app_response.second)->size() == 0)
						{
							delete cache_to_app_response.second;
							continue;
						}
						unsigned int s=source_vec.size();
						for (unsigned k=0; k<s; k++)
						{
							VERTEX_TYPE source_id=source_vec.back();
							source_vec.pop_back();
							int randomV=rand()%((cache_to_app_response.second)->size());//what if the size is zero?
							VERTEX_TYPE NextVid = (*(cache_to_app_response.second))[randomV];
							if ((from_vec[tid+(i%2)*NumOfRequestingThreads]).find(NextVid)==(from_vec[tid+(i%2)*NumOfRequestingThreads]).end())
							{
								vector<VERTEX_TYPE> tmp_vec;
								tmp_vec.push_back(source_id);
								(from_vec[tid+(i%2)*NumOfRequestingThreads])[NextVid]=tmp_vec;
							}
							else
							{
								((from_vec[tid+(i%2)*NumOfRequestingThreads])[NextVid]).push_back(source_id);
							}
							if ( ((from_vec[tid+(i%2)*NumOfRequestingThreads])[NextVid]).size()>MaxCount[tid] )
							{
								MaxCount[tid]=((from_vec[tid+(i%2)*NumOfRequestingThreads])[NextVid]).size();
								MaxVertex[tid]=NextVid;
							}
						}
						(*(cache_to_app_response.second)).clear();
						delete cache_to_app_response.second;
#if(MEASURE_TIME == 1)
						auxiliary_memory_timer[tid].end_timer();
#endif//MEASURE_TIME
					}
				}
			}
		}
		/*clear inactive map*/
		for (unsigned int t=0; t<NumOfRequestingThreads; t++)
		{
			(from_vec[t+((i+1)%2)*NumOfRequestingThreads]).clear();
		}
	}
	Finalize();
#if(MEASURE_TIME == 1)
	for(unsigned int i=0; i < NumOfRequestingThreads; i++)
	{
		cout << i << " ";
		auxiliary_memory_timer[i].print_timer("auxiliary mmeory update time ");
	}
#endif//MEASURE_TIME
	cout << "Program finished!" << endl;
	exit(0);
}

