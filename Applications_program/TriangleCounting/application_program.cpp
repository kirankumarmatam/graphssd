#include "../../GraphSSD_library/graphFiltering.h"
#define TC_DEBUG 0

extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;

extern vector<ofstream> application_output_file;

unsigned int *reqid;
vector< mutex_wrapper > req_res_lck;
vector< mutex_wrapper > map_lck;
vector< mutex_wrapper > set_lck;
unsigned int *Num2ndReq;
vector< set<VERTEX_TYPE> > large_adj_set,large_adj_adj_set;
vector< map<REQUEST_UNIQUE_ID_TYPE,VERTEX_TYPE> > reqid_to_vid_map;
unsigned int total_count;

void application_initialization()
{
	NumberOfInFlightRequests.resize(number_of_application_response_queues);
	for (unsigned int i=0; i<NumOfRequestingThreads; i++)
	{
		mutex_wrapper tmp_mtx,tmp_mtx2,tmp_mtx3;
		req_res_lck.push_back(tmp_mtx);
		map_lck.push_back(tmp_mtx2);
		set_lck.push_back(tmp_mtx3);
	}
	reqid = (unsigned int*)calloc(NumOfRequestingThreads, sizeof(unsigned int));
	Num2ndReq = (unsigned int*)calloc(NumOfRequestingThreads, sizeof(unsigned int));
	large_adj_set.resize(NumOfRequestingThreads);
	large_adj_adj_set.resize(NumOfRequestingThreads);
	reqid_to_vid_map.resize(NumOfRequestingThreads);
	total_count=0;
}

struct timespec tc_set_time_start, tc_set_time_end;
double tc_set_time_elapsed = 0;

void ReqFunc()
{
	unsigned long long int MaxIterations = NumNodes;
#if(TEST_RUN == 1)
	MaxIterations = 100000;
#endif//TEST_RUN
#pragma omp parallel num_threads(NumOfRequestingThreads)
	{
#pragma omp for nowait schedule(dynamic,10)
		for (int i=0; i<MaxIterations; i++)
		{
			unsigned int tid = omp_get_thread_num();
#if(GTL_TRANSLATION_CHECKING == 1 or MINIMAL_PRINT == 1)
			if(i % 1000 == 0)
			{
				application_output_file[tid] << "Node i = " << i << endl;
			}
#endif//GTL_TRANSLATION_CHECKING

			// check if the response thread is done using the two set (last vertex's processing is finished)
			while (1)
			{
				set_lck[tid].lock();
				if (large_adj_set[tid].empty())
				{
					assert(large_adj_adj_set[tid].empty());
					set_lck[tid].unlock();
					break;
				}
				else
				{
					set_lck[tid].unlock();
				}
			}

#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "release two lock = " << endl;
#endif//GTL_TRANSLATION_CHECKING

			//reserve space in the response queue
			while (1)
			{
				Cache_response_queue_lock[tid].lock();
				if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
				{
					NumberOfInFlightRequests[tid]++;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING

			//initialize before push so don't need to lock
			APP_REQUEST_TYPE App_to_cache_request = {i, tid, reqid[tid]++};

#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << " App_to_cache_request = " << i << " " << reqid[tid] <<  endl;
#endif//GTL_TRANSLATION_CHECKING
			//push the request
			while (1)
			{
				App_to_cache_request_queue_lock.lock();
				if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
				{
					App_to_cache_request_queue.push(App_to_cache_request);
#if(TC_DEBUG == 1)
					application_output_file[tid] << "stage 1 pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
#endif//TC_DEBUG
					App_to_cache_request_queue_lock.unlock();
					break;
				}
				else
				{
					App_to_cache_request_queue_lock.unlock();
				}
			}

			//try to get the response
			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
			while(1)
			{
				Cache_response_queue_lock[tid].lock();
				if(Cache_response_queue[tid].empty() == false)
				{
					cache_to_app_response = Cache_response_queue[tid].front();
					Cache_response_queue[tid].pop();
					NumberOfInFlightRequests[tid]--;
					Cache_response_queue_lock[tid].unlock();
					break;
				}
				else
				{
					Cache_response_queue_lock[tid].unlock();
				}
			}

#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "cache response = req id = " <<  cache_to_app_response.first << " adjacency list size = " << (cache_to_app_response.second)->size() << endl;
#endif//GTL_TRANSLATION_CHECKING

			//append the large_adj_set[tid]
			for (unsigned int j=0; j<(cache_to_app_response.second)->size(); j++)
			{
				VERTEX_TYPE tmp_v=(*(cache_to_app_response.second))[j];
				//			assert(tmp_v!=i);
				if (tmp_v>i)
				{
					large_adj_set[tid].insert(tmp_v);
				}
			}
			(*(cache_to_app_response.second)).clear();
			delete cache_to_app_response.second;

			if (large_adj_set[tid].empty()) //no neighbors larger than current vertex, skip
			{
				continue;
			}

			//set Num2ndReq first so the response thread will start working and get the response asap
			req_res_lck[tid].lock();
			Num2ndReq[tid]=large_adj_set[tid].size();
			req_res_lck[tid].unlock();

#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[tid] << "Num2ndReq = " << Num2ndReq[tid] << endl;
			for(auto it = large_adj_set[tid].begin(); it != large_adj_set[tid].end(); it++)
			{
				application_output_file[tid] << *it << " ";
			}
			application_output_file[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING

			//request for all the v in the large_adj_set[tid]
			set_lck[tid].lock();
			for (auto it=large_adj_set[tid].begin(); it!=large_adj_set[tid].end(); it++)
			{
				//reserve space in the response queue
				while (1)
				{
					Cache_response_queue_lock[tid].lock();
					if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
					{
						NumberOfInFlightRequests[tid]++;
						Cache_response_queue_lock[tid].unlock();
						break;
					}
					else
					{
						Cache_response_queue_lock[tid].unlock();
					}
				}

				//push the request
				App_to_cache_request = {*it, tid, reqid[tid]};
				map_lck[tid].lock();
				(reqid_to_vid_map[tid])[reqid[tid]++]=*it;
				map_lck[tid].unlock();
				while (1)
				{
					App_to_cache_request_queue_lock.lock();
					if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
					{
						App_to_cache_request_queue.push(App_to_cache_request);
#if(TC_DEBUG == 1)
						application_output_file[tid] << "stage 2 pushing vertex id = " << App_to_cache_request.vertex_id << " tid = " << App_to_cache_request.tid << " request id = " << App_to_cache_request.request_id << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << " set size = " << large_adj_set[tid].size() <<  endl;
#endif//TC_DEBUG
						App_to_cache_request_queue_lock.unlock();
						break;
					}
					else
					{
						App_to_cache_request_queue_lock.unlock();
					}
				}

			}
			set_lck[tid].unlock();

			//wait for the Num2ndReq become 0
			while (1)
			{
				req_res_lck[tid].lock();
				if(Num2ndReq[tid]==0)
				{
					req_res_lck[tid].unlock();
					break;
				}
				else
				{
					req_res_lck[tid].unlock();
				}
			}
		}
	}
}
void ResFunc()
{
#pragma omp parallel reduction(+:total_count) reduction(+:tc_set_time_elapsed) num_threads(NumberOfResponseThreads)
	{
		unsigned int tid = omp_get_thread_num();
		cout << tid << endl;
		while(1)
		{
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[NumberOfResponseThreads + tid] << "got a response 0 " << endl;
#endif//GTL_TRANSLATION_CHECKING
			//check if there is 2nd req
			while (1)
			{
				req_res_lck[tid].lock();
				if(Num2ndReq[tid]!=0)
				{
					req_res_lck[tid].unlock();
					break;
				}
				else
				{
					req_res_lck[tid].unlock();
				}
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			application_output_file[NumberOfResponseThreads + tid] << "got a response 1 " << endl;
#endif//GTL_TRANSLATION_CHECKING
			while (1)
			{
				//try to get the response
				std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
				while(1)
				{
					Cache_response_queue_lock[tid].lock();
					if(Cache_response_queue[tid].empty() == false)
					{
						cache_to_app_response = Cache_response_queue[tid].front();
						Cache_response_queue[tid].pop();
						NumberOfInFlightRequests[tid]--;
						Cache_response_queue_lock[tid].unlock();
						break;
					}
					else
					{
						Cache_response_queue_lock[tid].unlock();
					}
				}

#if(GTL_TRANSLATION_CHECKING == 1)
				application_output_file[NumberOfResponseThreads + tid] << "got a response 2 " << endl;
#endif//GTL_TRANSLATION_CHECKING

				//append the large_large_adj_set
				map_lck[tid].lock();
				unsigned int CurrentVid=(reqid_to_vid_map[tid])[cache_to_app_response.first];
				(reqid_to_vid_map[tid]).erase(cache_to_app_response.first);
				map_lck[tid].unlock();

				for (unsigned int j=0; j<(cache_to_app_response.second)->size(); j++)
				{
					VERTEX_TYPE tmp_v=(*(cache_to_app_response.second))[j];
#if(GTL_TRANSLATION_CHECKING == 1)
					/*				if(tmp_v == CurrentVid)
									{
									application_output_file[tid] << "CurrentVid = " << CurrentVid << endl;
									for (unsigned int j=0; j<(cache_to_app_response.second)->size(); j++)
									{
									application_output_file[tid] << (*(cache_to_app_response.second))[j] << " ";
									}
									application_output_file[tid] << endl;
									}
					 *///				assert(tmp_v!=CurrentVid);
#endif//GTL_TRANSLATION_CHECKING
					if (tmp_v>CurrentVid)
					{
						large_adj_adj_set[tid].insert(tmp_v);
					}
				}
				(*(cache_to_app_response.second)).clear();
				delete cache_to_app_response.second;

#if(GTL_TRANSLATION_CHECKING == 1)
				application_output_file[NumberOfResponseThreads + tid] << "got a response 3 " << endl;
#endif//GTL_TRANSLATION_CHECKING

				//get the intersection
				set<unsigned int> intersect;
				clock_gettime(CLOCK_MONOTONIC, &tc_set_time_start);
				set_intersection(large_adj_set[tid].begin(),large_adj_set[tid].end(),large_adj_adj_set[tid].begin(),large_adj_adj_set[tid].end(), inserter(intersect,intersect.begin()));
				clock_gettime(CLOCK_MONOTONIC, &tc_set_time_end);
#pragma omp atomic
				tc_set_time_elapsed += ((double)tc_set_time_end.tv_sec - (double)tc_set_time_start.tv_sec) * 1000000000;
#pragma omp atomic
				tc_set_time_elapsed += ((double)tc_set_time_end.tv_nsec - (double)tc_set_time_start.tv_nsec);
#pragma omp atomic
				total_count+=intersect.size();
				large_adj_adj_set[tid].clear();

#if(GTL_TRANSLATION_CHECKING == 1)
				application_output_file[NumberOfResponseThreads + tid] << "got a response 4 " << endl;
#endif//GTL_TRANSLATION_CHECKING

				//reduce one Num2ndReq
				req_res_lck[tid].lock();
				if (--Num2ndReq[tid]==0) //if it's the last request, clear the large_adj_set[tid]
				{
					req_res_lck[tid].unlock();
					set_lck[tid].lock();
					large_adj_set[tid].clear();
					set_lck[tid].unlock();
					break;
				}
				else
				{
					req_res_lck[tid].unlock();
				}

#if(GTL_TRANSLATION_CHECKING == 1)
				application_output_file[NumberOfResponseThreads + tid] << "got a response 5 " << Num2ndReq[tid] << endl;
#endif//GTL_TRANSLATION_CHECKING
			}
		}
	}
}

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();
	omp_set_nested(1);
	start_application_time();

#pragma omp parallel sections
	{
#pragma omp section
		{
			ReqFunc();
			Finalize();
			exit(0);
		}
#pragma omp section
		{
			ResFunc();
		}
	}

	cout << "the total number of triangles is: "<< total_count << endl;

	return;
}
void SSDInterface::application_print_info()
{
	        cout << "tc_set_time: " << (double)tc_set_time_elapsed / 1000000000 << " (sec)" << endl;
}
