#include "graphFiltering.h"

extern list<CPN_TYPE> freeCPN,usedCPN;
map<CPN_TYPE,list<CPN_TYPE>::iterator> LRUmap; //map CPN with usedCPN iterator
extern vector< shared_mutex_wrapper > cache_page_lock;

extern unsigned int MaxCacheSize;

void SSDInterface::LRU_cache_read(CPN_TYPE CPN)
{
	map<CPN_TYPE, std::list<CPN_TYPE>::iterator >::iterator it = LRUmap.find(CPN);
	if (it==LRUmap.end())// the CPN is not in the map, means we just request a new page in the cache so we need to map CPN with usedCPN.begin
	{
		LRUmap[CPN]=usedCPN.begin();
	}
	else //bring the CPN to the usedCPN.front
	{
		list<CPN_TYPE>::iterator usedCPN_it=LRUmap[CPN];
		usedCPN.erase(usedCPN_it);
		usedCPN.push_front(CPN);
		usedCPN_it=usedCPN.begin();
		LRUmap[CPN]=usedCPN_it;
	}
}

CPN_TYPE SSDInterface::LRU_cache_evict()
{
	unsigned int NumberOfCachePagesLocked = 0;
	while (1)
	{
		CPN_TYPE CPN=usedCPN.back();
		assert( (CPN >= 0) && (CPN <= (MaxCacheSize / SSD_PAGE_SIZE)) );
		usedCPN.pop_back();
		bool lock_page=cache_page_lock[CPN].try_lock();
		if (lock_page)
		{
			LRUmap.erase(CPN);
			cache_page_lock[CPN].unlock();
//			cout << "In LRU map evicting " << CPN << endl;
			return CPN;
		}
		else
		{
			usedCPN.push_front(CPN);
			LRUmap[CPN] = usedCPN.begin();//Is this correct?
			NumberOfCachePagesLocked++;
		}
		if(NumberOfCachePagesLocked == (MaxCacheSize / SSD_PAGE_SIZE)*100)
		{
			cout << "All cache pages are locked!" << endl;
			assert(0);
		}
	}
}
