#include "store_header.h"

#define SLBA 200
#define PRINT_CSR 0
#define CHECK_ERR 0
#define STORE_AT_SSD 1
#define PAGES_PER_CMD (32 / (SSD_PAGE_SIZE/4096))

int main(int argc, char **argv)
{
#if(STORE_LARGE_SCALE == 1)
	if(argc < 4)
	{
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt output_flash.txt" << endl;
		return 0; 
	}
	fstream myFlashFile;

	myFlashFile.open(argv[3]);
	if (!myFlashFile.is_open())
		cout << " Cannot open flash output file!" << endl;

	int err, fd;
	struct nvme_user_io io;

	fd = open(argv[1], O_RDWR);
	if (fd < 0)
	{
		printf("Error in commandline\n");
		//      goto perror;
	}

	void * buffer;

	if (posix_memalign(&buffer, SSD_PAGE_SIZE, REQUEST_SIZE)) {
		fprintf(stderr, "can not allocate io payload\n");
		return 0;
	}

	unsigned int pageCount = 0;
	unsigned int NumNodes;
	EdgeIndexType numOfEdgesInCSR;
	myFlashFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myFlashFile.read((char *)(&numOfEdgesInCSR), sizeof(EdgeIndexType));
	unsigned int SizeToRead;
	uint64_t i = 0;
	for(i = 0; i < ((NumNodes + 1)*sizeof(EdgeIndexType)); )
	{
		SizeToRead = PAGES_PER_CMD * SSD_PAGE_SIZE;
		if((i + SizeToRead) > ((NumNodes + 1)*sizeof(EdgeIndexType)))
		{
			SizeToRead = ((NumNodes + 1)*sizeof(EdgeIndexType)) - i;
		}
		myFlashFile.read((char *)buffer, SizeToRead);
		unsigned int page_read = (ceil((SizeToRead * 1.0) / SSD_PAGE_SIZE)) * (SSD_PAGE_SIZE / 4096);
		io.opcode = nvme_cmd_write;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long) 0;
		io.addr = (unsigned long) buffer;
		io.slba = SLBA + pageCount;
		io.nblocks = page_read - 1;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;

		err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		//              if (err < 0)
		//                      goto perror;
		if (err)
			fprintf(stderr, "nvme write status:%x\n", err);
		pageCount += page_read;
		i += SizeToRead;
	}
	for(; i < ((NumNodes + 1)*sizeof(EdgeIndexType) + numOfEdgesInCSR * sizeof(unsigned int));)
	{
		SizeToRead = PAGES_PER_CMD * SSD_PAGE_SIZE;
		if((i + SizeToRead) > ((NumNodes + 1)*sizeof(EdgeIndexType)) + numOfEdgesInCSR * sizeof(unsigned int))
		{
			SizeToRead = ((NumNodes + 1)*sizeof(EdgeIndexType) + numOfEdgesInCSR * sizeof(unsigned int)) - i;
		}
		myFlashFile.read((char *)buffer, SizeToRead);
		unsigned int page_read = (ceil((SizeToRead * 1.0) / SSD_PAGE_SIZE)) * (SSD_PAGE_SIZE / 4096);
		io.opcode = nvme_cmd_write;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long) 0;
		io.addr = (unsigned long) buffer;
		io.slba = SLBA + pageCount;
		io.nblocks = page_read - 1;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;

		err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		//              if (err < 0)
		//                      goto perror;
		if (err)
			fprintf(stderr, "nvme write status:%x\n", err);
		pageCount += page_read;
		i += SizeToRead;
	}
	myFlashFile.close();
#else
	if(argc < 3)
	{
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt" << endl;
		return 0; 
	}

	cout << "Entering before loading graph" << endl;

#if(IsUndirectedGraph == 1)
	PUNGraph G1 = TSnap::LoadEdgeList<PUNGraph>(argv[2], 0, 1);
	PNGraph G = TSnap::ConvertGraph<PNGraph>(G1);
#elif(IsUndirectedGraph == 0)
	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
#endif//IsUndirectedGraph
	//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
	map<unsigned int, unsigned int> vertexMapping;
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);
	assert(vertexMapping.size() == G->GetNodes());

	cout << "Entering before array " << endl;

	unsigned int *Array = (unsigned int *) malloc(sizeof(unsigned int) * (G->GetNodes()+1+G->GetEdges()));
	Array[0] = 0;
	unsigned int numOfEdgesInCSR = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		unsigned int i = vertexMapping[NI.GetId()];
		unsigned int MaxNeighborsPerVertex = (NI.GetOutDeg() < (16*4096 / sizeof(unsigned int))) ? NI.GetOutDeg() : (16*4096 / sizeof(unsigned int));
		numOfEdgesInCSR += MaxNeighborsPerVertex;
		Array[i+1] = Array[i] + MaxNeighborsPerVertex;
		cout << "vID = " << i << " edgeListSize = " << MaxNeighborsPerVertex << " indexes of A[i] = " << G->GetNodes() + 1 + Array[i] << " A[i+1] =  " << G->GetNodes() + 1 + Array[i+1] <<  endl;
		std::vector<unsigned int> EdgeList;
		for(unsigned int j = 0; j < MaxNeighborsPerVertex; j++)
		{
			EdgeList.push_back(vertexMapping[NI.GetOutNId(j)]);
		}
		std::sort(EdgeList.begin(), EdgeList.end());
		for(unsigned int j = 0; j < MaxNeighborsPerVertex; j++)
		{
			Array[G->GetNodes() + 1 + Array[i]+j] = EdgeList[j];
		}
		EdgeList.clear();
	}

	//	static const char *perrstr;
	int err, fd;
	struct nvme_user_io io;

	//	struct timespec time_start, time_end;
	//	double time_elapsed;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s <device>\n", argv[0]);
		return 1;
	}

	//	perrstr = argv[1];
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
	{
		printf("Error in commandline\n");
		//      goto perror;
	}
	void * buffer;

	if (posix_memalign(&buffer, 4096, REQUEST_SIZE)) {
		fprintf(stderr, "can not allocate io payload\n");
		return 0;
	}

	unsigned int pageCount = 0;
#if (STORE_AT_SSD == 1)
	for(unsigned int i = 0; i < (G->GetNodes() + 1 + numOfEdgesInCSR); i += (4096*PAGES_PER_CMD/sizeof(unsigned int)))
	{
		unsigned int valuesRemaining = (G->GetNodes() + 1 + numOfEdgesInCSR) - i;
		if(valuesRemaining < (4096*PAGES_PER_CMD/sizeof(unsigned int)))
		{
			memcpy(buffer, (void *)(Array+i), valuesRemaining * sizeof(unsigned int));
		}
		else 
		{
			memcpy(buffer, (void *)(Array+i), 4096*PAGES_PER_CMD);
		}

		io.opcode = nvme_cmd_write;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long) 0;
		io.addr = (unsigned long) buffer;
		io.slba = SLBA + pageCount;
		io.nblocks = PAGES_PER_CMD - 1;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;

		err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		//              if (err < 0)
		//                      goto perror;
		if (err)
			fprintf(stderr, "nvme write status:%x\n", err);
		pageCount += PAGES_PER_CMD;
	}
#endif
#if (CHECK_ERR == 1)
	pageCount = 0;
	unsigned int err_count = 0;
	for(unsigned int i = 0; i < (G->GetNodes() + 1 + numOfEdgesInCSR); i += (4096/sizeof(unsigned int)))
	{
		unsigned int valuesRemaining = (G->GetNodes() + 1 + numOfEdgesInCSR) - i;
		io.opcode = nvme_cmd_read;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long) 0;
		io.addr = (unsigned long) buffer;
		io.slba = SLBA + pageCount;
		io.nblocks = 0;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;

		err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		//              if (err < 0)
		//                      goto perror;
		if (err)
			fprintf(stderr, "nvme write status:%x\n", err);
		pageCount++;

		if(valuesRemaining < (4096/sizeof(unsigned int)))
		{
			for(unsigned int j = 0; j < valuesRemaining; j++)
			{
				if(*((unsigned int *)buffer + j) != Array[i+j])
					err_count += 1;
			}
		}
		else 
		{
			for(unsigned int j = 0; j < (4096 / sizeof(unsigned int)); j++)
			{
				if(*((unsigned int *)buffer + j) != Array[i+j])
					err_count += 1;
			}
		}
	}
	cout << "Error rate = " << (err_count * 1.0) / (G->GetNodes() + 1 + numOfEdgesInCSR) << endl;
#endif //CHECK_ERR

#if (PRINT_CSR == 1)
	for(int i = 0; i < (G->GetNodes() + 1 + numOfEdgesInCSR); i += 1)
	{
		cout << i << " " << Array[i] << endl;
	}
	cout << endl;
#endif
	free(Array);
#endif //STORE_LARGE_SCALE
	return 0;
}
