#include "graphFiltering.h"

unsigned int NumOfCacheThreads;
extern unsigned int NumNodes;

extern CacheBlock* Host_Cache;

mutex App_to_cache_request_queue_lock;
queue<APP_REQUEST_TYPE> App_to_cache_request_queue;

unsigned int App_to_cache_request_queue_max_size,cache_to_IO_request_queue_max_size,IO_response_queue_max_size;
unsigned int Cache_response_queue_max_size;
map<PPN_TYPE, pair< CPN_TYPE, vector<CACHE_TID_TYPE> > > In_flight_page_request_tracker;
mutex In_flight_tracker_lock;

vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
vector< mutex_wrapper > Cache_response_queue_lock;
vector< queue< pair<PPN_TYPE,CPN_TYPE> > > IO_response_queue;
vector< mutex_wrapper > IO_response_queue_lock;

extern queue<PPN_TYPE> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;

extern vector< shared_mutex_wrapper > cache_page_lock;

#if(GTL_TRANSLATION_CHECKING == 1)
extern PPN_TYPE *CPN_to_PPN_map;
extern map<PPN_TYPE,CPN_TYPE> PPN_to_CPN_map;
extern mutex cache_update_lock;
#endif//GTL_TRANSLATION_CHECKING

#if(MEASURE_PERFORMANCE == 1)
uint64_t rowPtr_hits, rowPtr_misses, colPtr_misses, colPtr_hits;
#endif//MEASURE_PERFORMANCE

extern vector<ofstream> cache_output_file;

extern unsigned int MaxCacheSize;

void SSDInterface::cache_manager()
{
	printf("Cache manager: entering here\n");
	#pragma omp parallel num_threads(NumOfCacheThreads)
	{
		CACHE_TID_TYPE tid = omp_get_thread_num();
		cache_manager_tid(tid);
	}
}

void SSDInterface::cache_manager_tid(CACHE_TID_TYPE cache_tid)
{
	map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, APP_REQUEST_TYPE> request_identification_to_request_map;
	map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> request_identification_to_number_of_need_pages_map;
	map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, vector<EDGE_TYPE>* > request_identification_to_adjlist_map;
	map<PPN_TYPE, vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > > PPN_request_identification_mapper;
	map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> request_identification_to_stage;
	map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > request_identification_to_indices;

#if(GTL_TRANSLATION_CHECKING == 1)
	cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread id = " << cache_tid << endl;
#endif//GTL_TRANSLATION_CHECKING
	unsigned int IO_response_queue_reserved_size=0;
	APP_REQUEST_TYPE appRequest;
	queue<PPN_TYPE> PPN_to_push_to_IO;
	while(1)
	{
		App_to_cache_request_queue_lock.lock();
		if(!App_to_cache_request_queue.empty())
		{
			appRequest = App_to_cache_request_queue.front();
			App_to_cache_request_queue.pop();
			App_to_cache_request_queue_lock.unlock();
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "Popped appRequest vertex_id = " << appRequest.vertex_id << " tid = " << appRequest.tid << " request_id = " << appRequest.request_id << endl;
#endif//
			pair<APP_TID_TYPE, REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair = make_pair(appRequest.tid, appRequest.request_id);
			request_identification_to_request_map[apptid_reqid_pair] = appRequest;
			request_identification_to_adjlist_map[apptid_reqid_pair] = new vector<EDGE_TYPE>;
			request_identification_to_stage[apptid_reqid_pair] = 1;
			process_vertex(request_identification_to_request_map, request_identification_to_number_of_need_pages_map, request_identification_to_adjlist_map, PPN_request_identification_mapper, request_identification_to_stage, request_identification_to_indices, make_pair(appRequest.tid, appRequest.request_id), cache_tid, PPN_to_push_to_IO);
		}
		else
		{
			App_to_cache_request_queue_lock.unlock();
		}
		while(!PPN_to_push_to_IO.empty())
		{
			if(IO_response_queue_reserved_size==IO_response_queue_max_size)
			{
#if(GTL_TRANSLATION_CHECKING == 1)
//				cache_output_file[cache_tid] << "breaking as IO_response_queue_max_size PPN = " << PPN_to_push_to_IO.front() << " CPN = " << In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first << endl;
#endif//GTL_TRANSLATION_CHECKING
				break;
			}
			else
			{
				cache_to_IO_request_queue_lock.lock();
				if(cache_to_IO_request_queue.size()==cache_to_IO_request_queue_max_size)
				{
#if(GTL_TRANSLATION_CHECKING == 1)
//					cache_output_file[cache_tid] << "breaking in cache_to_IO_request_queue_max_size " << PPN_to_push_to_IO.front() << " CPN = " << In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first << endl;
#endif//GTL_TRANSLATION_CHECKING
					cache_to_IO_request_queue_lock.unlock();
					break;
				}
				else //only the request and response queue both have place, we push the request
				{
					IO_response_queue_reserved_size++;
#if(GTL_TRANSLATION_CHECKING == 1)
/*					In_flight_tracker_lock.lock();
					cache_update_lock.lock();
					cache_output_file[cache_tid] << "pushing PPN = " << PPN_to_push_to_IO.front() << " CPN is = " << In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first << " cpn to ppn map = " << CPN_to_PPN_map[In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first] << endl;
//					assert(CPN_to_PPN_map[In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first] == 0);
					cache_update_lock.unlock();
					In_flight_tracker_lock.unlock();*/
#endif//GTL_TRANSLATION_CHECKING
					cache_to_IO_request_queue.push(PPN_to_push_to_IO.front());
					PPN_to_push_to_IO.pop();
					cache_to_IO_request_queue_lock.unlock();
				}
			}
		}
#if(GTL_TRANSLATION_CHECKING == 1)
		//		cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread: "<< " before IO response lock " << endl;
#endif//GTL_TRANSLATION_CHECKING
		//We don't want to acquire lock for every read, as we check the read regularly, or else we need a separate thread which responds to application, may be this is better, but there also one needs to snoop
		IO_response_queue_lock[cache_tid].lock();
#if(GTL_TRANSLATION_CHECKING == 1)
		//		cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread: " << " after IO response lock " <<  endl;
#endif//GTL_TRANSLATION_CHECKING
		if (!IO_response_queue[cache_tid].empty())
		{
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread: id is " << cache_tid << " IO response queue not empty, size is " << IO_response_queue[cache_tid].size() <<  endl;
#endif//GTL_TRANSLATION_CHECKING
			pair<PPN_TYPE, CPN_TYPE> response=IO_response_queue[cache_tid].front();//Pop request from the queue
			IO_response_queue[cache_tid].pop();
			IO_response_queue_reserved_size--;
			IO_response_queue_lock[cache_tid].unlock();

#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "From IO queue, PPN = " << response.first << " CPN = " << response.second << endl;
#endif//GTL_TRANSLATION_CHECKING

			vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> >APP_req_id_queue=PPN_request_identification_mapper[response.first];
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "PPN to request mapping " << response.first << " " << APP_req_id_queue.size() << endl;
#endif//GTL_TRANSLATION_CHECKING
			assert(PPN_request_identification_mapper.find(response.first) != PPN_request_identification_mapper.end());
			PPN_request_identification_mapper.erase(response.first);
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "PPN to request mapping erased" << endl;
#endif//GTL_TRANSLATION_CHECKING
			queue<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > might_access_locks;
			while (!APP_req_id_queue.empty()) //process all the app requests related to this PPN-CPN pair
			{
				pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair = APP_req_id_queue.back();
				APP_req_id_queue.pop_back();
				assert(request_identification_to_number_of_need_pages_map[apptid_reqid_pair] > 0);
				request_identification_to_number_of_need_pages_map[apptid_reqid_pair]--;
#if(GTL_TRANSLATION_CHECKING == 1)
				cache_output_file[cache_tid] << "appRequest tid = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " no. of needed pages = " << request_identification_to_number_of_need_pages_map[apptid_reqid_pair] << endl;
#endif//GTL_TRANSLATION_CHECKING
				processAPage(response.first, response.second, request_identification_to_request_map[apptid_reqid_pair].vertex_id, request_identification_to_stage[apptid_reqid_pair], apptid_reqid_pair, request_identification_to_indices, request_identification_to_adjlist_map);
				if (request_identification_to_number_of_need_pages_map[apptid_reqid_pair]==0)
				{
#if(GTL_TRANSLATION_CHECKING)
					cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread: " << " tid = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " adjacency list size =  " << request_identification_to_adjlist_map[apptid_reqid_pair]->size()<< endl;
#endif//GTL_TRANSLATION_CHECKING
					if(request_identification_to_number_of_need_pages_map[apptid_reqid_pair] == 0)
					{
						might_access_locks.push(apptid_reqid_pair);
					}
					else {
						completeProcessing(request_identification_to_request_map, request_identification_to_number_of_need_pages_map, request_identification_to_adjlist_map, PPN_request_identification_mapper, request_identification_to_stage, request_identification_to_indices, apptid_reqid_pair, cache_tid, PPN_to_push_to_IO);
					}
				}
			}
			cache_page_lock[response.second].unlock_shared(); //done using this page, release the lock
			while(!might_access_locks.empty())
			{
				pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair = might_access_locks.front();
				might_access_locks.pop();
				completeProcessing(request_identification_to_request_map, request_identification_to_number_of_need_pages_map, request_identification_to_adjlist_map, PPN_request_identification_mapper, request_identification_to_stage, request_identification_to_indices, apptid_reqid_pair, cache_tid, PPN_to_push_to_IO);
			}
		}
		else
		{
			IO_response_queue_lock[cache_tid].unlock();
		}
		//if it did not enter both the queue then consider sleeping?
	}
}
void SSDInterface::getPPN(unsigned int stage, pair<APP_TID_TYPE, REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair, vector <PPN_TYPE> &PPN_list, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > &request_identification_to_indices, VERTEX_TYPE vertex_id)
{
	unsigned int numOfElementsInAPage_rowPtr = SSD_PAGE_SIZE / sizeof(EDGE_INDEX);
	unsigned int numOfElementsInAPage_colPtr = SSD_PAGE_SIZE / sizeof(unsigned int);
	unsigned int rowPtr_pages = ceil((NumNodes * sizeof(EDGE_INDEX) * 1.0) / SSD_PAGE_SIZE);
	if(stage == 1)
	{
		if((vertex_id % numOfElementsInAPage_rowPtr) == (numOfElementsInAPage_rowPtr - 1))
		{
			PPN_list.push_back(SLBA_CONNECTIVITY/(SSD_PAGE_SIZE/PAGE_SIZE) + (vertex_id / (numOfElementsInAPage_rowPtr)));
			PPN_list.push_back(SLBA_CONNECTIVITY/(SSD_PAGE_SIZE/PAGE_SIZE) + ((vertex_id+1) / (numOfElementsInAPage_rowPtr)));
		}
		else {
			PPN_list.push_back(SLBA_CONNECTIVITY/(SSD_PAGE_SIZE/PAGE_SIZE) + (vertex_id / (numOfElementsInAPage_rowPtr)));
		}
	} else if(stage == 2)
	{
		EDGE_INDEX start_index = request_identification_to_indices[apptid_reqid_pair].first, end_index = request_identification_to_indices[apptid_reqid_pair].second;
		unsigned int numOfPages = (end_index / numOfElementsInAPage_colPtr) - (start_index / numOfElementsInAPage_colPtr);
#if(GTL_TRANSLATION_CHECKING == 1)
		cache_output_file[0] << "app id = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " vertex id = " << vertex_id << " numOfPages = " << numOfPages <<endl;
#endif//GTL_TRANSLATION_CHECKING
		for(unsigned int i = 0; i <= numOfPages; i++)
		{
			PPN_list.push_back((SLBA_CONNECTIVITY/(SSD_PAGE_SIZE/PAGE_SIZE)) + i + rowPtr_pages + start_index / numOfElementsInAPage_colPtr);
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[0] << "app id = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " vertex id = " << vertex_id << " column index ppn = " << (SLBA_CONNECTIVITY/(SSD_PAGE_SIZE/PAGE_SIZE)) + i + rowPtr_pages + start_index / numOfElementsInAPage_colPtr <<endl;
#endif//GTL_TRANSLATION_CHECKING
		}
	}
	else {
		assert(0);
	}
}

void  SSDInterface::processAPage(PPN_TYPE PPN, CPN_TYPE CPN, VERTEX_TYPE vertex_id, unsigned int stage, pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair, map<pair<APP_TID_TYPE, REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > & request_identification_to_indices, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, vector<EDGE_TYPE>* > & request_identification_to_adjlist_map)
{
	unsigned int numOfElementsInAPage_rowPtr = SSD_PAGE_SIZE / sizeof(EDGE_INDEX);
	unsigned int numOfElementsInAPage_colPtr = SSD_PAGE_SIZE / sizeof(unsigned int);
	unsigned int rowPtr_pages = ceil((NumNodes * sizeof(EDGE_INDEX) * 1.0) / SSD_PAGE_SIZE);
	if(stage == 1)
	{
		if((vertex_id % numOfElementsInAPage_rowPtr) == (numOfElementsInAPage_rowPtr - 1))
		{
			PPN_TYPE start_ppn = (SLBA_CONNECTIVITY/(SSD_PAGE_SIZE/PAGE_SIZE)) + (vertex_id / (numOfElementsInAPage_rowPtr));
			if(PPN == start_ppn)
			{
				request_identification_to_indices[apptid_reqid_pair].first = ((EDGE_INDEX *)(Host_Cache[CPN].data))[vertex_id % numOfElementsInAPage_rowPtr];
			} else {
				request_identification_to_indices[apptid_reqid_pair].second = ((EDGE_INDEX *)(Host_Cache[CPN].data))[(vertex_id + 1) % numOfElementsInAPage_rowPtr];
			}
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[0] << "indices, two pages"   << endl;
#endif//GTL_TRANSLATION_CHECKING
		}
		else {
			request_identification_to_indices[apptid_reqid_pair].first = ((EDGE_INDEX *)(Host_Cache[CPN].data))[vertex_id % numOfElementsInAPage_rowPtr];
			request_identification_to_indices[apptid_reqid_pair].second = ((EDGE_INDEX *)(Host_Cache[CPN].data))[(vertex_id + 1) % numOfElementsInAPage_rowPtr];
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[0] << "indices, single page"   << endl;
#endif//GTL_TRANSLATION_CHECKING
		}
#if(GTL_TRANSLATION_CHECKING == 1)
		cache_output_file[0] << "vertex id = " << vertex_id << "start index = " << request_identification_to_indices[apptid_reqid_pair].first << " end index = " << request_identification_to_indices[apptid_reqid_pair].second << endl;
#endif//GTL_TRANSLATION_CHECKING
	}
	else if(stage == 2){
		EDGE_INDEX start_index = request_identification_to_indices[apptid_reqid_pair].first;
		EDGE_INDEX end_index = request_identification_to_indices[apptid_reqid_pair].second;
		EDGE_INDEX start_ppn = (SLBA_CONNECTIVITY/(SSD_PAGE_SIZE/PAGE_SIZE)) + rowPtr_pages + start_index / numOfElementsInAPage_colPtr;
#if(GTL_TRANSLATION_CHECKING == 1)
		cache_output_file[0] << "start_index = " << start_index << " end_index = " << end_index << endl;
#endif//GTL_TRANSLATION_CHECKING
		
		unsigned int startElementInThisPage = 0;
		if(start_ppn == PPN)
		{
			startElementInThisPage = start_index % numOfElementsInAPage_colPtr;
		}
		EDGE_INDEX end_ppn = (SLBA_CONNECTIVITY/(SSD_PAGE_SIZE/PAGE_SIZE)) + rowPtr_pages + end_index / numOfElementsInAPage_colPtr;
		unsigned int endElementInThisPage = numOfElementsInAPage_colPtr;
		if(end_ppn == PPN)
		{
			endElementInThisPage = end_index % numOfElementsInAPage_colPtr;
		}
		for(unsigned int i=startElementInThisPage; i < endElementInThisPage; i++)
		{
			request_identification_to_adjlist_map[apptid_reqid_pair]->push_back(((EDGE_TYPE *)(Host_Cache[CPN].data))[i]);
		}
	}
	else {
		assert(0);
	}
}

void SSDInterface::completeProcessing(map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, APP_REQUEST_TYPE> & request_identification_to_request_map, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> & request_identification_to_number_of_need_pages_map, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, vector<EDGE_TYPE>* > & request_identification_to_adjlist_map, map<PPN_TYPE, vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > > & PPN_request_identification_mapper, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> & request_identification_to_stage, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > & request_identification_to_indices, pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair, CACHE_TID_TYPE cache_tid, queue<PPN_TYPE> & PPN_to_push_to_IO)
{
	unsigned int numOfElementsInAPage_rowPtr = SSD_PAGE_SIZE / sizeof(EDGE_INDEX);
	unsigned int numOfElementsInAPage_colPtr = SSD_PAGE_SIZE / sizeof(unsigned int);
	if(request_identification_to_stage[apptid_reqid_pair] == 1)
	{
#if(GTL_TRANSLATION_CHECKING == 1)
		cache_output_file[0] << "app id = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " finished stage 1" << endl;
#endif//GTL_TRANSLATION_CHECKING
		request_identification_to_stage[apptid_reqid_pair] = 2;
		request_identification_to_number_of_need_pages_map[apptid_reqid_pair] = (request_identification_to_indices[apptid_reqid_pair].second / numOfElementsInAPage_colPtr) - (request_identification_to_indices[apptid_reqid_pair].first / numOfElementsInAPage_colPtr) + 1;
		process_vertex(request_identification_to_request_map, request_identification_to_number_of_need_pages_map, request_identification_to_adjlist_map, PPN_request_identification_mapper, request_identification_to_stage, request_identification_to_indices, apptid_reqid_pair, cache_tid, PPN_to_push_to_IO);
	}
	else if(request_identification_to_stage[apptid_reqid_pair] == 2)
	{
#if(GTL_TRANSLATION_CHECKING == 1)
		cache_output_file[0] << "app id = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " finished stage 2" << endl;
#endif//GTL_TRANSLATION_CHECKING
		Cache_response_queue_lock[apptid_reqid_pair.first].lock();
#if(GTL_TRANSLATION_CHECKING == 1)
		cache_output_file[0] << "pushing into app id = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " adj. list size = " << request_identification_to_adjlist_map[apptid_reqid_pair]->size() << endl;
#endif//GTL_TRANSLATION_CHECKING
		Cache_response_queue[apptid_reqid_pair.first].push(make_pair(apptid_reqid_pair.second, request_identification_to_adjlist_map[apptid_reqid_pair]));
		Cache_response_queue_lock[apptid_reqid_pair.first].unlock();
		request_identification_to_request_map.erase(apptid_reqid_pair);
		request_identification_to_number_of_need_pages_map.erase(apptid_reqid_pair);
		request_identification_to_adjlist_map.erase(apptid_reqid_pair);
		request_identification_to_stage.erase(apptid_reqid_pair);
		request_identification_to_indices.erase(apptid_reqid_pair);
	}
	else {
		assert(0);
	}
}

void SSDInterface::process_vertex(map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, APP_REQUEST_TYPE> & request_identification_to_request_map, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> & request_identification_to_number_of_need_pages_map, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, vector<EDGE_TYPE>* > & request_identification_to_adjlist_map, map<PPN_TYPE, vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > > & PPN_request_identification_mapper, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> & request_identification_to_stage, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > & request_identification_to_indices, pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair, CACHE_TID_TYPE cache_tid, queue<PPN_TYPE> & PPN_to_push_to_IO)
{
	vector <PPN_TYPE> PPN_list;
	getPPN(request_identification_to_stage[apptid_reqid_pair], apptid_reqid_pair, PPN_list, request_identification_to_indices, request_identification_to_request_map[apptid_reqid_pair].vertex_id);
	request_identification_to_number_of_need_pages_map[apptid_reqid_pair]=PPN_list.size();
	assert(request_identification_to_number_of_need_pages_map[apptid_reqid_pair] != 0);
	assert(request_identification_to_adjlist_map[apptid_reqid_pair] != NULL);
	CPN_TYPE CPN;
	bool cache_hit;
	for (unsigned int i=0; i<PPN_list.size(); i++)
	{
		PPN_TYPE PPN=PPN_list[i];
		cache_hit=SSDInterface::HostCacheReadPPN(PPN,CPN);
		if (cache_hit)
		{
#if(MEASURE_PERFORMANCE == 1)
			if(request_identification_to_stage[apptid_reqid_pair] == 1)
			{
				rowPtr_hits += 1;
			}
			else {
				colPtr_hits += 1;
			}
#endif//MEASURE_PERFORMANCE
			processAPage(PPN, CPN, request_identification_to_request_map[apptid_reqid_pair].vertex_id, request_identification_to_stage[apptid_reqid_pair], apptid_reqid_pair, request_identification_to_indices, request_identification_to_adjlist_map);
			cache_page_lock[CPN].unlock_shared(); //done using the page, release the lock
			assert( (request_identification_to_number_of_need_pages_map[apptid_reqid_pair] > 0) && (request_identification_to_number_of_need_pages_map[apptid_reqid_pair] <= PPN_list.size()) );
			request_identification_to_number_of_need_pages_map[apptid_reqid_pair]--;
		}
		else
		{

			if(PPN_request_identification_mapper.find(PPN) != PPN_request_identification_mapper.end())
			{
#if(MEASURE_PERFORMANCE == 1)
			if(request_identification_to_stage[apptid_reqid_pair] == 1)
			{
				rowPtr_hits += 1;
			}
			else {
				colPtr_hits += 1;
			}
#endif//MEASURE_PERFORMANCE
				//here also it should be row pointer hits
				PPN_request_identification_mapper[PPN].push_back(apptid_reqid_pair);
			}
			else
			{
				//here it should be treated as a miss
				vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > tmp_vector_pair;
				tmp_vector_pair.push_back(apptid_reqid_pair);
				PPN_request_identification_mapper[PPN] = tmp_vector_pair;

				In_flight_tracker_lock.lock();
				if(In_flight_page_request_tracker.find(PPN) != In_flight_page_request_tracker.end())
				{
#if(MEASURE_PERFORMANCE == 1)
					if(request_identification_to_stage[apptid_reqid_pair] == 1)
					{
						rowPtr_hits += 1;
					}
					else {
						colPtr_hits += 1;
					}
#endif//MEASURE_PERFORMANCE
					(In_flight_page_request_tracker[PPN].second).push_back(cache_tid);
					cache_page_lock[In_flight_page_request_tracker[PPN].first].lock_shared();// shared lock CPN, because the PPN is in the tracker, so the PPN must be shared locked by someone else
				}
				else
				{
#if(MEASURE_PERFORMANCE == 1)
					if(request_identification_to_stage[apptid_reqid_pair] == 1)
					{
						rowPtr_misses += 1;
					}
					else {
						colPtr_misses += 1;
					}
#endif//MEASURE_PERFORMANCE
					CPN_TYPE reserved_CPN;
					reserved_CPN=SSDInterface::HostCacheRequestFreePage();//get the shared lock of the CPN in this step
					vector<CACHE_TID_TYPE> tmp_vector;
					tmp_vector.push_back(cache_tid);
					In_flight_page_request_tracker[PPN] = make_pair(reserved_CPN, tmp_vector);
					PPN_to_push_to_IO.push(PPN);
				}
				In_flight_tracker_lock.unlock();
			}
		}
	}
	if (request_identification_to_number_of_need_pages_map[apptid_reqid_pair]==0)
	{
		completeProcessing(request_identification_to_request_map, request_identification_to_number_of_need_pages_map, request_identification_to_adjlist_map, PPN_request_identification_mapper, request_identification_to_stage, request_identification_to_indices, apptid_reqid_pair, cache_tid, PPN_to_push_to_IO);
	}
}
