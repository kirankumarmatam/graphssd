#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <linux/aio_abi.h>
#include <sys/syscall.h> // syscall numbers
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <limits>
#include <time.h>
#include <queue>
#include <set>
//#include <boost/filesystem.hpp>
#include <experimental/filesystem>
//using namespace boost::filesystem;
namespace fs = std::experimental::filesystem::v1;
#include <cstddef>
#include <thread>
#include <atomic>
#include <mutex>
#include <shared_mutex>
#include "testing.h"
using namespace std;

#define RunFromOpenSSD 1
#define useCache 1
#define STORE_LARGE_GRAPH 1
#define UseCache_GetPagesFromSSD 1
#define LOAD_GRAPH_FROM_FILE 1
#define CHECK_ERROR ((!LOAD_GRAPH_FROM_FILE) && 1)
#define UseAsynchronousIO 1
#define CALCULATE_DEGREE 1

#define BufferInStorage 1

#define SLBA_CONNECTIVITY 200
#define SLBA_FLUSH_BUFFER 128640775

#define FLUSH_SSD_BUFFER 0
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define STORE_LARGE_SCALE 1

#define IsUndirectedGraph 1

typedef unsigned long long int REQUEST_UNIQUE_ID_TYPE;
typedef unsigned int APP_TID_TYPE, CACHE_TID_TYPE, IO_TID_TYPE;
typedef unsigned int PPN_TYPE;
typedef unsigned int VERTEX_TYPE;
typedef unsigned int EDGE_TYPE;
typedef unsigned int CPN_TYPE; //cache page number

//page size in bytes
#define PAGE_SIZE 4096
#define SSD_PAGE_SIZE 16384

#define EDGE_INDEX uint64_t
//#define EDGE_INDEX unsigned int
typedef EDGE_INDEX EdgeIndexType;

typedef struct HostCacheBlock
{
	char data[SSD_PAGE_SIZE];
} CacheBlock;

typedef struct Application_request_type
{
	VERTEX_TYPE vertex_id;
	APP_TID_TYPE tid;
	REQUEST_UNIQUE_ID_TYPE request_id;
} APP_REQUEST_TYPE;

struct mutex_wrapper : std::mutex
{
	  mutex_wrapper() = default;
	    mutex_wrapper(mutex_wrapper const&) noexcept : std::mutex() {}
	      bool operator==(mutex_wrapper const&other) noexcept { return this==&other; }
};

struct shared_mutex_wrapper : std::shared_mutex
{
	  shared_mutex_wrapper() = default;
	    shared_mutex_wrapper(shared_mutex_wrapper const&) noexcept : std::shared_mutex() {}
	      bool operator==(shared_mutex_wrapper const&other) noexcept { return this==&other; }
};

class SSDInterface
{
	public:
		int store_graph(int argc, char **argv);
		void printTime();
		void WriteToSSD(unsigned int j);
#if(UseAsynchronousIO == 1)
		void ReadFromSSDNToBuffer(unsigned int SLBA, unsigned int numBlocks, void *buffer, IO_TID_TYPE tid, aio_context_t &ioctx);
#else
		void ReadFromSSDNToBuffer(unsigned int SLBA, unsigned int numBlocks, void *buffer, IO_TID_TYPE tid);
#endif//UseAsynchronousIO

		void *data_nvme_command;
		int fd;
#if(UseAsynchronousIO == 0)
#if(RunFromOpenSSD == 1)
		fstream myFlashFile;
#else//RunFromOpenSSD
		vector<fstream> myFlashFile;
#endif//RunFromOpenSSD
#else//UseAsynchronousIO
		int myFlashFile;
//              aio_context_t ioctx;
		unsigned io_maxevents;
#endif//UseAsynchronousIO


		SSDInterface()
		{
			if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
				                                        fprintf(stderr, "cannot allocate io payload for data_wr\n");
									                                }
		}

		void getAdjEdgeList(VERTEX_TYPE vertex_id, std::vector<PPN_TYPE> &PPN_list);
		unsigned int binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number);
		void Initialization(int argc, char *argv[]);
		void Finalize();
		void cache_manager();
		void cache_manager_tid(CACHE_TID_TYPE cache_tid);
		void IO_manager();
#if(UseAsynchronousIO == 1)
		void IO_manager_async_tid(IO_TID_TYPE tid);
#else//UseAsynchronousIO
		void IO_manager_tid(IO_TID_TYPE tid);
#endif//UseAsynchronousIO
		int InitializeHostCache(unsigned int CacheSize);
		bool HostCacheReadPPN(PPN_TYPE PPN,CPN_TYPE &CPN);
		CPN_TYPE HostCacheRequestFreePage();

		void LRU_cache_read(CPN_TYPE CPN);
		CPN_TYPE LRU_cache_evict();
		void application_program(int argc, char *argv[]);

		void start_application_time();
		void end_application_time();
		void Increment_num_of_read_IO_pages(unsigned int SLBA, unsigned int numBlocks);
		void Decrement_average_parallelism();
		void print_info();
		void application_print_info();
		void getPPN(unsigned int stage, pair<APP_TID_TYPE, REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair, vector <PPN_TYPE> &PPN_list, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > &request_identification_to_indices, VERTEX_TYPE vertex_id);
		void  processAPage(PPN_TYPE PPN, CPN_TYPE CPN, VERTEX_TYPE vertex_id, unsigned int stage, pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair, map<pair<APP_TID_TYPE, REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > & request_identification_to_indices, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, vector<EDGE_TYPE>* > & request_identification_to_adjlist_map);
		void  completeProcessing(map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, APP_REQUEST_TYPE> & request_identification_to_request_map, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> & request_identification_to_number_of_need_pages_map, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, vector<EDGE_TYPE>* > & request_identification_to_adjlist_map, map<PPN_TYPE, vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > > & PPN_request_identification_mapper, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> & request_identification_to_stage, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > & request_identification_to_indices, pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair, CACHE_TID_TYPE cache_tid, queue<PPN_TYPE> & PPN_to_push_to_IO);
		void process_vertex(map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, APP_REQUEST_TYPE> & request_identification_to_request_map, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> & request_identification_to_number_of_need_pages_map, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, vector<EDGE_TYPE>* > & request_identification_to_adjlist_map, map<PPN_TYPE, vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > > & PPN_request_identification_mapper, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> & request_identification_to_stage, map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, pair<EDGE_INDEX, EDGE_INDEX> > & request_identification_to_indices, pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair, CACHE_TID_TYPE cache_tid, queue<PPN_TYPE> & PPN_to_push_to_IO);
#if(CALCULATE_DEGREE == 1)
		void calculate_degree();
#endif//CALCULATE_DEGREE
};
