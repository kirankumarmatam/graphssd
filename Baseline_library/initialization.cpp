#include "graphFiltering.h"
extern unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
unsigned int number_of_application_response_queues;

extern unsigned int NumOfCacheThreads;

extern unsigned int App_to_cache_request_queue_max_size, Cache_response_queue_max_size;
extern mutex App_to_cache_request_queue_lock;
extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;

extern unsigned int cache_to_IO_request_queue_max_size,IO_response_queue_max_size;
extern queue<PPN_TYPE> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;
extern vector< queue< pair<PPN_TYPE,CPN_TYPE> > > IO_response_queue;
extern vector< mutex_wrapper > IO_response_queue_lock;

unsigned int MaxCacheSize;

extern unsigned int NumOfIOThreads;

vector<ofstream> application_output_file;
vector<ofstream> cache_output_file;
vector<ofstream> IO_output_file;

#if(MEASURE_PERFORMANCE == 1)
extern uint64_t rowPtr_hits, rowPtr_misses, colPtr_misses, colPtr_hits;
#endif//MEASURE_PERFORMANCE

void SSDInterface::Initialization(int argc, char *argv[])
{
	/*---------Application related ones---------*/
	NumOfRequestingThreads = 1;
	NumberOfResponseThreads = 1;
	number_of_application_response_queues = NumOfRequestingThreads;
	App_to_cache_request_queue_max_size = 30000;
	Cache_response_queue.resize(number_of_application_response_queues);
	for (unsigned int i=0;i<number_of_application_response_queues;i++)
	{
		mutex_wrapper tmp_mtx;
		Cache_response_queue_lock.push_back(tmp_mtx);
	}
	Cache_response_queue_max_size = 30000;
	application_output_file.resize(NumOfRequestingThreads+NumberOfResponseThreads);
	string fileName;
	application_output_file.resize(NumOfRequestingThreads+NumberOfResponseThreads);
	for(unsigned int i = 0; i < (NumOfRequestingThreads+NumberOfResponseThreads); i++)
	{
		fileName = "/home/ossd/GraphSSD/Parallel_graphSSD/Output/application_"+to_string(i)+".txt";
		application_output_file[i].open(fileName, ios::trunc);
		assert(application_output_file[i].is_open());
	}
	/*---------Cache manager related ones---------*/
	NumOfCacheThreads = 3;
	cache_to_IO_request_queue_max_size = 1000;
	IO_response_queue.resize(NumOfCacheThreads);
	for (unsigned int i=0;i<NumOfCacheThreads;i++)
	{
		mutex_wrapper tmp_mtx;
		IO_response_queue_lock.push_back(tmp_mtx);
	}
	IO_response_queue_max_size = 1000;
	InitializeHostCache(MaxCacheSize);//store graph function should be called before this
	cache_output_file.resize(NumOfCacheThreads);
	for(unsigned int i = 0; i < (NumOfCacheThreads); i++)
	{
		fileName = "/home/ossd/GraphSSD/Parallel_graphSSD/Output/cache_"+to_string(i)+".txt";
		cache_output_file[i].open(fileName, ios::trunc);
		assert(cache_output_file[i].is_open());
	}
#if(MEASURE_PERFORMANCE == 1)
	rowPtr_hits = 0;
	colPtr_hits = 0;
	rowPtr_misses = 0;
	colPtr_misses = 0;
#endif//MEASURE_PERFORMANCE
/*---------I/O related ones---------*/
#if(UseAsynchronousIO == 1)
	NumOfIOThreads = 1;
#else//UseAsynchronousIO
	NumOfIOThreads = 1;
#endif//UseAsynchronousIO
	IO_output_file.resize(NumOfIOThreads);
	for(unsigned int i = 0; i < (NumOfIOThreads); i++)
	{
		fileName = "/home/ossd/GraphSSD/Parallel_graphSSD/Output/IO_"+to_string(i)+".txt";
		IO_output_file[i].open(fileName, ios::trunc);
		assert(IO_output_file[i].is_open());
	}
#if(UseAsynchronousIO == 1)
#if(RunFromOpenSSD == 0)
//	ioctx = 0;
	io_maxevents = 128;
	myFlashFile = open(argv[3], O_RDONLY | O_DIRECT);
	if (myFlashFile == -1) {
		perror(argv[3]);
		exit(1);
	}
#else//RunFromOpenSSD
	io_maxevents = 128;
	myFlashFile = open(argv[1], O_RDONLY | O_DIRECT);
	if (myFlashFile == -1) {
		perror(argv[1]);
		exit(1);
	}
#endif//RunFromOpenSSD == 1
#else//UseAsynchronousIO
#if(RunFromOpenSSD == 0)
	myFlashFile.resize(NumOfIOThreads);
	for(unsigned int i = 0; i < (NumOfIOThreads); i++)
	{
		myFlashFile[i].open(argv[3], ios::in);
		assert(myFlashFile[i].is_open());
	}
#endif//RunFromOpenSSD
#endif//UseAsynchronousIO
}

#if(UseAsynchronousIO == 1)
static inline int
io_destroy(aio_context_t ctx)
{
	            return syscall(SYS_io_destroy, ctx);
}
#endif//UseAsynchronousIO == 1

void SSDInterface::Finalize()
{
	end_application_time();
	print_info();
	for(unsigned int i = 0; i < (NumOfRequestingThreads+NumberOfResponseThreads); i++)
	{
		application_output_file[i].close();
	}
	for(unsigned int i = 0; i < NumOfCacheThreads; i++)
	{
		cache_output_file[i].close();
	}
	for(unsigned int i = 0; i < NumOfIOThreads; i++)
	{
		IO_output_file[i].close();
	}
#if(UseAsynchronousIO == 1)
//	io_destroy(ioctx);
	close(myFlashFile);
#else//UseAsynchronousIO
#if(RunFromOpenSSD == 0)
	for(unsigned int i = 0; i < NumOfIOThreads; i++)
	{
		myFlashFile[i].close();
	}
#endif//RunFromOpenSSD
#endif//UseAsynchronousIO
	return;
}
