#include "graphFiltering.h"
#include "testing.h"
#define STORE_GRAPH_AT_SSD 1

extern unsigned int NumNodes;
extern EdgeIndexType NumEdges;

int main(int argc, char *argv[])
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	SSDInstance.Initialization(argc, argv);

#if(GTL_TRANSLATION_CHECKING == 1)
	cout << "yay initialization done!" << endl;
#endif

	omp_set_nested(100);
	unsigned int tid;

#pragma omp parallel num_threads(3) private(tid)
	{
		tid = omp_get_thread_num();
		if(tid == 0)
		{
			SSDInstance.cache_manager();
		}
		else if (tid == 1)
		{
			SSDInstance.IO_manager();
		}
		else if (tid == 2)
		{
			SSDInstance.application_program(argc, argv); // when application program finishes work it calls exit();
		}
	}
	return 0;
}
