#include "graphFiltering.h"

#if(IsUndirectedGraph == 0)
PNGraph G;
#elif(IsUndirectedGraph == 1)
PUNGraph G1;
PNGraph G;
#endif
unsigned int NumNodes;
EdgeIndexType NumEdges;
unsigned int fd;

extern unsigned int MaxCacheSize;
unsigned int cache_percentage = 5;

int SSDInterface::store_graph(int argc, char **argv)
{
	if(argc < 4)
	{
#if(STORE_LARGE_SCALE == 1)
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt output_flash.txt" << endl;
#else
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt number_of_requests" << endl;
#endif//STORE_LARGE_SCALE
		exit(0);
	}

#if(STORE_LARGE_SCALE == 0)
#if(IsUndirectedGraph == 0)
	G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
	//      PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
	NumNodes = G->GetNodes();
	NumEdges = G->GetEdges();
#elif(IsUndirectedGraph == 1)
	G1 = TSnap::LoadEdgeList<PUNGraph>(argv[2], 0, 1);
	        G = TSnap::ConvertGraph<PNGraph>(G1);
	//      PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
	NumNodes = G->GetNodes();
	NumEdges = G->GetEdges();
#endif//IsUndirectedGraph
#else
	fstream myFlashFile;
	myFlashFile.open(argv[3]);
	cout << argv[3] << endl;
	myFlashFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myFlashFile.read((char *)(&NumEdges), sizeof(EdgeIndexType));
	myFlashFile.close();
	cout << "NumNodes = " << NumNodes  <<  " NumEdges = " << NumEdges << endl;
#endif//STORE_LARGE_SCALE == 1

#if(useCache==1)
	if(((NumNodes + NumEdges) * sizeof(unsigned int)) < (256*1024))
		MaxCacheSize = 256 * 1024;
	else MaxCacheSize = (((NumNodes + NumEdges) * sizeof(unsigned int)) * cache_percentage) / 100;
	MaxCacheSize = 1024*1024*1024;
#endif

	//      static const char *perrstr;
	//      int err, fd;
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		cout << "Error opening file!!" << endl;
	if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		return 0;
	}
#if(RunFromOpenSSD == 1)
#if (FLUSH_SSD_BUFFER == 1)
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1
#endif//RunFromOpenSSD
	return 0;
}

void SSDInterface::WriteToSSD(unsigned int j)
{
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA_FLUSH_BUFFER + j;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);

	return;

	//perror:
	//      perror(perrstr);
	//      return 1;
}
