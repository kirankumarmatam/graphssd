#include "graphFiltering.h"

struct timespec app_time_start, app_time_end;
#if(MEASURE_PERFORMANCE == 1)
extern uint64_t rowPtr_hits, rowPtr_misses, colPtr_misses, colPtr_hits;
#endif//MEASURE_PERFORMANCE

void SSDInterface::start_application_time()
{
	clock_gettime(CLOCK_MONOTONIC, &app_time_start);
}

void SSDInterface::end_application_time()
{
	clock_gettime(CLOCK_MONOTONIC, &app_time_end);
}

unsigned long long int Number_of_IO_pages_read = 0;
unsigned long long int Total_number_of_average_pages_read = 0, current_inflight_request = 0;
mutex IO_utility_lock;

void SSDInterface::Increment_num_of_read_IO_pages(unsigned int SLBA, unsigned int numBlocks)
{
	IO_utility_lock.lock();
	Number_of_IO_pages_read += 1;
	current_inflight_request++;
	Total_number_of_average_pages_read += current_inflight_request;
	IO_utility_lock.unlock();
}

void SSDInterface::Decrement_average_parallelism()
{
	IO_utility_lock.lock();
	current_inflight_request--;
	IO_utility_lock.unlock();
}

void  SSDInterface::print_info()
{
	double time_elapsed;
	time_elapsed = ((double)app_time_end.tv_sec - (double)app_time_start.tv_sec);
	time_elapsed += ((double)app_time_end.tv_nsec - (double)app_time_start.tv_nsec) / 1000000000.0;
	cout << "TIME_APP: " << time_elapsed << " (sec)" << endl;
	cout << "Bandwidth utilization is " << (Number_of_IO_pages_read * SSD_PAGE_SIZE * 1.0) / (1000000*time_elapsed) << "(MB/sec)" << endl;
	cout << "Average IO parallelism = " << (Total_number_of_average_pages_read * 1.0) / Number_of_IO_pages_read << endl;
	cout << "Number of IO pages read = " << Number_of_IO_pages_read << endl;
#if(MEASURE_PERFORMANCE == 1)
	cout << "rH = " << rowPtr_hits << " rM = " <<  rowPtr_misses << " cH = " <<  colPtr_hits << " cM = " << colPtr_misses << endl;
	cout << "nrR = " << (rowPtr_hits*1.0) / (rowPtr_hits+rowPtr_misses) << " ncR = " << (colPtr_hits*1.0) / (colPtr_hits+colPtr_misses)  << endl;
#endif//MEASURE_PERFORMANCE
	return;
}
