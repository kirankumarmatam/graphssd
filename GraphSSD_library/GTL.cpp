#include "graphFiltering.h"
#include "header.h"

extern map<unsigned int, unsigned int > vertexMapping;

#if(UseFTL == 3)
extern std::map<unsigned int, char *> Storage;
extern unsigned int currentGTTIndex_edge;
extern unsigned int GraphLPN;
extern struct GTTTable1 *gtt1;
#endif

extern unsigned int NumNodes;
unsigned int numOfNANDPageWrites = 0, numOfNANDPageReads = 0;

#define SECTOR_SIZE 4096
#if(UseFTL == 3)
void SSDInterface::WriteGraphAndDelete()
{
	for(std::map<unsigned int, char *>::iterator it = Storage.begin(); it != Storage.end(); it++)
	{
		memcpy((void *)data_nvme_command, (void *)it->second, SSD_PAGE_SIZE);
		WriteToSSDN( it->first * (SSD_PAGE_SIZE / SECTOR_SIZE), (SSD_PAGE_SIZE / SECTOR_SIZE));
	}

	unsigned int numOfEntries = 0, numOfEntriesInIter;
	while(1)
	{
		numOfEntriesInIter = ((currentGTTIndex_edge - numOfEntries + 1) >= (16*4096 / sizeof(unsigned int))) ? (16*4096 / sizeof(unsigned int)) : (currentGTTIndex_edge - numOfEntries + 1);
//		assert(currentGTTIndex_edge <= (16*4096 / sizeof(unsigned int)));
		for(unsigned int i = 0; i < numOfEntriesInIter; i++)
		{
			((unsigned int *)data_nvme_command)[2*i] = gtt1->GTTPointer1[numOfEntries + i].vertexId;
			((unsigned int *)data_nvme_command)[2*i+1] = gtt1->GTTPointer1[numOfEntries + i].LPN;
		//			cout << gtt1->GTTPointer1[i].vertexId << " " << gtt1->GTTPointer1[i].LPN << endl;
		}
		//	cout << "currentGTTIndex_edge = " << currentGTTIndex_edge << " GraphLPN =  "  << GraphLPN <<endl;
		struct nvme_user_io io;
		io.opcode = nvme_cmd_write;
		io.flags = 8;
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		io.slba = numOfEntriesInIter;
		io.nblocks = 31;
		io.dsmgmt = 0;
		io.reftag = GraphLPN;
		io.apptag = 0;
		io.appmask = 0;
		int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		if (err < 0)
			cout << "1 Error in executing nvme command!!" << endl;

		if (err)
		{
			fprintf(stderr, "1 nvme write status:%x\n", err);
			exit(0);
		}	
		numOfEntries += numOfEntriesInIter;
		if(numOfEntries > currentGTTIndex_edge)
			break;
	}

#if(UseCache_GetPagesFromSSD == 0)
	GraphFinalize();
#endif//UseCache_GetPagesFromSSD
	return;
}
#endif

void SSDInterface::WriteToSSDN(unsigned int SLBA, unsigned int numBlocks)
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA;
	io.nblocks = numBlocks - 1;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	//		cout << io.slba << " " << io.nblocks << endl;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
	{
		cout << "2 Error in executing nvme command!!" << endl;
		cout << "SLBA = " << SLBA << " numBlocks = " << numBlocks << endl;
	}

	if (err)
	{
		fprintf(stderr, "2 nvme write status:%x\n", err);
		exit(0);
	}
}

void SSDInterface::ReadFromSSDN(unsigned int SLBA, unsigned int numBlocks)
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA;
	io.nblocks = numBlocks - 1;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	//              cout << io.slba << " " << io.nblocks << endl;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "3 Error in executing nvme command!!" << endl;

	if (err)
	{
		fprintf(stderr, "3 nvme write status:%x\n", err);
		exit(0);
	}
}

void SSDInterface::InitializeGraphDataStructures(unsigned int NumNodes)
{
#if(UseFTL == 3)
		GraphInitialize();
#endif
#if(RunFromOpenSSD == 1)
		struct nvme_user_io io;
		io.opcode = nvme_cmd_write;
		io.flags = 4;//2; Initialize with correct flags
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		io.slba = NumNodes;
		io.nblocks = 0;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;
		int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		if (err < 0)
			cout << "4 Error in executing nvme command!! Inital" << endl;
		if (err)
		{
			fprintf(stderr, "4 nvme write status:%x\n", err);
			exit(0);
		}
#endif//RunFromOpenSSD

}

void SSDInterface::AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList)
{
	if(UseFTL == 3)
	{
		unsigned int num = EdgeList.size();
		if(EdgeList.size() * sizeof(unsigned int) > 16*4096)
			num = 16*4096 / (sizeof(unsigned int));
		memcpy((void *)data_nvme_command, EdgeList.data(), num*sizeof(unsigned int));
		putAdjEdgeList(vID,(char *)data_nvme_command, num);
#if(STORE_LARGE_GRAPH)
		if(Storage.size() > 1)
		{
			for(std::map<unsigned int, char *>::iterator it = Storage.begin(); it != Storage.end(); it++)
			{
				if((it->first != GraphLPN))//Need to extend it to GraphLPNWeight if adding edges
				{
					memcpy((void *)data_nvme_command, (void *)it->second, SSD_PAGE_SIZE);
					WriteToSSDN( it->first * (SSD_PAGE_SIZE / SECTOR_SIZE), (SSD_PAGE_SIZE / SECTOR_SIZE));
					free(it->second);
					Storage.erase(it);
				}
			}			
		}
#endif
		return;
	}
	else {
		//	assert(EdgeList.size()*sizeof(unsigned int) <= 32*4096);
		unsigned int num = EdgeList.size();
		if(EdgeList.size() * sizeof(unsigned int) > 16*4096)
			num = 16*4096 / (sizeof(unsigned int));
		memcpy((void *)data_nvme_command, EdgeList.data(), num*sizeof(unsigned int));
		struct nvme_user_io io;
		io.opcode = nvme_cmd_write;
		io.flags = 2;//2; Initialize with correct flags
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		io.slba = vID;     // use slba for passing the vertex Id
		//	io.nblocks = ceil((sizeof(unsigned int)*(EdgeList.size()+1)*1.0)/PAGE_SIZE);
		io.nblocks = ceil((sizeof(unsigned int)*(num+1)*1.0)/PAGE_SIZE);
		io.dsmgmt = 0;
		io.reftag = num;
		io.apptag = 0;
		io.appmask = 0;
		cout << "E" << endl;
		int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		cout << "E1" << endl;
		if (err < 0)
			cout << "5 Error in executing nvme command!!" << endl;

		if (err)
		{
			fprintf(stderr, "5 nvme write status:%x\n", err);
			exit(0);
		}
	}
}

void SSDInterface::AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList)
{
	assert(vID < vertexMapping.size());
	vertexToAdjListMap[vID];
	vertexToAdjListMap[vID] = EdgeList;
}

void SSDInterface::GetAdjListNoGTL(unsigned int vID, vector<unsigned int> &EdgeList)
{
	assert(vID < NumNodes);
	for(unsigned int i=0; i<vertexToAdjListMap[vID].size(); i++)
		EdgeList.push_back(vertexToAdjListMap[vID][i]);
}

void SSDInterface::AddVertexWeightUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights)
{
	assert(EdgeList.size()*sizeof(unsigned int) <= 16*4096);//Check if we need to increase the buffer size
	unsigned int num = EdgeList.size();
//	memcpy((void *)data_nvme_command, &num, sizeof(unsigned int));
	memcpy((void *)data_nvme_command, EdgeList.data(), EdgeList.size()*sizeof(unsigned int));
	memcpy((void *)((unsigned int *)data_nvme_command + EdgeList.size()), EdgeWeights.data(), EdgeWeights.size()*sizeof(unsigned int));
//	cout << data_nvme_command << " " << (unsigned int *)data_nvme_command + 1 << endl;
/*	cout << "vID = " << vID << " edgeListSize = " << EdgeList.size() << endl;
	for(int i=0; i < 2*EdgeList.size(); i++)
		cout <<*((unsigned int *)data_nvme_command + i) << " ";
	cout << endl;
*/	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 5;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;//Transfer number of vertices to write in metadata (reserved)?
	io.addr = (unsigned long)data_nvme_command;
//	cout << "io.addr = " << io.addr << endl;
	io.slba = vID;     // use slba for passing the vertex Id
	io.nblocks = ceil((sizeof(unsigned int)*(EdgeList.size()+EdgeWeights.size()+1)*1.0)/PAGE_SIZE);
	io.dsmgmt = 0;
//	io.reftag = 0;
	io.reftag = num;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "6 Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "6 nvme write status:%x\n", err);
}

void SSDInterface::WriteToSSD(unsigned int j)
{
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA_FLUSH_BUFFER + j;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "7 Error in executing nvme command" << endl;
	if (err)
		fprintf(stderr, "7 nvme write status:%x\n", err);

	return;

	//perror:
	//      perror(perrstr);
	//      return 1;
}

void SSDInterface::FinalizeGraphDataStructures()
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 6;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = 0;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "8 Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "8 nvme write status:%x\n", err);
		exit(0);
	}
	numOfNANDPageWrites = ((unsigned int *)data_nvme_command)[0];
	numOfNANDPageReads = ((unsigned int *)data_nvme_command)[1];
}

void SSDInterface::ClearQueues()
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 7;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = 0;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "9 Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "9 nvme write status:%x\n", err);
		exit(0);
	}
}

