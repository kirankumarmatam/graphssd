#include "graphFiltering.h"

extern CacheBlock* Host_Cache;
std::queue<PPN_TYPE> cache_to_IO_request_queue;
mutex cache_to_IO_request_queue_lock;

extern map<PPN_TYPE, pair< CPN_TYPE, vector<CACHE_TID_TYPE> > > In_flight_page_request_tracker;

extern mutex In_flight_tracker_lock;
extern vector< queue< pair<PPN_TYPE,CPN_TYPE> > > IO_response_queue;
extern std::vector< mutex_wrapper > IO_response_queue_lock;

extern mutex cache_update_lock;
extern PPN_TYPE *CPN_to_PPN_map;
extern map<PPN_TYPE,CPN_TYPE> PPN_to_CPN_map;

unsigned int NumOfIOThreads;

extern vector<ofstream> IO_output_file;

extern unsigned int MaxCacheSize;
extern unsigned int GraphLPN;
extern unsigned int StartingGraphLPN;
extern unsigned int NumOfCacheThreads;

#if(UseAsynchronousIO == 1)
static inline int
io_setup(unsigned maxevents, aio_context_t *ctx) {
	    return syscall(SYS_io_setup, maxevents, ctx);
}

static inline int
io_submit(aio_context_t ctx, long nr, struct iocb **iocbpp) {
	    return syscall(SYS_io_submit, ctx, nr, iocbpp);
}

static inline int
io_getevents(aio_context_t ctx, long min_nr, long nr,
		                 struct io_event *events, struct timespec *timeout) {
	    return syscall(SYS_io_getevents, ctx, min_nr, nr, events, timeout);
}
#endif//UseAsynchronousIO == 1

void SSDInterface::IO_manager()
{
	printf("IO manager: entering here\n");
	#pragma omp parallel num_threads(NumOfIOThreads)
	{
		IO_TID_TYPE tid = omp_get_thread_num();
#if(UseAsynchronousIO == 1)
		IO_manager_async_tid(tid);
#else
		IO_manager_tid(tid);
#endif//UseAsynchronousIO
	}
}

#if(UseAsynchronousIO == 1)
void SSDInterface::IO_manager_async_tid(IO_TID_TYPE tid)
{
#if(GTL_TRANSLATION_CHECKING == 1)
	IO_output_file[tid] << "GTL_TRANSLATION_CHECKING: IOThread id = " << tid << endl;
//	return;
#endif
	unsigned int io_currentEvents = 0;
	size_t nevents = 1;
	struct io_event events[nevents];
	struct timespec waitingTime;
	waitingTime.tv_sec = 0;
	waitingTime.tv_nsec = 0;
	aio_context_t ioctx;
	ioctx = 0;
	io_maxevents = 128;

	if (io_setup(io_maxevents, &ioctx) < 0) {
		perror("io_setup");
		exit(1);
	}

	while(1)
	{
		if(io_currentEvents < io_maxevents)
		{
			cache_to_IO_request_queue_lock.lock();
			if(!cache_to_IO_request_queue.empty())
			{
				unsigned int PPN = cache_to_IO_request_queue.front();
				assert((PPN >= 0) && (PPN <= GraphLPN));
				cache_to_IO_request_queue.pop();
				cache_to_IO_request_queue_lock.unlock();
#if(MEASURE_TIME == 1)
				vector <PPN_TYPE> PPN_list;
				PPN_list.push_back(PPN);
				stat_collect_channel_access_statistics(PPN_list);
				PPN_list.clear();
#endif//MEASURE_TIME
#if(GTL_TRANSLATION_CHECKING == 1)
				IO_output_file[tid] << "popped PPN = " << PPN << endl;
#endif//GTL_TRANSLATION_CHECKING
				In_flight_tracker_lock.lock();
				std::pair<CPN_TYPE, vector<CACHE_TID_TYPE> > PPN_mapping_info = In_flight_page_request_tracker[PPN];//Need to pass pointer for the list?
				//It copies the vector, checked it
				In_flight_tracker_lock.unlock();
				io_currentEvents++;
#if(GTL_TRANSLATION_CHECKING == 1)
				IO_output_file[tid] << "popped CPN = " << PPN_mapping_info.first << " io_currentEvents = " << io_currentEvents << endl;
#endif//GTL_TRANSLATION_CHECKING
#if(VERIFY_LOADED_DATA_ASYNC == 1)
				if(PPN == 1286620)
				{
					IO_output_file[tid] << "PPN = " << PPN << " LBA = " << PPN*(SSD_PAGE_SIZE/PAGE_SIZE) << " numOfBlocks = "<< SSD_PAGE_SIZE/PAGE_SIZE<< " CPN = " << PPN_mapping_info.first << " buffer address = "<< (uint64_t)(Host_Cache[PPN_mapping_info.first].data) << " bytes " << endl;
//					IO_output_file[tid] << "PPN = " << PPN << " LBA = " << PPN*(SSD_PAGE_SIZE/PAGE_SIZE) << " numOfBlocks = "<< SSD_PAGE_SIZE/PAGE_SIZE<< " CPN = " << PPN_mapping_info.first << " buffer address = "<< (Host_Cache[PPN_mapping_info.first].data) << " bytes " << endl;
				}
#endif//VERIFY_LOADED_DATA_ASYNC
				ReadFromSSDNToBuffer(PPN * (SSD_PAGE_SIZE / PAGE_SIZE), SSD_PAGE_SIZE/PAGE_SIZE, (void * )Host_Cache[PPN_mapping_info.first].data, tid, ioctx);//Using PPN or LBA?
			}
			else
			{
				cache_to_IO_request_queue_lock.unlock();
			}
		}
		//Perform this step only if io_currentEvents > 0
		int ret = io_getevents(ioctx, 1 /* min */, 1, events, &waitingTime);
//		int ret = io_getevents(ioctx, 1 /* min */, nevents, events, NULL);
//		cout << "ret = " << ret << endl;
		if (ret < 0) {
			perror("io_getevents");
			exit(1);
		}

		for (size_t i=0; i<ret; i++) {
			Decrement_average_parallelism();
			struct io_event *ev = &events[i];
			//            assert(ev->data == 0xbeef || ev->data == 0xbaba);
			unsigned int PPN = (ev->data / (SSD_PAGE_SIZE / PAGE_SIZE));
#if(GTL_TRANSLATION_CHECKING == 1)
			IO_output_file[tid] << "Event returned with ret = "<< ret<< " res= " << ev->res << " res2= "<<ev->res2<< " data = " << ev->data << " PPN = " << PPN << endl;
#endif//GTL_TRANSLATION_CHECKING
			assert((PPN >= 0) && (PPN <= GraphLPN));
#if(VERIFY_LOADED_DATA_ASYNC == 1)
//			char temp_buffer[SSD_PAGE_SIZE];
			In_flight_tracker_lock.lock();
			std::pair<CPN_TYPE, vector<CACHE_TID_TYPE> > PPN_mapping_info_temp = In_flight_page_request_tracker[PPN];
//			myFlashFile_temp.seekg(PPN*SSD_PAGE_SIZE, myFlashFile_temp.beg);//this work only for runopenssd = 1
//			myFlashFile_temp.read(temp_buffer, SSD_PAGE_SIZE);

			char *temp_buffer;
			if ( posix_memalign((void **)&temp_buffer, 16384, 8*4096) ) {
				fprintf(stderr, "cannot allocate io payload for data_wr\n");
			}
			ReadFromSSDN(PPN*4, (SSD_PAGE_SIZE/PAGE_SIZE));
			memcpy(temp_buffer, data_nvme_command, SSD_PAGE_SIZE);

			for(unsigned int k = 0; k < (SSD_PAGE_SIZE/(sizeof(unsigned int))); k++)
			{
				if((((unsigned int*)temp_buffer)[k] != ((unsigned int *)Host_Cache[PPN_mapping_info_temp.first].data)[k]) || (PPN == 1286620))
				{
					IO_output_file[tid] << "PPN is " << PPN << " CPN is " << PPN_mapping_info_temp.first << endl;
					IO_output_file[tid] << "Loaded one is" << endl;						
					for(unsigned int j = 0 ; j < SSD_PAGE_SIZE / sizeof(unsigned int); j++)
					{
						IO_output_file[tid] << j << "=" << ((unsigned int *)Host_Cache[PPN_mapping_info_temp.first].data)[j] << "; ";

					}
					IO_output_file[tid] << endl;
					IO_output_file[tid] << "Correct one is" << endl;
					for(unsigned int j = 0 ; j < SSD_PAGE_SIZE / sizeof(unsigned int); j++)
					{
						IO_output_file[tid] << j << "=" << ((unsigned int *)temp_buffer)[j] << "; ";
					}
					IO_output_file[tid] << endl;
					break;
				}
			}
			IO_output_file[tid] << "verified PPN = " << PPN << " cpn = " << PPN_mapping_info_temp.first << endl;
			free(temp_buffer);
			In_flight_tracker_lock.unlock();
#endif//VERIFY_LOADED_DATA_ASYNC
			In_flight_tracker_lock.lock();
			std::pair<CPN_TYPE, vector<CACHE_TID_TYPE> > PPN_mapping_info = In_flight_page_request_tracker[PPN];
			In_flight_tracker_lock.unlock();

			cache_update_lock.lock();
			if (CPN_to_PPN_map[PPN_mapping_info.first]==0)
			{
#if(GTL_TRANSLATION_CHECKING == 1)
				IO_output_file[tid] << "coming to update cpn_to_ppn " << PPN_mapping_info.first << " " << PPN << endl;
#endif//GTL_TRANSLATION_CHECKING
				if(PPN_to_CPN_map.find(PPN) == PPN_to_CPN_map.end()) //this PPN and CPN is not in the map
				{
					PPN_to_CPN_map[PPN]=PPN_mapping_info.first;
					assert( ( PPN_mapping_info.first >= 0) && (PPN_mapping_info.first <= (MaxCacheSize / SSD_PAGE_SIZE)) );
					CPN_to_PPN_map[PPN_mapping_info.first]=PPN;
#if(GTL_TRANSLATION_CHECKING == 1)
					IO_output_file[tid] << "GTL_TRANSLATION_CHECKING: IOThread " << " updating cpn_to_ppn " << PPN_mapping_info.first << " " << PPN << endl;
#endif//GTL_TRANSLATION_CHECKING
				}
			}
			else {
				IO_output_file[tid] << "Assert IOThread: " << " Overwriting a page in use" << endl;
				IO_output_file[tid] << "Assert IOThread: " << " " <<PPN_mapping_info.first << " " << CPN_to_PPN_map[PPN_mapping_info.first] << " " << PPN << endl;
				assert(0);
			}
			cache_update_lock.unlock();
			In_flight_tracker_lock.lock();
			PPN_mapping_info = In_flight_page_request_tracker[PPN];
#if(VERIFY_LOADED_DATA_ASYNC == 1)
			if(PPN == 1286620)
			{
				IO_output_file[tid] << "in io" << endl;
				for(unsigned int loop_it = 0; loop_it < (SSD_PAGE_SIZE/sizeof(unsigned int)); loop_it++)
				{
					IO_output_file[tid] << loop_it << " = " << ((unsigned int *)(Host_Cache[PPN_mapping_info.first].data))[loop_it] << "; ";
				}
				IO_output_file[tid] << endl;
			}
#endif//VERIFY_LOADED_DATA_ASYNC


			for(unsigned int i = 0; i < (PPN_mapping_info.second).size(); i++)
			{
				assert(((PPN_mapping_info.second)[i] < NumOfCacheThreads) && ( (PPN_mapping_info.second)[i] >= 0) );
				IO_response_queue_lock[(PPN_mapping_info.second)[i]].lock();
				IO_response_queue[ (PPN_mapping_info.second)[i] ].push(make_pair(PPN, PPN_mapping_info.first) );
				IO_response_queue_lock[(PPN_mapping_info.second)[i]].unlock();
				assert( ( PPN_mapping_info.first >= 0) && (PPN_mapping_info.first <= (MaxCacheSize / SSD_PAGE_SIZE)) );
				assert((PPN >= 0) && (PPN <= GraphLPN));
#if(GTL_TRANSLATION_CHECKING == 1)
				IO_output_file[tid] << "pushing to IO response queue tid = " << (PPN_mapping_info.second)[i] << " PPN = " << PPN << " CPN = " << PPN_mapping_info.first << endl;
#endif//GTL_TRANSLATION_CHECKING
			}
			In_flight_page_request_tracker.erase(PPN);
			In_flight_tracker_lock.unlock();
			io_currentEvents--;
#if(GTL_TRANSLATION_CHECKING == 1)
			IO_output_file[tid] << "io_currentEvents = " << io_currentEvents << endl;
#endif//GTL_TRANSLATION_CHECKING
		}
	}
}

void SSDInterface::ReadFromSSDNToBuffer(unsigned int SLBA, unsigned int numBlocks, void *buffer, IO_TID_TYPE tid, aio_context_t &ioctx)
{
	Increment_num_of_read_IO_pages(SLBA, numBlocks);
//	int64_t StartingIndex = (SLBA - (StartingGraphLPN * (SSD_PAGE_SIZE / PAGE_SIZE))) * PAGE_SIZE + sizeof(unsigned int);
#if(RunFromOpenSSD == 0)
	int64_t StartingIndex = ((int64_t)(SLBA - (StartingGraphLPN * (SSD_PAGE_SIZE / PAGE_SIZE)))) * PAGE_SIZE;
#else
	int64_t StartingIndex = ((int64_t)SLBA) * PAGE_SIZE;
#endif//RunFromOpenSSD
	struct iocb iocb1;// = {0};//do we need to allocate this?
	memset(&iocb1, 0, sizeof(iocb1));
	iocb1.aio_data       = SLBA;
	iocb1.aio_fildes     = myFlashFile;
	iocb1.aio_lio_opcode = IOCB_CMD_PREAD;
	iocb1.aio_reqprio    = 0;
	iocb1.aio_buf        = (uint64_t)buffer;
	iocb1.aio_nbytes     = numBlocks * PAGE_SIZE;
	iocb1.aio_offset     = StartingIndex;
//	iocb1.aio_nbytes     = 512;
//	iocb1.aio_offset     = 0;

	struct iocb *iocb_ptrs[1] = { &iocb1 };
	// submit operations
	int ret = io_submit(ioctx, 1, iocb_ptrs);
	if (ret < 0) {
		perror("io_submit");
		exit(1);
	} else if (ret != 1) {
		perror("io_submit: unhandled partial success");
		exit(1);
	}
//	IO_output_file[tid] << "SLBA = " << SLBA << " " << 
#if(GTL_TRANSLATION_CHECKING == 1)
	IO_output_file[tid] << "data = " << iocb1.aio_data << " filedes = " << iocb1.aio_fildes << " lio_opcode = " << iocb1.aio_lio_opcode << " reqprio = " << iocb1.aio_reqprio << " buf = " << iocb1.aio_buf << " nbytes = " << iocb1.aio_nbytes << " offset = " << iocb1.aio_offset << endl; 
#endif//GTL_TRANSLATION_CHECKING
}
#else//UseAsynchronousIO
void SSDInterface::IO_manager_tid(IO_TID_TYPE tid)
{
#if(GTL_TRANSLATION_CHECKING == 1)
	IO_output_file[tid] << "GTL_TRANSLATION_CHECKING: IOThread id = " << tid << endl;
//	return;
#endif
	while(1)
	{
		cache_to_IO_request_queue_lock.lock();
		if(!cache_to_IO_request_queue.empty())
		{
			unsigned int PPN = cache_to_IO_request_queue.front();
			assert((PPN >= 0) && (PPN <= GraphLPN));
			cache_to_IO_request_queue.pop();
			cache_to_IO_request_queue_lock.unlock();
			vector <PPN_TYPE> PPN_list;
			PPN_list.push_back(PPN);
			stat_collect_channel_access_statistics(PPN_list);
			PPN_list.clear();

#if(GTL_TRANSLATION_CHECKING == 1)
			IO_output_file[tid] << "popped PPN = " << PPN << endl;
#endif//GTL_TRANSLATION_CHECKING
			In_flight_tracker_lock.lock();
			std::pair<CPN_TYPE, vector<CACHE_TID_TYPE> > PPN_mapping_info = In_flight_page_request_tracker[PPN];//Need to pass pointer for the list?
			//It copies the vector, checked it
			In_flight_tracker_lock.unlock();
#if(GTL_TRANSLATION_CHECKING == 1)
			IO_output_file[tid] << "popped CPN = " << PPN_mapping_info.first << endl;
#endif//GTL_TRANSLATION_CHECKING
			ReadFromSSDNToBuffer(PPN * (SSD_PAGE_SIZE / PAGE_SIZE), SSD_PAGE_SIZE/PAGE_SIZE, (void * )Host_Cache[PPN_mapping_info.first].data, tid);//Using PPN or LBA?
			cache_update_lock.lock();
			if (CPN_to_PPN_map[PPN_mapping_info.first]==0)
			{
#if(GTL_TRANSLATION_CHECKING == 1)
				IO_output_file[tid] << "coming to update cpn_to_ppn " << PPN_mapping_info.first << " " << PPN << endl;
#endif//GTL_TRANSLATION_CHECKING
				if(PPN_to_CPN_map.find(PPN) == PPN_to_CPN_map.end()) //this PPN and CPN is not in the map
				{
					PPN_to_CPN_map[PPN]=PPN_mapping_info.first;
					assert( ( PPN_mapping_info.first >= 0) && (PPN_mapping_info.first <= (MaxCacheSize / SSD_PAGE_SIZE)) );
					CPN_to_PPN_map[PPN_mapping_info.first]=PPN;
#if(GTL_TRANSLATION_CHECKING == 1)
					IO_output_file[tid] << "GTL_TRANSLATION_CHECKING: IOThread " << " updating cpn_to_ppn " << PPN_mapping_info.first << " " << PPN << endl;
#endif//GTL_TRANSLATION_CHECKING
				}
			}
			else {
				IO_output_file[tid] << "Assert IOThread: " << " Overwriting a page in use" << endl;
				IO_output_file[tid] << "Assert IOThread: " << " " <<PPN_mapping_info.first << " " << CPN_to_PPN_map[PPN_mapping_info.first] << " " << PPN << endl;
				assert(0);
			}
			cache_update_lock.unlock();
			In_flight_tracker_lock.lock();
			PPN_mapping_info = In_flight_page_request_tracker[PPN];
			for(unsigned int i = 0; i < (PPN_mapping_info.second).size(); i++)
			{
				assert(((PPN_mapping_info.second)[i] < NumOfCacheThreads) && ( (PPN_mapping_info.second)[i] >= 0) );
				IO_response_queue_lock[(PPN_mapping_info.second)[i]].lock();
				IO_response_queue[ (PPN_mapping_info.second)[i] ].push(make_pair(PPN, PPN_mapping_info.first) );
				IO_response_queue_lock[(PPN_mapping_info.second)[i]].unlock();
				assert( ( PPN_mapping_info.first >= 0) && (PPN_mapping_info.first <= (MaxCacheSize / SSD_PAGE_SIZE)) );
				assert((PPN >= 0) && (PPN <= GraphLPN));
#if(GTL_TRANSLATION_CHECKING == 1)
				IO_output_file[tid] << "pushing to IO response queue tid = " << (PPN_mapping_info.second)[i] << " PPN = " << PPN << " CPN = " << PPN_mapping_info.first << endl;
#endif//GTL_TRANSLATION_CHECKING
			}
			In_flight_page_request_tracker.erase(PPN);
			In_flight_tracker_lock.unlock();
		}
		else
		{
			cache_to_IO_request_queue_lock.unlock();
		}
	}
}

void SSDInterface::ReadFromSSDNToBuffer(unsigned int SLBA, unsigned int numBlocks, void *buffer, IO_TID_TYPE tid)
{
	Increment_num_of_read_IO_pages(SLBA, numBlocks);
#if(RunFromOpenSSD == 1)
	struct nvme_user_io io;
	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)buffer;
	io.slba = SLBA;
	io.nblocks = numBlocks - 1;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	//              cout << io.slba << " " << io.nblocks << endl;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;

	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
#else
	unsigned long long int StartingIndex = ((unsigned long long int)(SLBA - (StartingGraphLPN * (SSD_PAGE_SIZE / PAGE_SIZE)))) * PAGE_SIZE + sizeof(unsigned int);
	myFlashFile[tid].seekg(StartingIndex, myFlashFile[tid].beg);//does seekg handle 64bit ones?
	myFlashFile[tid].read((char *)buffer, numBlocks * PAGE_SIZE);
#endif//RunFromOpenSSD
	Decrement_average_parallelism();
}
#endif//UseAsynchronousIO
