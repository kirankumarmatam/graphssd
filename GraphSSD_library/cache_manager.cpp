#include "graphFiltering.h"

unsigned int NumOfCacheThreads;

extern CacheBlock* Host_Cache;

#if(UseLockFreeQueues == 0)
mutex App_to_cache_request_queue_lock;
queue<APP_REQUEST_TYPE> App_to_cache_request_queue;

unsigned int App_to_cache_request_queue_max_size;
#else //UseLockFreeQueues
boost::lockfree::queue<APP_REQUEST_TYPE> App_to_cache_request_queue(1000);
#endif//UseLockFreeQueues
unsigned int cache_to_IO_request_queue_max_size,IO_response_queue_max_size;
unsigned int Cache_response_queue_max_size;
map<PPN_TYPE, pair< CPN_TYPE, vector<CACHE_TID_TYPE> > > In_flight_page_request_tracker;
mutex In_flight_tracker_lock;

vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
vector< mutex_wrapper > Cache_response_queue_lock;
vector< queue< pair<PPN_TYPE,CPN_TYPE> > > IO_response_queue;
vector< mutex_wrapper > IO_response_queue_lock;

extern queue<PPN_TYPE> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;

extern vector< shared_mutex_wrapper > cache_page_lock;
vector<uint64_t> Cache_access_count;

#if(GTL_TRANSLATION_CHECKING == 1)
extern PPN_TYPE *CPN_to_PPN_map;
extern map<PPN_TYPE,CPN_TYPE> PPN_to_CPN_map;
extern mutex cache_update_lock;
#endif//GTL_TRANSLATION_CHECKING
#if(MEASURE_TIME == 1)
extern mutex Cache_stat_lock;
extern unsigned int cache_hits, cache_misses;
extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, bool > > > Cache_response_queue_time;
#endif//MEASURE_TIME

extern vector<ofstream> cache_output_file;

extern unsigned int MaxCacheSize;
extern unsigned int GraphLPN;

void SSDInterface::cache_manager()
{
	printf("Cache manager: entering here\n");
	#pragma omp parallel num_threads(NumOfCacheThreads)
	{
		CACHE_TID_TYPE tid = omp_get_thread_num();
		cache_manager_tid(tid);
	}
}

void SSDInterface::cache_manager_tid(CACHE_TID_TYPE cache_tid)
{
	map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, APP_REQUEST_TYPE> request_identification_to_request_map;
map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, unsigned int> request_identification_to_number_of_need_pages_map;
map<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE>, vector<EDGE_TYPE>* > request_identification_to_adjlist_map;

map<PPN_TYPE, vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > > PPN_request_identification_mapper;


#if(GTL_TRANSLATION_CHECKING == 1)
	cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread id = " << cache_tid << endl;
#endif//GTL_TRANSLATION_CHECKING
	unsigned int IO_response_queue_reserved_size=0;
	APP_REQUEST_TYPE appRequest;
	queue<PPN_TYPE> PPN_to_push_to_IO;
	pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> apptid_reqid_pair;
	bool yeild = 0;
	while(1)
	{
#if(UseLockFreeQueues == 0)
		App_to_cache_request_queue_lock.lock();
		if(!App_to_cache_request_queue.empty())
		{
			appRequest = App_to_cache_request_queue.front();
			App_to_cache_request_queue.pop();
			App_to_cache_request_queue_lock.unlock();
#else//UseLockFreeQueues
			if(App_to_cache_request_queue.pop(appRequest))
			{
#endif//UseLockFreeQueues
				Cache_access_count[cache_tid]++;
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "Popped appRequest vertex_id = " << appRequest.vertex_id << " tid = " << appRequest.tid << " request_id = " << appRequest.request_id << endl;
#endif//
			vector <PPN_TYPE> PPN_list;
			getAdjEdgeList(appRequest.vertex_id, PPN_list,cache_tid);
//			stat_collect_channel_access_statistics(PPN_list);
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cache(thread): vid = " << appRequest.vertex_id << " PPN size = " << PPN_list.size() << endl;
			for (unsigned int i=0; i<PPN_list.size(); i++)
			{
				cache_output_file[cache_tid] << PPN_list[i] << " ";
			}
			cache_output_file[cache_tid] << endl;
			cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cache(thread): end" << endl;
#endif//GTL_TRANSLATION_CHECKING
			unsigned int number_of_need_pages=PPN_list.size();
			assert(number_of_need_pages != 0);
			vector<EDGE_TYPE> *adjacent_vertices_ptr=new vector<EDGE_TYPE>;
			assert(adjacent_vertices_ptr != NULL);
			CPN_TYPE CPN;
			bool cache_hit;
			for (unsigned int i=0; i<PPN_list.size(); i++)
			{
				PPN_TYPE PPN=PPN_list[i];
				assert((PPN > 0) && (PPN <= GraphLPN));
#if(GTL_TRANSLATION_CHECKING == 1)
				cache_output_file[cache_tid] << "checking PPN in cache " << PPN << endl;
#endif//GTL_TRANSLATION_CHECKING
				cache_hit=SSDInterface::HostCacheReadPPN(PPN,CPN);
#if(GTL_TRANSLATION_CHECKING == 1)
				cache_output_file[cache_tid] << "is PPN present in cache " << cache_hit << endl;
#endif//GTL_TRANSLATION_CHECKING
				if (cache_hit)
				{
					process_completionRequest((char *)(Host_Cache[CPN].data), adjacent_vertices_ptr, appRequest.vertex_id);
					cache_page_lock[CPN].unlock_shared(); //done using the page, release the lock
#if(GTL_TRANSLATION_CHECKING == 1)
					cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: " << " cache hit CPN = " << CPN << " PPN = " << PPN <<" vertex_id = " << appRequest.vertex_id << endl;
#endif//GTL_TRANSLATION_CHECKING
					assert( (number_of_need_pages > 0) && (number_of_need_pages <= PPN_list.size()) );
					number_of_need_pages--;
				}
				else
				{
					apptid_reqid_pair=make_pair(appRequest.tid,appRequest.request_id);
#if(GTL_TRANSLATION_CHECKING == 1)
					cache_output_file[cache_tid] << "cache miss PPN = " << PPN << " tid = " << appRequest.tid << " request id = " << appRequest.request_id << endl;
#endif//GTL_TRANSLATION_CHECKING
					if(PPN_request_identification_mapper.find(PPN) != PPN_request_identification_mapper.end())
					{
						PPN_request_identification_mapper[PPN].push_back(apptid_reqid_pair);
#if(GTL_TRANSLATION_CHECKING == 1)
						cache_output_file[cache_tid] << "p r id m PPN = " << PPN << " a_r_p tid = " << apptid_reqid_pair.first << " a_r_p r id = " << apptid_reqid_pair.second << endl;
#endif//GTL_TRANSLATION_CHECKING
					}
					else
					{
						vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> > tmp_vector_pair;
						tmp_vector_pair.push_back(apptid_reqid_pair);
						PPN_request_identification_mapper[PPN] = tmp_vector_pair;
#if(GTL_TRANSLATION_CHECKING == 1)
						cache_output_file[cache_tid] << "p r id m PPN = " << PPN << " a_r_p tid = " << apptid_reqid_pair.first << " a_r_p r id = " << apptid_reqid_pair.second << endl;
#endif//GTL_TRANSLATION_CHECKING

						In_flight_tracker_lock.lock();
						if(In_flight_page_request_tracker.find(PPN) != In_flight_page_request_tracker.end())
						{
#if(GTL_TRANSLATION_CHECKING == 1)
							cache_output_file[cache_tid] << "In_f_t hit PPN = " << PPN << " CPN = " << In_flight_page_request_tracker[PPN].first << endl;
#endif//GTL_TRANSLATION_CHECKING
							(In_flight_page_request_tracker[PPN].second).push_back(cache_tid);
							cache_page_lock[In_flight_page_request_tracker[PPN].first].lock_shared();// shared lock CPN, because the PPN is in the tracker, so the PPN must be shared locked by someone else
						}
						else
						{
							CPN_TYPE reserved_CPN;
							reserved_CPN=SSDInterface::HostCacheRequestFreePage();//get the shared lock of the CPN in this step
#if(GTL_TRANSLATION_CHECKING == 1)
							cache_output_file[cache_tid] << "In_f_t miss PPN = " << PPN << " resvering cpn = " << reserved_CPN << endl;
#endif
							vector<CACHE_TID_TYPE> tmp_vector;
							tmp_vector.push_back(cache_tid);
							In_flight_page_request_tracker[PPN] = make_pair(reserved_CPN, tmp_vector);
#if(GTL_TRANSLATION_CHECKING == 1)
							cache_output_file[cache_tid] << "p PPN " << PPN << " into the cache's pushing queue" << endl;
							cache_output_file[cache_tid] << "in In_f_t PPN = " << PPN << " CPN = " << In_flight_page_request_tracker[PPN].first << " cache tid size = " << (In_flight_page_request_tracker[PPN].second).size() << endl;
							cache_update_lock.lock();
							assert(CPN_to_PPN_map[reserved_CPN] == 0);
							cache_update_lock.unlock();
#endif//
							PPN_to_push_to_IO.push(PPN);
						}
						In_flight_tracker_lock.unlock();
					}
				}
			}
			if (number_of_need_pages==0)
			{
#if(MEASURE_TIME == 1)
				Cache_stat_lock.lock();
				cache_hits += 1;
				Cache_stat_lock.unlock();
#endif//MEASURE_TIME
				Cache_response_queue_lock[appRequest.tid].lock();
#if(GTL_TRANSLATION_CHECKING == 1)
				cache_output_file[cache_tid] << "pushing request to app tid = " << appRequest.tid << " request_id = " << appRequest.request_id << " size = " << adjacent_vertices_ptr->size()   << endl;
#endif//GTL_TRANSLATION_CHECKING
#if(MEASURE_TIME == 1)
				Cache_response_queue_time[appRequest.tid].push(make_pair(appRequest.request_id, 1));
#endif//MEASURE_TIME
				Cache_response_queue[appRequest.tid].push(make_pair(appRequest.request_id,adjacent_vertices_ptr));
				Cache_response_queue_lock[appRequest.tid].unlock();
			}
			else
			{
#if(MEASURE_TIME == 1)
				Cache_stat_lock.lock();
				cache_misses += 1;
				Cache_stat_lock.unlock();
#endif//MEASURE_TIME
				request_identification_to_request_map[apptid_reqid_pair]=appRequest;
				request_identification_to_number_of_need_pages_map[apptid_reqid_pair]=number_of_need_pages;
				request_identification_to_adjlist_map[apptid_reqid_pair]=adjacent_vertices_ptr;
#if(GTL_TRANSLATION_CHECKING == 1)
				cache_output_file[cache_tid] << "apptid_reqid_pair tid = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " appRequest tid = " << appRequest.tid << " appRequest vertex id = " << appRequest.vertex_id << " appRequest request_id " << appRequest.request_id << " number of needed pages = " << number_of_need_pages << " adjacent_vertices_ptr "<< adjacent_vertices_ptr->size()  << endl;
#endif//GTL_TRANSLATION_CHECKING
			}
			yeild = 0;
		}
		else
		{
#if(UseLockFreeQueues == 0)
			App_to_cache_request_queue_lock.unlock();
#else//UseLockFreeQueues
#endif//UseLockFreeQueues
			yeild = 1;
		}
		while(!PPN_to_push_to_IO.empty())
		{
			if(IO_response_queue_reserved_size==IO_response_queue_max_size)
			{
#if(GTL_TRANSLATION_CHECKING == 1)
//				cache_output_file[cache_tid] << "breaking as IO_response_queue_max_size PPN = " << PPN_to_push_to_IO.front() << " CPN = " << In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first << endl;
#endif//GTL_TRANSLATION_CHECKING
				yeild = 1;
				break;
			}
			else
			{
				cache_to_IO_request_queue_lock.lock();
				if(cache_to_IO_request_queue.size()==cache_to_IO_request_queue_max_size)
				{
#if(GTL_TRANSLATION_CHECKING == 1)
//					cache_output_file[cache_tid] << "breaking in cache_to_IO_request_queue_max_size " << PPN_to_push_to_IO.front() << " CPN = " << In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first << endl;
#endif//GTL_TRANSLATION_CHECKING
					cache_to_IO_request_queue_lock.unlock();
					yeild = 1;
					break;
				}
				else //only the request and response queue both have place, we push the request
				{
					IO_response_queue_reserved_size++;
#if(GTL_TRANSLATION_CHECKING == 1)
/*					In_flight_tracker_lock.lock();
					cache_update_lock.lock();
					cache_output_file[cache_tid] << "pushing PPN = " << PPN_to_push_to_IO.front() << " CPN is = " << In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first << " cpn to ppn map = " << CPN_to_PPN_map[In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first] << endl;
//					assert(CPN_to_PPN_map[In_flight_page_request_tracker[PPN_to_push_to_IO.front()].first] == 0);
					cache_update_lock.unlock();
					In_flight_tracker_lock.unlock();*/
#endif//GTL_TRANSLATION_CHECKING
					cache_to_IO_request_queue.push(PPN_to_push_to_IO.front());
					PPN_to_push_to_IO.pop();
					cache_to_IO_request_queue_lock.unlock();
					yeild = 0;
				}
			}
		}
#if(GTL_TRANSLATION_CHECKING == 1)
		//		cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread: "<< " before IO response lock " << endl;
#endif//GTL_TRANSLATION_CHECKING
		//We don't want to acquire lock for every read, as we check the read regularly, or else we need a separate thread which responds to application, may be this is better, but there also one needs to snoop
		IO_response_queue_lock[cache_tid].lock();
#if(GTL_TRANSLATION_CHECKING == 1)
		//		cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread: " << " after IO response lock " <<  endl;
#endif//GTL_TRANSLATION_CHECKING
		if (!IO_response_queue[cache_tid].empty())
		{
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread: id is " << cache_tid << " IO response queue not empty, size is " << IO_response_queue[cache_tid].size() <<  endl;
#endif//GTL_TRANSLATION_CHECKING
			pair<PPN_TYPE, CPN_TYPE> response=IO_response_queue[cache_tid].front();//Pop request from the queue
			IO_response_queue[cache_tid].pop();
			IO_response_queue_reserved_size--;
			IO_response_queue_lock[cache_tid].unlock();
#if(VERIFY_LOADED_DATA_ASYNC == 1)
				cache_output_file[cache_tid] << "after response " << response.first <<  endl;
				if(response.first == 1286620)
				{
					cache_output_file[cache_tid] << "after IO pop" << endl;
					for(unsigned int loop_it = 0; loop_it < (SSD_PAGE_SIZE/sizeof(unsigned int)); loop_it++)
					{
						cache_output_file[cache_tid] << loop_it << " = " << ((unsigned int *)(Host_Cache[response.second].data))[loop_it] << "; ";
					}
					cache_output_file[cache_tid] << endl;
				}
#endif//VERIFY_LOADED_DATA_ASYNC

#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "From IO queue, PPN = " << response.first << " CPN = " << response.second << endl;
#endif//GTL_TRANSLATION_CHECKING

			vector<pair<APP_TID_TYPE,REQUEST_UNIQUE_ID_TYPE> >APP_req_id_queue=PPN_request_identification_mapper[response.first];
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "PPN to request mapping " << response.first << " " << APP_req_id_queue.size() << endl;
#endif//GTL_TRANSLATION_CHECKING
			assert(PPN_request_identification_mapper.find(response.first) != PPN_request_identification_mapper.end());
			PPN_request_identification_mapper.erase(response.first);
#if(GTL_TRANSLATION_CHECKING == 1)
			cache_output_file[cache_tid] << "PPN to request mapping erased" << endl;
#endif//GTL_TRANSLATION_CHECKING
			while (!APP_req_id_queue.empty()) //process all the app requests related to this PPN-CPN pair
			{
				apptid_reqid_pair = APP_req_id_queue.back();
				APP_req_id_queue.pop_back();
				assert(request_identification_to_number_of_need_pages_map[apptid_reqid_pair] > 0);
				request_identification_to_number_of_need_pages_map[apptid_reqid_pair]--;
#if(GTL_TRANSLATION_CHECKING == 1)
				cache_output_file[cache_tid] << "appRequest tid = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " no. of needed pages = " << request_identification_to_number_of_need_pages_map[apptid_reqid_pair] << " vertex_id = " << request_identification_to_request_map[apptid_reqid_pair].vertex_id << endl;
#endif//GTL_TRANSLATION_CHECKING
#if(VERIFY_LOADED_DATA_ASYNC == 1)
				if(response.first == 1286620)
				{
					cache_output_file[cache_tid] << "after req pop" << endl;
					for(unsigned int loop_it = 0; loop_it < (SSD_PAGE_SIZE/sizeof(unsigned int)); loop_it++)
					{
						cache_output_file[cache_tid] << loop_it << " = " << ((unsigned int *)(Host_Cache[response.second].data))[loop_it] << "; ";
					}
					cache_output_file[cache_tid] << endl;
				}
#endif//VERIFY_LOADED_DATA_ASYNC
#if(GTL_TRANSLATION_CHECKING == 1)
				cache_output_file[cache_tid] << "ppn = " << response.first << " vertex id = " << request_identification_to_request_map[apptid_reqid_pair].vertex_id << " lV = " << ((unsigned int *)Host_Cache[response.second].data)[(SSD_PAGE_SIZE / sizeof(unsigned int)) - 2] << endl;
#endif//GTL_TRANSLATION_CHECKING
				process_completionRequest((char *)Host_Cache[response.second].data, request_identification_to_adjlist_map[apptid_reqid_pair], request_identification_to_request_map[apptid_reqid_pair].vertex_id);

				if (request_identification_to_number_of_need_pages_map[apptid_reqid_pair]==0)
				{
					Cache_response_queue_lock[apptid_reqid_pair.first].lock();
					Cache_response_queue[apptid_reqid_pair.first].push(make_pair(apptid_reqid_pair.second, request_identification_to_adjlist_map[apptid_reqid_pair]));
#if(MEASURE_TIME == 1)
					Cache_response_queue_time[apptid_reqid_pair.first].push(make_pair(apptid_reqid_pair.second, 0));
#endif//MEASURE_TIME
					Cache_response_queue_lock[apptid_reqid_pair.first].unlock();
#if(GTL_TRANSLATION_CHECKING)
					cache_output_file[cache_tid] << "GTL_TRANSLATION_CHECKING: cacheThread: " << " tid = " << apptid_reqid_pair.first << " request id = " << apptid_reqid_pair.second << " adjacency list size =  " << request_identification_to_adjlist_map[apptid_reqid_pair]->size()<< endl;
#endif//GTL_TRANSLATION_CHECKING
					request_identification_to_request_map.erase(apptid_reqid_pair);
					request_identification_to_number_of_need_pages_map.erase(apptid_reqid_pair);
					request_identification_to_adjlist_map.erase(apptid_reqid_pair);
				}
			}
			cache_page_lock[response.second].unlock_shared(); //done using this page, release the lock
			yeild = 0;
		}
		else
		{
			IO_response_queue_lock[cache_tid].unlock();
			yeild = 1;
		}
		//if it did not enter both the queue then consider sleeping?
//		if(yeild == 1)
//			sched_yield();
	}
}
