#include "graphFiltering.h"

CacheBlock *Host_Cache; //global
bool *Cache_Dirty;//global

unsigned int used_cache_size,free_cache_size; //in bytes
map<PPN_TYPE,CPN_TYPE> PPN_to_CPN_map;
PPN_TYPE *CPN_to_PPN_map;
mutex cache_update_lock;
vector< shared_mutex_wrapper > cache_page_lock;
list<CPN_TYPE> freeCPN,usedCPN;

extern vector<ofstream> cache_output_file;

extern unsigned int MaxCacheSize;
extern unsigned int GraphLPN;

int SSDInterface::InitializeHostCache(unsigned int CacheSize)//CacheSize in bytes, create a double link list of free pages and used pages, and reserve all cache space
{
	if(posix_memalign((void **)&Host_Cache, SSD_PAGE_SIZE, CacheSize/SSD_PAGE_SIZE*sizeof(CacheBlock))) //need to be 4K aligned
	{
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		exit(0);
	}
	Cache_Dirty=(bool *)calloc(CacheSize/SSD_PAGE_SIZE,sizeof(bool));
	assert(Cache_Dirty != NULL);
	CPN_to_PPN_map=(PPN_TYPE *)calloc(CacheSize/SSD_PAGE_SIZE,sizeof(PPN_TYPE));
	assert(CPN_to_PPN_map != NULL);
	used_cache_size=0;
	free_cache_size=(CacheSize / SSD_PAGE_SIZE) * SSD_PAGE_SIZE;
	freeCPN.clear();
	usedCPN.clear();
	for (unsigned int i=0; i<CacheSize/SSD_PAGE_SIZE; i++)
	{
		freeCPN.push_back(i);
		shared_mutex_wrapper tmp_mtx;
		cache_page_lock.push_back(tmp_mtx);
	}
	return 0;
}

bool SSDInterface::HostCacheReadPPN(PPN_TYPE PPN,CPN_TYPE &CPN)
{
	cache_update_lock.lock();
	if(PPN_to_CPN_map.find(PPN) == PPN_to_CPN_map.end())
	{
		cache_update_lock.unlock();
		return false;
	}
	else
	{
#if(GTL_TRANSLATION_CHECKING == 1)
//		cout << "In LRU cache: hit " << " PPN = " << PPN << " CPN " << CPN  << endl;
#endif//GTL_TRANSLATION_CHECKING
		CPN = PPN_to_CPN_map[PPN];
		assert( (CPN >= 0) && (CPN <= (MaxCacheSize / SSD_PAGE_SIZE)) );
		bool lock_page=cache_page_lock[CPN].try_lock_shared();
#if(GTL_TRANSLATION_CHECKING == 1)
//		cout << "In LRU cache: hit " << " PPN = " << PPN << " CPN " << CPN  << " is locked = " << lock_page <<  endl;
#endif//GTL_TRANSLATION_CHECKING
		if (lock_page)
		{
			LRU_cache_read(CPN);
			cache_update_lock.unlock();
			return true;
		}
		else
		{
			cache_update_lock.unlock();
			return false;
		}
	}
}

CPN_TYPE SSDInterface::HostCacheRequestFreePage()
{
	cache_update_lock.lock();
	CPN_TYPE CPN;
	PPN_TYPE PPN;
	if (free_cache_size==0)
	{
		CPN=LRU_cache_evict();
		assert( (CPN >= 0) && (CPN <= (MaxCacheSize / SSD_PAGE_SIZE)) );
		if (Cache_Dirty[CPN])
		{
			//TODO: write back this page
		}
//		cout << "In LRU cache: pushing to freeCPN CPN = " << CPN <<endl;
		freeCPN.push_back(CPN);
		free_cache_size+=SSD_PAGE_SIZE;
		assert( (free_cache_size >= 0) && (free_cache_size <= MaxCacheSize) );
	}
	CPN=freeCPN.front();
	assert( (CPN >= 0) && (CPN <= (MaxCacheSize / SSD_PAGE_SIZE)) );
//	cout << "In LRU cache: free CPN = " << CPN << endl;
	freeCPN.pop_front();
//	cout << "In LRU cache: popped from freeCPN front" << endl;
	free_cache_size-=SSD_PAGE_SIZE;
	assert( (free_cache_size >= 0) && (free_cache_size <= MaxCacheSize) );
	cache_page_lock[CPN].lock_shared();// shared lock this page
	//add this block to the front of the used list, we don't map PPN to CPN
	usedCPN.push_front(CPN);
	//clean the old map
	PPN=CPN_to_PPN_map[CPN];
	assert((PPN >= 0) && (PPN <= GraphLPN));
	if (PPN!=0)
	{
		PPN_to_CPN_map.erase(PPN);
		CPN_to_PPN_map[CPN]=0;
	}
	//call the cache policy (for LRU, it will add the LRU map only)
	LRU_cache_read(CPN);
//	cout << "resverd CPN :" << CPN << endl;
//	cout << "CPN to PPN map :" << CPN_to_PPN_map[CPN] << endl;
	cache_update_lock.unlock();
	return CPN;
}

//Need to take care of the cases where number of adjacent vertices for a vertex are zero
