#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <linux/aio_abi.h>
#include <sys/syscall.h> // syscall numbers
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <inttypes.h>
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <limits>
#include <time.h>
#include <queue>
#include <set>
//#include <boost/filesystem.hpp>
#include <experimental/filesystem>
//using namespace boost::filesystem;
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/atomic.hpp>
namespace fs = std::experimental::filesystem::v1;
#include <cstddef>
#include <thread>
#include <atomic>
#include <mutex>
#include <shared_mutex>
#include "testing.h"
#include "utility.h"
using namespace std;

#define RunFromOpenSSD 1
#define UseFTL 3
#define useCache 1
#define SLBA_FLUSH_BUFFER 128640775
#define STORE_LARGE_GRAPH 1
#define UseCache_GetPagesFromSSD 1
#define LOAD_GRAPH_FROM_FILE 1
#define CHECK_ERROR ((!LOAD_GRAPH_FROM_FILE) && 1)
#define Update_edges_from_file 1
#define Update_split_half 1
#define UseAsynchronousIO 1
#define UseLockFreeQueues 0
#define CALCULATE_DEGREE 1

#define AuxMemoryInStorage 0
#define AUXLBA  25743739
#define AuxDataValueType bool
//#define AuxDataValueType unsigned int

#define BufferInStorage 1
#define BUFFERLBA 28365179

#define IsUndirectedGraph 1
#define ADD_UPDATES_TO_BUFFER 0
#define ADD_FROM_MULTIPLE_FILES 1
#define Update_using_buffer 1
#define ADD_ALL_VERTICES_TO_BUFFER 1
#define ADDED_WITHOUT_TESTING 0

typedef unsigned long long int REQUEST_UNIQUE_ID_TYPE;
typedef unsigned int APP_TID_TYPE, CACHE_TID_TYPE, IO_TID_TYPE;
typedef unsigned int PPN_TYPE;
typedef unsigned int VERTEX_TYPE;
typedef unsigned int EDGE_TYPE;
typedef uint64_t EdgeIndexType;
//typedef unsigned int EdgeIndexType;
typedef unsigned int CPN_TYPE; //cache page number

//page size in bytes
#define PAGE_SIZE 4096
#define SSD_PAGE_SIZE 16384

typedef struct HostCacheBlock
{
	char data[SSD_PAGE_SIZE];
} CacheBlock;

typedef struct Application_request_type
{
	VERTEX_TYPE vertex_id;
	APP_TID_TYPE tid;
	REQUEST_UNIQUE_ID_TYPE request_id;
} APP_REQUEST_TYPE;

struct mutex_wrapper : std::mutex
{
	  mutex_wrapper() = default;
	    mutex_wrapper(mutex_wrapper const&) noexcept : std::mutex() {}
	      bool operator==(mutex_wrapper const&other) noexcept { return this==&other; }
};

struct shared_mutex_wrapper : std::shared_mutex
{
	  shared_mutex_wrapper() = default;
	    shared_mutex_wrapper(shared_mutex_wrapper const&) noexcept : std::shared_mutex() {}
	      bool operator==(shared_mutex_wrapper const&other) noexcept { return this==&other; }
};

class SSDInterface
{
	public:
		void InitializeGraphDataStructures(unsigned int NumNodes);
		void FinalizeGraphDataStructures();
		void WriteGraphAndDelete();
		int store_graph(int argc, char **argv);
		void ClearQueues();
		void cache_readAdjEdgeList(unsigned int vID, vector<unsigned int> &EdgeList);
		void AddVertexWeightUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights);
		void AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList);
#if(Update_edges_from_file == 0)
		void AddEdge(unsigned int numOfRandomEdges);
#elif(Update_edges_from_file == 1)
#if(Update_using_buffer == 0)
		void AddEdge(const char *addEdgeFilePtr);
#elif(Update_using_buffer == 1)
		void AddEdge(std::vector<std::pair<unsigned int, unsigned int> > &MergeVector);
#endif//Update_using_buffer
#endif
		void AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList);
		void GetAdjListNoGTL(unsigned int vID, vector<unsigned int> &EdgeList);
		void printTime();
		void WriteToSSD(unsigned int j);
		void WriteToSSDN(unsigned int SLBA, unsigned int numBlocks);
		void ReadFromSSDN(unsigned int SLBA, unsigned int numBlocks);
#if(UseAsynchronousIO == 1)
		void ReadFromSSDNToBuffer(unsigned int SLBA, unsigned int numBlocks, void *buffer, IO_TID_TYPE tid, aio_context_t &ioctx);
#else
		void ReadFromSSDNToBuffer(unsigned int SLBA, unsigned int numBlocks, void *buffer, IO_TID_TYPE tid);
#endif//UseAsynchronousIO
		void AddVertexWeight(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights)
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				AddVertexWeightUseNvmeCommands(vID, EdgeList, EdgeWeights);
			}
		}

                map<unsigned int, vector<unsigned int> > vertexToAdjListMap;
		void *data_nvme_command;
		int fd;
#if(UseAsynchronousIO == 0)
#if(RunFromOpenSSD == 1)
		fstream myFlashFile;
#else//RunFromOpenSSD
		vector<fstream> myFlashFile;
#endif//RunFromOpenSSD
#else//UseAsynchronousIO
		int myFlashFile;
//		aio_context_t ioctx;
		unsigned io_maxevents;
#if(VERIFY_LOADED_DATA_ASYNC == 1)
		fstream myFlashFile_temp;
#endif//VERIFY_LOADED_DATA_ASYNC == 1
#endif//UseAsynchronousIO

		SSDInterface()
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
					fprintf(stderr, "cannot allocate io payload for data_wr\n");
				}
			}
		}

		unsigned int putAdjEdgeList(unsigned int vID, char *host_buffer, unsigned int numAdjVertices);
		void getAdjEdgeList(VERTEX_TYPE vertex_id, std::vector<PPN_TYPE> &PPN_list, CACHE_TID_TYPE tid);
		unsigned int bufferingPage_loadToSSD(char *host_buffer, unsigned int numOfElementsToLoad);
		unsigned int bufferingPage_loadToNAND(unsigned int getNumAdjVertices, unsigned int isEdge);
		unsigned int putAdjEdgeList_weight(unsigned int vID, char *host_buffer, unsigned int numAdjVertices);
		unsigned int GraphInitialize();
		unsigned int binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number);
		unsigned int IdentifyPosition(unsigned int vertexId);
		void CopyFunction_InPage(char * devAddr, vector<EDGE_TYPE> *adjacent_vertices_ptr, unsigned int vertexId);
		unsigned int findEdge_inBuffer(char * Test_variable_size_buffer, char * Test_variable_size_buffer_weight, unsigned int destEdge, unsigned int NumberOfAdjVertices);
		void process_completionRequest(char *src_address, vector<EDGE_TYPE> *adjacent_vertices_ptr, VERTEX_TYPE vertex_id);
		void process_completionRequest_write(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit);
		void GraphLRUBufWrite(unsigned int curSect, unsigned int Len, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int lowestVertexId);
#if(useCache == 1 && UseCache_GetPagesFromSSD == 1)
		bool GraphLRUBufRead(unsigned int sectorNumber);
		void FlushCaches();
#else
		void GraphLRUBufRead(unsigned int sectorNumber);
#endif
		unsigned int GraphFinalize();
		void Initialization(int argc, char *argv[]);
		void Finalize();
		void cache_manager();
		void cache_manager_tid(CACHE_TID_TYPE cache_tid);
		void IO_manager();
#if(UseAsynchronousIO == 1)
		void IO_manager_async_tid(IO_TID_TYPE tid);
#else//UseAsynchronousIO
		void IO_manager_tid(IO_TID_TYPE tid);
#endif//UseAsynchronousIO
		int InitializeHostCache(unsigned int CacheSize);
		bool HostCacheReadPPN(PPN_TYPE PPN,CPN_TYPE &CPN);
		CPN_TYPE HostCacheRequestFreePage();

		void LRU_cache_read(CPN_TYPE CPN);
		CPN_TYPE LRU_cache_evict();
		void application_program(int argc, char *argv[]);

		void start_application_time();
		void end_application_time();
		void Increment_num_of_read_IO_pages(unsigned int SLBA, unsigned int numBlocks);
		void Decrement_average_parallelism();
		void print_info();
		void application_print_info();
		void stat_initialize_channel_configuration();
		void stat_collect_channel_access_statistics(vector<PPN_TYPE> PPN_list);
		void stat_increment_iteration();
		void stat_print_channel_access_conflicts();
#if(CALCULATE_DEGREE == 1)
		void calculate_degree();
#endif//CALCULATE_DEGREE
};
