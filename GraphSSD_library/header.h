#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <cstring>
#include <map>
#include <vector>
using namespace std;
/*unsigned int putAdjEdgeList(unsigned int vID, char *host_buffer, unsigned int numAdjVertices);
unsigned int getAdjEdgeList(unsigned int vID, char *host_buffer);
unsigned int getEdgeWeight(unsigned int vID, char *host_buffer, unsigned int destEdge);
unsigned int getTwoHopAdjEdgeList(unsigned int vID, char *host_buffer);
unsigned int bufferingPage_loadToSSD(char *host_buffer, unsigned int numOfElementsToLoad);
unsigned int bufferingPage_loadToNAND(unsigned int getNumAdjVertices, unsigned int isEdge);
unsigned int putAdjEdgeList_weight(unsigned int vID, char *host_buffer, unsigned int numAdjVertices);
unsigned int GraphInitialize();
unsigned int getEdgesToSSDDram_initiateLoading(unsigned int gttVidIndex, unsigned int gttVidNumber, unsigned int isEdge);
unsigned int binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number);
unsigned int CopyFunction_InPage(char * devAddr, char * Test_variable_size_buffer, unsigned int currentTopPointer, unsigned int vertexId);
unsigned int findEdge_inBuffer(char * Test_variable_size_buffer, char * Test_variable_size_buffer_weight, unsigned int destEdge, unsigned int NumberOfAdjVertices);
void process_completionRequest(unsigned int chNo, unsigned int wayNo, unsigned int front, unsigned int isLruBufHit);
void process_completionRequest_write(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit);
void GraphLRUBufWrite(unsigned int curSect, unsigned int Len, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int lowestVertexId);
void GraphLRUBufRead(unsigned int sectorNumber);
unsigned int GraphFinalize();
void cache_readAdjEdgeList_page(unsigned int LPN, unsigned int Number, char **data_pointer);
*/
struct GTTPointer {
        unsigned int vertexId;
        unsigned int LPN;
};

struct VertexCompletionInformation {
        unsigned int vertexId;
	char * host_addr;
        unsigned int numberOfVerticesRemainingToFetch;
        unsigned int isEdge;
        unsigned int destEdge;
        unsigned int NumOfAdjVertices;
        unsigned int NumOfAdjVertices_weight;
        unsigned int flags;
        unsigned int hopNum;
	std::vector<unsigned int> PPN_list;
};

#define MAX_NUM_OF_VERTICES 10000000
#define MAX_CHANNEL_NUM 8
#define MAX_WAY_NUM 8
#define MAX_COMPLETION_QUEUE_ENTRY 8
#define CHANNEL_NUM                             8
#define WAY_NUM                                 8
#define DIE_NUM                                 (CHANNEL_NUM * WAY_NUM)

struct GTTTable1 {
        struct GTTPointer       GTTPointer1[MAX_NUM_OF_VERTICES];
};

struct GTTTable1Weight {
        struct GTTPointer       GTTPointer1[MAX_NUM_OF_VERTICES];
};

struct CompletionQueueEntry {
        char * devAddr;
        unsigned int destPageOffset;
        unsigned int srcPageOffset;
        unsigned int Number;
        unsigned int lowestVertexId;
        unsigned int LPN;
};

struct CompletionQueuePointer {
        unsigned int front;
        unsigned int rear;
};

struct CompletionQueues {
        struct CompletionQueueEntry completionQueueEntry[MAX_CHANNEL_NUM][MAX_WAY_NUM][MAX_COMPLETION_QUEUE_ENTRY];
};

struct CompletionQueuePtrs {
        struct CompletionQueuePointer completionQueuePointer[MAX_CHANNEL_NUM][MAX_WAY_NUM];
};
