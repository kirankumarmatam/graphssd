#include "graphFiltering.h"
extern unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
unsigned int number_of_application_response_queues;

extern unsigned int NumOfCacheThreads;

extern unsigned int Cache_response_queue_max_size;
#if(UseLockFreeQueues == 0)
extern unsigned int App_to_cache_request_queue_max_size;
extern mutex App_to_cache_request_queue_lock;
extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
#else//UseLockFreeQueues
extern boost::lockfree::queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
#endif//UseLockFreeQueues
extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern vector<uint64_t> Cache_access_count;

extern unsigned int cache_to_IO_request_queue_max_size,IO_response_queue_max_size;
extern queue<PPN_TYPE> cache_to_IO_request_queue;
extern mutex cache_to_IO_request_queue_lock;
extern vector< queue< pair<PPN_TYPE,CPN_TYPE> > > IO_response_queue;
extern vector< mutex_wrapper > IO_response_queue_lock;

unsigned int MaxCacheSize;

extern unsigned int NumOfIOThreads;

vector<ofstream> application_output_file;
vector<ofstream> cache_output_file;
vector<ofstream> IO_output_file;

#if(MEASURE_BINARY_SEARCH == 1)
extern vector<Timer> time_binary_search;
#endif//MEASURE_BINARY_SEARCH

void SSDInterface::Initialization(int argc, char *argv[])
{
	/*---------Application related ones---------*/
	NumOfRequestingThreads = 1;
	NumberOfResponseThreads = 1;
	number_of_application_response_queues = NumOfRequestingThreads;
#if(UseLockFreeQueues == 0)
	App_to_cache_request_queue_max_size = 1000;
#else//UseLockFreeQueues
//	App_to_cache_request_queue.reserve(1000);
#endif//UseLockFreeQueues
	Cache_response_queue.resize(number_of_application_response_queues);
	for (unsigned int i=0;i<number_of_application_response_queues;i++)
	{
		mutex_wrapper tmp_mtx;
		Cache_response_queue_lock.push_back(tmp_mtx);
	}
	Cache_response_queue_max_size = 1000;
	application_output_file.resize(NumOfRequestingThreads+NumberOfResponseThreads);
	string fileName;
	application_output_file.resize(NumOfRequestingThreads+NumberOfResponseThreads);
	for(unsigned int i = 0; i < (NumOfRequestingThreads+NumberOfResponseThreads); i++)
	{
		fileName = "/home/ossd/GraphSSD/Parallel_graphSSD/Output/application_"+to_string(i)+".txt";
		application_output_file[i].open(fileName, ios::trunc);
		assert(application_output_file[i].is_open());
	}
	/*---------Cache manager related ones---------*/
	NumOfCacheThreads = 3;
	cache_to_IO_request_queue_max_size = 1000;
	IO_response_queue.resize(NumOfCacheThreads);
	for (unsigned int i=0;i<NumOfCacheThreads;i++)
	{
		mutex_wrapper tmp_mtx;
		IO_response_queue_lock.push_back(tmp_mtx);
	}
	IO_response_queue_max_size = 1000;
	InitializeHostCache(MaxCacheSize);//store graph function should be called before this
	cache_output_file.resize(NumOfCacheThreads);
	for(unsigned int i = 0; i < (NumOfCacheThreads); i++)
	{
		fileName = "/home/ossd/GraphSSD/Parallel_graphSSD/Output/cache_"+to_string(i)+".txt";
		cache_output_file[i].open(fileName, ios::trunc);
		assert(cache_output_file[i].is_open());
	}
	Cache_access_count.resize(NumOfCacheThreads, 0);
#if(MEASURE_BINARY_SEARCH == 1)
	time_binary_search.resize(NumOfCacheThreads);
#endif//MEASURE_BINARY_SEARCH
/*---------I/O related ones---------*/
#if(UseAsynchronousIO == 1)
	NumOfIOThreads = 1;
#else//UseAsynchronousIO
	NumOfIOThreads = 1;
#endif//UseAsynchronousIO
	IO_output_file.resize(NumOfIOThreads);
	for(unsigned int i = 0; i < (NumOfIOThreads); i++)
	{
		fileName = "/home/ossd/GraphSSD/Parallel_graphSSD/Output/IO_"+to_string(i)+".txt";
		IO_output_file[i].open(fileName, ios::trunc);
		assert(IO_output_file[i].is_open());
	}
#if(UseAsynchronousIO == 1)
#if(RunFromOpenSSD == 0)
	fstream myFlashFile_dst;
	myFlashFile_dst.open("/Data/Benchmarks/SNAP/Temp/temp.txt", ios::out | ios::trunc);
	fstream myFlashFile_src;
	myFlashFile_src.open(argv[3], ios::in);
	unsigned int numOfSSDPages;
	myFlashFile_src.read((char *)(&numOfSSDPages), sizeof(unsigned int));
	cout << "numOfSSDPages = " << numOfSSDPages << endl;
	unsigned int MaxReadPages = ((32*4096)/SSD_PAGE_SIZE);
	char *temp_buffer = (char *)malloc(sizeof(char) * MaxReadPages * SSD_PAGE_SIZE);
	for(unsigned int i = 0; i < numOfSSDPages; i+= MaxReadPages)
	{
#if(MINIMAL_PRINT == 1)
		if(i % (MaxReadPages*10000) == 0)
			cout << "Stored " << i << " pages" <<endl;
#endif//MINIMAL_PRINT
		unsigned int PagesToRead = (i+MaxReadPages > numOfSSDPages) ? (numOfSSDPages - i) : MaxReadPages;
		myFlashFile_src.read((char *)temp_buffer, SSD_PAGE_SIZE*PagesToRead);
		myFlashFile_dst.write((char *)temp_buffer, SSD_PAGE_SIZE*PagesToRead);
	}
	myFlashFile_src.close();
	myFlashFile_dst.close();
	free(temp_buffer);
	
//	ioctx = 0;
	io_maxevents = 128;
	myFlashFile = open("/home/ossd/GraphSSD/Parallel_graphSSD/Output/Temp/temp.txt", O_RDONLY | O_DIRECT);
	if (myFlashFile == -1) {
		perror(argv[3]);
		exit(1);
	}
#else//RunFromOpenSSD
	io_maxevents = 128;
	myFlashFile = open(argv[1], O_RDONLY | O_DIRECT);
	if (myFlashFile == -1) {
		perror(argv[1]);
		exit(1);
	}
#if(VERIFY_LOADED_DATA_ASYNC == 1)
	myFlashFile_temp.open(argv[1], ios::in);
	if (!myFlashFile_temp.is_open()) {
		perror(argv[1]);
		exit(1);
	}
/*	char *temp_buffer;
	if ( posix_memalign((void **)&temp_buffer, 16384, 32*4096) ) {
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
	}
//	myFlashFile_temp.seekg(262144*SSD_PAGE_SIZE, myFlashFile_temp.beg);//this work only for runopenssd = 1
//	myFlashFile_temp.read(temp_buffer, SSD_PAGE_SIZE);
	ReadFromSSDN(262144*4, 4);
	memcpy(temp_buffer, data_nvme_command, 16384);
	cout << "Correct one is" << endl;
	for(unsigned int j = 0 ; j < SSD_PAGE_SIZE / sizeof(unsigned int); j++)
	{
		cout << j << "=" << ((unsigned int *)temp_buffer)[j] << "; ";
	}
	cout << endl;*/
#endif//VERIFY_LOADED_DATA_ASYNC
#endif//RunFromOpenSSD == 1
#else//UseAsynchronousIO
#if(RunFromOpenSSD == 0)
	myFlashFile.resize(NumOfIOThreads);
	for(unsigned int i = 0; i < (NumOfIOThreads); i++)
	{
		myFlashFile[i].open(argv[3], ios::in);
		assert(myFlashFile[i].is_open());
	}
#endif//RunFromOpenSSD
#endif//UseAsynchronousIO
	/*-------------Statistics collection related ones--------------*/
	stat_initialize_channel_configuration();
}

#if(UseAsynchronousIO == 1)
static inline int
io_destroy(aio_context_t ctx)
{
	            return syscall(SYS_io_destroy, ctx);
}
#endif//UseAsynchronousIO == 1

void SSDInterface::Finalize()
{
	end_application_time();
	print_info();
#if(MEASURE_TIME == 1)
	stat_print_channel_access_conflicts();
#endif//MEASURE_TIME
	for(unsigned int i = 0; i < (NumOfRequestingThreads+NumberOfResponseThreads); i++)
	{
		application_output_file[i].close();
	}
	for(unsigned int i = 0; i < NumOfCacheThreads; i++)
	{
		cache_output_file[i].close();
	}
	for(unsigned int i = 0; i < NumOfIOThreads; i++)
	{
		IO_output_file[i].close();
	}
#if(UseAsynchronousIO == 1)
//	io_destroy(ioctx);
	close(myFlashFile);
#if(VERIFY_LOADED_DATA_ASYNC == 1)
	myFlashFile_temp.close();
#endif//VERIFY_LOADED_DATA_ASYNC
#else//UseAsynchronousIO
#if(RunFromOpenSSD == 0)
	for(unsigned int i = 0; i < NumOfIOThreads; i++)
	{
		myFlashFile[i].close();
	}
#endif//RunFromOpenSSD
#endif//UseAsynchronousIO
	return;
}
