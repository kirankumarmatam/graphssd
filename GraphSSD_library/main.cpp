#include "graphFiltering.h"
#include "header.h"
#include "testing.h"
#define STORE_GRAPH_AT_SSD 1

extern unsigned int NumNodes;
extern EdgeIndexType NumEdges;
extern unsigned int currentGTTIndex_edge;
extern struct GTTTable1 *gtt1;

int main(int argc, char *argv[])
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

#if(CHECK_STORING_GRAPH_INFO)
	cout << NumNodes << " " << NumEdges << endl;

	for(unsigned int i = 0; i <= currentGTTIndex_edge; i++)
	{
		cout << "vId = " << gtt1->GTTPointer1[i].vertexId << " LPN = " << gtt1->GTTPointer1[i].LPN << endl;
	}
	cout << "Before return" << endl;
	return 0;
	cout << "After return" << endl;
#endif//CHECK_STORING_GRAPH_INFO

	SSDInstance.Initialization(argc, argv);

#if(GTL_TRANSLATION_CHECKING == 1 or MINIMAL_PRINT == 1)
	cout << "yay initialization done!" << endl;
#endif

	omp_set_nested(100);
	unsigned int tid;

#pragma omp parallel num_threads(3) private(tid)
	{
		tid = omp_get_thread_num();
		if(tid == 0)
		{
			SSDInstance.cache_manager();
		}
		else if (tid == 1)
		{
			SSDInstance.IO_manager();
		}
		else if (tid == 2)
		{
			SSDInstance.application_program(argc, argv); // when application program finishes work it calls exit();
		}
	}
	return 0;
}
