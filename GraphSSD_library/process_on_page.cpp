#include "header.h"
#include "graphFiltering.h"
extern unsigned int GraphLPN;
extern char * Test_variable_size_buffer;
extern unsigned int numOfFreeElementsAvailableInTheBuffer;
extern unsigned int bufferPageTopPointer;
extern unsigned int currentGTTIndex_edge;
extern unsigned int currentGTTIndex_weight;
extern struct CompletionQueues *gtlCompletionQueues;
extern struct CompletionQueuePtrs *gtlCompletionQueuePtrs;

extern struct GTTTable1 *gtt1;
extern struct GTTTable1 *gtt1_weight;

extern struct VertexCompletionInformation vertexCompletionInformation;
std::map<unsigned int, char *> Storage;

extern unsigned int NumNodes;

unsigned int time_post_process_counter = 0;

//Need to work on this function

void SSDInterface::process_completionRequest(char *src_address, vector<EDGE_TYPE> *adjacent_vertices_ptr, VERTEX_TYPE vertex_id)
{
	assert(src_address != NULL);
	assert(adjacent_vertices_ptr != NULL);
	assert((vertex_id >= 0) && (vertex_id < NumNodes));
	CopyFunction_InPage(src_address, adjacent_vertices_ptr, vertex_id);
	return;
}
void SSDInterface::process_completionRequest_write(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit)
{
	if(!(vertexCompletionInformation.flags == 5 || vertexCompletionInformation.flags == 2))
	{
	        return;
	}
	unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
	unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
	if(front == rear)
	{
		printf("ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n", chNo, wayNo);
	}
	char * devAddr = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
	unsigned int destPageOffset = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].destPageOffset;
	unsigned int srcPageOffset = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].srcPageOffset;
	unsigned int Number = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].Number;
	unsigned int lowestVertexId = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].lowestVertexId;
	gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front = (front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

	memcpy((void *)(devAddr+destPageOffset), (void *)(Test_variable_size_buffer+srcPageOffset), Number*sizeof(unsigned int));
	unsigned int vertexId = vertexCompletionInformation.vertexId;
	((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3)] = vertexId;
	((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3 + 1)] = destPageOffset;
	((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3 + 2)] = Number;
	if(lowestVertexId == vertexId)
	{
		((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 1] = 1;
	}
	else {
		((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 1] += 1;
	}
	vertexCompletionInformation.numberOfVerticesRemainingToFetch -= Number;

	if(vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0)
	{
		if(vertexCompletionInformation.NumOfAdjVertices == 0 && vertexCompletionInformation.isEdge == 1)
		{
			vertexCompletionInformation.isEdge = 0;
			return;
		}
		vertexCompletionInformation.flags = 0;
		return;
	}
	return;
}

unsigned int time_copy_counter = 0;

#if(useCache == 1 && UseCache_GetPagesFromSSD == 1)
void SSDInterface::GraphLRUBufWrite(unsigned int curSect, unsigned int Len, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int lowestVertexId)
{
	unsigned int tempLpn, dieNo;

	tempLpn = curSect;

	if(Storage.find(tempLpn) == Storage.end())
	{
		Storage[tempLpn];
		Storage[tempLpn] = new char [SSD_PAGE_SIZE];
	}

	dieNo = tempLpn % DIE_NUM;
	unsigned int chNo = dieNo % CHANNEL_NUM;
	unsigned int wayNo = dieNo / CHANNEL_NUM;
	unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
	unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
	if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
	{
		printf("ERROR: COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", chNo, wayNo);
	}
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].devAddr = Storage[tempLpn];
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].destPageOffset = destPageOffset;
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].srcPageOffset = srcPageOffset;
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].Number = Len / sizeof(unsigned int);
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].lowestVertexId = lowestVertexId;
	gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);

	process_completionRequest_write(chNo, wayNo, 0, 1);

	return;
}
#endif//useCache == 1 && UseCache_GetPagesFromSSD == 1
