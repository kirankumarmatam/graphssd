#include "graphFiltering.h"
#include "header.h"
#define VERBOSE 0
#define FLUSH_SSD_BUFFER 0
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192

map<unsigned int, unsigned int> vertexMapping;

extern unsigned int MaxCacheSize;
unsigned int cache_percentage = 5;

#if(IsUndirectedGraph == 1)
PUNGraph G1;
PNGraph G;
#elif(IsUndirectedGraph == 0)
PNGraph G;
#endif//IsUndirectedGraph

#if(LOAD_GRAPH_FROM_FILE == 1)
extern unsigned int GraphLPN;
extern unsigned int currentGTTIndex_edge;
extern struct GTTTable1 *gtt1;
#endif
unsigned int NumNodes;
EdgeIndexType NumEdges;
extern unsigned int NumNodesNew;

int SSDInterface::store_graph(int argc, char **argv)
{
#if (UseFTL == 2 || UseFTL == 3)
#if(RunFromOpenSSD == 1)
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		cout << "Error opening file!!" << endl;
#endif//RunFromOpenSSD
#endif

#if(LOAD_GRAPH_FROM_FILE == 1)
	if(argc < 5)
	{
		cout << "Usage is ./a.out /dev/nvme0n1 inputDataSet.txt inputFlash_file.txt inputGTL_file.txt" << endl;
		return 0;
	}
	bool loadFlash_graph = 1;
	if(argc >= 6)
	{
		loadFlash_graph = atoi(argv[5]);
	}

	InitializeGraphDataStructures(0);
//#if(UseAsynchronousIO == 0)
	if(loadFlash_graph == 1)
	{
#if(RunFromOpenSSD == 1)
		fstream myFlashFile_temp;
		myFlashFile_temp.open(argv[3]);
		unsigned int numOfSSDPages;
		myFlashFile_temp.read((char *)(&numOfSSDPages), sizeof(unsigned int));
		cout << "numOfSSDPages = " << numOfSSDPages << endl;
		unsigned int MaxReadPages = ((32*4096)/SSD_PAGE_SIZE);
		for(unsigned int i = 0; i < numOfSSDPages; i+= MaxReadPages)
		{
#if(MINIMAL_PRINT == 1)
			if(i % (MaxReadPages*10000) == 0)
				cout << "Stored " << i << " pages" <<endl;
#endif//MINIMAL_PRINT
			myFlashFile_temp.read((char *)data_nvme_command, SSD_PAGE_SIZE*MaxReadPages);
			WriteToSSDN((GraphLPN+i+1)*(SSD_PAGE_SIZE/PAGE_SIZE), (SSD_PAGE_SIZE/PAGE_SIZE)*MaxReadPages);
#if(GTL_TRANSLATION_CHECKING == 1)
			for(unsigned int loop_2 = 0; loop_2 < MaxReadPages; loop_2++)
			{
				cout << "i = " << i + loop_2 << " PPN = " << GraphLPN+i+1+loop_2 << " " << ((unsigned int *)data_nvme_command)[(SSD_PAGE_SIZE/sizeof(unsigned int))*(loop_2+1) - 2] << " " << ((unsigned int *)data_nvme_command)[(SSD_PAGE_SIZE/sizeof(unsigned int))*(loop_2+1) - 1] << endl;
			}
#endif//GTL_TRANSLATION_CHECKING
//			cout << (GraphLPN+i+1)*(SSD_PAGE_SIZE/PAGE_SIZE) << " " << ((unsigned int *)data_nvme_command)[PAGE_SIZE - 1] << endl;
		}
		myFlashFile_temp.close();
#else
		//This file will be opened in initialization function
#endif//RunFromOpenSSD
	}
//#endif//UseAsynchronousIO
	fstream myGTLFile;
	myGTLFile.open(argv[4]);
	myGTLFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myGTLFile.read((char *)(&NumEdges), sizeof(EdgeIndexType));
	myGTLFile.read((char *)(&currentGTTIndex_edge), sizeof(unsigned int));
	myGTLFile.read((char *)(&GraphLPN), sizeof(unsigned int));
	cout << "NumNodes = " << NumNodes << " NumEdges = " << NumEdges << endl;
//	cout << "currentGTTIndex_edge =  " << currentGTTIndex_edge << " GraphLPN = " << GraphLPN << endl;
	unsigned int *GTLArray = new unsigned int [2*currentGTTIndex_edge + 2];

	myGTLFile.read((char *)GTLArray, sizeof(unsigned int) * 2 * (currentGTTIndex_edge + 1));
	for(unsigned int i = 0; i <= currentGTTIndex_edge; i++)
	{
		gtt1->GTTPointer1[i].vertexId = GTLArray[2*i];
		gtt1->GTTPointer1[i].LPN = GTLArray[2*i+1];
//		cout << gtt1->GTTPointer1[i].vertexId << " " << gtt1->GTTPointer1[i].LPN << endl; 
	}

	delete [] GTLArray;
	myGTLFile.close();
	//Need to write flush buffering etc
#else
	struct timespec time_start, time_end;
	double time_elapsed;

	if(argc < 3)
	{
		cout << "Usage is ./a.out inputDataSet.txt" << endl;
		return 0;
	}
#if(IsUndirectedGraph == 1)
	G1 = TSnap::LoadEdgeList<PUNGraph>(argv[2], 0, 1);
	G = TSnap::ConvertGraph<PNGraph>(G1);
#elif(IsUndirectedGraph == 0)
	G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
#endif//IsUndirectedGraph
	NumNodes = G->GetNodes(), NumEdges = G->GetEdges();
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);

	InitializeGraphDataStructures(G->GetNodes());
	cout <<"GetNodes " << G->GetNodes() << endl;
	vector<unsigned int> EdgeList;
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
#if(VERBOSE == 1)
		cout << "W " << vertexMapping[NI.GetId()] << endl;
#endif
//		for (int e = 0; e < NI.GetOutDeg(); e++)
		for (int e = 0; e < NI.GetOutDeg() && e < (16*4096 / sizeof(unsigned int)); e++)
		{
			assert(vertexMapping.find(NI.GetOutNId(e)) != vertexMapping.end());
			EdgeList.push_back(vertexMapping[NI.GetOutNId(e)]);
		}
		std::sort(EdgeList.begin(), EdgeList.end());
		AddVertexUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
#if(CHECK_ERROR == 1)
		AddVertexNoGTL(vertexMapping[NI.GetId()], EdgeList);
#endif // CHECK_ERROR == 1
		EdgeList.clear();
	}
#if(UseFTL == 3)
	WriteGraphAndDelete();
#endif
	
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_WR: %lf\n", time_elapsed);
#endif //LOAD_GRAPH_FROM_FILE

#if(useCache==1)
	if(((NumNodes + NumEdges) * sizeof(unsigned int)) < (256*1024))
		MaxCacheSize = 256 * 1024;
	else MaxCacheSize = (((NumNodes + NumEdges) * sizeof(unsigned int)) * cache_percentage) / 100;
	MaxCacheSize = 1024*1024*1024;
	cout << "MaxCacheSize = " << MaxCacheSize << endl;//temporarily
//	MaxCacheSize = 375 * 1024 * 1024;
#endif

#if(RunFromOpenSSD == 1)
#if(FLUSH_SSD_BUFFER == 1)
	/*Maximum graph size can be (SLBA_FLUSH_BUFFER-starting graph lpn) * 4096*/
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1
#endif//RunFromOpenSSD

#if(ADD_UPDATES_TO_BUFFER == 1)
	bufferRowPtrArray.resize(NumNodes, 0);
	bufferColumnIndexPtrArray.push_back(0);
	NumNodesNew = NumNodes;
#endif//ADD_UPDATES_TO_BUFFER

#if(RunFromOpenSSD == 1)
	ClearQueues();
#endif//RunFromOpenSSD

	return 0;
}
