#include "graphFiltering.h"
#include "header.h"

struct timespec app_time_start, app_time_end;

void SSDInterface::start_application_time()
{
	clock_gettime(CLOCK_MONOTONIC, &app_time_start);
}

void SSDInterface::end_application_time()
{
	clock_gettime(CLOCK_MONOTONIC, &app_time_end);
}

unsigned long long int Number_of_IO_pages_read = 0;
unsigned long long int Total_number_of_average_pages_read = 0, current_inflight_request = 0;
mutex IO_utility_lock;
extern unsigned int NumOfCacheThreads;

#if(MEASURE_TIME == 1)
mutex Cache_stat_lock;
unsigned int cache_hits = 0;
unsigned int cache_misses = 0;
extern double hit_elapsed_time_sum;
extern double miss_elapsed_time_sum;
#endif//MEASURE_TIME

#if(MEASURE_BINARY_SEARCH == 1)
extern vector<Timer> time_binary_search;
#endif//MEASURE_BINARY_SEARCH

extern vector<uint64_t> Cache_access_count;

void SSDInterface::Increment_num_of_read_IO_pages(unsigned int SLBA, unsigned int numBlocks)
{
	IO_utility_lock.lock();
	Number_of_IO_pages_read += 1;
	current_inflight_request++;
	Total_number_of_average_pages_read += current_inflight_request;
	IO_utility_lock.unlock();
}

void SSDInterface::Decrement_average_parallelism()
{
	IO_utility_lock.lock();
	current_inflight_request--;
	IO_utility_lock.unlock();
}

void  SSDInterface::print_info()
{
	double time_elapsed;
	time_elapsed = ((double)app_time_end.tv_sec - (double)app_time_start.tv_sec);
	time_elapsed += ((double)app_time_end.tv_nsec - (double)app_time_start.tv_nsec) / 1000000000.0;
	cout << "TIME_APP: " << time_elapsed << " (sec)" << endl;
	application_print_info();
	cout << "Bandwidth utilization is " << (Number_of_IO_pages_read * SSD_PAGE_SIZE * 1.0) / (1000000*time_elapsed) << "(MB/sec)" << endl;
	cout << "Average IO parallelism = " << (Total_number_of_average_pages_read * 1.0) / Number_of_IO_pages_read << endl;
	uint64_t cache_accesses = 0;
for(unsigned int loop_1 = 0; loop_1 < NumOfCacheThreads; loop_1++)
{
	cache_accesses += Cache_access_count[loop_1];
}
	cout << "Cache accesses = " << cache_accesses << " Number of IO pages read = " << Number_of_IO_pages_read << " ratio = " << (cache_accesses * 1.0) / Number_of_IO_pages_read << endl;
#if(MEASURE_TIME == 1)
	cout << "No. of cache hits = " << cache_hits << " no. of cache misses = " << cache_misses << endl;
	cout << "Cache hit latency = " << (hit_elapsed_time_sum *1.0/ cache_hits) / 1000000000 << " (sec) cache miss latency = " << (miss_elapsed_time_sum * 1.0/ cache_misses) / 1000000000 << " cache hit sum = " << hit_elapsed_time_sum << " cache miss sum = " << miss_elapsed_time_sum << endl;
#endif//MEASURE_TIME
#if(MEASURE_BINARY_SEARCH == 1)
	for(unsigned int i = 0; i < NumOfCacheThreads; i++)
{
	time_binary_search[i].print_timer("Binary search time = ");
}
#endif//MEASURE_BINARY_SEARCH
return;
}
/*Channel access statistics collection functions -- begin --*/
std::map<unsigned int, vector<unsigned int> > stat_channel_access_conflicts;
unsigned int stat_NUM_CHANNELS, stat_current_iteration;

void SSDInterface::stat_initialize_channel_configuration()
{
	stat_NUM_CHANNELS = CHANNEL_NUM;
	stat_current_iteration = 0;
}

void SSDInterface::stat_collect_channel_access_statistics(vector<PPN_TYPE> PPN_list)
{
	IO_utility_lock.lock();
	if(stat_channel_access_conflicts.find(stat_current_iteration) == stat_channel_access_conflicts.end())
	{
		stat_channel_access_conflicts[stat_current_iteration];
		stat_channel_access_conflicts[stat_current_iteration].resize(stat_NUM_CHANNELS, 0);
	}
	for (unsigned int i=0; i<PPN_list.size(); i++)
	{
		int channel_num = PPN_list[i] % stat_NUM_CHANNELS;
		stat_channel_access_conflicts[stat_current_iteration][channel_num]++;
	}
	IO_utility_lock.unlock();
}

void SSDInterface::stat_increment_iteration()
{
	stat_current_iteration++;
}

void SSDInterface::stat_print_channel_access_conflicts()
{
	cout << "Printing channel access conflicts" << endl;
	for(std::map<unsigned int, vector<unsigned int> >::iterator it = stat_channel_access_conflicts.begin(); it != stat_channel_access_conflicts.end(); it++)
	{
		cout << "iteration: " << it->first << " :";
		unsigned int i=0;
		for(vector<unsigned int>::iterator it1 = (it->second).begin(); it1 != (it->second).end(); it1++)
		{
			cout << i++ <<":"<<*it1 << " ";
		}
		cout << endl;
	}
}
/*Channel access statistics collection -- end --*/

void Timer::start_timer()
{
	clock_gettime(CLOCK_MONOTONIC, &timer_start);
}

void Timer::end_timer()
{
	clock_gettime(CLOCK_MONOTONIC, &timer_end);
	time_elapsed += ((double)timer_end.tv_sec - (double)timer_start.tv_sec) * 1000000000;
	time_elapsed += ((double)timer_end.tv_nsec - (double)timer_start.tv_nsec);
}

void Timer::print_timer(char *string)
{
	cout << string << " " << time_elapsed * 1.0 / 1000000000 << " (sec)"<< endl;
}

double Timer::get_timer()
{
	clock_gettime(CLOCK_MONOTONIC, &timer_end);
	time_elapsed += ((double)timer_end.tv_sec - (double)timer_start.tv_sec) * 1000000000;
	time_elapsed += ((double)timer_end.tv_nsec - (double)timer_start.tv_nsec);
//	cout << (double)timer_end.tv_sec << " " << (double)timer_end.tv_nsec << " " << (double)timer_start.tv_sec << " " << (double)timer_start.tv_nsec << endl;
	return time_elapsed;
}
