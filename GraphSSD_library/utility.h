class Timer {
	public:
		Timer()
		{
			time_elapsed = 0;
		}
		double time_elapsed;
		struct timespec timer_start, timer_end;
		void start_timer();
		void end_timer();
		void print_timer(char *str);
		double get_timer();
};
