// mutex example
#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <mutex>          // std::mutex
#include <vector>
using namespace std;

struct mutex_wrapper : std::mutex
{
	        mutex_wrapper() = default;
		        mutex_wrapper(mutex_wrapper const&) noexcept : std::mutex() {}
			        bool operator==(mutex_wrapper const&other) noexcept { return this==&other; }
};

