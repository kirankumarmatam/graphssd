#include"header.h"

extern vector< mutex_wrapper > Cache_response_queue_lock;

void print_block (int n, char c) {
	  // critical section (exclusive access to std::cout signaled by locking mtx):
	  Cache_response_queue_lock[0].lock();
	    for (int i=0; i<n; ++i) { std::cout << c; }
	      std::cout << '\n';
	        Cache_response_queue_lock[0].unlock();
}

int main ()
{
	for (unsigned int i=0;i<2;i++)
	{
		mutex_wrapper tmp_mtx;
		Cache_response_queue_lock.push_back(tmp_mtx);
	}

	  std::thread th1 (print_block,50,'*');
	    std::thread th2 (print_block,50,'$');

	      th1.join();
	        th2.join();

		  return 0;
}
