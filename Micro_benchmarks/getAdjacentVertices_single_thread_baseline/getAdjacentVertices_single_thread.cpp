#include "../../Baseline_library/graphFiltering.h"
#define CHECK_ADJACENCY_LIST_INFO 0

extern queue<APP_REQUEST_TYPE> App_to_cache_request_queue;
extern mutex App_to_cache_request_queue_lock;
extern unsigned int App_to_cache_request_queue_max_size;

extern vector< queue< pair<REQUEST_UNIQUE_ID_TYPE, vector<EDGE_TYPE> * > > > Cache_response_queue;
extern vector< mutex_wrapper > Cache_response_queue_lock;
extern unsigned int Cache_response_queue_max_size;

extern unsigned int NumNodes, NumEdges;
unsigned int NumOfRequestingThreads, NumberOfResponseThreads;
extern unsigned int number_of_application_response_queues;
std::vector<unsigned int> NumberOfInFlightRequests;
mutex NumberOfInFlightRequests_lock;

extern vector<ofstream> application_output_file;

void application_initialization()
{
	NumberOfInFlightRequests.reserve(number_of_application_response_queues);
}

void SSDInterface::application_program(int argc, char *argv[])
{
	application_initialization();
	map<pair<unsigned int,REQUEST_UNIQUE_ID_TYPE>, VERTEX_TYPE> request_id_to_vertex_id_map;
	mutex request_id_to_vertex_id_lock;

	unsigned int start_vertex_index = 0, end_vertex_index = 40;
	unsigned int num_threads_to_create = NumOfRequestingThreads + NumberOfResponseThreads;
#pragma omp parallel num_threads(num_threads_to_create) private(start_vertex_index)
	{
		start_vertex_index = 0;
		unsigned int tid = omp_get_thread_num();
#if(GTL_TRANSLATION_CHECKING == 1)
		application_output_file[tid] << "GTL_TRANSLATION_CHECKING: appThread " << " tid = " << tid << endl;
#endif//GTL_TRANSLATION_CHECKING

		if(tid < NumOfRequestingThreads)
		{
			NumberOfInFlightRequests_lock.lock();
			NumberOfInFlightRequests[tid] = 0;
			NumberOfInFlightRequests_lock.unlock();
			while(start_vertex_index < end_vertex_index)
			{
#if(GTL_TRANSLATION_CHECKING == 1)
				application_output_file[tid] << "GTL_TRANSLATION_CHECKING: appThread: " << " request id = "<< start_vertex_index << " tid = " << tid <<endl;
#endif//GTL_TRANSLATION_CHECKING
				while(1)
				{
					Cache_response_queue_lock[tid].lock();
					NumberOfInFlightRequests_lock.lock();
					if(NumberOfInFlightRequests[tid] < Cache_response_queue_max_size)
					{
						NumberOfInFlightRequests[tid]++;
						NumberOfInFlightRequests_lock.unlock();
						Cache_response_queue_lock[tid].unlock();
						break;
					}
					NumberOfInFlightRequests_lock.unlock();
					Cache_response_queue_lock[tid].unlock();
				}
#if(GTL_TRANSLATION_CHECKING == 1)
				NumberOfInFlightRequests_lock.lock();
				application_output_file[tid] << "GTL_TRANSLATION_CHECKING: appThread: " << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid] << endl;
				NumberOfInFlightRequests_lock.unlock();
#endif//GTL_TRANSLATION_CHECKING
				VERTEX_TYPE vertex_id = rand() % NumNodes;
				request_id_to_vertex_id_lock.lock();
				request_id_to_vertex_id_map[make_pair(tid, start_vertex_index)] = vertex_id;
				request_id_to_vertex_id_lock.unlock();
				APP_REQUEST_TYPE App_to_cache_request = {vertex_id, tid, start_vertex_index};
#if(GTL_TRANSLATION_CHECKING == 1)
				application_output_file[tid] << "GTL_TRANSLATION_CHECKING: appThread: " << " App_to_cache_request = " << App_to_cache_request.vertex_id << " " << App_to_cache_request.tid << " " << App_to_cache_request.request_id << endl;
#endif//GTL_TRANSLATION_CHECKING
				while(1)
				{
					App_to_cache_request_queue_lock.lock();
					if(App_to_cache_request_queue.size() < App_to_cache_request_queue_max_size)
					{
						App_to_cache_request_queue.push(App_to_cache_request);
						App_to_cache_request_queue_lock.unlock();
						break;
					}
					App_to_cache_request_queue_lock.unlock();
				}
				start_vertex_index++;
			}
		} else {
#if(GTL_TRANSLATION_CHECKING == 1)
//			while(1){}
			application_output_file[tid] << "GTL_TRANSLATION_CHECKING: " << " entering in application response thread " << endl;
#endif//GTL_TRANSLATION_CHECKING

			std::pair<REQUEST_UNIQUE_ID_TYPE, std::vector<EDGE_TYPE> * > cache_to_app_response;
			while(1) {
				while(1)
				{
					Cache_response_queue_lock[tid-NumOfRequestingThreads].lock();
					if(!Cache_response_queue[tid-NumOfRequestingThreads].empty())
					{
						cache_to_app_response = Cache_response_queue[tid-NumOfRequestingThreads].front();
						Cache_response_queue[tid-NumOfRequestingThreads].pop();
						NumberOfInFlightRequests_lock.lock();
						NumberOfInFlightRequests[tid-NumOfRequestingThreads]--;
						NumberOfInFlightRequests_lock.unlock();
						Cache_response_queue_lock[tid-NumOfRequestingThreads].unlock();
						break;
					}
					Cache_response_queue_lock[tid-NumOfRequestingThreads].unlock();
				}
#if(GTL_TRANSLATION_CHECKING == 1)
				NumberOfInFlightRequests_lock.lock();
				application_output_file[tid] << "GTL_TRANSLATION_CHECKING: appThread " << " NumberOfInFlightRequests = " << NumberOfInFlightRequests[tid-NumOfRequestingThreads] << endl;
				NumberOfInFlightRequests_lock.unlock();
#endif//GTL_TRANSLATION_CHECKING
				request_id_to_vertex_id_lock.lock();
				if(request_id_to_vertex_id_map.find(make_pair(tid-NumOfRequestingThreads, cache_to_app_response.first)) == request_id_to_vertex_id_map.end())
				{
					request_id_to_vertex_id_lock.unlock();
					application_output_file[tid] << "Error - reponse id not found " << cache_to_app_response.first << endl;
					assert(0);
				}
				else {
					VERTEX_TYPE vertex_id = request_id_to_vertex_id_map[make_pair(tid-NumOfRequestingThreads, cache_to_app_response.first)];
					request_id_to_vertex_id_lock.unlock();
#if(CHECK_ADJACENCY_LIST_INFO == 1)
					vector<EDGE_TYPE> Orignal_edgeList;
					GetAdjListNoGTL(vertex_id, Orignal_edgeList);
					if(Orignal_edgeList.size() != (cache_to_app_response.second)->size())
					{
						application_output_file[tid] << "Error - edge lists size not matching " <<  " original one = " << Orignal_edgeList.size() << " fetched adj. list size = " << (cache_to_app_response.second)->size() << " response id = "<< cache_to_app_response.first << " vertex id = " << vertex_id<< endl;
						assert(0);
					}
					else {
					/*				for(unsigned int i = 0; i < (cache_to_app_response.second)->size(); i++)
									{
									}
					 */
						sort(Orignal_edgeList.begin(), Orignal_edgeList.begin()+Orignal_edgeList.size());
						sort((cache_to_app_response.second)->begin(), (cache_to_app_response.second)->begin()+(cache_to_app_response.second)->size());
						for(unsigned int k=0; k < Orignal_edgeList.size(); k++)
						{
							application_output_file[tid] << "GTL_TRANSLATION_CHECKING: " << " Orig index = " << k <<  " " << Orignal_edgeList[k] << endl;
						}
						for(unsigned int k=0; k < (cache_to_app_response.second)->size(); k++)
						{
							application_output_file[tid] << "GTL_TRANSLATION_CHECKING: " << " fetched one, index = " << k << " " << (*(cache_to_app_response.second))[k] << endl;
						}
						for(unsigned int k=0; k < Orignal_edgeList.size(); k++)
						{
							assert(Orignal_edgeList[k] == (*(cache_to_app_response.second))[k]);
						}
					}
#endif//CHECK_ADJACENCY_LIST_INFO
#if(GTL_TRANSLATION_CHECKING == 1)
					application_output_file[tid] << "GTL_TRANSLATION_CHECKING: appThread " << " request id = " << cache_to_app_response.first << " adjacency list size = " << (*(cache_to_app_response.second)).size() << endl;
#endif//GTL_TRANSLATION_CHECKING
					(*(cache_to_app_response.second)).clear();
#if(GTL_TRANSLATION_CHECKING == 1)
					application_output_file[tid] << "GTL_TRANSLATION_CHECKING: appThread " << " finished erasing adjacency list " << endl;
#endif//GTL_TRANSLATION_CHECKING
					request_id_to_vertex_id_lock.lock();
					request_id_to_vertex_id_map.erase(make_pair(tid-NumOfRequestingThreads, cache_to_app_response.first));
					request_id_to_vertex_id_lock.unlock();
#if(GTL_TRANSLATION_CHECKING == 1)
					application_output_file[tid] << "GTL_TRANSLATION_CHECKING: appThread " << " finished erasing request id " << endl;
#endif//GTL_TRANSLATION_CHECKING
				}
				start_vertex_index++;
				if(start_vertex_index == end_vertex_index)
					break;
			}
		}
	}
	cout << "Program exiting" << endl;
	Finalize();
	return;
}
