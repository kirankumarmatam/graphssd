There are two versions of GraphSSD code, one is with blocking type SSD graph commands, and another one is with non-blocking type SSD graph commands.

----------GraphSSD with non-blocking command support----------
For running the GraphSSD system with non-blocking command support, we need to perform the following steps:

1) Convert the graph from native file format to CSR format
2) Convert CSR format to GraphSSD format
3) Execute the graph application

Each step is explained in more detail here:

1) Convert the graph from native file format to CSR format: For converting the graph downloaded from any source like the internet to CSR format, we use the graphchi framework. We load the graph using graphchi and convert it to the CSR format file. The code for this can be found in Utilities/graphchi-cpp/myapps/convert.cpp. The sequence of steps for executing this conversion can be found in the Utilities/script5.py file.

2) Convert CSR format to GraphSSD format: The code to convert from CSR format graph to GraphSSD format file can be found in Utilities/Layout_generators/Layout_generator_csr_to_graphSSD directory. The command to compile and run the code can be found in compile.sh and query.sh files, respectively, which are located in that directory itself. This generates two files, one for the GTT and another from graph edge data.

3) Execute the graph application: Applications with non-blocking requests are located in the Applications_program directory. In this Applications_program directory, the GraphSSD application code can be found with the application name. Corresponding code for the baseline (with CSR graph format) can be found with _baseline added to the application name. Command to compile the application can be found in compile_app.sh file in the application's directory itself. Command to run the application can be found in the query.sh file in the application's directory itself. The code for getting adjacent neighbors microbenchmark can be found in the Micro_benchmarks directory. The sequence of steps to execute the applications can be found in scripts/script10.py. The two files generated in the previous step, GTT and graph edge data, are given as arguments when executing the application.

The code for handling the non-blocking requests can be found in the GraphSSD_library directory. This directory contains the code for the cache manager, IO manager, etc., which can be found in this directory. The main program, main.cpp, can also be found in GraphSSD_library. The code for handling the non-blocking requests for the baseline can be found in the Baseline_library directory.  Similar to GraphSSD_library, the code for cache manager, IO manager, etc., for the baseline with CSR format can be found in this directory. Similarly, the main program, main.cpp, can also be found in Baseline_library.

The code for graphchi applications is located in graphchi-cpp/myapps directory. The sequence of steps to execute the applications can be found in scripts/script12.py
----------***********----------
----------GraphSSD with non-blocking command support----------
The code for blocking requests can be found in the ssd-graph directory. The code for graph handling on the ssd side is located in the ssd-graph/ossd_firmware directory. This code modifies the OpenSSD firmware. A short tutorial on how to use OpenSSD can be found here, https://gitlab.com/kirankumarmatam/openssd-toy-project

The host side code requesting the ssd using graph commands is located in the ssd-graph/HostCodes directory. The graph commands to ssd are blocking, and applications are written using a single thread. Commands to compile and execute can be found in compile.sh and run*.sh files, respectively.

To run this load, the firmware on the OpenSSD platform. Then execute the host side applications.
----------***********----------
