#include "graphFiltering.h"
#include "header.h"
#define VERBOSE 0

int SSDInterface::store_graph(int argc, char **argv)
{
	if(argc < 5)
	{
		cout << "Usage is ./a.out /dev/nvme0n1 input_csr_file.txt outputFile_flash.txt outputFile_gtl.txt" << endl;
		return 0;
	}

#if(UseMMAP == 0)
	myCsrFile.open(argv[2], fstream::in);
	unsigned int NumNodes;
	EdgeIndexType NumEdges;
	myCsrFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myCsrFile.read((char *)(&NumEdges), sizeof(EdgeIndexType));
	cout << "CSR numNodes = " << NumNodes << " numEdges = " << NumEdges << endl;
	EdgeIndexType *Array_input_rowPtr = new EdgeIndexType [NumNodes + 1];
	unsigned int *Array_input_colPtr = new unsigned int [NumEdges];
	myCsrFile.read((char *)Array_input_rowPtr, ((uint64_t)(NumNodes+1))*(sizeof(EdgeIndexType)));
	myCsrFile.read((char *)Array_input_colPtr, (NumEdges)*(sizeof(unsigned int)));
	myCsrFile.close();
#else
	myCsrFile.open(argv[2], fstream::in);
	unsigned int NumNodes;
	EdgeIndexType NumEdges;
	myCsrFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myCsrFile.read((char *)(&NumEdges), sizeof(EdgeIndexType));
	cout << "CSR numNodes = " << NumNodes << " numEdges = " << NumEdges << endl;
	myCsrFile.close();

	int fd;

	fd = open(argv[2], O_RDONLY);
	if (fd == -1) {
		perror("Error opening file for reading");
		exit(EXIT_FAILURE);
	}

	uint64_t FileSize = sizeof(unsigned int) + sizeof(EdgeIndexType) + ((uint64_t)(NumNodes+1))*(sizeof(EdgeIndexType)) + (NumEdges)*(sizeof(unsigned int));
	void *temp_array_address = mmap64(0, FileSize, PROT_READ, MAP_SHARED, fd, 0);
	if (temp_array_address == MAP_FAILED) {
		close(fd);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}

	EdgeIndexType *Array_input_rowPtr = (EdgeIndexType *)((char *)temp_array_address + sizeof(unsigned int) + sizeof(EdgeIndexType));//new EdgeIndexType [NumNodes + 1];
	unsigned int *Array_input_colPtr = (unsigned int *)((char *)temp_array_address + sizeof(unsigned int) + sizeof(EdgeIndexType) + ((uint64_t)(NumNodes+1))*(sizeof(EdgeIndexType)));//new unsigned int [NumEdges];
#endif//UseMMAP

	InitializeGraphDataStructures(NumNodes);

	myFlashFile.open(argv[3], fstream::in | fstream::out | fstream::trunc);
	if (!myFlashFile.is_open())
		cout << " Cannot open flash output file!" << endl;

	myFlashFile.write((char *)(&argc), sizeof(unsigned int));
	if (myFlashFile.fail())
	{
		cout <<"In store_graph" << endl;
		cout << "Failed = " << myFlashFile.fail() << endl;
		exit(0);
	}

	cout <<"GetNodes " << NumNodes << endl;
	vector<unsigned int> EdgeList;

	for (unsigned int i = 0; i < NumNodes; i++)
	{
		if(i % 10000000 == 0)
			cout << "Printed 10M nodes into flash file" << endl;
		for (EdgeIndexType e = Array_input_rowPtr[i]; e < Array_input_rowPtr[i+1]; e++)
		{
			EdgeList.push_back(Array_input_colPtr[e]);
		}
		AddVertexUseNvmeCommands(i, EdgeList);
		EdgeList.clear();
	}

	cout << " Almost finished writing flash to file" << endl;
	//Open file here, write that file here
	myGTLFile.open(argv[4], fstream::in | fstream::out | fstream::trunc);
	myGTLFile.write((char *)(&NumNodes), sizeof(unsigned int));
	myGTLFile.write((char *)(&NumEdges), sizeof(EdgeIndexType));
	cout << "Wrote node and edge info to gtl file" << endl;
	if (!myGTLFile.is_open())
		cout << " Cannot open flash output file!" << endl;

	WriteGraphAndDelete();
	cout << "Finished writing to flash and gtl file" << endl;

	myGTLFile.close();
	myFlashFile.close();

#if(UseMMAP == 1)
	if (munmap(temp_array_address, FileSize) == -1) {
		perror("Error un-mmapping the file");
	}
	close(fd);
#endif//UseMMAP

	return 0;
}
