#include "../Convert_csr_to_graphSSD_format/graphFiltering.h"
#include "../Convert_csr_to_graphSSD_format/header.h"
#define STORE_GRAPH_AT_SSD 1

#if(CalculateNANDpageUsageEfficiency == 1)
extern unsigned long long int UnusedNANDPageSpace;
extern unsigned int currentGTTIndex_edge;
#endif//CalculateNANDpageUsageEfficiency

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

#if(CalculateNANDpageUsageEfficiency == 1)
	cout << "NandPageUsageEfficiency = " << (UnusedNANDPageSpace * 100.0) / ((currentGTTIndex_edge+1) * (SSD_PAGE_SIZE / (sizeof(unsigned int)))) << endl;
#endif//CalculateNANDpageUsageEfficiency

	fstream myFile;
	myFile.open(argv[4], fstream::in);
	unsigned int GTLsize;
	myFile.read((char *)(&GTLsize), sizeof(unsigned int));
	cout << "GTLsize = " << GTLsize << endl;
	myFile.close();
	return 0;
}
