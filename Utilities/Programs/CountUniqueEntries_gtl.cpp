#include <iostream>
#include <cstdint>
#include <fstream>
using namespace std;

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cout << "usage: ./a.out filename" << endl;
		return 0;
	}
	ifstream myFile;
	myFile.open(argv[1], ios::in);
	if(!myFile.is_open()) { cout << "file not opening" << endl; return 0;}
	unsigned int NumNodes;
	uint64_t NumEdges;
	unsigned int NumPages, temp;
	myFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myFile.read((char *)(&NumEdges), sizeof(uint64_t));
	myFile.read((char *)(&NumPages), sizeof(unsigned int));
	myFile.read((char *)(&temp), sizeof(unsigned int));
	cout << "NumNodes = " << NumNodes << " NumEdges = " << NumEdges << endl;
	unsigned int uniqueCount = 0;
	unsigned int vId1, pId1, vId2, pId2;
	unsigned int prevDiffVertex = 1;
	myFile.read((char *)&vId1, sizeof(unsigned int));
	myFile.read((char *)&pId1, sizeof(unsigned int));
	for(unsigned int loop_1 = 0; loop_1 < NumPages; loop_1++)
	{
		myFile.read((char *)&vId2, sizeof(unsigned int));
		myFile.read((char *)&pId2, sizeof(unsigned int));
		cout << vId2 << " " << pId2 << endl;
		if(vId1 == vId2)
		{
//		cout << vId1 << " " << vId2 << endl;
			if(prevDiffVertex == 1)
			{
				uniqueCount += 1;
			}
			prevDiffVertex = 0;
		}
		else {
			prevDiffVertex = 1;
		}
		vId1 = vId2;

	}
	cout << "uniqueCount = " << uniqueCount << " percentage = " << ((uniqueCount * 1.0) / NumNodes) * 100 << endl;
	return 0;
}
