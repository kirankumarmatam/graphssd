#include <iostream>
#include <cstdint>
#include <fstream>
using namespace std;

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cout << "usage: ./a.out filename" << endl;
		return 0;
	}
	ifstream myFile;
	myFile.open(argv[1], ios::in);
	if(!myFile.is_open()) { cout << "file not opening" << endl; return 0;}
	unsigned int NumNodes;
	uint64_t NumEdges;
	myFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myFile.read((char *)(&NumEdges), sizeof(uint64_t));
	cout << "NumNodes = " << NumNodes << " NumEdges = " << NumEdges << endl;
	uint64_t *RowPtr = (uint64_t *)malloc((NumNodes+1) * sizeof(uint64_t));
	unsigned int *ColPtr = (unsigned int *) malloc((NumEdges) * sizeof(unsigned int));
	myFile.read((char *)(RowPtr), (NumNodes+1) * sizeof(uint64_t));
	myFile.read((char *)(ColPtr), (NumEdges) * sizeof(unsigned int));
	for(uint64_t loop_1 = 0; loop_1 < NumNodes+1; loop_1++)
	{
		cout << loop_1 << " " << RowPtr[loop_1] << endl;
	}
	for(uint64_t loop_1 = 0; loop_1 < NumEdges; loop_1++)
	{
		cout << loop_1 << " " << ColPtr[loop_1] << endl;
	}
	return 0;
}
