#include <iostream>
#include <cstdint>
#include <fstream>
#include "stdlib.h"
using namespace std;

#define SSD_PAGE_SIZE 16384

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cout << "usage: ./a.out input_filename output_filename" << endl;
		return 0;
	}
	ifstream myFile;
	myFile.open(argv[1], ios::in);
	if(!myFile.is_open()) { cout << "file not opening" << endl; return 0;}
	unsigned int NumSSDpages;
	myFile.read((char *)(&NumSSDpages), sizeof(unsigned int));

	ofstream outFile;
	outFile.open(argv[2], ofstream::out);
	if(!outFile.is_open()) {cout << "file not opening" << endl; return 0;}
	char *data;
	data = (char *)malloc(SSD_PAGE_SIZE);
	for(unsigned int i = 0; i < NumSSDpages; i++)
	{
		myFile.read((char *)data, SSD_PAGE_SIZE);
		for(unsigned int j = 0 ; j < SSD_PAGE_SIZE / sizeof(unsigned int); j++)
		{
			outFile << j << "=" << ((unsigned int *)data)[j] << "; ";
		}
		outFile << endl;
	}
	return 0;
}
