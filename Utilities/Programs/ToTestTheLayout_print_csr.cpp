#include <iostream>
#include <cstdint>
#include <fstream>
#include <assert.h>
using namespace std;

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cout << "usage: ./a.out filename" << endl;
		return 0;
	}
	ifstream myFile;
	myFile.open(argv[1], ios::in);
	if(!myFile.is_open()) { cout << "file not opening" << endl; return 0;}
	unsigned int NumNodes;
	uint64_t NumEdges;
	myFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myFile.read((char *)(&NumEdges), sizeof(uint64_t));
	cout << "NumNodes = " << NumNodes << " NumEdges = " << NumEdges << endl;
	unsigned long long int index;
	for(unsigned int i = 0; i <= NumNodes; i++)
	{
		myFile.read((char *)(&index), sizeof(unsigned long long int));
//		cout << "I["<<i<<"] = " << index << endl;
//		assert((i < 10) || (index != 0));
	}

	unsigned edge;
	unsigned long long int numOfZeros = 0;
	for(unsigned long long int i = 0; i < NumEdges; i++)
	{
		myFile.read((char *)(&edge), sizeof(unsigned int));
		cout << "E["<<i<<"] = " << edge << endl;
		if(edge == 0)
			numOfZeros++;
	}
	cout << "numOfZeros = " << numOfZeros << endl;
	return 0;
}
