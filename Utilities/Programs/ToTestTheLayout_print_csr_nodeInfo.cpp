#include <iostream>
#include <cstdint>
#include <fstream>
using namespace std;

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cout << "usage: ./a.out filename" << endl;
		return 0;
	}
	ifstream myFile;
	myFile.open(argv[1], ios::in);
	if(!myFile.is_open()) { cout << "file not opening" << endl; return 0;}
	unsigned int NumNodes;
	uint64_t NumEdges;
	myFile.read((char *)(&NumNodes), sizeof(unsigned int));
	myFile.read((char *)(&NumEdges), sizeof(uint64_t));
	cout << "NumNodes = " << NumNodes << " NumEdges = " << NumEdges << endl;
	return 0;
}
