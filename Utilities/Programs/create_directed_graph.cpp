#include <iostream>
#include <cstdint>
#include <fstream>
#include "stdlib.h"
using namespace std;

#define SSD_PAGE_SIZE 16384

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cout << "usage: ./a.out input_filename output_filename" << endl;
		return 0;
	}
	ifstream myFile;
	myFile.open(argv[1], ios::in);
	if(!myFile.is_open()) { cout << "file not opening" << endl; return 0;}

	ofstream outFile;
	outFile.open(argv[2], ofstream::out);
	if(!outFile.is_open()) {cout << "file not opening" << endl; return 0;}
	unsigned long long int src, dst;
	while(myFile >> src >>  dst)
	{
		outFile << src << " " << dst << endl;
		outFile << dst << " " << src << endl;
	}
	myFile.close();
	outFile.close();
	return 0;
}
