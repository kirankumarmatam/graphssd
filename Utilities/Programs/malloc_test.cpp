#include <iostream>
#include <stdlib.h>

int main() {
	    void *block = malloc(1024LL * 1024LL * 1024LL * 6);
	        if (block)
			        std::cout << "Allocated 6 Gig block\n";
		    else
			            std::cout << "Unable to allocate 6 Gig block.\n";
		        return 0;
}
