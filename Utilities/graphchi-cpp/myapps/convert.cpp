/**
 * @file
 * @author  Aapo Kyrola <akyrola@cs.cmu.edu>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Copyright [2012] [Aapo Kyrola, Guy Blelloch, Carlos Guestrin / Carnegie Mellon University]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

 *
 * @section DESCRIPTION
 *
 * Template for GraphChi applications. To create a new application, duplicate
 * this template.
 */



#include <string>
#include <iostream>
#include <fstream>
#include "graphchi_basic_includes.hpp"
#include <set>
#include <vector>
#include "engine/graphchi_engine.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#define UseMMAP 1
#define PRINTRESULT 0

using namespace graphchi;
using namespace std;

/**
  * Type definitions. Remember to create suitable graph shards using the
  * Sharder-program.
  */
typedef vid_t VertexDataType;
typedef vid_t EdgeDataType;

typedef uint64_t EdgeIndexType;
//typedef unsigned int EdgeIndexType;

EdgeIndexType *adjcount;
unsigned int *Array;
unsigned int NumNodes;
vector< set<unsigned int> > neigh;

#if(UseMMAP == 1)
int fd;
void * temp_array_address;
uint64_t FileSize;
#endif//UseMMAP

/**
  * GraphChi programs need to subclass GraphChiProgram<vertex-type, edge-type>
  * class. The main logic is usually in the update function.
  */
struct ConvertProgram : public GraphChiProgram<VertexDataType, EdgeDataType>
{
	/**
	 *  Vertex update function.
	 */
	void update(graphchi_vertex<VertexDataType, EdgeDataType> &vertex, graphchi_context &gcontext)
	{
		if (gcontext.iteration == 0)
		{
			for(int i=0; i < vertex.num_edges(); i++)
			{
				unsigned int tmp=vertex.edge(i)->vertex_id();
				neigh[vertex.id()].insert(tmp);
			}
				adjcount[vertex.id()+1]=neigh[vertex.id()].size();
				neigh[vertex.id()].clear();
		}
		else if (gcontext.iteration==1)
		{
			for(int i=0; i < vertex.num_edges(); i++)
			{
				unsigned int tmp=vertex.edge(i)->vertex_id();
				neigh[vertex.id()].insert(tmp);
			}

			unsigned int i=0;
			for (set<unsigned int>::iterator it=neigh[vertex.id()].begin(); it!=neigh[vertex.id()].end(); ++it)
			{
				Array[ adjcount[vertex.id()] + (i++) ]=*it;
			}
				neigh[vertex.id()].clear();
		}
	}

	/**
	 * Called before an iteration starts.
	 */
	void before_iteration(int iteration, graphchi_context &gcontext)
	{
		/*converged = iteration > 0;*/
		if (gcontext.iteration == 0)
		{
			gcontext.set_last_iteration(1);
		}
	}

	/**
	 * Called after an iteration has finished.
	 */
	void after_iteration(int iteration, graphchi_context &gcontext)
	{
		if (gcontext.iteration == 0)
		{
			for (vid_t i=1; i<NumNodes+1; i++)
			{
				adjcount[i]+=adjcount[i-1];
			}
#if(UseMMAP == 0)
			Array=(unsigned int *) calloc(adjcount[NumNodes],sizeof(unsigned int));
#else
			FileSize += adjcount[NumNodes] * sizeof(unsigned int);
			int result = lseek64(fd, FileSize, SEEK_SET);
			if (result == -1) {
				close(fd);
				perror("Error calling lseek64() to 'stretch' the file");
				exit(EXIT_FAILURE);
			}
			result = write(fd, "", 1);
			if (result != 1) {
				close(fd);
				perror("Error writing last byte of the file");
				exit(EXIT_FAILURE);
			}
			
			temp_array_address = mmap64(0, FileSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
			if (temp_array_address == MAP_FAILED) {
				close(fd);
				perror("Error mmapping the file");
				exit(EXIT_FAILURE);
			}

			Array=(unsigned int *)((char *)temp_array_address + sizeof(unsigned int) + sizeof(EdgeIndexType) + ((uint64_t)(NumNodes+1))*(sizeof(EdgeIndexType)));
			for(uint64_t loop_1=0; loop_1 < adjcount[NumNodes]; loop_1++)
			{
				Array[loop_1] = 0;
			}
#endif//UseMMAP
		}
	}

	/**
	 * Called before an execution interval is started.
	 */
	void before_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
	{
	}

	/**
	 * Called after an execution interval has finished.
	 */
	void after_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
	{
	}

};

int main(int argc, const char ** argv)
{
	/* GraphChi initialization will read the command line
	   arguments and the configuration file. */
	graphchi_init(argc, argv);

	/* Metrics object for keeping track of performance counters
	   and other information. Currently required. */
	metrics m("Convert");

	/* Basic arguments for application */
	std::string filename = get_option_string("file");  // Base filename
	int niters           = get_option_int("niters", 9999); // Number of iterations
	bool scheduler       = get_option_int("scheduler", 0); // Whether to use selective scheduling

	std::string output_filename = get_option_string("output_file", "csr_output.txt");

	/* Detect the number of shards or preprocess an input to create them */
	int nshards          = convert_if_notexists<EdgeDataType>(filename,
						   get_option_string("nshards", "auto"));

	/* Run */
	ConvertProgram program;
	graphchi_engine<VertexDataType, EdgeDataType> engine(filename, nshards, scheduler, m);

	NumNodes = engine.num_vertices();

#if(UseMMAP == 1)
	fd = open(output_filename.data(), O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
	if (fd == -1) {
		perror("Error opening file for writing");
		exit(EXIT_FAILURE);
	}

	/* Stretch the file size to the size of the (mmapped) array of ints
	 */
	FileSize = sizeof(unsigned int) + sizeof(EdgeIndexType) + ((uint64_t)(NumNodes+1))*sizeof(EdgeIndexType);
#endif//UseMMAP
	std::cout << "Before allocating rowPtr" << std::endl;
	adjcount=(EdgeIndexType *)calloc(NumNodes+1,sizeof(EdgeIndexType));
	std::cout << "After allocating rowPtr" << std::endl;
	neigh.resize(NumNodes);
	std::cout << "Before running engine" << std::endl;


	engine.run(program, niters);

	/* Report execution metrics */
	metrics_report(m);

#if(PRINTRESULT == 1)
	for (unsigned int i=0; i<NumNodes+1; i++)
	{
		cout << adjcount[i] << endl;
	}
	cout << endl;
	for (unsigned int i=0; i<adjcount[NumNodes]; i++)
	{
		cout << Array[i] << endl;
	}
	cout << endl;
#endif//PRINTRESULT
#if(UseMMAP == 0)
	ofstream myFlashFile;

	myFlashFile.open(output_filename.data(), fstream::in | fstream::out | fstream::trunc);
	if (!myFlashFile.is_open())
		cout << " Cannot open csr output file!" << endl;

	cout << "Opened file for writing" << endl;
	EdgeIndexType numOfEdgesInCSR = adjcount[NumNodes];
	myFlashFile.write((char *)(&NumNodes), sizeof(unsigned int));
	myFlashFile.write((char *)(&numOfEdgesInCSR), sizeof(EdgeIndexType));
	myFlashFile.write((char *)(adjcount), (NumNodes + 1) * sizeof(EdgeIndexType));
	myFlashFile.write((char *)(Array), (numOfEdgesInCSR) * sizeof(unsigned int));
	cout << "Wrote CSR matrix for file" << endl;
	myFlashFile.close();
#else	
	*((unsigned int *)temp_array_address) = NumNodes;
	cout << "NumNodes = " << NumNodes << endl;
	*((EdgeIndexType *)((char *)temp_array_address + sizeof(unsigned int))) = adjcount[NumNodes];
	cout << "NumEdges = " << adjcount[NumNodes] << endl;
	for (uint64_t i=0; i<=NumNodes; i++)
	{
		*((EdgeIndexType *)((char *)temp_array_address + sizeof(unsigned int) + sizeof(EdgeIndexType)) + i) = adjcount[i];
	}
	cout << "Finished generating CSR file " << adjcount[NumNodes] << endl;
	if (munmap(temp_array_address, FileSize) == -1) {
		perror("Error un-mmapping the file");
	}
	close(fd);
#endif//UseMMAP

	return 0;
}
