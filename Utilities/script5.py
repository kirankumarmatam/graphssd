#This script is to run the programs for generating csr and graphSSD layouts in google cloud server

import os

Datasets = ["/Data/Benchmarks/SNAP/LargeGraphs/YahooWebScope/ydata-yaltavista-webmap-v1_0_links.txt", "/Data/Benchmarks/SNAP/LargeGraphs/graph500_512M.txt", "/Data/Benchmarks/SNAP/LargeGraphs/graph500_1G.txt"]
Memory = ["3000", "3600","7200"]
Filetype = ["adjlist", "edgelist", "edgelist"]
Datasets_csr = ["/Data/Benchmarks/SNAP/ProcessedGraphs/csr_files/YahooWebScope_csr_backup.txt","/Data/Benchmarks/SNAP/ProcessedGraphs/csr_files/graph500_csr_512M.txt","/Data/Benchmarks/SNAP/ProcessedGraphs/csr_files/graph500_csr_1G.txt"]
Datasets_graphSSD_flash = ["/Data/Benchmarks/SNAP/ProcessedGraphs/graphSSD_files/YahooWebScope_flash_backup.txt","/Data/Benchmarks/SNAP/ProcessedGraphs/graphSSD_files/graph500_flash_512M.txt","/Data/Benchmarks/SNAP/ProcessedGraphs/graphSSD_files/graph500_flash_1G.txt"]
Datasets_graphSSD_gtl = ["/Data/Benchmarks/SNAP/ProcessedGraphs/graphSSD_files/YahooWebScope_gtl_backup.txt","/Data/Benchmarks/SNAP/ProcessedGraphs/graphSSD_files/graph500_gtl_512M.txt","/Data/Benchmarks/SNAP/ProcessedGraphs/graphSSD_files/graph500_gtl_1G.txt"]

#for i in range(len(Datasets)):
for i in range(len(Datasets)-2):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Utilities/graphchi-cpp")
	os.system("make myapps/convert")
	os.system("bin/myapps/convert file "+ Datasets[i] +" output_file " + Datasets_csr[i] + " membudget_mb " + Memory[i] + " filetype "+Filetype[i])
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Utilities/Layout_generators/Layout_generator_csr_to_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering temp " + Datasets_csr[i] + " " + Datasets_graphSSD_flash[i] + " " + Datasets_graphSSD_gtl[i])
