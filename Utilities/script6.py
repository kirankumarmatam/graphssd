#This script is to run the programs for generating csr and graphSSD layouts in google cloud server

import os

Datasets = ["/Datasets/Benchmarks/SNAP/GraphChi/soc-LiveJournal1.net","/Datasets/Benchmarks/SNAP/GraphChi/graph500_32M.txt","/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.net","/Datasets/Benchmarks/SNAP/GraphChi/graph500_128M.txt"]
Memory = ["20","225","800", "900"]
os.system("cgcreate -g memory,cpu:graphChiGroup")
os.system("cgset -r memory.limit_in_bytes=$((4*1024*1024*1024)) graphChiGroup")
for i in range(len(Datasets)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Utilities/graphchi-cpp")
	os.system("cgexec -g memory,cpu:graphChiGroup bin/example_apps/trianglecounting file "+ Datasets[i] + " membudget_mb " + Memory[i] + " filetype edgelist")
