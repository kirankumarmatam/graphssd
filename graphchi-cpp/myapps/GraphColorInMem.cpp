/**
 * @file
 * @author  Aapo Kyrola <akyrola@cs.cmu.edu>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Copyright [2012] [Aapo Kyrola, Guy Blelloch, Carlos Guestrin / Carnegie Mellon University]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

 *
 * @section DESCRIPTION
 *
 * Template for GraphChi applications. To create a new application, duplicate
 * this template.
 */



#include <string>

#include "graphchi_basic_includes.hpp"
#include "graphchi_types.hpp"

#define INFINI 0xffffffff

#define INS 3
#define TENTAINS 2
#define NOTINS 1
#define UNKNOWN 0
#define COLORED 4
#define PRINTRESULT 0

using namespace graphchi;
using namespace std;
unsigned int *valid_degree;
typedef unsigned short StatusType;
StatusType *status;
int *id;
unsigned int NumNodes;
vid_t TotalColor;
vid_t step;
int isMIS;

/**
  * Type definitions. Remember to create suitable graph shards using the
  * Sharder-program.
  */
typedef vid_t VertexDataType;
typedef vid_t EdgeDataType; // modify src/graphchi_types.hpp and src/preprocessing/conversions.hpp
typedef unsigned long long int EdgeIndexType;

/**
  * GraphChi programs need to subclass GraphChiProgram<vertex-type, edge-type>
  * class. The main logic is usually in the update function.
  */
struct GraphColorProgram : public GraphChiProgram<VertexDataType, EdgeDataType>
{
	bool converged;
	/**
	 *  Vertex update function.
	 */
	bool HasUnknown;
	bool fired;
	void update(graphchi_vertex<VertexDataType, EdgeDataType> &vertex, graphchi_context &gcontext)
	{
		if (gcontext.iteration!=gcontext.last_iteration)
		{
			vid_t vid=vertex.id();
			vid_t nid;
			switch(step)
			{
			case 1 :
			{
				if(status[vid+NumNodes]==UNKNOWN)
				{

					for(int i=0; i < vertex.num_inedges(); i++)
					{
						nid=(vertex.inedge(i))->vertex_id();
						if(status[nid+NumNodes]==UNKNOWN)
						{
							valid_degree[vid]++;
						}
					}
//					float p=(float)rand()/(float)(RAND_MAX);
//					if (p*2*valid_degree[vid]<1.0)
					if( (valid_degree[vid] == 0) || (valid_degree[vid] !=0 && (rand() % (2*valid_degree[vid]) == 1)))
					{
						status[vid]=TENTAINS;
						for(int i=0; i < vertex.num_outedges(); i++)
						{
							nid=(vertex.outedge(i))->vertex_id();
							//TODO lock
                                                        if ((int)vid<id[nid] || id[nid]==-1)
					                {
								id[nid]=vid;
							}
						}
						fired=1;
					}
				}
				converged=0;
				break;
			}
			case 2 :
			{
				if(status[vid]==TENTAINS)
				{
					if ((int)vid<id[vid] || id[vid]==-1)
					{
						//TODO lock
						status[vid]=INS;
						for(int i=0; i < vertex.num_outedges(); i++)
						{
							nid=(vertex.outedge(i))->vertex_id();
							//TODO lock
							if (status[nid]<INS)
							{
								status[nid]=NOTINS;
							}
						}
					}
					else
					{
						status[vid]=UNKNOWN;// it's single thread so it's fine. if using multi thread, should make sure this vertex is not in NOINS state;
					}
				}
				converged=0;
				break;
			}
			case 3 :
			{
				if(status[vid]==UNKNOWN)
				{
					HasUnknown=1;
				}
				converged=0;
				break;
			}
			case 4 :
			{
				if(status[vid]==INS)
				{
					status[vid]=COLORED+TotalColor;
				}
				else if(status[vid]==NOTINS)
				{
					status[vid]=UNKNOWN;
					converged=0;
				}
				break;
			}
			}
		}
		else
		{
#if(PRINTRESULT==1)
			cout << "v: " << vertex.id() << " color is: " << status[vertex.id()]-COLORED << endl;
#endif
		}
	}

	/**
	 * Called before an iteration starts.
	 */
	void before_iteration(int iteration, graphchi_context &gcontext)
	{
		converged = iteration > 0;
		fired=0;
		HasUnknown=0;
	}

	/**
	 * Called after an iteration has finished.
	 */
	void after_iteration(int iteration, graphchi_context &gcontext)
	{
#if(PRINTRESULT==1)
		std::cout << "iteration " << iteration << " step " << step << " finished" <<std::endl;
#endif
		switch (step)
		{
		case 0:
		{
			step=1;
			break;
		}
		case 1:
		{
			if (fired)
			{
				step=2;
			}
			else
			{
				step=1;
			}
			break;
		}
		case 2:
		{
			step=3;
			break;
		}
		case 3:
		{
			if (HasUnknown)
			{
				step=1;
				memcpy(status+NumNodes,status,(EdgeIndexType)NumNodes*sizeof(StatusType)/sizeof(char));
			}
			else
			{
				step=4;
				if(isMIS == 1)
				{
					converged = 1;
				}
			}
			memset(id,-1,(EdgeIndexType)NumNodes*sizeof(int)/sizeof(char));
			memset(valid_degree,0,(EdgeIndexType)NumNodes*sizeof(unsigned int)/sizeof(char));

			break;
		}
		case 4:
		{
			step=1;
			TotalColor++;
			memcpy(status+NumNodes,status,(EdgeIndexType)NumNodes*sizeof(StatusType)/sizeof(char));
			break;
		}
		}

		if (converged)
		{
			std::cout << "Total number of colors is " << TotalColor <<std::endl;
#if(PRINTRESULT==1)
			if (iteration!=gcontext.last_iteration)
			{
				gcontext.set_last_iteration(iteration+1);
			}
#else
			gcontext.set_last_iteration(iteration);
#endif
		}
	}

	/**
	 * Called before an execution interval is started.
	 */
	void before_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
	{
	}

	/**
	 * Called after an execution interval has finished.
	 */
	void after_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
	{
	}

};

int main(int argc, const char ** argv)
{
	/* GraphChi initialization will read the command line
	   arguments and the configuration file. */
	graphchi_init(argc, argv);

	/* Metrics object for keeping track of performance counters
	   and other information. Currently required. */
	metrics m("GraphColor");

	/* Basic arguments for application */
	std::string filename = get_option_string("file");  // Base filename
	int niters           = get_option_int("niters", 20); // Number of iterations
	bool scheduler       = get_option_int("scheduler", 0); // Whether to use selective scheduling
	isMIS = get_option_int("isMIS", 0);

	/* Detect the number of shards or preprocess an input to create them */
	int nshards          = convert_if_notexists<EdgeDataType>(filename,
	                       get_option_string("nshards", "auto"));

	/* Run */
	GraphColorProgram program;
	graphchi_engine<VertexDataType, EdgeDataType> engine(filename, nshards, scheduler, m);

	NumNodes = engine.num_vertices();
	valid_degree=(unsigned int *)calloc(NumNodes,sizeof(unsigned int));
	id=(int *)calloc(NumNodes,sizeof(int));
	memset(id,-1,(EdgeIndexType)NumNodes*sizeof(int)/sizeof(char));
	status=(StatusType *)calloc((EdgeIndexType)NumNodes*2,sizeof(StatusType));
	TotalColor=0;
	step=1;

	engine.run(program, niters);

	/* Report execution metrics */
	metrics_report(m);
	return 0;
}
