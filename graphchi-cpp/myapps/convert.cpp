/**
 * @file
 * @author  Aapo Kyrola <akyrola@cs.cmu.edu>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Copyright [2012] [Aapo Kyrola, Guy Blelloch, Carlos Guestrin / Carnegie Mellon University]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

 *
 * @section DESCRIPTION
 *
 * Template for GraphChi applications. To create a new application, duplicate
 * this template.
 */



#include <string>
#include <iostream>
#include <fstream>
#include "graphchi_basic_includes.hpp"
#include <set>
#include <vector>
#include "engine/graphchi_engine.hpp"

#define PRINTRESULT 0

using namespace graphchi;
using namespace std;

/**
  * Type definitions. Remember to create suitable graph shards using the
  * Sharder-program.
  */
typedef vid_t VertexDataType;
typedef vid_t EdgeDataType;

typedef uint64_t EdgeIndexType;
//typedef unsigned int EdgeIndexType;

EdgeIndexType *adjcount;
unsigned int *Array;
unsigned int NumNodes;
vector< set<unsigned int> > neigh;

/**
  * GraphChi programs need to subclass GraphChiProgram<vertex-type, edge-type>
  * class. The main logic is usually in the update function.
  */
struct ConvertProgram : public GraphChiProgram<VertexDataType, EdgeDataType>
{
	/**
	 *  Vertex update function.
	 */
	void update(graphchi_vertex<VertexDataType, EdgeDataType> &vertex, graphchi_context &gcontext)
	{
		if (gcontext.iteration == 0)
		{
			for(int i=0; i < vertex.num_edges(); i++)
			{
				unsigned int tmp=vertex.edge(i)->vertex_id();
				neigh[vertex.id()].insert(tmp);
			}
				adjcount[vertex.id()+1]=neigh[vertex.id()].size();
		}
		else if (gcontext.iteration==1)
		{
			unsigned int i=0;
			for (set<unsigned int>::iterator it=neigh[vertex.id()].begin(); it!=neigh[vertex.id()].end(); ++it)
			{
				Array[ adjcount[vertex.id()] + (i++) ]=*it;
			}
		}
	}

	/**
	 * Called before an iteration starts.
	 */
	void before_iteration(int iteration, graphchi_context &gcontext)
	{
		/*converged = iteration > 0;*/
		if (gcontext.iteration == 0)
		{
			gcontext.set_last_iteration(1);
		}
	}

	/**
	 * Called after an iteration has finished.
	 */
	void after_iteration(int iteration, graphchi_context &gcontext)
	{
		if (gcontext.iteration == 0)
		{
			for (vid_t i=1; i<NumNodes+1; i++)
			{
				adjcount[i]+=adjcount[i-1];
			}
			Array=(unsigned int *) calloc(adjcount[NumNodes],sizeof(unsigned int));
		}
	}

	/**
	 * Called before an execution interval is started.
	 */
	void before_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
	{
	}

	/**
	 * Called after an execution interval has finished.
	 */
	void after_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
	{
	}

};

int main(int argc, const char ** argv)
{
	/* GraphChi initialization will read the command line
	   arguments and the configuration file. */
	graphchi_init(argc, argv);

	/* Metrics object for keeping track of performance counters
	   and other information. Currently required. */
	metrics m("Convert");

	/* Basic arguments for application */
	std::string filename = get_option_string("file");  // Base filename
	int niters           = get_option_int("niters", 9999); // Number of iterations
	bool scheduler       = get_option_int("scheduler", 0); // Whether to use selective scheduling

	std::string output_filename = get_option_string("output_file", "csr_output.txt");

	/* Detect the number of shards or preprocess an input to create them */
	int nshards          = convert_if_notexists<EdgeDataType>(filename,
						   get_option_string("nshards", "auto"));

	/* Run */
	ConvertProgram program;
	graphchi_engine<VertexDataType, EdgeDataType> engine(filename, nshards, scheduler, m);

	NumNodes = engine.num_vertices();
	adjcount=(EdgeIndexType *)calloc(NumNodes+1,sizeof(EdgeIndexType));
	neigh.resize(NumNodes);

	engine.run(program, niters);

	/* Report execution metrics */
	metrics_report(m);

#if(PRINTRESULT == 1)
	for (unsigned int i=0; i<NumNodes+1; i++)
	{
		cout << adjcount[i] << endl;
	}
	cout << endl;
	for (unsigned int i=0; i<adjcount[NumNodes]; i++)
	{
		cout << Array[i] << endl;
	}
	cout << endl;
#endif//PRINTRESULT
	ofstream myFlashFile;

	myFlashFile.open(output_filename.data(), fstream::in | fstream::out | fstream::trunc);
	if (!myFlashFile.is_open())
		cout << " Cannot open csr output file!" << endl;

	cout << "Opened file for writing" << endl;
	EdgeIndexType numOfEdgesInCSR = adjcount[NumNodes];
	myFlashFile.write((char *)(&NumNodes), sizeof(unsigned int));
	myFlashFile.write((char *)(&numOfEdgesInCSR), sizeof(EdgeIndexType));
	myFlashFile.write((char *)(adjcount), (NumNodes + 1) * sizeof(EdgeIndexType));
	myFlashFile.write((char *)(Array), (numOfEdgesInCSR) * sizeof(unsigned int));
	cout << "Wrote CSR matrix for file" << endl;
	myFlashFile.close();

	return 0;
}
