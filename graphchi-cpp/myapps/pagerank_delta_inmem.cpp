/**
 * @file
 * @author  Aapo Kyrola <akyrola@cs.cmu.edu>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Copyright [2012] [Aapo Kyrola, Guy Blelloch, Carlos Guestrin / Carnegie Mellon University]
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 
 *
 * @section DESCRIPTION
 *
 * Simple pagerank implementation. Uses the basic vertex-based API for
 * demonstration purposes. A faster implementation uses the functional API,
 * "pagerank_functional".
 */

#include <string>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <assert.h>

#define GRAPHCHI_DISABLE_COMPRESSION


#include "graphchi_basic_includes.hpp"
#include "util/toplist.hpp"

using namespace graphchi;
 
#define THRESHOLD 0.4
#define RANDOMRESETPROB 0.15
#define ALPHA 0.85

typedef float VertexDataType;
typedef float EdgeDataType;
typedef float ValueType;

struct PagerankProgram : public GraphChiProgram<VertexDataType, EdgeDataType> {
    
    /**
      * Called before an iteration starts. Not implemented.
      */
    void before_iteration(int iteration, graphchi_context &info) {
    }
    
    /**
      * Called after an iteration has finished. Not implemented.
      */
    void after_iteration(int iteration, graphchi_context &ginfo) {
	    std::cout << "num_iteration = " << iteration << " num_vertices = " << ginfo.nvertices << " num active vertices? = " << ginfo.scheduler->num_tasks() << std::endl;
    }
    
    /**
      * Called before an execution interval is started. Not implemented.
      */
    void before_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &ginfo) {        
    }
    
    
    /**
      * Pagerank update function.
      */
    void update(graphchi_vertex<VertexDataType, EdgeDataType> &v, graphchi_context &ginfo) {
	    float sum=0;
	    if (ginfo.iteration == 0) {
		    /* On first iteration, initialize vertex */
		    for(int i=0; i < v.num_outedges(); i++) {
			    graphchi_edge<float> * edge = v.outedge(i);
			    edge->set_data(1.0f / v.num_outedges());
			    if (ginfo.scheduler != NULL)
				    ginfo.scheduler->add_task(edge->vertex_id());
		    }
		    v.set_data(RANDOMRESETPROB);

		    /* If using selective scheduling, schedule myself for next iteration */
		    if (ginfo.scheduler != NULL)
			    ginfo.scheduler->add_task(v.id());
	    } else {
		    /* Compute the sum of neighbors' weighted pageranks */
		    for(int i=0; i < v.num_inedges(); i++) {
			    float val = v.inedge(i)->get_data();
//			    std::cout << v.id() << " " << v.num_inedges() << " " << v.inedge(i)->vertex_id() << " " << val << std::endl;
			    sum += val;
		    }

		    /* Compute my pagerank */
		    float pagerank = RANDOMRESETPROB + (1 - RANDOMRESETPROB) * sum;
		    float oldvalue = v.get_data();
		    float delta = (float) fabs(oldvalue - pagerank);
		    bool significant_change = (delta >= THRESHOLD);

//		    std::cout << v.id() <<" O = " << oldvalue << " N = " <<  pagerank << std::endl;

		    if (v.num_outedges() > 0) {
			    float pagerankcont = pagerank/v.num_outedges();
			    for(int i=0; i < v.num_outedges(); i++) {
				    graphchi_edge<float> * edge = v.outedge(i);

				    /* If using selective scheduling, and the change was larger than
				       a threshold, add neighbor to schedule. */
				    if (ginfo.scheduler != NULL) {
					    if (significant_change) {
						    ginfo.scheduler->add_task(edge->vertex_id());
					    }
				    }
				    edge->set_data(pagerankcont);
			    }
		    }
		    v.set_data(pagerank);

		    /* Keep track of the progression of the computation */
		    ginfo.log_change(delta);
	    }
    }
    
};

/**
  * Faster version of pagerank which holds vertices in memory. Used only if the number
  * of vertices is small enough.
  */
struct PagerankProgramInmem : public GraphChiProgram<VertexDataType, EdgeDataType> {
	bool *dirty;
	bool *oddEven;
	ValueType *PageRank;
	bool OddEven;
	unsigned int *degree;
	PagerankProgramInmem(int nvertices)
	{
		std::cout << "nvertices = " << nvertices << " 2*nvertices = " << 2*(unsigned int)nvertices << std::endl;
		dirty = (bool*)calloc(2*(unsigned int)nvertices, sizeof(bool));
		assert(dirty != NULL);
		std::fill_n(dirty, nvertices, 1);
		PageRank = (ValueType*)calloc(2*(unsigned int)nvertices, sizeof(ValueType));
		assert(PageRank != NULL);
		std::fill_n(PageRank,nvertices,RANDOMRESETPROB);
		std::fill_n(PageRank+nvertices, nvertices, 1);
		oddEven = (bool*)calloc(nvertices, sizeof(bool));
		assert(oddEven != NULL);
		std::fill_n(oddEven, nvertices, 0);
		degree = (unsigned int *) calloc(nvertices, sizeof(unsigned int));
		assert(degree != NULL);
		OddEven = 0;
	}

	/**
	 * Called before an iteration starts. Not implemented.
	 */
	void before_iteration(int iteration, graphchi_context &info) {
		std::cout << "starting iteration = " << iteration << std::endl;
	}

	/**
	 * Called after an iteration has finished. Not implemented.
	 */
	void after_iteration(int iteration, graphchi_context &ginfo) {
		if(ginfo.iteration != 0)
		{
			unsigned int totalActiveVertices=0;
			for(unsigned int j = 0; j < ginfo.nvertices; j++)
			{
				if(dirty[j+OddEven*(ginfo.nvertices)] == 1)
				{
					oddEven[j] = 1 - oddEven[j];
					dirty[j+OddEven*(ginfo.nvertices)] = 0;
					totalActiveVertices += 1;
				}
			}
			OddEven=!OddEven;

			std::cout << "num_iteration = " << iteration << " num_vertices = " << ginfo.nvertices << " num active vertices? = " << ginfo.scheduler->num_tasks() << std::endl;
			std::cout << "num_iteration = " << iteration << " num_vertices = " << ginfo.nvertices << " num active vertices = " << totalActiveVertices << std::endl;
		}
	}

	void update(graphchi_vertex<VertexDataType, EdgeDataType> &v, graphchi_context &ginfo) {
		if(ginfo.iteration == 0)
		{
			degree[v.id()] = v.num_edges();
			ginfo.scheduler->add_task(v.id());
		}
		else 
		{
//				std::cout << ginfo.iteration << " " << v.id() << std::endl;
			if(dirty[v.id() + (unsigned int)(OddEven*ginfo.nvertices)] != 0)
			{
//				std::cout << ginfo.iteration << " " << v.id() << std::endl;
				ValueType PageRank_new = 0;
				for (int j = 0; j < v.num_edges(); j++)
				{
					unsigned int tmp_v = v.edge(j)->vertexid;
					if(ginfo.iteration == 1)
					{
						PageRank_new += 1.0 / (degree[tmp_v]);                                                  //set the dirty bit(need to lock now)
					} else {
						PageRank_new += PageRank[(unsigned int)tmp_v+oddEven[tmp_v]*(ginfo.nvertices)] / (degree[tmp_v]);//set the dirty bit(need to lock now)
					}
				}
				PageRank[(unsigned int)(v.id())+(1-OddEven)*(ginfo.nvertices)] = RANDOMRESETPROB + ALPHA * PageRank_new;
				if(fabs(PageRank[(unsigned int)(v.id())+(1-OddEven)*(ginfo.nvertices)] - PageRank[(unsigned int)(v.id())+oddEven[v.id()]*(ginfo.nvertices)]) > THRESHOLD)//set the dirty bit(need to lock now)
				{
					for (int j=0; j<v.num_edges(); j++)
					{
						unsigned int tmp_v=v.edge(j)->vertexid;
						//#pragma omp critical
						{
							dirty[tmp_v + (1-OddEven)*(ginfo.nvertices)]=1;
//						if(ginfo.iteration > 1)	std::cout << "dirty = " << tmp_v << std::endl;
						}
						if (ginfo.scheduler != NULL) {
							ginfo.scheduler->add_task(tmp_v);
						}
					}
				}
			}
		}
	}
};

int main(int argc, const char ** argv) {
    graphchi_init(argc, argv);
    metrics m("pagerank");
    global_logger().set_log_level(LOG_DEBUG);

    /* Parameters */
    std::string filename    = get_option_string("file"); // Base filename
    int niters              = get_option_int("niters", 4);
    bool scheduler          = true;                    // Non-dynamic version of pagerank. /*changed to true*/
//    int ntop                = get_option_int("top", 20);
    
    /* Process input file - if not already preprocessed */
    int nshards             = convert_if_notexists<EdgeDataType>(filename, get_option_string("nshards", "auto"));

    /* Run */
    graphchi_engine<float, float> engine(filename, nshards, scheduler, m); 
    engine.set_modifies_inedges(false); // Improves I/O performance.
    
    bool inmemmode = engine.num_vertices() * sizeof(EdgeDataType) < (size_t)engine.get_membudget_mb() * 1024L * 1024L;
    inmemmode = true;
    if (inmemmode) {
        logstream(LOG_INFO) << "Running Pagerank by holding vertices in-memory mode!" << std::endl;
        engine.set_modifies_outedges(false);
        engine.set_disable_outedges(true);
        engine.set_only_adjacency(true);
        PagerankProgramInmem program(engine.num_vertices());
        engine.run(program, niters);
    } else {
        PagerankProgram program;
        engine.run(program, niters);
    }
    
    /* Output top ranked vertices */
/*    std::vector< vertex_value<float> > top = get_top_vertices<float>(filename, ntop);
    std::cout << "Print top " << ntop << " vertices:" << std::endl;
    for(int i=0; i < (int)top.size(); i++) {
        std::cout << (i+1) << ". " << top[i].vertex << "\t" << top[i].value << std::endl;
    }
  */  
    metrics_report(m);    
    return 0;
}

