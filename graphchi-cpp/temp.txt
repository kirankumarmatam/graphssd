num_iteration = 0 num_vertices = 33554431 num active vertices? = 33554431
num_iteration = 1 num_vertices = 33554431 num active vertices? = 33554431
num_iteration = 2 num_vertices = 33554431 num active vertices? = 16978857
num_iteration = 3 num_vertices = 33554431 num active vertices? = 15641092
num_iteration = 4 num_vertices = 33554431 num active vertices? = 8309832
num_iteration = 5 num_vertices = 33554431 num active vertices? = 5622586
Print top 20 vertices:
1. 3831266	15808.1
2. 31112893	4973.5
3. 12424936	4972.73
4. 10117088	4963.95
5. 31066300	4962.02
6. 15155955	4961.42
7. 1414549	4960.12
8. 27301738	4957.44
9. 4010466	4957.07
10. 3876833	4955.58
11. 18543889	4954.33
12. 32768873	4953.75
13. 22686692	4952.35
14. 1048851	4951.33
15. 23732904	4950.74
16. 20119859	4949.85
17. 22731565	4949.84
18. 3921548	4945.44
19. 27923901	4945.19
20. 33211791	4945.08

 === REPORT FOR pagerank() ===
[Numeric]
cachesize_mb:		0
compression:		0
execthreads:		1
loadthreads:		4
membudget_mb:		225
niothreads:		4
niters:		6
nshards:		147
nvertices:		3.35544e+07
scheduler:		1
stripesize:		1.07374e+09
updates:		1.13661e+08
work:		1.27061e+10
[Timings]
blockload:		106.072s	 (count: 31246, min: 0s, max: 1.51307, avg: 0.00339473s)
commit:		0.615162s	 (count: 1788, min: 8.4e-05s, max: 0.033135, avg: 0.00034405s)
commit_thr:		25.0123s	 (count: 23604, min: 0.000216s, max: 1.366, avg: 0.00105966s)
execute-updates:		140.214s	 (count: 1764, min: 0.015696s, max: 0.222442, avg: 0.0794865s)
iomgr_init:		7.3e-05 s
memoryshard_create_edges:		172.652s	 (count: 1764, min: 0.040901s, max: 0.226946, avg: 0.0978751s)
memshard_commit:		1.66517s	 (count: 882, min: 0.000277s, max: 1.12627, avg: 0.00188794s)
preada_now:		229.898s	 (count: 37420, min: 0s, max: 4.13698, avg: 0.00614372s)
pwritea_now:		7.50915s	 (count: 4614, min: 4.3e-05s, max: 3.79403, avg: 0.00162747s)
read_next_vertices:		437.237s	 (count: 257544, min: 0.000213s, max: 1.51346, avg: 0.00169772s)
runtime:		526.327 s
stripedio_wait_for_reads:		67.3134s	 (count: 1764, min: 0s, max: 4.37721, avg: 0.0381595s)
stripedio_wait_for_writes:		1.87114s	 (count: 889, min: 0s, max: 0.955116, avg: 0.00210477s)
[Other]
app:	pagerank
engine:	default
file:	/Datasets/Benchmarks/SNAP/GraphChi/graph500_directed_32M.txt
