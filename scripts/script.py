#This script is to run the graphchi applications

import os
#Datasets = ["/ssd/graph500/graph500_128M.txt","/ssd/graph500/graph500_32M.txt","/ssd/graph500/soc-LiveJournal1.net","/ssd/graph500/com-friendster.ungraph.net"]
Datasets = ["/ssd/graph500/soc-LiveJournal1.net","/ssd/graph500/graph500_32M.txt"]
Datasets_root = ["0","2","101"]
Datasets_target = ["1","9533","100000000","100000000", "1766476", "10948708", "100000000", "100000000"]
Memory = ["20","225","800"]
os.chdir("/home/kiran/Parallel_graphSSD/graphchi-cpp")

os.system("make myapps/BFS")
for i in range(len(Datasets)):
	for j in range(4):
		os.system("bin/myapps/BFS file "+ Datasets[i] + " filetype edgelist execthreads 2 loadthreads 4 niothreads 4 membudget_mb " + Memory[i] + " root " + Datasets_root[i] + " target " + Datasets_target[i*4+j])
'''
os.system("make example_apps/pagerank")
for i in range(len(Datasets)):	
	os.system("bin/example_apps/pagerank file "+ Datasets[i] + " filetype edgelist execthreads 8 loadthreads 4 niothreads 4 membudget_mb " + Memory[i])

os.system("make example_apps/connectedcomponents")
for i in range(len(Datasets)):
	os.system("bin/example_apps/connectedcomponents file "+ Datasets[i] + " filetype edgelist execthreads 2 loadthreads 4 niothreads 4 membudget_mb " + Memory[i])
'''
os.system("make example_apps/trianglecounting")
for i in range(len(Datasets)):
	os.system("bin/example_apps/trianglecounting file "+ Datasets[i] + " filetype edgelist execthreads 4 loadthreads 4 niothreads 4 membudget_mb " + Memory[i])
