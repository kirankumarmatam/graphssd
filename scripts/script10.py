#This script is to run graph applications and collect performance numbers for analysing
#!/bin/bash

import os

Dataset_directory = "/Data/Benchmarks/"
'''
Graphs_to_run = ["YahooWebScope", "graph500_512M", "graph500_1G"]
Graphs_flash_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/YahooWebScope_flash.txt", "SNAP/ProcessedGraphs/graphSSD_files/graph500_flash_512M.txt", "graph500/graph500_layout/graphSSD_files/graph500_flash_1G.txt"]
Graphs_gtl_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/YahooWebScope_gtl.txt", "SNAP/ProcessedGraphs/graphSSD_files/graph500_gtl_512M.txt", "graph500/graph500_layout/graphSSD_files/graph500_gtl_1G.txt"]
Graphs_flash_to_run_baseline = ["SNAP/ProcessedGraphs/csr_files/YahooWebScope_csr.txt", "SNAP/ProcessedGraphs/csr_files/graph500_csr_512M.txt", "graph500/graph500_layout/csr_files/graph500_csr_1G.txt"]
'''
Graphs_to_run = ["com-friendster","YahooWebScope"]
Graphs_flash_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/com-friendster.ungraph_flash.net", "SNAP/ProcessedGraphs/graphSSD_files/YahooWebScope_flash.txt"]
Graphs_gtl_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/com-friendster.ungraph_gtl.net", "SNAP/ProcessedGraphs/graphSSD_files/YahooWebScope_gtl.txt"]
Graphs_flash_to_run_baseline = ["SNAP/ProcessedGraphs/csr_files/com-friendster.ungraph_csr.net", "SNAP/ProcessedGraphs/csr_files/YahooWebScope_csr.txt"]
Output_file = "/home/ossd/GraphSSD/Parallel_graphSSD/output.net"
Processed_output_file = "/home/ossd/GraphSSD/Parallel_graphSSD/Processed_output.net"
os.system("rm "+Output_file)
os.system("rm "+Processed_output_file)
os.system("touch "+Output_file)
os.system("touch "+Processed_output_file)

initial_run = 1
##Page rank -- BEGIN --
Run = [0,0]
NumberOfIOThreads = [1]
NumOfCacheThreads = 3
NumOfRequestingThreads = 1
NumberOfResponseThreads = 1
PageRank_MaxIterations = 5

os.system("echo GraphSSDResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/PageRank_delta")
		os.system("echo " + Graphs_to_run[i] + " PageRank_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " PageRank_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		if(Run[i] == 1):
			os.system("rm PageRank;sh compile_app.sh; sudo ./PageRank /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+ " " + str(PageRank_MaxIterations) + "  >> " + Output_file)

os.system("echo BaselineResults >> "+Output_file)
Run = [0,0]

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	if(Run[i] == 1):
		os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/PageRank_delta_baseline")
		os.system("echo " + Graphs_to_run[i] + " PageRank_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " PageRank_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../Baseline_library/initialization.cpp")	
		if(Run[i] == 1):
			os.system("rm PageRank;sh compile_app.sh; sudo ./PageRank /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 "+ str(PageRank_MaxIterations) + "  >> " + Output_file)
##Page rank -- END --

##BFS -- BEGIN --
Run = [0,0,0,0,0,0,0,0,0,0,0,0]
NumberOfIOThreads = [1]
NumOfCacheThreads = 3
NumOfRequestingThreads = 1
NumberOfResponseThreads = 1
BFS_MaxLevelNumber = [24, 1553]
#BFS_iterations = [10.0, 3, 3.0/2, 1]
BFS_iterations = [2, 4, 6, 8, 12, 16, 2, 3, 4, 5, 6, 7]
#BFS_iterations = [1]
BFS_root = ["101", "5"]

os.system("echo GraphSSDResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	initial_run = 1
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/BFS_parallel")
		os.system("echo " + Graphs_to_run[i] + " BFS_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " BFS_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		for k in range(len(BFS_iterations)):
			max_iteration = BFS_iterations[k]
#			if(max_iteration == 0):
#				max_iteration = 1
#			else:
#			 	max_iteration = int(BFS_MaxLevelNumber[i] / BFS_iterations[k])
#			if(Run[i*len(BFS_iterations) + k] == 1):
			if(Run[k] == 1):
				 os.system("rm BFS;sh compile_app.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+" 2000000000 " + str(max_iteration) + " " + BFS_root[i] + "  >> " + Output_file)
				 initial_run = 0

os.system("echo BaselineResults >> "+Output_file)
Run = [1,1,1,1,1,1,1,1,1,1,1,1]
Run_baseline = [0,1]

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	if(Run_baseline[i] == 1):
		os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/BFS_parallel_baseline")
		os.system("echo " + Graphs_to_run[i] + " BFS_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " BFS_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../Baseline_library/initialization.cpp")
		for k in range(len(BFS_iterations)):
			max_iteration = BFS_iterations[k]
#			if(max_iteration == 0):
#				max_iteration = 1
#			else:
#			 	max_iteration = int(BFS_MaxLevelNumber[i] / BFS_iterations[k])
#			if(Run[i*len(BFS_iterations) + k] == 1):
			if((Run[k] == 1) and (Run_baseline[i] == 1)):
				os.system("rm BFS;sh compile_app.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 2000000000 " + str(max_iteration) + " " + BFS_root[i] + "  >> " + Output_file)
##BFS -- END --

##Connected components -- BEGIN --
initial_run = 1
Run = [0,0]
NumberOfIOThreads = [1]
NumOfCacheThreads = 3
NumOfRequestingThreads = 1
NumberOfResponseThreads = 1

os.system("echo GraphSSDResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/Connected_components")
		os.system("echo " + Graphs_to_run[i] + " CC_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " CC_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		if(Run[i] == 1):
			os.system("rm CC;sh compile_app.sh; sudo ./CC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+" 2000000000 " + str(2000000000) + "  >> " + Output_file)

Run = [0,0]
os.system("echo BaselineResults >> "+Output_file)
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	if(Run[i] == 1):
		os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/Connected_components_baseline")
		os.system("echo " + Graphs_to_run[i] + " CC_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " CC_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../Baseline_library/initialization.cpp")
		if(Run[i] == 1):
			os.system("rm CC;sh compile_app.sh; sudo ./CC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 2000000000 " + str(2000000000) + "  >> " + Output_file)

##Connected components -- END --

##Random walk -- BEGIN --
Run = [0,0]
NumberOfIOThreads = [1]
NumOfCacheThreads = 3
NumOfRequestingThreads = 8
NumberOfResponseThreads = 8
RandomWalk_MaxIterations = [8000]
#RandomWalk_MaxStep = [10,50,100,1000]
RandomWalk_MaxStep = [10]
RandomWalk_modulo = [1000,10000]

os.system("echo GraphSSDResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/RandomWalk")
		os.system("echo " + Graphs_to_run[i] + " RandomWalk_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " RandomWalk_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		if(Run[i] == 1):
			for loop_3 in range(len(RandomWalk_MaxIterations)):
				for loop_4 in range(len(RandomWalk_MaxStep)):
					os.system("rm RandomWalk;sh compile_app.sh; sudo ./RandomWalk /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+ " " + str(RandomWalk_MaxIterations[loop_3]) + "  " + str(RandomWalk_MaxStep[loop_4]) + " " + str(RandomWalk_modulo[i]) + " >> " + Output_file)
					initial_run = 0
			initial_run = 1

os.system("echo BaselineResults >> "+Output_file)
Run = [0,0]
#app to cache queue size
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	if(Run[i] == 1):
		os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/RandomWalk_baseline")
		os.system("echo " + Graphs_to_run[i] + " RandomWalk_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " RandomWalk_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../Baseline_library/initialization.cpp")	
		if(Run[i] == 1):
			for loop_3 in range(len(RandomWalk_MaxIterations)):
				for loop_4 in range(len(RandomWalk_MaxStep)):
					os.system("rm RandomWalk;sh compile_app.sh; sudo ./RandomWalk /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 "+ " " + str(RandomWalk_MaxIterations[loop_3]) + "  " + str(RandomWalk_MaxStep[loop_4]) + " " + str(RandomWalk_modulo[i]) + " >> " + Output_file)
##Random walk -- END --

#Graphs_to_run = ["temp1", "temp2", "temp3","temp4"]
#Graphs_flash_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/soc-LiveJournal1_flash.net", "SNAP/ProcessedGraphs/graphSSD_files/graph500_flash_32M.txt", "SNAP/ProcessedGraphs/graphSSD_files/com-friendster.ungraph_flash.net", "graph500/graph500_layout/graphSSD_files/graph500_flash_128M.txt"]
#Graphs_gtl_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/soc-LiveJournal1_gtl.net", "SNAP/ProcessedGraphs/graphSSD_files/graph500_gtl_32M.txt", "SNAP/ProcessedGraphs/graphSSD_files/com-friendster.ungraph_gtl.net", "graph500/graph500_layout/graphSSD_files/graph500_gtl_128M.txt"]
#Graphs_flash_to_run_baseline = ["SNAP/ProcessedGraphs/csr_files/soc-LiveJournal1_csr.net", "SNAP/ProcessedGraphs/csr_files/graph500_csr_32M.txt", "SNAP/ProcessedGraphs/csr_files/com-friendster.ungraph_csr.net", "graph500/graph500_layout/csr_files/graph500_csr_128M.txt"]

##Micro Benchmarks -- BEGIN --
Run = [0,0]
NumberOfIOThreads = [1]
NumOfCacheThreads = 3
NumOfRequestingThreads = 1
NumberOfResponseThreads = 1
numOfRequests = 1000000

os.system("echo GraphSSDResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/Bottleneck_tests/Cache_latency")
		os.system("echo " + Graphs_to_run[i] + " MB_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " MB_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../../GraphSSD_library/initialization.cpp")
		if(Run[i] == 1):
			os.system("rm GC;sh compile_app.sh; sudo ./GC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+ " " + str(numOfRequests) + " >> " + Output_file)
os.system("echo BaselineResults >> "+Output_file)
Run = [0,0]

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	if(Run[i] == 1):
		os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/Bottleneck_tests/Cache_latency_baseline")
		os.system("echo " + Graphs_to_run[i] + " MB_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " MB_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../../Baseline_library/initialization.cpp")	
		if(Run[i] == 1):
			os.system("rm GC;sh compile_app.sh; sudo ./GC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 " + str(numOfRequests) + " >> "+Output_file)

##Micro Benchmarks -- END --

##GraphColoring -- BEGIN --
Run = [0,0]
NumberOfIOThreads = [1]
NumOfCacheThreads = 3
NumOfRequestingThreads = 1
NumberOfResponseThreads = 1

os.system("echo GraphSSDResults >> "+Output_file)
initial_run = 1

for i in range(len(Graphs_to_run)):
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/GraphColoringInMem")
		os.system("echo " + Graphs_to_run[i] + " GC_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " GC_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		if(Run[i] == 1):
			os.system("rm GC;sh compile_app.sh; sudo ./GC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+ " 2 1000 >> " + Output_file)
os.system("echo BaselineResults >> "+Output_file)
Run = [0,0]

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	if(Run[i] == 1):
		os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/GraphColoringInMem_baseline")
		os.system("echo " + Graphs_to_run[i] + " GC_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " GC_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../Baseline_library/initialization.cpp")	
		if(Run[i] == 1):
			os.system("rm GC;sh compile_app.sh; sudo ./GC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 2 1000 >> "+Output_file)

##GraphColoring -- END --

##Maximal Independent Set -- BEGIN --
Run = [0,0]
NumberOfIOThreads = [1]
NumOfCacheThreads = 3
NumOfRequestingThreads = 1
NumberOfResponseThreads = 1

os.system("echo GraphSSDResults >> "+Output_file)
initial_run = 1

for i in range(len(Graphs_to_run)):
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/GraphColoringInMem")
		os.system("echo " + Graphs_to_run[i] + " GC_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " GC_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		if(Run[i] == 1):
			os.system("rm GC;sh compile_app.sh; sudo ./GC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+ " 1 1000 >> " + Output_file)
os.system("echo BaselineResults >> "+Output_file)
Run = [0,0]

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	if(Run[i] == 1):
		os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/GraphColoringInMem_baseline")
		os.system("echo " + Graphs_to_run[i] + " GC_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " GC_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../Baseline_library/initialization.cpp")	
		if(Run[i] == 1):
			os.system("rm GC;sh compile_app.sh; sudo ./GC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 1 1000 >> "+Output_file)

##Maximal Independent Set -- END --


