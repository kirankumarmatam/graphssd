#This script is to run the graphchi applications

import os
#Datasets = ["/Datasets/Benchmarks/SNAP/GraphChi/soc-LiveJournal1.net","/Datasets/Benchmarks/SNAP/GraphChi/graph500_32M.txt","/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.net","/Datasets/Benchmarks/SNAP/GraphChi/graph500_128M.txt"]
Datasets = ["/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph_directed.net", "/Data/Benchmarks/SNAP/LargeGraphs/YahooWebScope/ydata-yaltavista-webmap-v1_0_links.txt"]
#Datasets = ["/ssd/graph500/soc-LiveJournal1.net","/ssd/graph500/graph500_32M.txt"]
#Memory = ["20","225","800", "900"]
Memory = ["800","1000"]
os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/graphchi-cpp")
Filetype = ["edgelist","adjlist"]

Run = [0,0,0,0,0,0,0,0]
Datasets_root = ["101","5"]
Datasets_target = ["2000000000","2000000000","2000000000","2000000000","2000000000","2000000000","2000000000","2000000000"]
Datasets_max_level = [24,1553]
Datasets_iterations = [10.0, 3, 3.0/2, 1]
os.system("make myapps/BFS_inmem")
for i in range(len(Datasets)):
	for j in range(4):
		max_iteration = Datasets_iterations[j]
		if(max_iteration == 0):
			max_iteration = 1
		else:
			max_iteration = int(Datasets_max_level[i] / Datasets_iterations[j])
		if(Run[i*4+j] == 1):
			os.system("bin/myapps/BFS_inmem file " + Datasets[i] + " filetype " + Filetype[i] + " execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i] + " root " + Datasets_root[i] + " target " + Datasets_target[i*4+j] + " maximum_level " + str(max_iteration) + " scheduler 1")

Run = [0,0]
os.system("make myapps/pagerank_delta_inmem")
for i in range(len(Datasets)):
	if(Run[i] == 1):
		os.system("bin/myapps/pagerank_delta_inmem file "+ Datasets[i] + " filetype "+ Filetype[i] +" execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i]+" niters 5")

Run = [1,1]
os.system("make myapps/randomwalksInMem")
for i in range(len(Datasets)):
	if(Run[i] == 1):
		os.system("bin/myapps/randomwalksInMem file "+ Datasets[i] + " filetype "+ Filetype[i] +" execthreads 8 loadthreads 4 niothreads 4 membudget_mb " + Memory[i] + " scheduler 1")

Run = [0,0]
os.system("make myapps/GraphColorInMem")
for i in range(len(Datasets)):
	if(Run[i] == 1):
		os.system("bin/myapps/GraphColorInMem file "+ Datasets[i] + " filetype "+ Filetype[i] +" execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i] + " isMIS 1 niters 1000")

Run = [0,0]
os.system("make myapps/GraphColorInMem")
for i in range(len(Datasets)):
	if(Run[i] == 1):
		os.system("bin/myapps/GraphColorInMem file "+ Datasets[i] + " filetype "+ Filetype[i] +" execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i] + " isMIS 0 niters 1000")

'''
os.system("make example_apps/trianglecounting")
for i in range(len(Datasets)):
	os.system("bin/example_apps/trianglecounting file "+ Datasets[i] + " filetype edgelist execthreads 4 loadthreads 4 niothreads 4 membudget_mb " + Memory[i])
'''
