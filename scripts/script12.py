#This script is to run graph applications and collect performance numbers for analysing
#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/"
Graphs_to_run = ["soc-LiveJournal1", "graph500_32M", "com-friendster", "graph500_128M"]
#Graphs_to_run = ["temp1", "temp2", "temp3"]
Graphs_flash_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/soc-LiveJournal1_flash.net", "SNAP/ProcessedGraphs/graphSSD_files/graph500_flash_32M.txt", "SNAP/ProcessedGraphs/graphSSD_files/com-friendster.ungraph_flash.net", "graph500/graph500_layout/graphSSD_files/graph500_flash_128M.txt"]
Graphs_gtl_to_run = ["SNAP/ProcessedGraphs/graphSSD_files/soc-LiveJournal1_gtl.net", "SNAP/ProcessedGraphs/graphSSD_files/graph500_gtl_32M.txt", "SNAP/ProcessedGraphs/graphSSD_files/com-friendster.ungraph_gtl.net", "graph500/graph500_layout/graphSSD_files/graph500_gtl_128M.txt"]
Graphs_flash_to_run_baseline = ["SNAP/ProcessedGraphs/csr_files/soc-LiveJournal1_csr.net", "SNAP/ProcessedGraphs/csr_files/graph500_csr_32M.txt", "SNAP/ProcessedGraphs/csr_files/com-friendster.ungraph_csr.net", "graph500/graph500_layout/csr_files/graph500_csr_128M.txt"]
#Graphs_to_run = ["SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net", "SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt", "SNAP/roadNet-CA.txt"]
#Graphs_flash_to_run = ["SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_flash.net","SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_flash.txt", "SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_flash.txt"]
#Graphs_gtl_to_run = ["SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_gtl.net", "SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_gtl.txt", "SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_gtl.txt"]
#Graphs_flash_to_run_baseline = ["SNAP/ProcessedGraphs/CSRLayout/Undirected_graphs/soc-LiveJournal1_undirected_sorted_flash.net", "SNAP/ProcessedGraphs/CSRLayout/Undirected_graphs/graph500_32M_basline_undirected_sorted_flash.txt", "SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_undirected_sorted_flash.txt"]
Output_file = "/home/ossd/GraphSSD/Parallel_graphSSD/output.net"
Processed_output_file = "/home/ossd/GraphSSD/Parallel_graphSSD/Processed_output.net"
os.system("rm "+Output_file)
os.system("rm "+Processed_output_file)
os.system("touch "+Output_file)
os.system("touch "+Processed_output_file)

initial_run = 1
##Connected components -- BEGIN --
'''
NumberOfIOThreads = [64]
NumOfCacheThreads = 4
NumOfRequestingThreads = 1
NumberOfResponseThreads = 1

os.system("echo GraphSSDResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/Connected_components")
		os.system("echo " + Graphs_to_run[i] + " CC_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " CC_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("rm CC;sh compile_app.sh; sudo ./CC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+" 3000000000 " + str(100000000) + "  >> " + Output_file)
		os.system("grep TIME_APP "+Output_file+" >> "+Processed_output_file)

os.system("echo BaselineResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/Connected_components_baseline")
		os.system("echo " + Graphs_to_run[i] + " CC_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " CC_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " NumOfCacheThreads = " + str(NumOfCacheThreads) + " NumOfRequestingThreads = " + str(NumOfRequestingThreads) + " NumberOfResponseThreads = " + str(NumberOfResponseThreads) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfCacheThreads = \w\+;/NumOfCacheThreads = "+str(NumOfCacheThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumOfRequestingThreads = \w\+;/NumOfRequestingThreads = "+str(NumOfRequestingThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("sed -i 's/NumberOfResponseThreads = \w\+;/NumberOfResponseThreads = "+str(NumberOfResponseThreads)+";/g' ../../Baseline_library/initialization.cpp")
		os.system("rm CC;sh compile_app.sh; sudo ./CC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 3000000000 " + str(100000000) + "  >> " + Output_file)
		os.system("grep TIME_APP "+Output_file+" >> "+Processed_output_file)

##Connected components -- END --
'''
