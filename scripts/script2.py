#This script is to measure the scalability of application with IO threads, i.e.  performance vs IO threads
#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt", "roadNet-CA.txt"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_flash.net","ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_flash.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_flash.txt"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_gtl.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_gtl.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_gtl.txt"]
Output_file = "/home/ossd/GraphSSD/Parallel_graphSSD/output.net"
Processed_output_file = "/home/ossd/GraphSSD/Parallel_graphSSD/Processed_output.net"
os.system("rm "+Output_file)
os.system("rm "+Processed_output_file)
os.system("touch "+Output_file)
os.system("touch "+Processed_output_file)
NumberOfIOThreads = [1,2,4,8,16,32,64]
for i in range(len(Graphs_to_run)):
	initial_run = 1
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/BFS_parallel")
		os.system("echo " + Graphs_to_run[i] + " BFS_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " BFS_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("rm BFS;sh compile_app.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+" 100000000 >> "+Output_file)
		initial_run = 0
		os.system("grep TIME_APP "+Output_file+" >> "+Processed_output_file)
