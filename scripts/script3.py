#This script is to run graph applications and collect performance numbers for GraphSSD and Baseline
#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
#Graphs_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net"]
#Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_flash.net"]
#Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_gtl.net"]
#Graphs_to_run = ["roadNet-CA.txt"]
#Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_flash.txt"]
#Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_gtl.txt"]
Graphs_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt", "roadNet-CA.txt"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_flash.net","ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_flash.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_flash.txt"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_gtl.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_gtl.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_gtl.txt"]
Output_file = "/home/ossd/GraphSSD/Parallel_graphSSD/output.net"
Processed_output_file = "/home/ossd/GraphSSD/Parallel_graphSSD/Processed_output.net"
os.system("rm "+Output_file)
os.system("rm "+Processed_output_file)
os.system("touch "+Output_file)
os.system("touch "+Processed_output_file)
NumberOfIOThreads = [64]
BFS_MaxLevelNumber = 10
BFS_iterations = [1,int(BFS_MaxLevelNumber/3), int((BFS_MaxLevelNumber*2)/3), BFS_MaxLevelNumber]
PageRank_MaxIterations = 5
RandomWalk_MaxIterations = 5
RandomWalk_MaxStep = 5

for i in range(len(Graphs_to_run)):
	initial_run = 1
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/BFS_parallel")
		os.system("echo " + Graphs_to_run[i] + " BFS_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " BFS_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
##BFS iterations
		for k in range(len(BFS_iterations)):
			os.system("rm BFS;sh compile_app.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+" 100000000 " + str(BFS_iterations[k]) + "  >> " + Output_file)
			initial_run = 0
			os.system("grep TIME_APP "+Output_file+" >> "+Processed_output_file)
##Page rank
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/PageRank")
		os.system("echo " + Graphs_to_run[i] + " PageRank_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " PageRank_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("rm PageRank;sh compile_app.sh; sudo ./PageRank /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+ " " + str(PageRank_MaxIterations) + "  >> " + Output_file)
##Random walk
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/RandomWalk")
		os.system("echo " + Graphs_to_run[i] + " RandomWalk_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " RandomWalk_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("rm RandomWalk;sh compile_app.sh; sudo ./RandomWalk /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+ " " + str(RandomWalk_MaxIterations) + "  " + str(RandomWalk_MaxStep) + " >> " + Output_file)

os.system("echo BaselineResults >> "+Output_file)

#Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/Undirected_graphs/soc-LiveJournal1_undirected_sorted_flash.net"]
#Graphs_flash_to_run_baseline = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_undirected_sorted_flash.txt"]
Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/Undirected_graphs/soc-LiveJournal1_undirected_sorted_flash.net", "ProcessedGraphs/CSRLayout/Undirected_graphs/graph500_32M_basline_undirected_sorted_flash.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_undirected_sorted_flash.txt"]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/BFS_parallel_baseline")
		os.system("echo " + Graphs_to_run[i] + " BFS_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " BFS_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
##BFS iterations
		for k in range(len(BFS_iterations)):
			os.system("rm BFS;sh compile_app.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 100000000 " + str(BFS_iterations[k]) + "  >> " + Output_file)
			os.system("grep TIME_APP "+Output_file+" >> "+Processed_output_file)
##Page rank
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/PageRank_baseline")
		os.system("echo " + Graphs_to_run[i] + " PageRank_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " PageRank_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("rm PageRank;sh compile_app.sh; sudo ./PageRank /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 "+ str(PageRank_MaxIterations) + "  >> " + Output_file)
##Random walk
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/RandomWalk")
		os.system("echo " + Graphs_to_run[i] + " RandomWalk_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " RandomWalk_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("rm RandomWalk;sh compile_app.sh; sudo ./RandomWalk /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 0 0 "+ " " + str(RandomWalk_MaxIterations) + "  " + str(RandomWalk_MaxStep) + " >> " + Output_file)

os.system("echo GraphSSDResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	initial_run = 1
	for j in range(len(NumberOfIOThreads)):
##TriangleCounting
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/TriangleCounting")
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../GraphSSD_library/initialization.cpp")
		os.system("echo " + Graphs_to_run[i] + " TraingleCounting_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " TriangleCounting_graphSSD " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("rm TC;sh compile_app.sh; sudo ./TC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" "+str(initial_run)+ " >> " + Output_file)
		initial_run = 0

os.system("echo BaselineResults >> "+Output_file)

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Baseline_library/StoreInCSR")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo ./storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	for j in range(len(NumberOfIOThreads)):
##TriangleCounting
		os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Applications_program/TriangleCounting_baseline")
		os.system("sed -i 's/NumOfIOThreads = \w\+;/NumOfIOThreads = "+str(NumberOfIOThreads[j])+";/g' ../../Baseline_library/initialization.cpp")
		os.system("echo " + Graphs_to_run[i] + " TraingleCounting_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Output_file)
		os.system("echo " + Graphs_to_run[i] + " TriangleCounting_baseline " + " NumberOfIOThreads = " + str(NumberOfIOThreads[j]) + " >> " + Processed_output_file)
		os.system("rm TC;sh compile_app.sh; sudo ./TC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)

