#This script is to run the programs which generate the csr and graphSSD layout

import os

Datasets = ["/Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net"]
Datasets_csr = ["/Datasets/Benchmarks/temp/soc-LiveJournal1_csr_64.bin"]
Datasets_graphSSD_flash = ["/Datasets/Benchmarks/temp/soc-LiveJournal1_graphSSD_flash_64.bin"]
Datasets_graphSSD_gtl = ["/Datasets/Benchmarks/temp/soc-LiveJournal1_graphSSD_gtl_64.bin"]

for i in range(len(Datasets)):
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/graphchi-cpp")
	os.system("make myapps/convert")
	os.system("bin/myapps/convert file "+ Datasets[i] +" output_file " + Datasets_csr[i])
	os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/Utilities/Layout_generators/Layout_generator_csr_to_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering temp " + Datasets_csr[i] + " " + Datasets_graphSSD_flash[i] + " " + Datasets_graphSSD_gtl[i])
