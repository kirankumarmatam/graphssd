#This script is to run the programs for generating csr and graphSSD layouts in google cloud server

import os

Datasets = [""]
Datasets_csr = [""]
Datasets_graphSSD_flash = [""]
Datasets_graphSSD_gtl = [""]

for i in range(len(Datasets)):
	os.chdir("")
	os.system("make myapps/convert")
	os.system("bin/myapps/convert file "+ Datasets[i] +" output_file " + Datasets_csr[i])
	os.chdir("")
	os.system("sh compile.sh;sudo ./graphFiltering temp " + Datasets_csr[i] + " " + Datasets_graphSSD_flash[i] + " " + Datasets_graphSSD_gtl[i])
