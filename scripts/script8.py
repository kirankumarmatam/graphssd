#This script is to run the graphchi applications

import os
#Datasets = ["/Datasets/Benchmarks/SNAP/GraphChi/soc-LiveJournal1.net","/Datasets/Benchmarks/SNAP/GraphChi/graph500_32M.txt","/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.net","/Datasets/Benchmarks/SNAP/GraphChi/graph500_128M.txt"]
Datasets = ["/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph_directed.net"]
#Datasets = ["/ssd/graph500/soc-LiveJournal1.net","/ssd/graph500/graph500_32M.txt"]
Datasets_root = ["0","2","101","0"]
Datasets_target = ["1","9533","100000000","100000000", "1766476", "10948708", "100000000", "100000000","100000000","100000000","100000000","100000000","100000000","100000000","100000000","100000000"]
#Memory = ["20","225","800", "900"]
Memory = ["800"]
os.chdir("/home/ossd/GraphSSD/Parallel_graphSSD/graphchi-cpp")
'''
os.system("make myapps/BFS")
for i in range(len(Datasets)):
	for j in range(4):
		os.system("bin/myapps/BFS file "+ Datasets[i] + " filetype edgelist execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i] + " root " + Datasets_root[i] + " target " + Datasets_target[i*4+j])
'''
'''
os.system("make myapps/pagerank_delta")
for i in range(len(Datasets)):
	os.system("bin/myapps/pagerank_delta file "+ Datasets[i] + " filetype edgelist execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i])
'''
os.system("make example_apps/randomwalks")
for i in range(len(Datasets)):
	os.system("bin/example_apps/randomwalks file "+ Datasets[i] + " filetype edgelist execthreads 8 loadthreads 4 niothreads 4 membudget_mb " + Memory[i])
'''
os.system("make myapps/GraphColor")
for i in range(len(Datasets)):
	os.system("bin/myapps/GraphColor file "+ Datasets[i] + " filetype edgelist execthreads 1 loadthreads 4 niothreads 4 membudget_mb " + Memory[i])
'''
'''
os.system("make example_apps/trianglecounting")
for i in range(len(Datasets)):
	os.system("bin/example_apps/trianglecounting file "+ Datasets[i] + " filetype edgelist execthreads 4 loadthreads 4 niothreads 4 membudget_mb " + Memory[i])
'''
