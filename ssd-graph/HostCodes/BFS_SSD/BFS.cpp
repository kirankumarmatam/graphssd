#include "../Baseline_library/graphFiltering.h"
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define READ_REQUEST_AS_ARGUMENT 1

extern unsigned int numOfNodes, numOfEdges;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
       struct timespec time_start_overhead, time_end_overhead;
       double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

#if(useCache == 1)
       extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
       extern unsigned int TotNumOfRequestsPerAccess_host, TotNumOfRequestsPerAccess_SSD;
       extern unsigned int MaxCacheSize;
#endif

       extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;
       extern PNGraph G;

int main(int argc, char **argv)
{
	set_context(argc, argv);
#if(STOP_SIMULATION == 1)
	        NUMBER_OF_REQUESTS = atoi(argv[3]);
#endif//STOP_SIMULATION == 1

        vector<unsigned int> EdgeList;
	struct timespec time_start, time_end;
	double time_elapsed;
#if(MEASURE_IO_TIME == 1)
	struct timespec time_start_io, time_end_io;
	double time_elapsed_io_iter;
	double time_elapsed_io = 0;
#endif // MEASURE_IO_TIME == 1
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	list<unsigned int> bfsQueue;
		bool elementFound = 0;
	for(int numOfrun = 0; numOfrun < 1; numOfrun++)
	{
//		unsigned int root = rand() % G->GetNodes(), reqElement = rand() % G->GetNodes();
//		unsigned int root = 0, reqElement = 100000;
#if(READ_REQUEST_AS_ARGUMENT == 1)
unsigned int root = 0, reqElement = atoi(argv[4]);
#else
unsigned int root = 0, reqElement = 4000;
#endif
		bfsQueue.push_back(root); // Need to get ROOT as input element, root element needs to be < NumVertices
#if(AuxMemoryInStorage == 0)
		bool *visited = (bool *)calloc(numOfNodes, sizeof(bool));
#else 
		for(unsigned int i =0; i < numOfNodes; i++)
		{
			AuxDataValueType value = 0;
			AccessAuxMemory(i, value, (bool)0);
		}
#endif
		elementFound = 0;
#if(STOP_SIMULATION == 1)
		unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1

		while(!bfsQueue.empty())
		{
			unsigned int topElement = bfsQueue.front();
#if (VERBOSE == 1)
			cout << topElement << ",";
#endif
			bfsQueue.pop_front();
			if(topElement == reqElement) // requiredElement is input to the program
			{
				cout << "Found the required element" << endl;
				elementFound = 1;
				break;
			}
#if (MEASURE_IO_TIME == 1)
			clock_gettime(CLOCK_MONOTONIC, &time_start_io);
			GetAdjList(topElement, EdgeList);
			clock_gettime(CLOCK_MONOTONIC, &time_end_io);
			time_elapsed_io_iter = ((double)time_end_io.tv_sec - (double)time_start_io.tv_sec);
			time_elapsed_io_iter += ((double)time_end_io.tv_nsec - (double)time_start_io.tv_nsec) / 1000000000.0;
			time_elapsed_io += time_elapsed_io_iter;
#endif // MEASURE_IO_TIME == 1
#if (VERBOSE_PRINT_ADJLIST == 1)
			cout << topElement << " :";
			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
				cout << " " << EdgeList[i];
			}
			cout << endl;
#endif //VERBOSE_PRINT_ADJLIST

			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
#if(STORE_LARGE_SCALE == 1)
				assert(EdgeList[i] < numOfNodes);
#else
				assert(EdgeList[i] < vertexMapping.size());
#endif//STORE_LARGE_SCALE
#if(AuxMemoryInStorage == 0)
				if(visited[EdgeList[i]] == 0)
				{
					bfsQueue.push_back(EdgeList[i]);
					visited[EdgeList[i]] = 1;
				}
#else
				AuxDataValueType value;
				AccessAuxMemory(EdgeList[i], value, 1);
				if(value == 0)
				{
					bfsQueue.push_back(EdgeList[i]);
					value = 1;
					AccessAuxMemory(EdgeList[i], value, 0);
				}
#endif // AuxMemoryInStorage
			}
			EdgeList.clear();
#if(STOP_SIMULATION == 1)
			number_requests_executed++;
//			cout << number_requests_executed << endl;
			if(number_requests_executed == NUMBER_OF_REQUESTS)
				break;
#endif//STOP_SIMULATION == 1

		}
		bfsQueue.clear();
#if(AuxMemoryInStorage == 0)
		free(visited);
#endif
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#if(MEASURE_IO_TIME == 1)
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD_IO: %lf\n", time_elapsed_io);
#endif // MEASURE_IO_TIME == 1
	FinalizeGraphDataStructures();
#if(useCache == 1)
	printTime();
#endif

	if(elementFound == 0)
		cout << "Did not find the required element" << endl;
	return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
