#include "../Baseline_library/graphFiltering.h"
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define READ_REQUEST_AS_ARGUMENT 1

extern unsigned int numOfNodes, numOfEdges;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
       struct timespec time_start_overhead, time_end_overhead;
       double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1


       extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;
       extern PNGraph G;

int main(int argc, char **argv)
{
	set_context(argc, argv);
#if(STOP_SIMULATION == 1)
	NUMBER_OF_REQUESTS = atoi(argv[3]);
#endif//STOP_SIMULATION == 1

	vector<unsigned int> EdgeList;
	struct timespec time_start, time_end;
	double time_elapsed;
	clock_gettime(CLOCK_MONOTONIC, &time_start);
#if (PERFORM_BFS == 1)
	std::queue<unsigned int> BfsQueue[2];
	for(unsigned int i = 0; i < numOfNodes; i++)
		BfsQueue[0].push(i);

#if(READ_REQUESTS_FROM_FILE == 1)
	unsigned int root = 0, reqElement;
	fscanf(request_file, "%d\n", &reqElement);
#elif(READ_REQUEST_AS_ARGUMENT == 1)
	unsigned int root = 0, reqElement = atoi(argv[6]);
#else
	unsigned int root = 0, reqElement = 4000;
#endif

#if(AuxMemoryInStorage == 0)
	bool *visited = (bool *)malloc(numOfNodes * sizeof(bool));
	for(unsigned int element = 0; element < numOfNodes; element++)
	{
		if(element == root)
		{
			visited[element] = 1;
		}
		else {
			visited[element] = 0;
		}
	}
#elif(AuxMemoryInStorage == 1)
	for(unsigned int element = 0; element < numOfNodes; element++)
	{
		AuxDataValueType value;
		if(element == root)
		{
			value = 1;
		} else {
			value = 0;
		}
		AccessAuxMemory(element, value, 0);
	}
#endif//AuxMemoryInStorage

	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int firstOrSecondQueue = 0;
	unsigned int niters = 9999;
	bool elementFound  = 0;
	for(unsigned int iteration = 0; iteration < niters; iteration++)
	{
		while(!BfsQueue[firstOrSecondQueue].empty())
		{
			unsigned int element = BfsQueue[firstOrSecondQueue].front();
			BfsQueue[firstOrSecondQueue].pop();
			EdgeList.clear();
			GetAdjList(element, EdgeList);
#if(AuxMemoryInStorage == 0)
			bool elementValue = visited[element];
#elif(AuxMemoryInStorage == 1)
			AuxDataValueType elementValue;
			AccessAuxMemory(element, elementValue, 1);
#endif//AuxMemoryInStorage
			if((element == reqElement) && (elementValue == 1))
			{
				elementFound = 1;
				break;
			}

			for(unsigned int edgeNum = 0; edgeNum < EdgeList.size(); edgeNum++)
			{
#if(AuxMemoryInStorage == 0)
				bool oldValue = visited[EdgeList[edgeNum]];
				bool newValue = std::max(elementValue, oldValue);
				visited[EdgeList[edgeNum]] = newValue;
#elif(AuxMemoryInStorage == 1)
				AuxDataValueType oldValue;
				AccessAuxMemory(EdgeList[edgeNum], oldValue, 1);
				bool newValue = std::max(elementValue, oldValue);
				AccessAuxMemory(EdgeList[edgeNum], newValue, 0);
#endif//AuxMemoryInStorage
				if(oldValue != newValue)
				{
					BfsQueue[(firstOrSecondQueue+1) % 2].push(EdgeList[edgeNum]);
				}
			}
		}
		if(elementFound == 1)
		{
			cout << "Found the element" << endl;
			break;
		}
		else {
			firstOrSecondQueue = (firstOrSecondQueue + 1) % 2;
			if(BfsQueue[firstOrSecondQueue].empty())
				break;
		}
	}
	if(elementFound == 0)
		cout << "Did not find the required element" << endl;

#endif//AuxMemoryInStorage

	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	FinalizeGraphDataStructures();
#if(useCache == 1)
	printTime();
#endif

	return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
