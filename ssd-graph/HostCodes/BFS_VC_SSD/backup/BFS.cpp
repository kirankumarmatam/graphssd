#include "graphFiltering.h"
#define PAGE_PER_CMD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 1
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 1
#define FLUSH_SSD_BUFFER 1
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192


unsigned int numOfNodes;
unsigned int fd;
void *data_nvme_command;
int main(int argc, char **argv)
{
	if(argc < 4)
	{
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt number_of_requests" << endl;
		return 0;
	}
#if(STOP_SIMULATION == 1)
	        NUMBER_OF_REQUESTS = atoi(argv[3]);
#endif//STOP_SIMULATION == 1

	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
	numOfNodes = G->GetNodes();

//	static const char *perrstr;
//	int err, fd;
	fd = open(argv[1], O_RDWR);
        if (fd < 0)
                cout << "Error opening file!!" << endl;
	if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		return 0;
	}
#if (FLUSH_SSD_BUFFER == 1)
		        int j;
			        for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
					        {
							                WriteToSSD(j);
									        }
#endif //FLUSH_SSD_BUFFER == 1

        vector<unsigned int> EdgeList;
	struct timespec time_start, time_end;
	double time_elapsed;
#if(MEASURE_IO_TIME == 1)
	struct timespec time_start_io, time_end_io;
	double time_elapsed_io_iter;
	double time_elapsed_io = 0;
#endif // MEASURE_IO_TIME == 1
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	list<unsigned int> bfsQueue;
		bool elementFound = 0;
	for(int numOfrun = 0; numOfrun < 1; numOfrun++)
	{
//		unsigned int root = rand() % G->GetNodes(), reqElement = rand() % G->GetNodes();
//		unsigned int root = 0, reqElement = 100000;
		unsigned int root = 0, reqElement = 4000;
		bfsQueue.push_back(root); // Need to get ROOT as input element, root element needs to be < NumVertices
		bool *visited = (bool *)calloc(numOfNodes, sizeof(bool));
		elementFound = 0;
#if(STOP_SIMULATION == 1)
		unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1

		while(!bfsQueue.empty())
		{
			unsigned int topElement = bfsQueue.front();
#if (VERBOSE == 1)
			cout << topElement << ",";
#endif
			bfsQueue.pop_front();
			if(topElement == reqElement) // requiredElement is input to the program
			{
				cout << "Found the required element" << endl;
				elementFound = 1;
				break;
			}
#if (MEASURE_IO_TIME == 1)
			clock_gettime(CLOCK_MONOTONIC, &time_start_io);
			GetAdjList(topElement, EdgeList);
			clock_gettime(CLOCK_MONOTONIC, &time_end_io);
			time_elapsed_io_iter = ((double)time_end_io.tv_sec - (double)time_start_io.tv_sec);
			time_elapsed_io_iter += ((double)time_end_io.tv_nsec - (double)time_start_io.tv_nsec) / 1000000000.0;
			time_elapsed_io += time_elapsed_io_iter;
#endif // MEASURE_IO_TIME == 1
#if (VERBOSE_PRINT_ADJLIST == 1)
			cout << topElement << " :";
			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
				cout << " " << EdgeList[i];
			}
			cout << endl;
#endif //VERBOSE_PRINT_ADJLIST

			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
				assert(EdgeList[i] < vertexMapping.size());
				if(visited[EdgeList[i]] == 0)
				{
					bfsQueue.push_back(EdgeList[i]);
					visited[EdgeList[i]] = 1;
				}
			}
			EdgeList.clear();
#if(STOP_SIMULATION == 1)
			number_requests_executed++;
//			cout << number_requests_executed << endl;
			if(number_requests_executed == NUMBER_OF_REQUESTS)
				break;
#endif//STOP_SIMULATION == 1

		}
		bfsQueue.clear();
		free(visited);
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#if(MEASURE_IO_TIME == 1)
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD_IO: %lf\n", time_elapsed_io);
#endif // MEASURE_IO_TIME == 1

	if(elementFound == 0)
		cout << "Did not find the required element" << endl;
	return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
