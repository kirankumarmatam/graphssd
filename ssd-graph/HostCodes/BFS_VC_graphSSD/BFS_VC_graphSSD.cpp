#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_BFS 1
#define VERBOSE_BFS 0
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1
#define PRINT_LEVEL_NUMBER 0
#define READ_REQUESTS_FROM_FILE 0 // This is incomplete
#define READ_REQUEST_AS_ARGUMENT 1

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;
extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	cout << NumNodes << " " << NumEdges << endl;

	struct timespec time_start, time_end;
	double time_elapsed;

	vector<unsigned int> EdgeList;

#if (PERFORM_BFS == 1)

	std::queue<unsigned int> BfsQueue[2];
#if(LOAD_GRAPH_FROM_FILE == 1)
	for(unsigned int i = 0; i < NumNodes; i++)
		BfsQueue[0].push(i);
#else 
	for(unsigned int i = 0; i < vertexMapping.size(); i++)
		BfsQueue[0].push(i);
#endif//LOAD_GRAPH_FROM_FILE

#if(READ_REQUESTS_FROM_FILE == 1)
		unsigned int root = 0, reqElement;
		fscanf(request_file, "%d\n", &reqElement);
#elif(READ_REQUEST_AS_ARGUMENT == 1)
		unsigned int root = 0, reqElement = atoi(argv[6]);
#else
		unsigned int root = 0, reqElement = 4000;
#endif

#if(AuxMemoryInStorage == 0)
#if(LOAD_GRAPH_FROM_FILE == 1)
		bool *visited = (bool *)malloc(NumNodes * sizeof(bool));
		for(unsigned int element = 0; element < NumNodes; element++)
#else
		bool *visited = (bool *)malloc(vertexMapping.size() * sizeof(bool));
		for(unsigned int element = 0; element < vertexMapping.size(); element++)
#endif
		{
			if(element == root)
					{
						visited[element] = 1;
					}
					else {
						visited[element] = 0;
					}
		}
#elif(AuxMemoryInStorage == 1)
		for(unsigned int element = 0; element < NumNodes; element++)
		{
			AuxDataValueType value;
			if(element == root)
			{
				value = 1;
			} else {
				value = 0;
			}
			SSDInstance.AccessAuxMemory(element, value, 0);
		}
#endif//AuxMemoryInStorage


		clock_gettime(CLOCK_MONOTONIC, &time_start);
		unsigned int firstOrSecondQueue = 0;
		unsigned int niters = 9999;
		bool elementFound  = 0;
		for(unsigned int iteration = 0; iteration < niters; iteration++)
		{
			while(!BfsQueue[firstOrSecondQueue].empty())
			{
				unsigned int element = BfsQueue[firstOrSecondQueue].front();
				BfsQueue[firstOrSecondQueue].pop();
				EdgeList.clear();
				SSDInstance.GetAdjListUseNvmeCommands(element, EdgeList);
#if(AuxMemoryInStorage == 0)
				bool elementValue = visited[element];
#elif(AuxMemoryInStorage == 1)
				AuxDataValueType elementValue;
				SSDInstance.AccessAuxMemory(element, elementValue, 1);
#endif//AuxMemoryInStorage
				if((element == reqElement) && (elementValue == 1))
				{
					elementFound = 1;
					break;
				}

				for(unsigned int edgeNum = 0; edgeNum < EdgeList.size(); edgeNum++)
				{
#if(AuxMemoryInStorage == 0)
					bool oldValue = visited[EdgeList[edgeNum]];
					bool newValue = std::max(elementValue, oldValue);
					visited[EdgeList[edgeNum]] = newValue;
#elif(AuxMemoryInStorage == 1)
					AuxDataValueType oldValue;
					SSDInstance.AccessAuxMemory(EdgeList[edgeNum], oldValue, 1);
					bool newValue = std::max(elementValue, oldValue);
					SSDInstance.AccessAuxMemory(EdgeList[edgeNum], newValue, 0);
#endif//AuxMemoryInStorage
					if(oldValue != newValue)
					{
						BfsQueue[(firstOrSecondQueue+1) % 2].push(EdgeList[edgeNum]);
					}
				}
			}
			if(elementFound == 1)
			{
				cout << "Found the element" << endl;
				break;
			}
			else {
				firstOrSecondQueue = (firstOrSecondQueue + 1) % 2;
				if(BfsQueue[firstOrSecondQueue].empty())
					break;
			}
		}
		if(elementFound == 0)
			cout << "Did not find the required element" << endl;


	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	SSDInstance.FinalizeGraphDataStructures();	
#if(useCache == 1)
	SSDInstance.printTime();
#endif//useCache
#endif //PERFORM_BFS
	return 0;
}
