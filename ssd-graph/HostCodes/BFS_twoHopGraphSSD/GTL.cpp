#include "graphFiltering.h"

extern map<unsigned int, unsigned int > vertexMapping;

void SSDInterface::InitializeGraphDataStructures(unsigned int NumNodes)
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 4;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = NumNodes;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);
}

void SSDInterface::AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList)
{
	assert(EdgeList.size()*sizeof(unsigned int) <= 32*4096);
	unsigned int num = EdgeList.size();
//	memcpy((void *)data_nvme_command, &num, sizeof(unsigned int));
	memcpy((void *)data_nvme_command, EdgeList.data(), EdgeList.size()*sizeof(unsigned int));
//	cout << data_nvme_command << " " << (unsigned int *)data_nvme_command + 1 << endl;
/*	cout << EdgeList.size() << endl;
	for(int i=0; i < EdgeList.size(); i++)
		cout <<*((unsigned int *)data_nvme_command + i) << " ";
	cout << endl;
*/	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 2;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;//Transfer number of vertices to write in metadata (reserved)?
	io.addr = (unsigned long)data_nvme_command;
//	cout << "io.addr = " << io.addr << endl;
	io.slba = vID;     // use slba for passing the vertex Id
	io.nblocks = ceil((sizeof(unsigned int)*(EdgeList.size()+1)*1.0)/PAGE_SIZE);
	io.dsmgmt = 0;
//	io.reftag = 0;
	io.reftag = num;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);
}
void SSDInterface::AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList)
{
	assert(vID < vertexMapping.size());
	vertexToAdjListMap[vID];
	vertexToAdjListMap[vID] = EdgeList;
}
void SSDInterface::AddVertexUseGTL(unsigned int vID, vector<unsigned int> EdgeList)
{
	assert(vID < vertexMapping.size());
	unsigned long long int *ReturnList = bufferingPage(EdgeList);
	assert(ReturnList != NULL);
	GTLTable1[vID];
	if(ReturnList[0] == 0)
	{
		GTLTable1[vID].number = 0;
		GTLTable1[vID].direct = 1;
	}
	else if(ReturnList[0] == 1)
	{
		GTLTable1[vID].address = ReturnList[1] & (~(PAGE_SIZE-1));
		GTLTable1[vID].offset =  ReturnList[1] & (PAGE_SIZE-1);
		GTLTable1[vID].number = ReturnList[2];
		GTLTable1[vID].direct = 1;
		free(ReturnList);
	}
	else {
		GTLTable1[vID].address = (unsigned long long int)ReturnList;
		GTLTable1[vID].offset = 1;
		GTLTable1[vID].number = ReturnList[0];
		GTLTable1[vID].direct = 0;
	}
/*	cout << EdgeList.size() << " " << ReturnList[0] << " : ";
	for(unsigned int i = 0; i < ReturnList[0]; i++)
	{
		cout << ReturnList[2*i+1] << " " << ReturnList[2*i+2] << " ";
	}
	cout << endl;
*/
}

void SSDInterface::GetAdjListUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList)
{
	assert(vID < vertexMapping.size());
	struct nvme_user_io io;
	io.opcode = nvme_cmd_read;
	io.flags = 3;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
//	cout << "io.addr = " << io.addr << endl;
	io.slba = vID;     // use slba for passing the vertex Id
	io.nblocks = 31;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme read status:%x\n", err);
	
	assert(EdgeList.size()*sizeof(unsigned int) <= 32*4096);
	//First value should be the number of adjacent vertices returned
//	cout << "Number of adj vertices = " << *((unsigned int *)data_nvme_command) << endl;
	for(unsigned int i=0; i < *((unsigned int *)data_nvme_command); i++)
	{
//		cout << "Edge " << i << " = " << *((unsigned int *)data_nvme_command + i + (4096/sizeof(unsigned int)) ) << endl;
//		cout << "Edge " << i << " = " << *((unsigned int *)data_nvme_command + i + 1) << endl;
//		EdgeList.push_back(*((unsigned int *)data_nvme_command + i + (4096/sizeof(unsigned int)) ));
		EdgeList.push_back(*((unsigned int *)data_nvme_command + i + 1) );
	}
//	if(EdgeList.size() == 0)return;
/*	cout << vID << " :";
	for(unsigned int i=0; i < *((unsigned int *)data_nvme_command); i++)
	{
		cout << " " << *((unsigned int *)data_nvme_command + i + 1);
	}
	cout << endl;
*/}
void SSDInterface::GetAdjListNoGTL(unsigned int vID, vector<unsigned int> &EdgeList)
{
	assert(vID < vertexMapping.size());
	for(unsigned int i=0; i<vertexToAdjListMap[vID].size(); i++)
		EdgeList.push_back(vertexToAdjListMap[vID][i]);
}
void SSDInterface::GetAdjListUseGTL(unsigned int vID, vector<unsigned int> &EdgeList)
{
	assert(GTLTable1[vID].find(vID) != GTLTable1[vID].end());
	unsigned long long int pageAddress;
	unsigned  int pageOffset;
	if(GTLTable1[vID].direct == 1)
	{
		if(GTLTable1[vID].number != 0)
		{
			pageAddress = GTLTable1[vID].address;
			pageOffset = GTLTable1[vID].offset;
			for(unsigned int i = 0; i < GTLTable1[vID].number; i++)
			{
				EdgeList.push_back(*(unsigned int *)((char *)RamDisk + pageAddress + pageOffset + i * sizeof(unsigned int)));
			}
		}
	}
	else {
		for(unsigned int i = 0; i < GTLTable1[vID].number; i++)
		{
			pageAddress = ((unsigned long long int *)(GTLTable1[vID].address))[2*i+1];
			pageOffset = pageAddress & (PAGE_SIZE -1);
			pageAddress = pageAddress & (~(PAGE_SIZE-1));
			unsigned long long int number = ((unsigned long long int *)(GTLTable1[vID].address))[2*i+2];
			for(unsigned int j = 0; j < number; j++)
			{
				EdgeList.push_back(*(unsigned int *)((char *)RamDisk + pageAddress + pageOffset + j * sizeof(unsigned int)));
			}
		}
	}
}

void SSDInterface::ClearQueues()
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 7;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = 0;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}
