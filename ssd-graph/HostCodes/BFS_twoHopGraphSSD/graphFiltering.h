#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <vector>
#include <list>

#include <time.h>

using namespace std;

#define UseFTL 2
class GTLPointer {
	public:
		unsigned long long int address;
		unsigned int offset;
		unsigned long long int number;
		unsigned int direct;
};

class SSDInterface
{	
	public:
		void InitializeGraphDataStructures(unsigned int NumNodes);
		void ClearQueues();
		void AddVertexNoGTL(unsigned int, vector<unsigned int> EdgeList);
		void AddVertexUseGTL(unsigned int, vector<unsigned int> EdgeList);
		void GetAdjListNoGTL(unsigned int, vector<unsigned int> &EdgeList);
		void GetAdjListUseGTL(unsigned int, vector<unsigned int> &EdgeList);
		void GetAdjListUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList);
		void AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList);

		map<unsigned int, vector<unsigned int> > vertexToAdjListMap;

		void AddVertex(unsigned int vID, vector<unsigned int> EdgeList)
		{
			if(UseFTL==1)
			{
				AddVertexUseGTL(vID, EdgeList);
			}
			else if(UseFTL == 0)
			{
				AddVertexNoGTL(vID, EdgeList);
			}
			else if(UseFTL == 2)
			{
				AddVertexUseNvmeCommands(vID, EdgeList);
			}
		}
		void GetAdjList(unsigned int vID, vector<unsigned int> &EdgeList)
		{
			if(UseFTL==1)
			{
				GetAdjListUseGTL(vID, EdgeList);
			}
			else if(UseFTL == 0)
			{
				GetAdjListNoGTL(vID, EdgeList);
			}
			else if(UseFTL == 2)
			{
				GetAdjListUseNvmeCommands(vID, EdgeList);
			}
		}

		void *RamDisk;
		unsigned long long int currentRamDiskAddress;
		unsigned int PAGE_SIZE;
		void *data_nvme_command;
		int fd;

		SSDInterface()
		{
			RamDisk = (void *)malloc(64*1024*1024);
			assert(RamDisk != NULL);
			currentRamDiskAddress = 0;
			PAGE_SIZE = 4096;
			if(UseFTL == 2)
			{
				if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
					fprintf(stderr, "cannot allocate io payload for data_wr\n");
				}
			}
		}
		map<unsigned int, GTLPointer > GTLTable1;

		unsigned long long int *bufferingPage(vector<unsigned int> EdgeList)
		{
			unsigned int MaxPages = ceil((EdgeList.size()*sizeof(unsigned int)*1.0) / PAGE_SIZE );
			unsigned long long int *ReturnList = (unsigned long long int*)malloc((2*MaxPages+1)*sizeof(unsigned long long int));
			assert(ReturnList != NULL);
			ReturnList[0] = MaxPages;
			unsigned int num = EdgeList.size();
			unsigned int valuesRemainingToAdd = num;
			unsigned int numPages = 0;
			while(valuesRemainingToAdd > 0)
			{
				unsigned int numOfItemsToCopy = (valuesRemainingToAdd*sizeof(unsigned int) <= PAGE_SIZE) ? valuesRemainingToAdd : (PAGE_SIZE / sizeof(unsigned int));
				ReturnList[2*numPages+1] = currentRamDiskAddress;
				ReturnList[2*numPages+2] = numOfItemsToCopy;
				memcpy((void *)((char *)RamDisk + currentRamDiskAddress), (char *)(EdgeList.data())+((num-valuesRemainingToAdd)*sizeof(unsigned int)), sizeof(unsigned int)*numOfItemsToCopy);
				valuesRemainingToAdd -= numOfItemsToCopy;
				currentRamDiskAddress += PAGE_SIZE;
				numPages++;
			}
			return ReturnList;
		}
};
