#include "graphFiltering.h"

extern map<unsigned int, unsigned int > vertexMapping;
#if(CALCULATE_DATA_FETCHED == 1)
extern unsigned int baseline_data_fetched;
extern unsigned int adjacencyList_data_fetched;
#endif // CALCULATE_DATA_FETCHED == 1

#if(IsUndirectedGraph == 0)
PNGraph G;
#elif(IsUndirectedGraph == 1)
PUNGraph G1;
PNGraph G;
#endif
unsigned int numOfNodes, numOfEdges;
unsigned int fd;
void *data_nvme_command;

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotNumOfRequestsPerAccess_host, TotNumOfRequestsPerAccess_SSD;
extern unsigned int MaxCacheSize;
unsigned int cache_percentage = 5;
extern unsigned int numOfCacheHits_aux, numOfCacheEvictions_aux, numOfCacheAccesses_aux, numOfCacheMisses_aux;
unsigned int numOfRowPtrMisses = 0, numOfRowPtrHits = 0, numOfColumnIndexMisses = 0, numOfColumnIndexHits = 0;
#endif

unsigned int numOfNANDPageWrites = 0, numOfNANDPageReads = 0;

#if (CALCULATE_OVERHEAD_TIME == 1)
       extern struct timespec time_start_overhead, time_end_overhead;
       extern double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

void GetAdjList(unsigned int vID, vector<unsigned int> &EdgeList)
{
#if (useCache == 0)
#if(CALCULATE_OVERHEAD_TIME == 1)
		clock_gettime(CLOCK_MONOTONIC, &time_start_overhead);
#endif // CALCULATE_OVERHEAD_TIME == 1
		unsigned int numOfElementsInAPage = 4096 / sizeof(unsigned int);
		assert(vID < vertexMapping.size());
		struct nvme_user_io io;
		unsigned int err;
		io.opcode = nvme_cmd_read;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		io.slba = SLBA + (vID / (numOfElementsInAPage));
		if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
		{
			io.nblocks = 1;
		}
		else 
		{
			io.nblocks = 0;
		}
#if(CALCULATE_DATA_FETCHED ==1)
		baseline_data_fetched += (io.nblocks+1) * 4096;
#endif // CALCULATE_DATA_FETCHED == 1
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;
		err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		if (err < 0)
			cout << "Error in executing nvme command" << endl;
		if (err)
		{
			fprintf(stderr, "nvme read status:%x\n", err);
			exit(0);
		}

		unsigned int *RowPtr = (unsigned int *)malloc(sizeof(unsigned int) * 2);
		memcpy(RowPtr, (unsigned int *)data_nvme_command + (vID % numOfElementsInAPage), 2*sizeof(unsigned int));

#if(CALCULATE_OVERHEAD_TIME == 1)
		clock_gettime(CLOCK_MONOTONIC, &time_end_overhead);
		time_elapsed_overhead += ((double)time_end_overhead.tv_sec - (double)time_start_overhead.tv_sec);
		time_elapsed_overhead += ((double)time_end_overhead.tv_nsec - (double)time_start_overhead.tv_nsec) / 1000000000.0;
#endif //CALCULATE_OVERHEAD_TIME == 1

		if(RowPtr[0] == RowPtr[1]) 
			return;

		io.opcode = nvme_cmd_read;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		io.slba = SLBA + (numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage;
	//	io.nblocks = (RowPtr[1] / numOfElementsInAPage) - (RowPtr[0] / numOfElementsInAPage) + 1;
		io.nblocks = ((numOfNodes + 1 + RowPtr[1]) / numOfElementsInAPage) - ((numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage);
#if(CALCULATE_DATA_FETCHED ==1)
		baseline_data_fetched += (io.nblocks+1) * 4096;
#endif // CALCULATE_DATA_FETCHED == 1
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;
		err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		if (err < 0)
			cout << "Error in executing nvme command" << endl;
		if (err)
		{
			fprintf(stderr, "nvme read status:%x\n", err);
			exit(0);
		}
#if(CALCULATE_DATA_FETCHED ==1)
//		adjacencyList_data_fetched += ((RowPtr[1] - RowPtr[0] + 1) * sizeof(unsigned int));
		adjacencyList_data_fetched += (((numOfNodes + 1 + RowPtr[1]) / numOfElementsInAPage) - ((numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage)) * sizeof(unsigned int);
#endif // CALCULATE_DATA_FETCHED == 1

		unsigned int startingElement = (numOfNodes + 1 + RowPtr[0]) % numOfElementsInAPage;
		for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
			EdgeList.push_back(*((unsigned int *)data_nvme_command + startingElement + i));


		/*	cout << vID << " :";
			for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
			cout << " " << *((unsigned int *)data_nvme_command + startingElement + i);
			cout << endl;
		 */
		//perror:
		//	perror(perrstr);
		//	return 1;
#endif
#if (useCache == 1)
#if(VERTEX_CACHE_MAP == 1)
		char *host_buffer = new char [32*4096];
		if(cache_readAdjEdgeList_vertex(vID, host_buffer) == 1)
		{
			assert(EdgeList.size()*sizeof(unsigned int) <= 32*4096);
			assert(*((unsigned int *)host_buffer)*sizeof(unsigned int) <= 32*4096);
			for(unsigned int i=0; i < *((unsigned int *)host_buffer); i++)
			{
				//              cout << "Edge " << i << " = " << *((unsigned int *)data_nvme_command + i + (4096/sizeof(unsigned int)) ) << endl;
				//              cout << "Edge " << i << " = " << *((unsigned int *)data_nvme_command + i + 1) << endl;
				//              EdgeList.push_back(*((unsigned int *)data_nvme_command + i + (4096/sizeof(unsigned int)) ));
				EdgeList.push_back(*((unsigned int *)host_buffer + i + 1) );
			}
			free(host_buffer);
			return;
		}
			free(host_buffer);
#endif//VERTEX_CACHE_MAP
#if(CALCULATE_OVERHEAD_TIME == 1)
		clock_gettime(CLOCK_MONOTONIC, &time_start_overhead);
#endif // CALCULATE_OVERHEAD_TIME == 1
		unsigned int numOfElementsInAPage = 4096 / sizeof(unsigned int);
		assert(vID < vertexMapping.size());
		unsigned int slba = SLBA + (vID / (numOfElementsInAPage));
		unsigned int nblocks;
		if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
		{
			nblocks = 1;
		}
		else
		{
			nblocks = 0;
		}
#if (VERBOSE == 1)
		cout << "vID = " << vID << " numOfElementsInAPage =  "  << numOfElementsInAPage << " slba = " << slba << " nblocks =  "  << nblocks << endl;
#endif
#if(CALCULATE_DATA_FETCHED ==1)
		baseline_data_fetched += (nblocks+1) * 4096;
#endif // CALCULATE_DATA_FETCHED == 1

		char **data_pointer = new char * [nblocks+1];
//		cache_readAdjEdgeList(slba, nblocks+1, data_pointer);
		bool returnValue = cache_readAdjEdgeList(slba, nblocks+1, data_pointer);
		if(returnValue == 1)
			numOfRowPtrMisses += 1;
		else numOfRowPtrHits += 1;
		if(nblocks == 1)
		{
			if((slba % 4) == 3)
			{
				if(returnValue == 1)
				{
					numOfCacheMisses++;
				}
				numOfCacheAccesses++;
			}
		}

		unsigned int *RowPtr = (unsigned int *)malloc(sizeof(unsigned int) * 2);
		RowPtr[0] = ((unsigned int *)data_pointer[0])[vID % numOfElementsInAPage];
		if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
		{
			RowPtr[1] = ((unsigned int *)data_pointer[1])[0];
		}
		else {
			RowPtr[1] = ((unsigned int *)data_pointer[0])[(vID+1) % numOfElementsInAPage];
		}
#if (VERBOSE == 1)
		cout << "RowPtr[0] = " << RowPtr[0] << " RowPtr[1] = " << RowPtr[1] << endl;
#endif
		free(data_pointer);

#if(CALCULATE_OVERHEAD_TIME == 1)
		clock_gettime(CLOCK_MONOTONIC, &time_end_overhead);
		time_elapsed_overhead += ((double)time_end_overhead.tv_sec - (double)time_start_overhead.tv_sec);
		time_elapsed_overhead += ((double)time_end_overhead.tv_nsec - (double)time_start_overhead.tv_nsec) / 1000000000.0;
#endif //CALCULATE_OVERHEAD_TIME == 1

		if(RowPtr[0] == RowPtr[1])
			return;

		slba = SLBA + (numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage;
//		nblocks = (RowPtr[1] / numOfElementsInAPage) - (RowPtr[0] / numOfElementsInAPage) + 1;
		nblocks = ((numOfNodes + 1 + RowPtr[1]) / numOfElementsInAPage) - ((numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage);
//		cout << "Start = " << RowPtr[0] << " " << RowPtr[1] << endl;
//		cout << "R = " << nblocks  << " " << ((numOfNodes + 1 + RowPtr[1]) / numOfElementsInAPage) << " " << ((numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage) << endl;
#if (VERBOSE == 1)
		cout << "slba = " << slba << " nblocks =  " << nblocks+1 << endl;
#endif

#if(CALCULATE_DATA_FETCHED ==1)
		baseline_data_fetched += (nblocks+1) * 4096;
#endif // CALCULATE_DATA_FETCHED == 1
		data_pointer = new char * [nblocks+1];
		returnValue = cache_readAdjEdgeList(slba, nblocks+1, data_pointer);
		if(returnValue == 1)
			numOfColumnIndexMisses += 1;
		else numOfColumnIndexHits += 1;
#if(CALCULATE_DATA_FETCHED ==1)
		adjacencyList_data_fetched += ((RowPtr[1] - RowPtr[0] + 1) * sizeof(unsigned int));
#endif // CALCULATE_DATA_FETCHED == 1

		unsigned int startingElement = (numOfNodes + 1 + RowPtr[0]) % numOfElementsInAPage;
#if (VERBOSE == 1)
		cout << "startingElement = " << startingElement << endl;
#endif 
		for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
		{	//              EdgeList.push_back(*((unsigned int *)data_nvme_command + startingElement + i));
//			cout << " " << ((startingElement + i) / numOfElementsInAPage)  << " " << (startingElement + i) % numOfElementsInAPage << endl;
			EdgeList.push_back(((unsigned int *)(data_pointer[(startingElement + i) / numOfElementsInAPage]))[(startingElement + i) % numOfElementsInAPage]);
		}
#if (VERBOSE == 1)
		cout << "return GetAdjList" << endl;
#endif
#if(VERTEX_CACHE_MAP == 1)
		host_buffer = new char [32*4096];
		assert(EdgeList.size()*sizeof(unsigned int) <= 32*4096);
		unsigned int NUM = EdgeList.size();
		memcpy(host_buffer, &NUM, sizeof(unsigned int));
		memcpy(host_buffer + sizeof(unsigned int), (void *)EdgeList.data(), sizeof(unsigned int) * EdgeList.size());
		cache_writeAdjEdgeList_vertex(vID, host_buffer); 
		free(host_buffer);
#endif//VERTEX_CACHE_MAP
		free(data_pointer);
		return;
#endif
}

void GetAdjEdgeWeights(unsigned int vID, vector<unsigned int> &EdgeList)
{
#if (useCache == 1)
#if(CALCULATE_OVERHEAD_TIME == 1)
		clock_gettime(CLOCK_MONOTONIC, &time_start_overhead);
#endif // CALCULATE_OVERHEAD_TIME == 1
		unsigned int numOfElementsInAPage = 4096 / sizeof(unsigned int);
		assert(vID < numOfNodes);
		unsigned int slba = SLBA + (vID / (numOfElementsInAPage));
		unsigned int nblocks;
		if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
		{
			nblocks = 1;
		}
		else
		{
			nblocks = 0;
		}
#if (VERBOSE == 1)
		cout << "vID = " << vID << " numOfElementsInAPage =  "  << numOfElementsInAPage << " slba = " << slba << " nblocks =  "  << nblocks << endl;
#endif
#if(CALCULATE_DATA_FETCHED ==1)
		baseline_data_fetched += (nblocks+1) * 4096;
#endif // CALCULATE_DATA_FETCHED == 1

		char **data_pointer = new char * [nblocks+1];
		bool returnValue = cache_readAdjEdgeList(slba, nblocks+1, data_pointer);
		if(nblocks == 1)
		{
			if((slba % 4) == 3)
			{
				if(returnValue == 1)
				{
					numOfCacheMisses++;
				}
				numOfCacheAccesses++;
			}
		}

		unsigned int *RowPtr = (unsigned int *)malloc(sizeof(unsigned int) * 2);
		RowPtr[0] = ((unsigned int *)data_pointer[0])[vID % numOfElementsInAPage];
		if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
		{
			RowPtr[1] = ((unsigned int *)data_pointer[1])[0];
		}
		else {
			RowPtr[1] = ((unsigned int *)data_pointer[0])[(vID+1) % numOfElementsInAPage];
		}
#if (VERBOSE == 1)
		cout << "RowPtr[0] = " << RowPtr[0] << " RowPtr[1] = " << RowPtr[1] << endl;
#endif
		free(data_pointer);

#if(CALCULATE_OVERHEAD_TIME == 1)
		clock_gettime(CLOCK_MONOTONIC, &time_end_overhead);
		time_elapsed_overhead += ((double)time_end_overhead.tv_sec - (double)time_start_overhead.tv_sec);
		time_elapsed_overhead += ((double)time_end_overhead.tv_nsec - (double)time_start_overhead.tv_nsec) / 1000000000.0;
#endif //CALCULATE_OVERHEAD_TIME == 1

		if(RowPtr[0] == RowPtr[1])
			return;

		slba = SLBA + (numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage;
//		nblocks = (RowPtr[1] / numOfElementsInAPage) - (RowPtr[0] / numOfElementsInAPage) + 1;
		nblocks = ((numOfNodes + 1 + RowPtr[1]) / numOfElementsInAPage) - ((numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage);
//		cout << "Start = " << RowPtr[0] << " " << RowPtr[1] << endl;
//		cout << "R = " << nblocks  << " " << ((numOfNodes + 1 + RowPtr[1]) / numOfElementsInAPage) << " " << ((numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage) << endl;
#if (VERBOSE == 1)
		cout << "slba = " << slba << " nblocks =  " << nblocks+1 << endl;
#endif

#if(CALCULATE_DATA_FETCHED ==1)
		baseline_data_fetched += (nblocks+1) * 4096;
#endif // CALCULATE_DATA_FETCHED == 1
		data_pointer = new char * [nblocks+1];
		cache_readAdjEdgeList(slba, nblocks+1, data_pointer);
#if(CALCULATE_DATA_FETCHED ==1)
		adjacencyList_data_fetched += ((RowPtr[1] - RowPtr[0] + 1) * sizeof(unsigned int));
#endif // CALCULATE_DATA_FETCHED == 1

		unsigned int startingElement = (numOfNodes + 1 + RowPtr[0]) % numOfElementsInAPage;
#if (VERBOSE == 1)
		cout << "startingElement = " << startingElement << endl;
#endif 
		for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
		{	//              EdgeList.push_back(*((unsigned int *)data_nvme_command + startingElement + i));
//			cout << " " << ((startingElement + i) / numOfElementsInAPage)  << " " << (startingElement + i) % numOfElementsInAPage << endl;
			EdgeList.push_back(((unsigned int *)(data_pointer[(startingElement + i) / numOfElementsInAPage]))[(startingElement + i) % numOfElementsInAPage]);
		}
#if (VERBOSE == 1)
		cout << "return GetAdjList" << endl;
#endif

		slba = SLBA_WEIGHT + (numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage;
		cache_readAdjEdgeList(slba, nblocks+1, data_pointer);

		free(data_pointer);
		return;
#endif
}
void WriteToSSD(unsigned int j)
{
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA_FLUSH_BUFFER + j;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}

	return;

	//perror:
	//      perror(perrstr);
	//      return 1;
}

void WriteToSSDN_SLBA(unsigned int j, unsigned int num)
{
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA + j;
	io.nblocks = (num - 1);
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}

	return;

	//perror:
	//      perror(perrstr);
	//      return 1;
}

void InitializeGraphDataStructures(unsigned int NumNodes)
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 4;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = NumNodes;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}

void FinalizeGraphDataStructures()
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 6;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = 0;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
	numOfNANDPageWrites = ((unsigned int *)data_nvme_command)[0];
	numOfNANDPageReads = ((unsigned int *)data_nvme_command)[1];
}

void AccessAuxMemory(unsigned int index, AuxDataValueType &value, bool isRead)
{
	unsigned int LPN = AUXLBA + index / (4096/sizeof(AuxDataValueType));
	unsigned int offset = index % (4096/sizeof(AuxDataValueType));
	cache_readNumber(LPN, offset, 1, value, isRead);
	return;
}


int set_context(int argc, char **argv)
{
	if(argc < 4)
	{
#if(STORE_LARGE_SCALE == 1)
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt output_flash.txt" << endl;
#else
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt number_of_requests" << endl;
#endif//STORE_LARGE_SCALE
		exit(0);
	}

#if(STORE_LARGE_SCALE == 0)
#if(IsUndirectedGraph == 0)
	G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
	//      PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
	numOfNodes = G->GetNodes();
	numOfEdges = G->GetEdges();
#elif(IsUndirectedGraph == 1)
	G1 = TSnap::LoadEdgeList<PUNGraph>(argv[2], 0, 1);
	        G = TSnap::ConvertGraph<PNGraph>(G1);
	//      PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
	numOfNodes = G->GetNodes();
	numOfEdges = G->GetEdges();
#endif//IsUndirectedGraph
#else
	fstream myFlashFile;
	myFlashFile.open(argv[3]);
	cout << argv[3] << endl;
	myFlashFile.read((char *)(&numOfNodes), sizeof(unsigned int));
	myFlashFile.read((char *)(&numOfEdges), sizeof(unsigned int));
	myFlashFile.close();
	cout << "NumNodes = " << numOfNodes  <<  " numOfEdges = " << numOfEdges << endl;
#endif//STORE_LARGE_SCALE == 1

#if(useCache==1)
	if(((numOfNodes + numOfEdges) * sizeof(unsigned int)) < (256*1024))
		MaxCacheSize = 256 * 1024;
	else MaxCacheSize = (((numOfNodes + numOfEdges) * sizeof(unsigned int)) * cache_percentage) / 100;
#endif

	//      static const char *perrstr;
	//      int err, fd;
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		cout << "Error opening file!!" << endl;
	if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		return 0;
	}
#if(STORE_LARGE_SCALE == 1)
	InitializeGraphDataStructures(numOfNodes);
#else
	InitializeGraphDataStructures(G->GetNodes());
#endif//STORE_LARGE_SCALE
#if (FLUSH_SSD_BUFFER == 1)
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1
	return 0;
}

void printTime()
{

	double AvgNumOfRequestsPerAccess_SSD = (TotNumOfRequestsPerAccess_SSD*1.0) /  numOfCacheMisses;
	double time = (numOfCacheAccesses - numOfCacheAccesses_aux) * 1000 + (numOfCacheMisses)* 28000 + ((numOfNANDPageReads * 1.0) / AvgNumOfRequestsPerAccess_SSD) * 875000 + numOfCacheMisses_aux * 17000 + numOfCacheAccesses_aux * 100;
	cout << "MaxCacheSize = " << MaxCacheSize << " numOfCacheAccesses = " << numOfCacheAccesses << " numOfCacheAccesses_aux = " << numOfCacheAccesses_aux << " numOfCacheMisses = " << numOfCacheMisses << " numOfCacheMisses_aux = " << numOfCacheMisses_aux << " numOfCacheEvictions = " << numOfCacheEvictions << " numOfCacheEvictions_aux = " << numOfCacheEvictions_aux  << " numOfNANDPageReads = " << numOfNANDPageReads << " TotNumOfRequestsPerAccess_SSD = " << TotNumOfRequestsPerAccess_SSD << " AvgNumOfRequestsPerAccess_SSD = " << AvgNumOfRequestsPerAccess_SSD << " numOfRowPtrMisses = "<< numOfRowPtrMisses << " numOfRowPtrHits = " << numOfRowPtrHits << " numOfColumnIndexMisses = " << numOfColumnIndexMisses << " numOfColumnIndexHits = " << numOfColumnIndexHits << endl;
	cout << "Time_RD (in sec): " << time / (1000000000) << endl;
	return;
}
