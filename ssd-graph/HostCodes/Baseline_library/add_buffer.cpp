#include "graphFiltering.h"
//Also keep an array to indicate which vertices have been added
bool myComparator(std::pair<unsigned int, unsigned int> comparisionObject1, std::pair<unsigned int, unsigned int> comparisionObject2)
{
	return (comparisionObject1.first < comparisionObject2.first);
}

std::set<unsigned int> verticesToMerge;

std::vector<unsigned int> bufferRowPtrArray;//Need to initialize this, you can initialize this where you initialize other things
std::vector<unsigned int> bufferColumnIndexPtrArray;
extern unsigned int numOfNodes;

unsigned int currentTopInTheLog = 1;//For first vertex keep it as 1
void AddToBuffer(char *bufferFile)
{
#if(ADD_UPDATES_TO_BUFFER == 1)
	bufferRowPtrArray.resize(numOfNodes, 0);
	bufferColumnIndexPtrArray.push_back(0);
#endif//ADD_UPDATES_TO_BUFFER

	FILE *bufferFilePtrHandle = fopen(bufferFile, "r");
	std::vector< std::pair<unsigned int, unsigned int> > randomEdges;
	unsigned int srcId, targetId;
	while(fscanf(bufferFilePtrHandle, "%d %d", &srcId, &targetId) != EOF)
	{
		randomEdges.push_back(std::make_pair(srcId, targetId));
	}
	fclose(bufferFilePtrHandle);
	sort(randomEdges.begin(), randomEdges.end(), myComparator);
	unsigned int prevSrcId = -1;
	unsigned int lastEntryInTheLog;
	vector<unsigned int> EdgeList;
	for(std::vector<std::pair<unsigned int, unsigned int> >::iterator it = randomEdges.begin(); it != randomEdges.end(); it++)
	{
		srcId = it->first, targetId = it->second;
		verticesToMerge.insert(srcId);
		if(prevSrcId != srcId)
		{
			if(prevSrcId != -1)
			{
				bufferRowPtrArray[prevSrcId] = currentTopInTheLog;
				bufferColumnIndexPtrArray.push_back(EdgeList.size());
				currentTopInTheLog++;
				for(unsigned int i=0; i < EdgeList.size(); i++)
				{
				bufferColumnIndexPtrArray.push_back(EdgeList[i]);
				currentTopInTheLog++;
				}
				bufferColumnIndexPtrArray.push_back(lastEntryInTheLog);
				currentTopInTheLog++;
				EdgeList.clear();
			}
			lastEntryInTheLog = bufferRowPtrArray[srcId];
			prevSrcId = srcId;
		}
		EdgeList.push_back(targetId);
	}
	if(prevSrcId != -1)
	{
		bufferRowPtrArray[prevSrcId] = currentTopInTheLog;
		bufferColumnIndexPtrArray.push_back(EdgeList.size());
		currentTopInTheLog++;
		for(unsigned int i=0; i < EdgeList.size(); i++)
		{
			bufferColumnIndexPtrArray.push_back(EdgeList[i]);
			currentTopInTheLog++;
		}
		bufferColumnIndexPtrArray.push_back(lastEntryInTheLog);
		currentTopInTheLog++;
		EdgeList.clear();
	}
}

void AccessLinkedListBuffer(unsigned int vertexId, vector<unsigned int> &EdgeList)
{
	unsigned int address = bufferRowPtrArray[vertexId];
	while(address != 0)
	{
		for(unsigned int i=0; i < bufferColumnIndexPtrArray[address]; i++)
		{
			EdgeList.push_back(bufferColumnIndexPtrArray[address+i+1]);//use it as address
		}
		address = bufferColumnIndexPtrArray[address+bufferColumnIndexPtrArray[address]+1];
	}
}

void MergeBuffer()
{
	//Merge all of them to a buffer in main memory and from there call the merge routine
	//Which vertices to access? which vertices to access to? which vertices to access to? which vertices to access to? which vertices to? which vertices to access to? which vertices to access to? which vertices to access to? which vertices to access to? Assume a list of modified vertices, you can assume it as a map, what will be the performance of the scheme? assume x% of page are accessed during the merging process and y pages are accessed for the indirection pointers, then what is the cost of merging? (xP/100) + y
	std::vector< std::pair<unsigned int, unsigned int> > EdgesToAddInMemory;
	vector<unsigned int> EdgeList;
	for(std::set<unsigned int>::iterator it = verticesToMerge.begin(); it != verticesToMerge.end(); it++)//Items in set needs to be sorted
	{
		AccessLinkedListBuffer(*it, EdgeList);
		//Add edgelist to an array in main memory, what is that array?
		for(unsigned int i = 0; i < EdgeList.size(); i++)
		{
			EdgesToAddInMemory.push_back(std::make_pair(*it, EdgeList[i]));
		}
	}
	AddEdge(EdgesToAddInMemory);

	//Modify merge buffer to accept array as a parameter
//	Call MergeBuffer
	//Then get the file from rohit and store initial file after converting it to CSR format
	//Then call store graph, call bfs, add updates, call bfs, merge updates, call bfs
	//for baseline also call store graph - done, call bfs - done, add updates - same as GraphSSD one, call bfs - for accessing updates - same as GraphSSD one, merge updates - what to do here?, call bfs
}
