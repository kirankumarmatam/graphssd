#include "graphFiltering.h"
#define CacheType 2
#include "lruCache_SSD.h"

extern map<unsigned int, unsigned int > vertexMapping;
extern void * data_nvme_command;
extern unsigned int numOfNodes;
extern unsigned int fd;

std::map<unsigned int, std::pair<char *, bool> >hostCache;
std::vector<unsigned int> randomEvictionVector;

unsigned int MaxCacheSize;
unsigned int CacheUtilized;
unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
unsigned int TotNumOfRequestsPerAccess_host = 0, TotNumOfRequestsPerAccess_SSD = 0;
unsigned int numOfCacheHits_aux = 0, numOfCacheEvictions_aux = 0, numOfCacheAccesses_aux = 0, numOfCacheMisses_aux = 0;

#if(VERTEX_CACHE_MAP == 1)
std::map<unsigned int, std::pair<char *, unsigned int> >hostCache_vertex;

bool cache_readAdjEdgeList_vertex(unsigned int vID, char *host_buffer)
{
	unsigned int vertexId = (vID | (1 << 31));
	if(hostCache_vertex.find(vertexId) == hostCache_vertex.end())
	{
		return 0;
	}
	else {
		memcpy((void *)host_buffer, &(hostCache_vertex[vertexId].second), sizeof(unsigned int));
		if(hostCache_vertex[vertexId].second != 0)
		{
			memcpy((void *)(host_buffer + sizeof(unsigned int)), hostCache_vertex[vertexId].first, hostCache_vertex[vertexId].second * sizeof(unsigned int) );
		}
		update_lruCache(vertexId);
		return 1;
	}
}

void cache_writeAdjEdgeList_vertex(unsigned int vID, char *host_buffer)
{
	unsigned int vertexId = (vID | (1 << 31));
	assert(hostCache_vertex.find(vertexId) == hostCache_vertex.end());
	int freeSpace = MaxCacheSize - CacheUtilized;
	int numOfAdjElements = sizeof(unsigned int) * (*((unsigned int *)host_buffer) + 1);
	struct nvme_user_io io;
	while(freeSpace < (numOfAdjElements))
	{
		numOfCacheEvictions++;
#if(CacheType == 1)
		int indexForEviction = rand() % randomEvictionVector.size();
		std::map<unsigned int, std::pair<char *, bool> >::iterator it1 = hostCache.find(randomEvictionVector[indexForEviction]);
#elif(CacheType == 2)
		int EvictedLPN = evict_lruCache();
		std::map<unsigned int, std::pair<char *, bool> >::iterator it1;
		if((EvictedLPN & (1 << 31)) == (1 << 31))
		{
			freeSpace += ((hostCache_vertex[EvictedLPN].second + 1) * sizeof(unsigned int));
			CacheUtilized -= ((hostCache_vertex[EvictedLPN].second+1) * sizeof(unsigned int));
			if(hostCache_vertex[EvictedLPN].second != 0)
			{
				free(hostCache_vertex[EvictedLPN].first);
			}
			hostCache_vertex.erase(EvictedLPN);
			continue;
		}
		it1 = hostCache.find(EvictedLPN);
#endif //CacheType
		assert(it1 != hostCache.end());
		freeSpace += (1 + 4096);
		CacheUtilized -= (1 + 4096);
		if(hostCache[it1->first].second == 1)
		{
			memcpy((void *)data_nvme_command, (void *)hostCache[it1->first].first, 4096);
			io.opcode = nvme_cmd_write;
			io.flags = 0;
			io.control = 0;
			io.metadata = (unsigned long)0;
			io.addr = (unsigned long)data_nvme_command;
			io.slba = it1->first;
			io.nblocks = 0;
			io.dsmgmt = 0;
			io.reftag = 0;
			io.apptag = 0;
			io.appmask = 0;
			int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
			if (err < 0)
				cout << "Error in executing nvme command!!" << endl;
			if (err)
			{
				fprintf(stderr, "nvme read status:%x\n", err);
				exit(0);
			}
		}
		free((it1->second).first);
		hostCache.erase(it1);
#if(CacheType == 1)
		randomEvictionVector[indexForEviction] = randomEvictionVector.back();
		randomEvictionVector.pop_back();
#endif //CacheType == 2
	}

	hostCache_vertex[vertexId].second = *(unsigned int *)host_buffer;
	if(hostCache_vertex[vertexId].second != 0)
	{
		hostCache_vertex[vertexId].first = (char *) malloc(sizeof(unsigned int) * (*(unsigned int *)host_buffer));
		memcpy(hostCache_vertex[vertexId].first, host_buffer + sizeof(unsigned int), sizeof(unsigned int) * hostCache_vertex[vertexId].second);
	}
	CacheUtilized += sizeof(unsigned int) * (hostCache_vertex[vertexId].second + 1);
	update_lruCache(vertexId);
}
#endif//VERTEX_CACHE_MAP

bool cache_readAdjEdgeList(unsigned int LPN, unsigned int Number, char **data_pointer)
{
	numOfCacheAccesses++;
//	TotNumOfRequestsPerAccess_host += (Number - (LPN % (4)) + 3) / 4 + ((LPN % 4 == 0) ? 0 : 1);
	TotNumOfRequestsPerAccess_host += ((Number + 3) - ((4 - (LPN % (4)))%4)) / 4 + ((LPN % 4 == 0) ? 0 : 1);
	int miss = 0;
	for(unsigned int i=0; i<Number; i++)
	{
		if(hostCache.find(LPN+i) == hostCache.end())
		{
			miss = 1;
			break;
		}
	}
#if (VERBOSE == 1)
	cout << "LPN = " << LPN <<" Number = " << Number << endl;
	cout << "miss = " << miss << endl;
#endif
//Allocate and free memory for data_pointer
	if(miss == 0)
	{
		numOfCacheHits++;
		//push the values in to the EdgeList
		for(unsigned int i=0; i < Number; i++)
		{
			data_pointer[i] = hostCache[LPN+i].first;
#if(CacheType == 2)
			update_lruCache(LPN+i);
#endif //CacheType
		}
		return 0;
	}
	else {
		numOfCacheMisses++;
		TotNumOfRequestsPerAccess_SSD += ((Number + 3) - ((4 - (LPN % (4)))%4)) / 4 + ((LPN % 4 == 0) ? 0 : 1);
//		cout << "T_S = " << numOfCacheMisses << " " << TotNumOfRequestsPerAccess_SSD << " N = " << Number << " LPN = " << LPN << endl;
		struct nvme_user_io io;

		int freeSpace = MaxCacheSize - CacheUtilized;
		int numOfAdjElements = 4096 * Number;
#if (VERBOSE == 1)
		cout << "freeSpace = " << freeSpace << " CacheUtilized = " << CacheUtilized << " LPN =  "<< LPN << " numOfAdjElements = " << numOfAdjElements << " EdgeListSpace = " << (Number + numOfAdjElements) << endl;
		for(unsigned int i = 0; i < Number * 4096 / sizeof(unsigned int); i++)
			cout << ((unsigned int *)data_nvme_command)[i] << " ";
		cout << endl;
#endif
		while(freeSpace < (Number + numOfAdjElements))
		{
			numOfCacheEvictions++;
#if(CacheType == 1)
			int indexForEviction = rand() % randomEvictionVector.size();
			std::map<unsigned int, std::pair<char *, bool> >::iterator it1 = hostCache.find(randomEvictionVector[indexForEviction]);
#elif(CacheType == 2)
			int EvictedLPN = evict_lruCache();
			std::map<unsigned int, std::pair<char *, bool> >::iterator it1 = hostCache.find(EvictedLPN);
#if(VERTEX_CACHE_MAP)
			if((EvictedLPN & (1 << 31)) == (1 << 31))
			{
				freeSpace += ((hostCache_vertex[EvictedLPN].second + 1) * sizeof(unsigned int));
				CacheUtilized -= ((hostCache_vertex[EvictedLPN].second+1) * sizeof(unsigned int));
				if(hostCache_vertex[EvictedLPN].second != 0)
				{
					free(hostCache_vertex[EvictedLPN].first);
				}
				hostCache_vertex.erase(EvictedLPN);
				continue;
			}
#endif//VERTEX_CACHE_MAP
#endif //CacheType
			assert(it1 != hostCache.end());
			freeSpace += (1 + 4096);
			CacheUtilized -= (1 + 4096);
			if(hostCache[it1->first].second == 1)
			{
				memcpy((void *)data_nvme_command, (void *)hostCache[it1->first].first, 4096);
				io.opcode = nvme_cmd_write;
				io.flags = 0;
				io.control = 0;
				io.metadata = (unsigned long)0;
				io.addr = (unsigned long)data_nvme_command;
				io.slba = it1->first;
				io.nblocks = 0;
				io.dsmgmt = 0;
				io.reftag = 0;
				io.apptag = 0;
				io.appmask = 0;
				int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
				if (err < 0)
					cout << "Error in executing nvme command!!" << endl;
				if (err)
				{
					fprintf(stderr, "nvme read status:%x\n", err);
					exit(0);
				}
			}
			free((it1->second).first);
			hostCache.erase(it1);
#if(CacheType == 1)
			randomEvictionVector[indexForEviction] = randomEvictionVector.back();
			randomEvictionVector.pop_back();
#endif //CacheType == 2
		}
		io.opcode = nvme_cmd_read;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		io.slba = LPN;
		io.nblocks = Number - 1;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;
		int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		if (err < 0)
			cout << "Error in executing nvme command!!" << endl;
		if (err)
		{
			fprintf(stderr, "nvme read status:%x\n", err);
			exit(0);
		}

		for(unsigned int i=0; i < Number; i++)
		{
			if(hostCache.find(LPN + i) == hostCache.end())
			{
				hostCache[LPN+i];
				hostCache[LPN+i].first = new char [4096];
				hostCache[LPN+i].second = 0;
				memcpy((void *)hostCache[LPN+i].first, (void *)((char *)data_nvme_command + 4096 * i), 4096);
#if(CacheType == 1)
				randomEvictionVector.push_back(LPN+i);
#endif//CacheType
				CacheUtilized += (1 + 4096);
			}
			data_pointer[i] = hostCache[LPN+i].first;
#if(CacheType == 2)
			update_lruCache(LPN+i);
#endif//CacheType
		}
		return 1;
	}
}
//Need to take care of the cases where number of adjacent vertices for a vertex are zero

//writeAdjEdgeList -- no need to support it now

void cache_readNumber(unsigned int LPN, unsigned int Offset, unsigned int Number, AuxDataValueType &value, bool isRead)
{
	numOfCacheAccesses++;
	numOfCacheAccesses_aux++;
	TotNumOfRequestsPerAccess_host += ((Number + 3) - ((4 - (LPN % (4)))%4)) / 4 + ((LPN % 4 == 0) ? 0 : 1);
	int miss = 0;
	if(hostCache.find(LPN) == hostCache.end())
	{
		miss = 1;
	}
	if(miss == 0)
	{
		numOfCacheHits++;
		numOfCacheHits_aux++;
		if(isRead == 1)
			value = ((AuxDataValueType *)(hostCache[LPN].first))[Offset];
		else {
			((AuxDataValueType *)(hostCache[LPN].first))[Offset] = value;
			hostCache[LPN].second = 1;
		}
#if(CacheType == 2)
		update_lruCache(LPN);
#endif //CacheType
	}
	else {
		numOfCacheMisses++;
		numOfCacheMisses_aux++;
		TotNumOfRequestsPerAccess_SSD += ((Number + 3) - ((4 - (LPN % (4)))%4)) / 4 + ((LPN % 4 == 0) ? 0 : 1);
		struct nvme_user_io io;
		int freeSpace = MaxCacheSize - CacheUtilized;

		int numOfAdjElements = 4096 * Number;
		while(freeSpace < (Number + numOfAdjElements))
		{
			numOfCacheEvictions++;
			numOfCacheEvictions_aux++;
#if(CacheType == 1)
			int indexForEviction = rand() % randomEvictionVector.size();
			std::map<unsigned int, std::pair<char *, bool> >::iterator it1 = hostCache.find(randomEvictionVector[indexForEviction]);
#elif(CacheType == 2)
			int EvictedLPN = evict_lruCache();
			std::map<unsigned int, std::pair<char *, bool> >::iterator it1 = hostCache.find(EvictedLPN);
#if(VERTEX_CACHE_MAP)
			if((EvictedLPN & (1 << 31)) == (1 << 31))
			{
				freeSpace += ((hostCache_vertex[EvictedLPN].second + 1) * sizeof(unsigned int));
				CacheUtilized -= ((hostCache_vertex[EvictedLPN].second+1) * sizeof(unsigned int));
				if(hostCache_vertex[EvictedLPN].second != 0)
				{
					free(hostCache_vertex[EvictedLPN].first);
				}
				hostCache_vertex.erase(EvictedLPN);
				continue;
			}
#endif//VERTEX_CACHE_MAP

#endif //CacheType
 			assert(it1 != hostCache.end());
			freeSpace += (1 + 4096);
			CacheUtilized -= (1 + 4096);
			if(hostCache[it1->first].second == 1)
			{
				memcpy((void *)data_nvme_command, (void *)hostCache[it1->first].first, 4096);
				io.opcode = nvme_cmd_write;
				io.flags = 0;
				io.control = 0;
				io.metadata = (unsigned long)0;
				io.addr = (unsigned long)data_nvme_command;
				io.slba = it1->first;
				io.nblocks = 0;
				io.dsmgmt = 0;
				io.reftag = 0;
				io.apptag = 0;
				io.appmask = 0;
				int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
				if (err < 0)
					cout << "Error in executing nvme command!!" << endl;
				if (err)
				{
					fprintf(stderr, "nvme read status:%x\n", err);
					exit(0);
				}
			}
			free((it1->second).first);
			hostCache.erase(it1);
#if(CacheType == 1)
			randomEvictionVector[indexForEviction] = randomEvictionVector.back();
			randomEvictionVector.pop_back();
#endif //CacheType == 2
		}
		io.opcode = nvme_cmd_read;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		io.slba = LPN;
		io.nblocks = Number - 1;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;
		int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		if (err < 0)
			cout << "Error in executing nvme command!!" << endl;
		if (err)
		{
			fprintf(stderr, "nvme read status:%x\n", err);
			exit(0);
		}


		for(unsigned int i=0; i < Number; i++)
		{
			if(hostCache.find(LPN + i) == hostCache.end())
			{
				hostCache[LPN+i];
				hostCache[LPN+i].first = new char [4096];
				memcpy((void *)hostCache[LPN+i].first, (void *)((char *)data_nvme_command + 4096 * i), 4096);
#if(CacheType == 1)
				randomEvictionVector.push_back(LPN+i);
#endif//CacheType
				CacheUtilized += (1 + 4096);
			}
#if(CacheType == 2)
			update_lruCache(LPN+i);
#endif//CacheType
		}
		if(isRead == 1)
			value = ((AuxDataValueType *)(hostCache[LPN].first))[Offset];
		else {
			((AuxDataValueType *)(hostCache[LPN].first))[Offset] = value;
			hostCache[LPN].second = 1;
		}
	}
	return;
}
