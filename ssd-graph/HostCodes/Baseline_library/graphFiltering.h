#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

#include <time.h>

using namespace std;
#define SLBA 200
#define SLBA_WEIGHT 12863678
#define SLBA_FLUSH_BUFFER 25727355

#define CALCULATE_DATA_FETCHED  0
#define CALCULATE_OVERHEAD_TIME 1

#define useCache 1
#define VERTEX_CACHE_MAP 1
#define VERBOSE 0

#define FLUSH_SSD_BUFFER 1
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define STORE_LARGE_SCALE 1

#define IsUndirectedGraph 1
#define SSD_PAGE_SIZE 16384
#define SECTOR_SIZE 4096

int set_context(int argc, char **argv);
void GetAdjList(unsigned int vID, vector<unsigned int> &EdgeList);
void GetAdjEdgeWeights(unsigned int vID, vector<unsigned int> &EdgeList);
void WriteToSSD(unsigned int j);
void WriteToSSDN_SLBA(unsigned int j, unsigned int num);
void InitializeGraphDataStructures(unsigned int NumNodes);
void FinalizeGraphDataStructures();
bool cache_readAdjEdgeList(unsigned int LPN, unsigned int Number, char **data_pointer);
#if(VERTEX_CACHE_MAP == 1)
bool cache_readAdjEdgeList_vertex(unsigned int vID, char *host_buffer);
void cache_writeAdjEdgeList_vertex(unsigned int vID, char *host_buffer);
#endif//VERTEX_CACHE_MAP
void printTime();

#define AuxMemoryInStorage 1
#define AUXLBA	25743739
#define AuxDataValueType bool
//#define AuxDataValueType unsigned int
void AccessAuxMemory(unsigned int index, AuxDataValueType &value, bool isRead);
void cache_readNumber(unsigned int LPN, unsigned int Offset, unsigned int Number, AuxDataValueType &value, bool isRead);

#define Update_using_buffer 1
#if(Update_using_buffer == 1)
void AddToBuffer(char *bufferFile);
void AccessLinkedListBuffer(unsigned int vertexId, vector<unsigned int> &EdgeList);
void MergeBuffer();
#endif//Update_using_buffer
