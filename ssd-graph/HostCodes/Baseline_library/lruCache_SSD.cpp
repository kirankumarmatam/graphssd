#include "lruCache_SSD.h"

#define LRUVectorImplementation 0

#if(LRUVectorImplementation == 0)
std::list<unsigned int> LRUlist;
std::map<unsigned int, std::list<unsigned int>::iterator > LRUmap;

inline void delete_lruCache(unsigned int LPN)
{
	std::map<unsigned int, std::list<unsigned int>::iterator >::iterator it = LRUmap.find(LPN);
	if(it != LRUmap.end())
	{
		LRUlist.erase(it->second);
		LRUmap.erase(it);
	}
}

void update_lruCache(unsigned int LPN)
{
	delete_lruCache(LPN);
	LRUlist.push_front(LPN);
	LRUmap[LPN] = LRUlist.begin();
}

unsigned int evict_lruCache()
{
	unsigned int LPN = LRUlist.back();
	LRUlist.pop_back();
	LRUmap.erase(LPN);
	return LPN;
}
#elif(LRUVectorImplementation == 0)
std::vector<unsigned int> LRU_vector;

void delete_lruCache(unsigned int LPN)
{
	for(std::vector<unsigned int>::iterator it=LRU_vector.begin(); it != LRU_vector.end(); it++)
	{
		if(*it == LPN)
		{
			LRU_vector.erase(it);
			break;
		}
	}
}

void update_lruCache(unsigned int LPN)
{
	delete_lruCache(LPN);
	LRU_vector.insert(LRU_vector.begin(), LPN);
}

unsigned int evict_lruCache()
{
	unsigned int LPN = LRU_vector.back();
	LRU_vector.pop_back();
	return LPN;
}
#endif//LRUVectorImplementation
