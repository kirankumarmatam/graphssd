#include "graphFiltering.h"

extern unsigned int numOfNodes;
extern unsigned int numOfEdges;
extern void *data_nvme_command;

void MergeBuffer()
{
	std::vector<unsigned int> TemporaryMergeArray;
	for(unsigned int i = 0; i < (numOfNodes+1); i++)
	{
		TemporaryMergeArray.push_back(0);
	}
	TemporaryMergeArrayTopPtr = 0;
	std::vector<unsigned int> EdgeList1, EdgeList2;
	unsigned int i = 0, j = 0;
	while(i < numOfNodes)//For now discard adding vertices beyond numOfNodes
	{
		if(i > j)
		{
			assert(0);
		}
		EdgeList1.clear();
		GetAdjList(i, EdgeList1);
		for(unsigned int index = 0; index < EdgeList1.size(); index++)
		{
			TemporaryMergeArray.push_back(EdgeList1[index]);
		}
		TemporaryMergeArrayTopPtr += EdgeList1.size();
		unsigned int currentVertexSize = EdgeList1.size();
		if(i == j)
		{
			EdgeList2.clear();
			AccessLinkedListBuffer(j, EdgeList2);
			unsigned int addedValues = 0;
			for(unsigned int index = 0; index < EdgeList2.size(); index++)
			{
				if((currentVertexSize++) >= (16*4096 / sizeof(unsigned int)))
				{
					break;
				}
				addedValues++;
				TemporaryMergeArray.push_back(EdgeList2[index]);
			}
			TemporaryMergeArrayTopPtr += addedValues;
			j++;
		}
		TemporaryMergeArray[i+1] = TemporaryMergeArrayTopPtr;
		i++;
	}
	unsigned int numOfItemsToCopy = TemporaryMergeArray.size();
	numOfEdges = TemporaryMergeArrayTopPtr;
	for(unsigned int index = 0; index < numOfItemsToCopy; index += (SSD_PAGE_SIZE / sizeof(unsigned int)))
	{
		memcpy(data_nvme_command, TemporaryMergeArray.data() + index; SSD_PAGE_SIZE);
		WriteToSSDN((index * sizeof(unsigned int)) / SECTOR_SIZE, SSD_PAGE_SIZE / SECTOR_SIZE);
	}
}
