#include "../Baseline_library/graphFiltering.h"
#define PAGE_PER_CMD 1
#define VERBOSE_PRINT_ADJLIST 1
#define MEASURE_IO_TIME	1
#define PERFORM_GET_EDGE 1
extern unsigned int numOfNodes, numOfEdges;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
struct timespec time_start_overhead, time_end_overhead;
double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;
extern map<unsigned int, unsigned int > vertexMapping;
extern PNGraph G;

int main(int argc, char **argv)
{
	set_context(argc, argv);
	srand(5);
	cout << "numOfNodes = " << numOfNodes << " numOfEdges = " << numOfEdges << endl;

	// Declarations of Graph filter related quantites.
	unsigned int filter_length = 30;
	vector<unsigned int> EdgeList;
	vector<float> inputSignal(numOfNodes), outputSignal(numOfNodes);
	vector<float>filterCoefficients(filter_length);

	for (unsigned int vert = 0; vert < numOfNodes; vert++){
		inputSignal[vert] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		outputSignal[vert] = 0;
	}

	for (unsigned int coeff = 0; coeff < filter_length; coeff++)
		filterCoefficients[coeff] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/10);

	////

	struct timespec time_start, time_end;
	double time_elapsed;

#if (PERFORM_GET_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0;
	while (i < filter_length)
	{
		for (unsigned int vert = 0; vert < numOfNodes; vert++){
			GetAdjList(i, EdgeList);
			for (unsigned int neighbor = 0; neighbor < EdgeList.size(); neighbor++){
				outputSignal[vert] += inputSignal[EdgeList[neighbor]];//*weight of each edge;
			}
			outputSignal[vert] *= filterCoefficients[i];
			EdgeList.clear();
		}
		i++;
		inputSignal = outputSignal;
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#endif //PERFORM_GET_EDGE
	FinalizeGraphDataStructures();
#if(useCache == 1)
	printTime();
#endif
	return 0;

	//perror:
	//	perror(perrstr);
	//	return 1;
}
