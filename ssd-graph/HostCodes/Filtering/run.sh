#!/bin/bash
FILES=~/GraphSSD/Benchmarks/SNAP/Run/*
sh compile_storeInCSR.sh
sh compile_bfsSSD.sh
for i in $FILES
do
	echo $i
	sudo ./storeInCSR /dev/nvme0n1 $i
	sudo ./BFS /dev/nvme0n1 $i
done
