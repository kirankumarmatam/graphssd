#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_BFS 1
#define VERBOSE_BFS 0
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1
#define PRINT_LEVEL_NUMBER 0
#define READ_REQUESTS_FROM_FILE 0 // This is incomplete
#define READ_REQUEST_AS_ARGUMENT 1

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;
extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	cout << NumNodes << " " << NumEdges << endl;

	// Declarations of Graph filter related quantites.
	unsigned int filter_length = 10;
	vector<unsigned int> EdgeList;
	vector<float> inputSignal(NumNodes), outputSignal(NumNodes);
	vector<float>filterCoefficients(filter_length);

	for (unsigned int vert = 0; vert < NumNodes; vert++){
		inputSignal[vert] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		outputSignal[vert] = 0;
	}

	for (unsigned int coeff = 0; coeff < filter_length; coeff++)
		filterCoefficients[coeff] = static_cast <float> (rand()) / static_cast <float> (RAND_MAX/10);

	struct timespec time_start, time_end;
	double time_elapsed;

#if (PERFORM_BFS == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0;
	while (i < filter_length)
	{
		for (unsigned int vert = 0; vert < NumNodes; vert++){
			SSDInstance.GetAdjListUseNvmeCommands(i, EdgeList);
			for (unsigned int neighbor = 0; neighbor < EdgeList.size(); neighbor++){
				outputSignal[vert] += inputSignal[EdgeList[neighbor]];//*weight of each edge;
			}
			outputSignal[vert] *= filterCoefficients[i];
			EdgeList.clear();
		}
		i++;
		inputSignal = outputSignal;
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	SSDInstance.FinalizeGraphDataStructures();	
#if(useCache == 1)
	SSDInstance.printTime();
#endif//useCache
#endif //PERFORM_BFS
	return 0;
}
