#include "../Baseline_library/graphFiltering.h"
#include <limits>
#define PAGE_PER_CMD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 0
#define FLUSH_SSD_BUFFER 1
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define STORE_LARGE_SCALE 0

#define INS 3
#define TENTAINS 2
#define NOTINS 1
#define UNKNOWN 0

#define ISCOLORED 128
#define NOTMIN 4
#define NEIGHBORINS 8

extern unsigned int numOfNodes;
extern unsigned int numOfEdges;
extern unsigned int fd;
extern void *data_nvme_command;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
struct timespec time_start_overhead, time_end_overhead;
double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

int main(int argc, char **argv)
{
	set_context(argc, argv);

	vector<unsigned int> EdgeList;
    unsigned char *vertex_props=(unsigned char *)calloc(numOfNodes, sizeof(unsigned char));
    bool AllColored=0;
    bool HasUnkonwn=0;
    unsigned int TotalCount=0;
    struct timespec time_start, time_end;
    double time_elapsed;
    clock_gettime(CLOCK_MONOTONIC, &time_start);
#if(AuxMemoryInStorage == 0)
    short int *degree = (short int *)calloc(numOfNodes, sizeof(short int));
#else
    for(unsigned int i =0; i < numOfNodes; i++)
    {
        AuxDataValueType value = 0;
        AccessAuxMemory(i, value, (bool)0);
    }
#endif
    for(int numOfrun = 0; numOfrun < 1; numOfrun++)
    {

#if(STOP_SIMULATION == 1)
        unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1
//        srand(time(NULL));
        while (!AllColored)
        {
            for(unsigned int i =0; i < numOfNodes; i++)//calculate degree and set to unknown
            {
                if (vertex_props[i]<ISCOLORED)//not colored, set to unknown and calculate degree
                {
                    EdgeList.clear();
                    GetAdjList(i, EdgeList);
#if(AuxMemoryInStorage == 0)
		    degree[i] = 0;
#else
                            AuxDataValueType tmp = 0;
#endif//AuxMemoryInStorage
                    for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
                    {

                        if (vertex_props[*it]<ISCOLORED)// neighbor is not colored, add degree
                        {
#if(AuxMemoryInStorage == 0)
                            degree[i]++;
#else
                            tmp++;
#endif
                        }
                    }
#if(AuxMemoryInStorage == 1)
                            AccessAuxMemory(i, tmp, 0);
#endif//AuxMemoryInStorage
                    vertex_props[i]=0;//set to unknown,and reset everything
                    HasUnkonwn=1;
                }
            }
            while(HasUnkonwn)
            {
                bool fired=0;
                while(!fired)
                {
                    for(unsigned int i =0; i < numOfNodes; i++)//based on probability,set to TentativelyInS
                    {
                        if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==UNKNOWN)//not colored and is unknown
                        {
#if(AuxMemoryInStorage == 0)
                            short int current_degree = degree[i];
#else
                            AuxDataValueType current_degree;
                            AccessAuxMemory(i, current_degree, 1);
#endif
                            float p=(float)rand()/(float)(RAND_MAX);
                            if (p*2*current_degree<1.0)
                            {
                                fired=1;
                                vertex_props[i]+=2; //set to TentativelyInS
                                EdgeList.clear();
                                GetAdjList(i, EdgeList);
                                for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
                                {
                                    if (i<*it)
                                    {
                                        vertex_props[*it]|=NOTMIN; //id less than neighbor, set neighbor to not_min
                                    }
                                }
                            }
                        }
                    }
                }

                for(unsigned int i =0; i < numOfNodes; i++)//if in TentativelyInS and is not not_min, set to InS
                {
                    if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==TENTAINS)//not colored and in TentativelyInS
                    {
                        if (vertex_props[i]%8==TENTAINS)//is not not_min
                        {
                            vertex_props[i]+=(INS-TENTAINS); //set to InS
                            EdgeList.clear();
                            GetAdjList(i, EdgeList);
                            for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
                            {
                                vertex_props[*it]|=NEIGHBORINS; //set neighbor to neighborInS (unharmful to set colored neighbor to not InS)
                            }
                        }
                        else
                        {
                            vertex_props[i]-=TENTAINS;//set to unknown
                        }
                    }
                }

                HasUnkonwn=0;
                for(unsigned int i =0; i < numOfNodes; i++)//if in neighborInS set self to notInS and decrease unknown neighbor's degree
                {
                    if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==UNKNOWN)//not colored and in unknown
                    {
                        if (vertex_props[i]%16>=NEIGHBORINS)//is neighborInS
                        {
                            vertex_props[i]+=NOTINS; //set to NotInS
                            EdgeList.clear();
                            GetAdjList(i, EdgeList);
                            for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)//decrease unknown neighbor's degree
                            {
                                if (vertex_props[*it]%4==UNKNOWN)
                                {
#if(AuxMemoryInStorage == 0)
                                    degree[*it]--;
#else
                                    AuxDataValueType tmp = 0;
                                    AccessAuxMemory(*it, tmp, 1);
                                    tmp--;
                                    AccessAuxMemory(*it, tmp, 0);
#endif
                                }
                            }
                        }
                        else //remain unknown
                        {
                            vertex_props[i]=0;
                            HasUnkonwn=1;
                        }
                    }

                }
            }
            AllColored=1;
            for(unsigned int i =0; i < numOfNodes; i++)
            {
                if (vertex_props[i]<ISCOLORED)
                {
                    if (vertex_props[i]%4==INS)
                    {
                        vertex_props[i]+=ISCOLORED;
                        //cout << i << endl;
                    }
                    else
                    {
                        AllColored=0;
                    }
                }
            }
            TotalCount++;
            cout <<"aaa:"<< TotalCount << endl;
        }

        cout << TotalCount << endl;
        TotalCount=0;
    }
    free(vertex_props);
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
    time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
    FinalizeGraphDataStructures();
#if(useCache == 1)
    printTime();
#endif


//perror:
//	perror(perrstr);
//	return 1;
}
