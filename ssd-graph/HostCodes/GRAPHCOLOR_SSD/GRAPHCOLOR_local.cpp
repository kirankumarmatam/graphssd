#include "../Baseline_library/header.h"
#include <limits>

#define INS 3
#define TENTAINS 2
#define NOTINS 1
#define UNKNOWN 0

#define ISCOLORED 128
#define NOTMIN 4
#define NEIGHBORINS 8
using namespace std;

unsigned int numOfNodes;
unsigned int numOfEdges;

int main(int argc, char **argv)
{
    PUNGraph G1 = TSnap::LoadEdgeList<PUNGraph>(argv[1], 0, 1);
    PNGraph G = TSnap::ConvertGraph<PNGraph>(G1);
    numOfNodes = G->GetNodes();
    numOfEdges = G->GetEdges();

    vector<unsigned int> EdgeList;
    unsigned char *vertex_props=(unsigned char *)calloc(numOfNodes, sizeof(unsigned char));
    bool AllColored=0;
    bool HasUnkonwn=0;
    unsigned int TotalCount=0;
    short int *degree = (short int *)calloc(numOfNodes, sizeof(short int));

    for(int numOfrun = 0; numOfrun < 1; numOfrun++)
    {
//        srand(time(NULL));
        while (!AllColored)
        {
            for(unsigned int i =0; i < numOfNodes; i++)//calculate degree and set to unknown
            {
                if (vertex_props[i]<ISCOLORED)//not colored, set to unknown and calculate degree
                {
                    TNGraph::TNodeI NI = G->GetNI(i);
                    EdgeList.clear();
					degree[i]=0;
                    for (int e = 0; e < NI.GetOutDeg(); e++)
                    {
                        EdgeList.push_back(NI.GetOutNId(e));
                    }
                    for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
                    {

                        if (vertex_props[*it]<ISCOLORED)// neighbor is not colored, add degree
                        {
                            degree[i]++;
                        }
                    }
                    vertex_props[i]=0;//set to unknown,and reset everything
                    HasUnkonwn=1;
                }
            }
            while(HasUnkonwn)
            {
                bool fired=0;
                while (!fired)
                {
                    for(unsigned int i =0; i < numOfNodes; i++)//based on probability,set to TentativelyInS
                    {
                        if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==UNKNOWN)//not colored and is unknown
                        {
                            short int current_degree = degree[i];
                            float p=(float)rand()/(float)(RAND_MAX);
                            if (p*2*current_degree<1.0)
                            {
                                vertex_props[i]+=2; //set to TentativelyInS
                                fired=1;
                                TNGraph::TNodeI NI = G->GetNI(i);
                                EdgeList.clear();
                                for (int e = 0; e < NI.GetOutDeg(); e++)
                                {
                                    EdgeList.push_back(NI.GetOutNId(e));
                                }

                                for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
                                {
                                    if (i<*it)
                                    {
                                        vertex_props[*it]|=NOTMIN; //id less than neighbor, set neighbor to not_min
                                    }
                                }
                            }
                        }
                    }
                }

                for(unsigned int i =0; i < numOfNodes; i++)//if in TentativelyInS and is not not_min, set to InS
                {
                    if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==TENTAINS)//not colored and in TentativelyInS
                    {
                        if (vertex_props[i]%8==TENTAINS)//is not not_min
                        {
                            vertex_props[i]+=(INS-TENTAINS); //set to InS
                            TNGraph::TNodeI NI = G->GetNI(i);
                            EdgeList.clear();
                            for (int e = 0; e < NI.GetOutDeg(); e++)
                            {
                                EdgeList.push_back(NI.GetOutNId(e));
                            }
                            for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
                            {
                                vertex_props[*it]|=NEIGHBORINS; //set neighbor to neighborInS (unharmful to set colored neighbor to not InS)
                            }
                        }
                        else
                        {
                            vertex_props[i]-=TENTAINS;//set to unknown
                        }
                    }
                }

                HasUnkonwn=0;
                for(unsigned int i =0; i < numOfNodes; i++)//if in neighborInS set self to notInS and decrease unknown neighbor's degree
                {
                    if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==UNKNOWN)//not colored and in unknown
                    {
                        if (vertex_props[i]%16>=NEIGHBORINS)//is neighborInS
                        {
                            vertex_props[i]+=NOTINS; //set to NotInS
                            TNGraph::TNodeI NI = G->GetNI(i);
                            EdgeList.clear();
                            for (int e = 0; e < NI.GetOutDeg(); e++)
                            {
                                EdgeList.push_back(NI.GetOutNId(e));
                            }
                            for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)//decrease unknown neighbor's degree
                            {
                                if (vertex_props[*it]%4==UNKNOWN)
                                {
                                    degree[*it]--;
                                }
                            }
                        }
                        else //remain unknown
                        {
                            vertex_props[i]=0;
                            HasUnkonwn=1;
                        }
                    }

                }
            }
            AllColored=1;
            for(unsigned int i =0; i < numOfNodes; i++)
            {
                if (vertex_props[i]<ISCOLORED)
                {
                    if (vertex_props[i]%4==INS)
                    {
                        vertex_props[i]+=ISCOLORED;
                        //cout << i << endl;
                    }
                    else
                    {
                        AllColored=0;
                    }
                }
            }
            TotalCount++;
            cout <<"aaa:"<< TotalCount << endl;
        }

        cout << TotalCount << endl;
        TotalCount=0;
    }
    free(vertex_props);
    return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
