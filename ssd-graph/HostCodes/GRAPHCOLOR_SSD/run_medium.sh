#!/bin/bash
FILES=/Datasets/Benchmarks/SNAP/MediumGraphs/*
sh compile_graphcolorSSD.sh.sh
for i in $FILES
do
	echo $i
	sudo ../Baseline_library/storeInCSR /dev/nvme0n1 $i 10000
	sudo ./GRAPHCOLOR /dev/nvme0n1 $i 10000
done
