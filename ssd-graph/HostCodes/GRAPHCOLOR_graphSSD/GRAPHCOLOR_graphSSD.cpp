#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1

#define INS 3
#define TENTAINS 2
#define NOTINS 1
#define UNKNOWN 0

#define ISCOLORED 128
#define NOTMIN 4
#define NEIGHBORINS 8

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
extern unsigned int cache_percentage;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

#if(LOAD_GRAPH_FROM_FILE == 0)
	NumNodes = G->GetNodes(), NumEdges = G->GetEdges();
#endif
	cout << NumNodes << " " << NumEdges << endl;

	struct timespec time_start, time_end;
	double time_elapsed;

	vector<unsigned int> EdgeList;
	unsigned char *vertex_props=(unsigned char *)calloc(NumNodes, sizeof(unsigned char));
	bool AllColored=0;
	bool HasUnkonwn=0;
	unsigned int TotalCount=0;

	clock_gettime(CLOCK_MONOTONIC, &time_start);
	list<unsigned int> updatedQueue;

	unsigned int temp_debugging = 0;
#if(STOP_SIMULATION == 1)
	unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1

//	srand(time(NULL));
#if(AuxMemoryInStorage == 0)
#if(LOAD_GRAPH_FROM_FILE == 1)
	short int *degree = (short int *)calloc(NumNodes, sizeof(short int));
#else
	short int *degree = (short int *)calloc(vertexMapping.size(), sizeof(short int));
#endif
#else
	AuxDataValueType value = 0;
	for(unsigned int i=0; i < NumNodes; i++)
	{
		SSDInstance.AccessAuxMemory(i, value, 0);
	}
#endif//AuxMemoryInStorage
	while (!AllColored)
	{
		for(unsigned int i =0; i < NumNodes; i++)//calculate degree and set to unknown
		{
			if (vertex_props[i]<ISCOLORED)//not colored, set to unknown and calculate degree
			{
				EdgeList.clear();
				SSDInstance.GetAdjListUseNvmeCommands(i, EdgeList);
#if(AuxMemoryInStorage == 0)//changed degree initialization
				degree[i] = 0;
#else
				AuxDataValueType tmp = 0;
#endif//AuxMemoryInStorage
				for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
				{
					if (vertex_props[*it]<ISCOLORED)// neighbor is not colored, add degree
					{
#if(AuxMemoryInStorage == 0)
						degree[i]++;
#else
						tmp++;
#endif//AuxMemoryInStorage
					}
				}
#if(AuxMemoryInStorage == 1)
				SSDInstance.AccessAuxMemory(i, tmp, 0);
#endif//AuxMemoryInStorage
				vertex_props[i]=0;//set to unknown,and reset everything
				HasUnkonwn=1;
			}
		}
		while(HasUnkonwn)
		{
			bool fired=0;
			while(!fired)
			{
				for(unsigned int i =0; i < NumNodes; i++)//based on probability,set to TentativelyInS
				{
					if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==UNKNOWN)//not colored and is unknown
					{
#if(AuxMemoryInStorage == 0)
						short int current_degree = degree[i];
#else
						AuxDataValueType current_degree;
						SSDInstance.AccessAuxMemory(i, current_degree, 1);
#endif
						float p=(float)rand()/(float)(RAND_MAX);
						if (p*2*current_degree<1.0)
						{
							fired=1;
							vertex_props[i]+=2; //set to TentativelyInS
							EdgeList.clear();
							SSDInstance.GetAdjListUseNvmeCommands(i, EdgeList);
							for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
							{
								if (i<*it)
								{
									vertex_props[*it]|=NOTMIN; //id less than neighbor, set neighbor to not_min
								}
							}
						}
					}
				}
			}

			for(unsigned int i =0; i < NumNodes; i++)//if in TentativelyInS and is not not_min, set to InS
			{
				if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==TENTAINS)//not colored and in TentativelyInS
				{
					if (vertex_props[i]%8==TENTAINS)//is not not_min
					{
						vertex_props[i]+=(INS-TENTAINS); //set to InS
						EdgeList.clear();
						SSDInstance.GetAdjListUseNvmeCommands(i, EdgeList);
						for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
						{
							vertex_props[*it]|=NEIGHBORINS; //set neighbor to neighborInS (unharmful to set colored neighbor to not InS)
						}
					}
					else
					{
						vertex_props[i]-=TENTAINS;//set to unknown
					}
				}
			}

			HasUnkonwn=0;
			for(unsigned int i =0; i < NumNodes; i++)//if in neighborInS set self to notInS and decrease unknown neighbor's degree
			{
				if (vertex_props[i]<ISCOLORED && vertex_props[i]%4==UNKNOWN)//not colored and in unknown
				{
					if (vertex_props[i]%16>=NEIGHBORINS)//is neighborInS
					{
						vertex_props[i]+=NOTINS; //set to NotInS
						EdgeList.clear();
						SSDInstance.GetAdjListUseNvmeCommands(i, EdgeList);
						for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)//decrease unknown neighbor's degree
						{
							if (vertex_props[*it]%4==UNKNOWN)
							{
#if(AuxMemoryInStorage == 0)
								degree[*it]--;
#else
								AuxDataValueType tmp = 0;
								SSDInstance.AccessAuxMemory(*it, tmp, 1);
								tmp--;
								SSDInstance.AccessAuxMemory(*it, tmp, 0);
#endif
							}
						}
					}
					else //remain unknown
					{
						vertex_props[i]=0;
						HasUnkonwn=1;
					}
				}

			}
		}
		AllColored=1;
		for(unsigned int i =0; i < NumNodes; i++)
		{
			if (vertex_props[i]<ISCOLORED)
			{
				if (vertex_props[i]%4==INS)
				{
					vertex_props[i]+=ISCOLORED;
					//cout << i << endl;
				}
				else
				{
					AllColored=0;
				}
			}
		}
		TotalCount++;
		cout <<"aaa:"<< TotalCount << endl;
	}
	cout << TotalCount << endl;
	TotalCount=0;
#if(AuxMemoryInStorage == 0)
	free(degree);
#endif
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	SSDInstance.FinalizeGraphDataStructures();
#if(useCache == 1)
	SSDInstance.printTime();
#endif//useCache
	return 0;
}
