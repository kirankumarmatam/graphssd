#include "header.h"
#include "graphFiltering.h"

unsigned int SSDInterface::CopyFunction_InPage(char * devAddr, char * Test_variable_size_buffer, unsigned int currentTopPointer, unsigned int vertexId)
{
	assert(devAddr != NULL);
	unsigned int lowestVertexId = ((unsigned int *)devAddr)[(SSD_PAGE_SIZE / sizeof(unsigned int)) - 2];
	assert(vertexId >= lowestVertexId);
	unsigned int distance = vertexId - lowestVertexId;
	assert((3 * distance + 4) <= (SSD_PAGE_SIZE / sizeof(unsigned int)));
	unsigned int offset = ((unsigned int *)devAddr)[(SSD_PAGE_SIZE / sizeof(unsigned int)) - (3*distance + 3)];
	unsigned int number = ((unsigned int *)devAddr)[(SSD_PAGE_SIZE / sizeof(unsigned int)) - (3*distance + 4)];
	assert(offset <= SSD_PAGE_SIZE);
	assert((number * sizeof(unsigned int)) <= SSD_PAGE_SIZE);
	assert(currentTopPointer <= (1024*1024));
	memcpy((void *)(Test_variable_size_buffer + currentTopPointer), (void *)(devAddr + offset), number * sizeof(unsigned int));
	return number;
}

unsigned int SSDInterface::binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number)
{
        while (l < r)
        {
                int m = l + (r-l)/2;
//		printf("%d %d %d\n", l, m, r);

                if (arr[m].vertexId == x) 
                {
                        unsigned int start = m;
                        m = m-1;
                        while((m >= 0) && (arr[m].vertexId == x))
                        {
                                start = m;
                                m = m-1;
                        }
                        m = start;
                        unsigned int count=0;
                        while(((unsigned int)m <= r) && (arr[m].vertexId == x))
                        {
                                count++;
                                m++;
                        }
                        *index = start;
                        *number = count;
                        return 0;
                }
                else if (arr[m].vertexId < x) 
                {
                        if(x < arr[m+1].vertexId)
                        {
                                *index = m;
                                *number = 1;
                                return 0;
                        }
                        else if(x > arr[m+1].vertexId)
                        {
                                l = m+1;
                        }
                        else if(x == arr[m+1].vertexId)
                        {
                                unsigned int start = m+1;
                                m = start;
                                unsigned int count = 0;
                                while(((unsigned int)m <= r) && (arr[m].vertexId == x))
                                {
                                        count++;
                                        m++;
                                }
                                *index = start;
                                *number = count;
                                return 0;
                        }
                }
                else if(arr[m].vertexId > x) 
                {
                        r = m;
                }
        }
        if(l == r)
        {
                if(arr[l].vertexId <= x)
                {
                        *index = l;
                        *number = 1;
                        return 0;
                }
                else {
                        return 1;
                }
        }

        return 1;
}

unsigned int SSDInterface::findEdge_inBuffer(char *Test_variable_size_buffer, char *Test_variable_size_buffer_weight, unsigned int destEdge, unsigned int NumberOfAdjVertices)
{
	unsigned int i;
	for(i=0; i < NumberOfAdjVertices; i++)
	{
		if(destEdge == ((unsigned int *)Test_variable_size_buffer)[i])
		{
			return ((unsigned int *)Test_variable_size_buffer_weight)[i];
		}
	}
	return -1;
}
