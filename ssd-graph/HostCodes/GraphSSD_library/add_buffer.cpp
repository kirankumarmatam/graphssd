#include "graphFiltering.h"
#include "header.h"

#define GraphSSD_1 0

bool myComparator(std::pair<unsigned int, unsigned int> comparisionObject1, std::pair<unsigned int, unsigned int> comparisionObject2)
{
	return (comparisionObject1.first < comparisionObject2.first);
}

std::set<unsigned int> verticesToMerge;
std::set<unsigned int> ExtraVerticesAdded;
extern unsigned int NumNodes;
unsigned int NumNodesNew;
std::map<unsigned int, unsigned int> labelMapping;

unsigned int printed_new_information_to_file = 1;
FILE *temporary_output_file_for_new_updates;
FILE *temporary_output_file_for_old_updates;
std::set<unsigned int> newlyAddedOrUpdatedVertices;

extern unsigned int GraphLPN;
extern unsigned int currentGTTIndex_edge;
extern struct GTTTable1 *gtt1;
unsigned int SSDInterface::IdentifyPosition(unsigned int vertexId)
{
	unsigned int gttVidIndex, gttVidNumber;
	binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, vertexId, &gttVidIndex, &gttVidNumber);
	unsigned int positionToAdd = gttVidIndex + gttVidNumber - 1;
	return positionToAdd;
}

std::set<unsigned int> markedGTTPages;

unsigned int currentTopInTheLog = 1;//For first vertex keep it as 1
#if(ADD_UPDATES_TO_BUFFER == 1)
void SSDInterface::AddToBuffer(char *bufferFile, char *mapFile)
{
	unsigned int fileLabel, graphSSDLabel;
	FILE *mapFilePtr = fopen(mapFile, "r");
	while(fscanf(mapFilePtr, "%u %u", &fileLabel, &graphSSDLabel) != EOF)
	{
		labelMapping[fileLabel] = graphSSDLabel;
	}
	fclose(mapFilePtr);
	cout << "Entering 1" << endl;
	//Need to use map file to update labels
	FILE *bufferFilePtrHandle = fopen(bufferFile, "r");
	std::vector< std::pair<unsigned int, unsigned int> > randomEdges;
	unsigned int srcId, targetId;
	unsigned int ExtraVertices = 0, ExtraEdges = 0;

	if(printed_new_information_to_file == 0)
	{
		temporary_output_file_for_new_updates = fopen("youtube_extra_new_updates", "w");
		temporary_output_file_for_old_updates = fopen("youtube_extra_old_updates", "w");
		while(fscanf(bufferFilePtrHandle, "%d %d", &srcId, &targetId) != EOF)
		{
			if((labelMapping.find(srcId) == labelMapping.end()))
			{
				fprintf(temporary_output_file_for_new_updates, "%u %u\n", srcId, targetId);
			}
			else {
				fprintf(temporary_output_file_for_old_updates, "%u %u\n", srcId, targetId);
			}
		}
		fseek(bufferFilePtrHandle, 0, SEEK_SET);

		fclose(temporary_output_file_for_new_updates);
		fclose(temporary_output_file_for_old_updates);
		printed_new_information_to_file = 1;
		return;
	}

	while(fscanf(bufferFilePtrHandle, "%d %d", &srcId, &targetId) != EOF)
	{
		if((labelMapping.find(srcId) != labelMapping.end()))
		{
			markedGTTPages.insert(IdentifyPosition(labelMapping[srcId]));
		}
	}
	fseek(bufferFilePtrHandle, 0, SEEK_SET);
	while(fscanf(bufferFilePtrHandle, "%d %d", &srcId, &targetId) != EOF)
	{
		if((labelMapping.find(srcId) != labelMapping.end()))
		{
		}
		else {
				if(ExtraVerticesAdded.find(srcId) == ExtraVerticesAdded.end())
				{
					ExtraVerticesAdded.insert(srcId);
					ExtraVertices++;
				}

			ExtraEdges++;//ExtraV edges added will be incorrect as something is not labels are being created
		}
	}
	fseek(bufferFilePtrHandle, 0, SEEK_SET);

	while(fscanf(bufferFilePtrHandle, "%d %d", &srcId, &targetId) != EOF)
	{
		if((labelMapping.find(srcId) != labelMapping.end()) && (labelMapping.find(targetId) != labelMapping.end()))
		{
			randomEdges.push_back(std::make_pair(labelMapping[srcId], labelMapping[targetId]));
		}
		else {
			if(labelMapping.find(srcId) == labelMapping.end())
			{
				labelMapping[srcId] = NumNodesNew++;
			}
			if(labelMapping.find(targetId) == labelMapping.end())
			{
				labelMapping[targetId] = NumNodesNew++;

			}
			randomEdges.push_back(std::make_pair(labelMapping[srcId], labelMapping[targetId]));
		
			}
		newlyAddedOrUpdatedVertices.insert(labelMapping[srcId]);
	}

	bufferRowPtrArray.resize(NumNodesNew, 0);
	cout << "ExtraVerticesAdded = " << ExtraVertices << " ExtraEdgesAdded = " << ExtraEdges << endl;
	cout << "size of newlyAddedOrUpdatedVertices = " << newlyAddedOrUpdatedVertices.size() << endl;
	fclose(bufferFilePtrHandle);
	cout << "Entering 2" << endl;
	sort(randomEdges.begin(), randomEdges.end(), myComparator);
	unsigned int prevSrcId = -1;
	unsigned int lastEntryInTheLog;
	vector<unsigned int> EdgeList;
	for(std::vector<std::pair<unsigned int, unsigned int> >::iterator it = randomEdges.begin(); it != randomEdges.end(); it++)
	{
		srcId = it->first, targetId = it->second;
		verticesToMerge.insert(srcId);
		if(prevSrcId != srcId)
		{
			if(prevSrcId != -1)
			{
				bufferRowPtrArray[prevSrcId] = currentTopInTheLog;
				bufferColumnIndexPtrArray.push_back(EdgeList.size());
				currentTopInTheLog++;
				for(unsigned int i=0; i < EdgeList.size(); i++)
				{
				bufferColumnIndexPtrArray.push_back(EdgeList[i]);
				currentTopInTheLog++;
				}
				bufferColumnIndexPtrArray.push_back(lastEntryInTheLog);
				currentTopInTheLog++;
				EdgeList.clear();
			}
			lastEntryInTheLog = bufferRowPtrArray[srcId];
			prevSrcId = srcId;
		}
		EdgeList.push_back(targetId);
	}
	cout << "Entering 3" << endl;
	if(prevSrcId != -1)
	{
		bufferRowPtrArray[prevSrcId] = currentTopInTheLog;
		bufferColumnIndexPtrArray.push_back(EdgeList.size());
		currentTopInTheLog++;
		for(unsigned int i=0; i < EdgeList.size(); i++)
		{
			bufferColumnIndexPtrArray.push_back(EdgeList[i]);
			currentTopInTheLog++;
		}
		bufferColumnIndexPtrArray.push_back(lastEntryInTheLog);
		currentTopInTheLog++;
		EdgeList.clear();
	}
#if(BufferInStorage == 1)
	cout << "Entering 4" << endl;
	unsigned int numOfValuesWritten = 0;
	unsigned int LBA_topPointer = 0;
	while(numOfValuesWritten < (NumNodesNew+currentTopInTheLog))
	{
		unsigned int remainingRowPtrValues;
		if(numOfValuesWritten < NumNodesNew)
		{
			remainingRowPtrValues = ((32*4096/sizeof(unsigned int)) > (NumNodesNew - numOfValuesWritten)) ? (NumNodesNew - numOfValuesWritten) : (32*4096/sizeof(unsigned int));
			memcpy(data_nvme_command, bufferRowPtrArray.data()+numOfValuesWritten, sizeof(unsigned int) * remainingRowPtrValues);
			numOfValuesWritten += remainingRowPtrValues;
			if(remainingRowPtrValues != ((32*4096/sizeof(unsigned int))))
			{
				unsigned int remainingColumnIndexValues = (((32*4096/sizeof(unsigned int)) - remainingRowPtrValues) > (currentTopInTheLog))?currentTopInTheLog : ((32*4096/sizeof(unsigned int)) - remainingRowPtrValues);
				memcpy((char *)data_nvme_command + remainingRowPtrValues * sizeof(unsigned int), bufferColumnIndexPtrArray.data(), remainingColumnIndexValues * sizeof(unsigned int));
				numOfValuesWritten += remainingColumnIndexValues;
			}
		}
		else if(numOfValuesWritten < (NumNodesNew + currentTopInTheLog))
		{
			unsigned int yetToBeWrittenColumnIndices = (NumNodesNew + currentTopInTheLog) - numOfValuesWritten;
			unsigned int remainingColumnIndexValues = (yetToBeWrittenColumnIndices > (32*4096/sizeof(unsigned int))) ? (32*4096/sizeof(unsigned int)) : yetToBeWrittenColumnIndices;
			unsigned int positionInColumnIndex = numOfValuesWritten - NumNodesNew;
			memcpy(data_nvme_command, bufferColumnIndexPtrArray.data() + positionInColumnIndex, remainingColumnIndexValues * sizeof(unsigned int));
			numOfValuesWritten += remainingColumnIndexValues;
		}
	/*	for(unsigned int i = 0; i < (32*4096/sizeof(unsigned int)); i++)
		{
			cout << i+ (LBA_topPointer*4096/sizeof(unsigned int)) << " " << ((unsigned int *)data_nvme_command)[i] << endl;
		}
	*/	WriteToSSDN(BUFFERLBA + LBA_topPointer, 32);
		LBA_topPointer += 32;
	}
	cout << "Entering 5" << endl;
#endif//BufferInStorage
}

std::set<unsigned int> mergedVertices;

unsigned int SSDInterface::AccessLinkedListBuffer(unsigned int vertexId, vector<unsigned int> &EdgeList)
{
#if(BufferInStorage == 0)
	unsigned int address = bufferRowPtrArray[vertexId];
	unsigned int chainLength = 0;
	while(address != 0)
	{
		for(unsigned int i=0; i < bufferColumnIndexPtrArray[address]; i++)
		{
		EdgeList.push_back(bufferColumnIndexPtrArray[address+i+1]);//use it as address
		}
		address = bufferColumnIndexPtrArray[address+bufferColumnIndexPtrArray[address]+1];
		chainLength++;
	}
	return chainLength;
#elif(BufferInStorage == 1)

	//check access buffer memory function also
//	if(mergedVertices.find(vertexId) != mergedVertices.end())
//		return 0;
#if(GraphSSD_1 == 0)
if(markedGTTPages.find(IdentifyPosition(vertexId)) == markedGTTPages.end())
{
	return 0;
}
#endif//GraphSSD_1

	unsigned int value;
	AccessBufMemory(vertexId, value, 1);
	unsigned int address = value;
//	cout << "vertexId = " << vertexId << " address = " << address << endl;
	while(address != 0)
	{
		AccessBufMemory(NumNodesNew + address, value, 1);
		unsigned int numOfAdjValues = value;
//		cout << "numOfAdjValues = " << numOfAdjValues << endl;
		for(unsigned int i=0; i < numOfAdjValues; i++)
		{
			AccessBufMemory(NumNodesNew + address + i + 1, value, 1);
			EdgeList.push_back(value);//use it as address
		}
		AccessBufMemory(NumNodesNew + address + numOfAdjValues + 1, value, 1);
		address = value;
//		cout << "vertexId = " << vertexId << " address = " << address << endl;
	}
	return 0;
#endif//BufferInStorage
}

std::set<unsigned int> UniqueSSDPagesTouched;
std::map<unsigned int, unsigned int> VerticesPerSSDPage;
std::map<unsigned int, unsigned int> EdgesAddedPerSSDPage;
std::map<unsigned int, unsigned int> InitialVerticesPerSSDPage;
std::map<unsigned int, unsigned int> ChainLengthInApage;
unsigned int TotalChainLength = 0;
unsigned int TotalUpdatedOldVertices = 0;

#define MERGE_ONLY_TOP_10 0

void SSDInterface::MergeBuffer(unsigned int NumOfPagesToMerge)
{
	//Merge all of them to a buffer in main memory and from there call the merge routine
	//Which vertices to access? which vertices to access to? which vertices to access to? which vertices to access to? which vertices to? which vertices to access to? which vertices to access to? which vertices to access to? which vertices to access to? Assume a list of modified vertices, you can assume it as a map, what will be the performance of the scheme? assume x% of page are accessed during the merging process and y pages are accessed for the indirection pointers, then what is the cost of merging? (xP/100) + y
	std::vector< std::pair<unsigned int, unsigned int> > EdgesToAddInMemory;
	vector<unsigned int> EdgeList;
	for(std::set<unsigned int>::iterator it = verticesToMerge.begin(); it != verticesToMerge.end(); it++)//Items in set needs to be sorted
	{
		unsigned int gttVidIndex, gttVidNumber;
		binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, *it, &gttVidIndex, &gttVidNumber);
		unsigned int positionToAdd = gttVidIndex + gttVidNumber - 1;

		UniqueSSDPagesTouched.insert(positionToAdd);
		if(VerticesPerSSDPage.find(positionToAdd) == VerticesPerSSDPage.end())
		{
			VerticesPerSSDPage[positionToAdd] = 0;
			EdgesAddedPerSSDPage[positionToAdd] = 0;
			ChainLengthInApage[positionToAdd] = 0;
		}
		VerticesPerSSDPage[positionToAdd] += 1;
		EdgeList.clear();
		unsigned int chainLength = AccessLinkedListBuffer(*it, EdgeList);
		ChainLengthInApage[positionToAdd] += chainLength;
		TotalChainLength += chainLength;
		TotalUpdatedOldVertices++;
		EdgesAddedPerSSDPage[positionToAdd] += EdgeList.size();
		EdgeList.clear();

//		cout << "vM " << *it << endl;
#if(MERGE_ONLY_TOP_10 == 0)
		EdgeList.clear();
		AccessLinkedListBuffer(*it, EdgeList);
		//Add edgelist to an array in main memory, what is that array?
		for(unsigned int i = 0; i < EdgeList.size(); i++)
		{
			EdgesToAddInMemory.push_back(std::make_pair(*it, EdgeList[i]));
		}
#endif//MERGE_ONLY_TOP_10
	}
	for(unsigned int i = 0; i < currentGTTIndex_edge; i++)
	{
		InitialVerticesPerSSDPage[i] =  gtt1->GTTPointer1[i+1].vertexId - gtt1->GTTPointer1[i].vertexId;
	}
	//sort vertices
	std::vector< std::pair<unsigned int, unsigned int> > randomEdges;
	for(std::map<unsigned int, unsigned int>::iterator it = VerticesPerSSDPage.begin(); it != VerticesPerSSDPage.end(); it++)
	{
		randomEdges.push_back(std::make_pair(it->second, it->first));
	}
	sort(randomEdges.begin(), randomEdges.end(), myComparator);
	/*
	for(unsigned int i = 0; i < currentGTTIndex_edge; i++)
	{
		if(VerticesPerSSDPage.find(i) != VerticesPerSSDPage.end())
		{
			cout << "Num = " << i << " InitialVerticesPerSSDPage = " << InitialVerticesPerSSDPage[i] << " AddedVertices = " << VerticesPerSSDPage[i] << " AddedEdges = " << EdgesAddedPerSSDPage[i] << " AverageChainLength = "  << (ChainLengthInApage[i] * 1.0) / VerticesPerSSDPage[i]  << endl;
		} else {
			cout << "Num = " << i << " InitialVerticesPerSSDPage = " << InitialVerticesPerSSDPage[i] << " AddedVertices = 0 AddedEdges = 0 ChainLengthInApage = 0" << endl;
		}
	}
	cout << "TotalUpdatedOldVertices = " << TotalUpdatedOldVertices << " AverageChainLength = " << (TotalChainLength * 1.0)/ TotalUpdatedOldVertices<< endl;
	*/

//	for(unsigned int i = 0; i < randomEdges.size(); i++)
//	{
//		cout << "Num = " << i << " PageNum = " << randomEdges[i].second << " Inserted edges = " << randomEdges[i].first << endl;

		//              cout << randomEdges[randomEdges.size() - i -1].first << " " << randomEdges[randomEdges.size() - i -1].second << endl;
//	}

	//clear those vertices
	std::set<unsigned int> PagesToClear;
	for(unsigned int i = 0; i < NumOfPagesToMerge; i++)
	{
		PagesToClear.insert(randomEdges[randomEdges.size() - i -1].second);
	}

	for(std::set<unsigned int>::iterator it = verticesToMerge.begin(); it != verticesToMerge.end(); it++)//Items in set needs to be sorted
	{
		unsigned int positionToAdd = IdentifyPosition(*it);
		if(PagesToClear.find(positionToAdd) != PagesToClear.end())
		{
			if(markedGTTPages.find(positionToAdd) != markedGTTPages.end())
			{
				markedGTTPages.erase(positionToAdd);
			}
			//Need to make sure that other code is removed so that they won't cause cache disturbances and so on
			verticesToMerge.erase(it);
		}
	}

	return;
	
	for(std::set<unsigned int>::iterator it = verticesToMerge.begin(); it != verticesToMerge.end(); it++)//Items in set needs to be sorted
	{
		unsigned int gttVidIndex, gttVidNumber;
		binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, *it, &gttVidIndex, &gttVidNumber);
		unsigned int positionToAdd = gttVidIndex + gttVidNumber - 1;
		if(PagesToClear.find(positionToAdd) != PagesToClear.end())
		{
			
			unsigned int value = 0;
			AccessBufMemory(*it, value, 0);

#if(MERGE_ONLY_TOP_10 == 1)
			EdgeList.clear();
			AccessLinkedListBuffer(*it, EdgeList);
			//Add edgelist to an array in main memory, what is that array?
			for(unsigned int i = 0; i < EdgeList.size(); i++)
			{
				EdgesToAddInMemory.push_back(std::make_pair(*it, EdgeList[i]));
			}

#endif//MERGE_ONLY_TOP_10
		}
	}



/*	cout << "UniqueSSDPagesTouched = " << UniqueSSDPagesTouched.size() << endl;
	for(std::map<unsigned int, unsigned int>::iterator it = VerticesPerSSDPage.begin(); it != VerticesPerSSDPage.end(); it++)
	{
		cout << "PageNum = " << it->first << " " << it->second << endl;
	}
	*/
//	AddEdge(EdgesToAddInMemory);

	//Modify merge buffer to accept array as a parameter
//	Call MergeBuffer
	//Then get the file from rohit and store initial file after converting it to CSR format
	//Then call store graph, call bfs, add updates, call bfs, merge updates, call bfs
	//for baseline also call store graph - done, call bfs - done, add updates - same as GraphSSD one, call bfs - for accessing updates - same as GraphSSD one, merge updates - what to do here?, call bfs
}
#endif//ADD_UPDATES_TO_BUFFER
