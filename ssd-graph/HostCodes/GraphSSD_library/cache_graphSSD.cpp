#include "graphFiltering.h"
#include "header.h"

extern map<unsigned int, unsigned int > vertexMapping;

std::map<unsigned int, char *>graphCache;
std::vector<unsigned int> randomEvictionVector;

unsigned int MaxCacheSize = 100 * 1024*1024;
unsigned int CacheUtilized = 0;
unsigned int numOfCacheHits = 0, numOfCacheEvictions = 0, numOfCacheAccesses = 0, numOfCacheMisses = 0;

void SSDInterface::cache_readAdjEdgeList(unsigned int vID, vector<unsigned int> &EdgeList)
{
	//Write an if condition here to check if we need to process at host or at SSD - done
	//Then do binary search - copy that code here - done
	//Then initate loading - for now using only the existing code, unoptmizations till now are, limits on edgelist loading, multiple copying
	//Then do cache management code - see if you can use the existing cache management code or copy from the SSD firmware, for now we can use the baseline cache management code itslef
	//After the page has been loaded into the cache initate processing the request
	//Our goal now is to measure the statistics in the cache management code.

#if(UseCache_GetPagesFromSSD == 1)
	assert(vID < vertexMapping.size());
	*((unsigned int *)data_nvme_command) = 0;
	getAdjEdgeList(vID, (char *)data_nvme_command);

	assert(EdgeList.size()*sizeof(unsigned int) <= 32*4096);
	assert(*(unsigned int *)data_nvme_command <= 32*4096);
	for(unsigned int i=0; i < *((unsigned int *)data_nvme_command); i++)
	{
		//              cout << "Edge " << i << " = " << *((unsigned int *)data_nvme_command + i + (4096/sizeof(unsigned int)) ) << endl;
		//              cout << "Edge " << i << " = " << *((unsigned int *)data_nvme_command + i + 1) << endl;
		//              EdgeList.push_back(*((unsigned int *)data_nvme_command + i + (4096/sizeof(unsigned int)) ));
		EdgeList.push_back(*((unsigned int *)data_nvme_command + i + 1) );
	}
	//      if(EdgeList.size() == 0)return;
	/*      cout << vID << " :";
		for(unsigned int i=0; i < *((unsigned int *)data_nvme_command); i++)
		{
		cout << " " << *((unsigned int *)data_nvme_command + i + 1);
		}
		cout << endl;
	 */
	return;
#elif(UseCache_GetPagesFromSSD == 0)
	numOfCacheAccesses++;
	std::map<unsigned int, char *>::iterator it = graphCache.find(vID);
	if(it != graphCache.end())
	{
		numOfCacheHits++;
		//push the values in to the EdgeList
		EdgeList.reserve(*((unsigned int *)graphCache[vID]));
		for(unsigned int i = 0; i < *((unsigned int *)graphCache[vID]); i++)
		{
			EdgeList.push_back(((unsigned int *)graphCache[vID])[i+1]);
		}
	}
	else {
		numOfCacheMisses++;
		//load from the storage into the cache 
		//If there isn't enough space then evict an entry and make space
		assert(vID < vertexMapping.size());
		struct nvme_user_io io;
		io.opcode = nvme_cmd_read;
		io.flags = 3;
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		*((unsigned int *)data_nvme_command) = 0;
		io.slba = vID;
		io.nblocks = 31;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;
		int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		if (err < 0)
			cout << "Error in executing nvme command!!" << endl;
		if (err)
		{
			fprintf(stderr, "nvme read status:%x\n", err);
			exit(0);
		}

		int freeSpace = MaxCacheSize - CacheUtilized;
		int numOfAdjElements = *((unsigned int *)data_nvme_command);
//		cout << "freeSpace = " << freeSpace << " CacheUtilized = " << CacheUtilized << " vID =  "<< vID << " numOfAdjElements = " << numOfAdjElements << " EdgeListSpace = " << sizeof(unsigned int) * (2 + numOfAdjElements) << endl;
		while(freeSpace < (sizeof(unsigned int) * (2 + numOfAdjElements)))
		{
			numOfCacheEvictions++;
			int indexForEviction = rand() % randomEvictionVector.size();
			std::map<unsigned int, char *>::iterator it1 = graphCache.find(randomEvictionVector[indexForEviction]);
 			assert(it1 != graphCache.end());
			freeSpace += sizeof(unsigned int) * (2 + *((unsigned int *)(it1->second)));
			CacheUtilized -= sizeof(unsigned int) * (2 + *((unsigned int *)(it1->second)));
			free(it1->second);
			graphCache.erase(it1);
			randomEvictionVector[indexForEviction] = randomEvictionVector.back();
			randomEvictionVector.pop_back();
		}
		graphCache[vID];
		graphCache[vID] = new char [(numOfAdjElements + 1) * sizeof(unsigned int)];
		memcpy((void *)graphCache[vID], (void *)data_nvme_command, (numOfAdjElements + 1) * sizeof(unsigned int));
		randomEvictionVector.push_back(vID);

		if(numOfAdjElements != 0)
		{
			EdgeList.reserve(numOfAdjElements);
			for(unsigned int i = 0; i < numOfAdjElements; i++)
			{
				EdgeList.push_back(((unsigned int *)data_nvme_command)[i+1]);
			}
		}
		CacheUtilized += sizeof(unsigned int) * (2 + numOfAdjElements);
	}
	return;
#endif
}
//Need to take care of the cases where number of adjacent vertices for a vertex are zero

//writeAdjEdgeList -- no need to support it now
