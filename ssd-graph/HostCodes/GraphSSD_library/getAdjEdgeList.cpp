#include "header.h"
#include "graphFiltering.h"

unsigned int GraphLPN;
unsigned int GraphLPNWeight;
char *Test_variable_size_buffer;
unsigned int numOfFreeElementsAvailableInTheBuffer;
unsigned int numOfFreeElementsAvailableInTheBufferWeight;
unsigned int bufferPageTopPointer;
unsigned int bufferPageTopPointerWeight;
unsigned int currentGTTIndex_edge;
unsigned int currentGTTIndex_weight;
struct GTLTable1 *gtlTable1;
struct CompletionQueues *gtlCompletionQueues;
struct CompletionQueuePtrs *gtlCompletionQueuePtrs;

struct GTTTable1 *gtt1;
struct GTTTable1Weight *gtt1_weight;
struct VertexCompletionInformation vertexCompletionInformation;

#if(useCache == 1)
	unsigned int TotRequests_host = 0, TotRequests_SSD = 0;
	unsigned int numOfCacheHits_aux = 0, numOfCacheEvictions_aux = 0, numOfCacheAccesses_aux = 0, numOfCacheMisses_aux = 0;
	unsigned int numOfCacheHits_buf = 0, numOfCacheEvictions_buf = 0, numOfCacheAccesses_buf = 0, numOfCacheMisses_buf = 0;
#endif

unsigned int SSDInterface::putAdjEdgeList(unsigned int vID, char *host_buffer, unsigned int getNumAdjVertices)
{
	vertexCompletionInformation.vertexId = vID;
	bufferingPage_loadToSSD(host_buffer, getNumAdjVertices);
	vertexCompletionInformation.flags = 2;
	vertexCompletionInformation.host_addr = host_buffer;
	vertexCompletionInformation.isEdge = 0;
	vertexCompletionInformation.NumOfAdjVertices = getNumAdjVertices;
	vertexCompletionInformation.numberOfVerticesRemainingToFetch = getNumAdjVertices;
	bufferingPage_loadToNAND(getNumAdjVertices, 1);
	return 0;
}

unsigned int SSDInterface::bufferingPage_loadToSSD(char *host_buffer, unsigned int getNumAdjVertices)
{
	if(getNumAdjVertices == 0) return 0;
	assert(getNumAdjVertices <= (16*4096 / sizeof(unsigned int)));
	unsigned int numItems = 2*getNumAdjVertices;
	memcpy((void *)Test_variable_size_buffer, (void *)host_buffer, numItems*sizeof(unsigned int));
	return 0;
}

unsigned int SSDInterface::bufferingPage_loadToNAND(unsigned int getNumAdjVertices, unsigned int isEdges)
{
	unsigned int valuesRemainingToAdd = getNumAdjVertices;
	unsigned int numOfItemsToCopy;
	do {
//		printf("b_l = %d\n", valuesRemainingToAdd);
		if (isEdges == 1) {
			if ((valuesRemainingToAdd + 3) > numOfFreeElementsAvailableInTheBuffer)
			{
				GraphLPN++;
				numOfFreeElementsAvailableInTheBuffer = (SSD_PAGE_SIZE / (sizeof(unsigned int))) - 1;
				bufferPageTopPointer = 0;
				currentGTTIndex_edge += 1;
				gtt1->GTTPointer1[currentGTTIndex_edge].vertexId = vertexCompletionInformation.vertexId;
				gtt1->GTTPointer1[currentGTTIndex_edge].LPN = GraphLPN;
			}
		} else {
			if ((valuesRemainingToAdd + 3) > numOfFreeElementsAvailableInTheBufferWeight) {
				GraphLPNWeight++;
				numOfFreeElementsAvailableInTheBufferWeight = (SSD_PAGE_SIZE / (sizeof(unsigned int))) - 1;
				bufferPageTopPointerWeight = 0;
				currentGTTIndex_weight += 1;
				gtt1_weight->GTTPointer1[currentGTTIndex_weight].vertexId = vertexCompletionInformation.vertexId;
				gtt1_weight->GTTPointer1[currentGTTIndex_weight].LPN = GraphLPNWeight;
			}
		}

		if (isEdges == 1) {
			numOfItemsToCopy = (valuesRemainingToAdd + 3) < numOfFreeElementsAvailableInTheBuffer ?	valuesRemainingToAdd : numOfFreeElementsAvailableInTheBuffer - 3;
		} else {
			numOfItemsToCopy = (valuesRemainingToAdd + 3) < numOfFreeElementsAvailableInTheBufferWeight ?	valuesRemainingToAdd :	numOfFreeElementsAvailableInTheBufferWeight - 3;
		}
		if (isEdges == 1) {
			GraphLRUBufWrite(GraphLPN, numOfItemsToCopy * sizeof(unsigned int),	bufferPageTopPointer * sizeof(unsigned int), (getNumAdjVertices - valuesRemainingToAdd) * sizeof(unsigned int), gtt1->GTTPointer1[currentGTTIndex_edge].vertexId);
		} else {
			GraphLRUBufWrite(GraphLPNWeight, numOfItemsToCopy * sizeof(unsigned int), bufferPageTopPointerWeight * sizeof(unsigned int), (2 * getNumAdjVertices - valuesRemainingToAdd) * sizeof(unsigned int), gtt1_weight->GTTPointer1[currentGTTIndex_weight].vertexId);
		}

		valuesRemainingToAdd = valuesRemainingToAdd - numOfItemsToCopy;
		if (isEdges == 1) {
			bufferPageTopPointer += numOfItemsToCopy;
			numOfFreeElementsAvailableInTheBuffer = numOfFreeElementsAvailableInTheBuffer - numOfItemsToCopy - 3;
		} else {
			bufferPageTopPointerWeight += numOfItemsToCopy;
			numOfFreeElementsAvailableInTheBufferWeight = numOfFreeElementsAvailableInTheBufferWeight - numOfItemsToCopy - 3;
		}
	} while (valuesRemainingToAdd > 0);
	return 0;
}

unsigned int SSDInterface::GraphInitialize()
{
	Test_variable_size_buffer = new char [1024*1024];
	gtt1 = new GTTTable1;
	gtt1_weight = new GTTTable1Weight;
	gtlCompletionQueues = new CompletionQueues;
	gtlCompletionQueuePtrs = new CompletionQueuePtrs;
	GraphLPN = 50-1;
	GraphLPNWeight = 786407-1;
	numOfFreeElementsAvailableInTheBuffer = 0;
	bufferPageTopPointer = 0;
	numOfFreeElementsAvailableInTheBufferWeight = 0;
	bufferPageTopPointerWeight = 0;
	currentGTTIndex_edge = -1;
	currentGTTIndex_weight = -1;
	int i,j;
	for(i=0; i < MAX_CHANNEL_NUM; i++)
	{
		for(j=0; j < MAX_WAY_NUM; j++)
		{
			(gtlCompletionQueuePtrs->completionQueuePointer[i][j]).front = 0;
			(gtlCompletionQueuePtrs->completionQueuePointer[i][j]).rear = 0;
		}
	}
	vertexCompletionInformation.flags = 0;
	vertexCompletionInformation.vertexId = -1;
	return 0;
}
extern std::map<unsigned int, char *> Storage;
unsigned int SSDInterface::GraphFinalize()
{
	free(Test_variable_size_buffer);
	free(gtt1);
	free(gtt1_weight);
	free(gtlCompletionQueues);
	free(gtlCompletionQueuePtrs);
	for(std::map<unsigned int, char *>::iterator it = Storage.begin(); it != Storage.end(); it++)	
	{
		free(it->second);
	}
	Storage.clear();
#if(ADD_UPDATES_TO_BUFFER == 1)
	bufferRowPtrArray.clear();
	bufferColumnIndexPtrArray.clear();
#endif//ADD_UPDATES_TO_BUFFER
	return 0;
}

unsigned int SSDInterface::getEdgesToSSDDram_initiateLoading(unsigned int gttVidIndex, unsigned int gttVidNumber, unsigned int isEdge)
{
#if(useCache == 1 && UseCache_GetPagesFromSSD == 1)
//	cout << "gttVidIndex = " << gttVidIndex << " gttVidNumber = " << gttVidNumber << endl;
	TotRequests_host += 1;
	unsigned int i=0;
	unsigned int hits = 0;
        for(i = 0; i < gttVidNumber; i++)
        {
//		cout << "LPN = " << gtt1->GTTPointer1[gttVidIndex+i].LPN << endl;
                if(isEdge == 0)
                        hits += GraphLRUBufRead(gtt1->GTTPointer1[gttVidIndex+i].LPN);
                else if(isEdge == 1)
		{
                        hits += GraphLRUBufRead(gtt1->GTTPointer1[gttVidIndex+i].LPN + SLBA_EDGE_WEIGHT);
//                        hits += GraphLRUBufRead(gtt1_weight->GTTPointer1[gttVidIndex+i].LPN);
		}
        }
	if(hits >= 1)
		TotRequests_SSD += gttVidNumber;
#else
        unsigned int i=0;
        for(i = 0; i < gttVidNumber; i++)
        {
                if(isEdge == 0)
                        GraphLRUBufRead(gtt1->GTTPointer1[gttVidIndex+i].LPN);
                else if(isEdge == 1)
		{
			GraphLRUBufRead(gtt1->GTTPointer1[gttVidIndex+i].LPN + SLBA_EDGE_WEIGHT);
//			GraphLRUBufRead(gtt1_weight->GTTPointer1[gttVidIndex+i].LPN);
		}
        }
#endif
        return 0;
}

unsigned int SSDInterface::getEdgeWeight(unsigned int vID, char *host_buffer, unsigned int destEdge)
{
        vertexCompletionInformation.flags = 5;
        vertexCompletionInformation.vertexId = vID;
        vertexCompletionInformation.host_addr = host_buffer;
        vertexCompletionInformation.destEdge = destEdge;
        unsigned int gttVidIndex, gttVidNumber, gttVidIndex_weight, gttVidNumber_weight;
        binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, vID, &gttVidIndex, &gttVidNumber);
        binarySearch( gtt1_weight->GTTPointer1, 0, currentGTTIndex_weight, vID, &gttVidIndex_weight, &gttVidNumber_weight);
        vertexCompletionInformation.numberOfVerticesRemainingToFetch = gttVidNumber + gttVidNumber_weight;
        vertexCompletionInformation.isEdge = 1;
        vertexCompletionInformation.NumOfAdjVertices = 0;
        vertexCompletionInformation.NumOfAdjVertices_weight = 0;
        getEdgesToSSDDram_initiateLoading(gttVidIndex, gttVidNumber, 0);
        getEdgesToSSDDram_initiateLoading(gttVidIndex_weight, gttVidNumber_weight, 1);
        return 0;
}

unsigned int SSDInterface::getAdjEdgeWeights(unsigned int vID, char * host_buffer)
{
	vertexCompletionInformation.flags = 2;
	vertexCompletionInformation.vertexId = vID;
	vertexCompletionInformation.host_addr = host_buffer;
	unsigned int gttVidIndex, gttVidNumber;
	binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, vID, &gttVidIndex, &gttVidNumber);
	vertexCompletionInformation.numberOfVerticesRemainingToFetch = gttVidNumber;
	vertexCompletionInformation.isEdge = 1;
	vertexCompletionInformation.NumOfAdjVertices = 0;
	getEdgesToSSDDram_initiateLoading(gttVidIndex, gttVidNumber, 0);
	vertexCompletionInformation.flags = 3;
	getEdgesToSSDDram_initiateLoading(gttVidIndex, gttVidNumber, 1);
	vertexCompletionInformation.flags = 0;
	return 0;
}

unsigned int time_binary_search_counter = 0;

unsigned int SSDInterface::getAdjEdgeList(unsigned int vID, char * host_buffer)
{
#if(VERTEX_CACHE_MAP == 1)
	if(cache_readAdjEdgeList_vertex(vID, host_buffer) == 1)
		return 0;
#endif//VERTEX_CACHE_MAP
	struct timespec time_start, time_end;
        double time_elapsed;
	clock_gettime(CLOCK_MONOTONIC, &time_start);
        vertexCompletionInformation.flags = 2;
        vertexCompletionInformation.vertexId = vID;
        vertexCompletionInformation.host_addr = host_buffer;
        unsigned int gttVidIndex, gttVidNumber;
        binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, vID, &gttVidIndex, &gttVidNumber);
	assert((0 <= gttVidIndex) && (gttVidIndex <= currentGTTIndex_edge));
	assert(gttVidNumber <= currentGTTIndex_edge);
        vertexCompletionInformation.numberOfVerticesRemainingToFetch = gttVidNumber;
        vertexCompletionInformation.isEdge = 1;
	vertexCompletionInformation.NumOfAdjVertices = 0;
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	if(time_binary_search_counter <= 1)
	{
		time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
		time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
//		fprintf(stdout, "\n");
//		fprintf(stdout, "TIME_BINARY_SEARCH: %lf %lf %lf\n", (double)time_end.tv_nsec, (double)time_start.tv_nsec, time_elapsed);
		time_binary_search_counter++;
	}
        getEdgesToSSDDram_initiateLoading(gttVidIndex, gttVidNumber, 0);
#if(VERTEX_CACHE_MAP == 1)
	cache_writeAdjEdgeList_vertex(vID, host_buffer);
#endif//VERTEX_CACHE_MAP
        return 0;
}

unsigned int SSDInterface::getTwoHopAdjEdgeList(unsigned int vID, char *host_buffer)
{
        vertexCompletionInformation.flags = 8;
        vertexCompletionInformation.vertexId = vID;
        vertexCompletionInformation.host_addr = host_buffer;
        unsigned int gttVidIndex, gttVidNumber;
        binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, vID, &gttVidIndex, &gttVidNumber);
        vertexCompletionInformation.numberOfVerticesRemainingToFetch = gttVidNumber;
        vertexCompletionInformation.isEdge = 1;
        vertexCompletionInformation.NumOfAdjVertices = 0;
        vertexCompletionInformation.hopNum = 1;
        getEdgesToSSDDram_initiateLoading(gttVidIndex, gttVidNumber, 0);
        return 0;
}

void SSDInterface::AccessAuxMemory(unsigned int index, AuxDataValueType &value, bool isRead)
{
#if(useCache == 1)
	TotRequests_host += 1;
	unsigned int LPN = AUXLBA + index / (4096/sizeof(AuxDataValueType));
	unsigned int offset = index % (4096/sizeof(AuxDataValueType));
	unsigned int hits = cache_readNumber(LPN, offset, 1, value, isRead);
	if(hits >= 1)
		TotRequests_SSD += 1;
#endif//useCache
	return;
}

void SSDInterface::AccessBufMemory(unsigned int index, unsigned int &value, bool isRead)
{
#if(useCache == 1)
	TotRequests_host += 1;
	unsigned int LPN = BUFFERLBA + index / (4096/sizeof(unsigned int));
	unsigned int offset = index % (4096/sizeof(unsigned int));
	unsigned int hits = cache_readNumber_buf(LPN, offset, 1, value, isRead);
	if(hits >= 1)
		TotRequests_SSD += 1;
#endif//useCache
	return;
}

#if(useCache == 1 && UseCache_GetPagesFromSSD == 1)
extern unsigned int numOfCacheHits, numOfCacheAccesses, numOfCacheMisses, numOfCacheEvictions;
extern unsigned int numOfCacheHits_aux, numOfCacheAccesses_aux, numOfCacheMisses_aux;
extern unsigned int numOfCacheHits_buf, numOfCacheAccesses_buf, numOfCacheMisses_buf;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;
extern unsigned int MaxCacheSize;

void SSDInterface::printTime()
{

	double AvgNumOfRequestsPerAccess_SSD = (TotRequests_SSD*1.0) /  numOfCacheMisses;
	double time = (numOfCacheAccesses - numOfCacheAccesses_aux) * 1000 + (numOfCacheMisses)* 28000 + ((numOfNANDPageReads * 1.0) / AvgNumOfRequestsPerAccess_SSD) * 875000 + numOfCacheMisses_aux * 17000 + numOfCacheAccesses_aux * 100;
	cout << "MaxCacheSize = " << MaxCacheSize <<  " numOfCacheAccesses = " << numOfCacheAccesses << " numOfCacheAccesses_aux = " << numOfCacheAccesses_aux << " numOfCacheMisses = " << numOfCacheMisses << " numOfCacheMisses_aux = " << numOfCacheMisses_aux << " numOfCacheEvictions = " << numOfCacheEvictions << " numOfCacheEvictions_aux = " << numOfCacheEvictions_aux << " numOfNANDPageReads = " << numOfNANDPageReads << " TotRequests_SSD = " << TotRequests_SSD << " AvgNumOfRequestsPerAccess_SSD = " << AvgNumOfRequestsPerAccess_SSD << endl;
	cout << "numOfNANDPageWrites = " << numOfNANDPageWrites << endl;
	cout << "Time_RD (in sec): " << time / (1000000000) << endl;
	cout << " numOfCacheAccesses_buf = " << numOfCacheAccesses_buf << " numOfCacheMisses_buf = " << numOfCacheMisses_buf << " numOfCacheEvictions_buf = " << numOfCacheEvictions_buf << endl;
	return;
}
#endif
