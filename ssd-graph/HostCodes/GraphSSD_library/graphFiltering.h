#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include <fstream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <limits>
#include <time.h>
#include <queue>
#include <set>
//#include <boost/filesystem.hpp>
#include <experimental/filesystem>
//using namespace boost::filesystem;
namespace fs = std::experimental::filesystem::v1;

using namespace std;

#define UseFTL 3
#define useCache 1
#define VERTEX_CACHE_MAP 1
#define SLBA_FLUSH_BUFFER 5145631
#define STORE_LARGE_GRAPH 1
#define UseCache_GetPagesFromSSD 1
#define LOAD_GRAPH_FROM_FILE 1
#define CHECK_ERROR ((!LOAD_GRAPH_FROM_FILE) && 1)
#define Update_edges_from_file 1
#define Update_split_half 1

#define AuxMemoryInStorage 1
#define AUXLBA  25743739
#define AuxDataValueType bool
//#define AuxDataValueType unsigned int

#define BufferInStorage 1
#define BUFFERLBA 28365179

#define SLBA_EDGE_WEIGHT 786407
#define IsUndirectedGraph 1
#define ADD_UPDATES_TO_BUFFER 0
#define ADD_FROM_MULTIPLE_FILES 1
#define Update_using_buffer 1
#define ADD_ALL_VERTICES_TO_BUFFER 1
#define ADDED_WITHOUT_TESTING 0
	
class SSDInterface
{	
	public:
		void InitializeGraphDataStructures(unsigned int NumNodes);
		void FinalizeGraphDataStructures();
		void WriteGraphAndDelete();
		int store_graph(int argc, char **argv);
		void ClearQueues();
		void cache_readAdjEdgeList(unsigned int vID, vector<unsigned int> &EdgeList);
		void GetEdgeWeightNvmeCommands(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight);
		void GetAdjEdgeWeightsUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList);
		void AddVertexWeightUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights);
		void GetAdjListUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList);
		void AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList);
#if(Update_edges_from_file == 0)
		void AddEdge(unsigned int numOfRandomEdges);
#elif(Update_edges_from_file == 1)
#if(Update_using_buffer == 0)
		void AddEdge(const char *addEdgeFilePtr);
#elif(Update_using_buffer == 1)
		void AddEdge(std::vector<std::pair<unsigned int, unsigned int> > &MergeVector);
#endif//Update_using_buffer
#endif
		void AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList);
		void GetAdjListNoGTL(unsigned int vID, vector<unsigned int> &EdgeList);
		void printTime();
		void WriteToSSD(unsigned int j);
		void WriteToSSDN(unsigned int SLBA, unsigned int numBlocks);
		void ReadFromSSDN(unsigned int SLBA, unsigned int numBlocks);
		void AddVertexWeight(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights)
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				AddVertexWeightUseNvmeCommands(vID, EdgeList, EdgeWeights);
			}
		}
		void GetEdgeWeight(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight)
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				GetEdgeWeightNvmeCommands(srcVId, destVId, edgeWeight);
			}
		}

                map<unsigned int, vector<unsigned int> > vertexToAdjListMap;
		void *data_nvme_command;
		int fd;
		unsigned int PAGE_SIZE;

		SSDInterface()
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				PAGE_SIZE = 4096;
				if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
					fprintf(stderr, "cannot allocate io payload for data_wr\n");
				}
			}
		}

		unsigned int putAdjEdgeList(unsigned int vID, char *host_buffer, unsigned int numAdjVertices); 
		unsigned int getAdjEdgeList(unsigned int vID, char *host_buffer); 
		unsigned int getEdgeWeight(unsigned int vID, char *host_buffer, unsigned int destEdge); 
		unsigned int getAdjEdgeWeights(unsigned int vID, char * host_buffer);
		unsigned int getTwoHopAdjEdgeList(unsigned int vID, char *host_buffer); 
		unsigned int bufferingPage_loadToSSD(char *host_buffer, unsigned int numOfElementsToLoad); 
		unsigned int bufferingPage_loadToNAND(unsigned int getNumAdjVertices, unsigned int isEdge); 
		unsigned int putAdjEdgeList_weight(unsigned int vID, char *host_buffer, unsigned int numAdjVertices); 
		unsigned int GraphInitialize(); 
		unsigned int getEdgesToSSDDram_initiateLoading(unsigned int gttVidIndex, unsigned int gttVidNumber, unsigned int isEdge); 
		unsigned int binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number); 
		unsigned int IdentifyPosition(unsigned int vertexId);
		unsigned int CopyFunction_InPage(char * devAddr, char * Test_variable_size_buffer, unsigned int currentTopPointer, unsigned int vertexId); 
		unsigned int findEdge_inBuffer(char * Test_variable_size_buffer, char * Test_variable_size_buffer_weight, unsigned int destEdge, unsigned int NumberOfAdjVertices); 
		void process_completionRequest(unsigned int chNo, unsigned int wayNo, unsigned int front, unsigned int isLruBufHit); 
		void process_completionRequest_write(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit); 
		void GraphLRUBufWrite(unsigned int curSect, unsigned int Len, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int lowestVertexId); 
#if(useCache == 1 && UseCache_GetPagesFromSSD == 1)
		bool GraphLRUBufRead(unsigned int sectorNumber);
		void FlushCaches();
#else
		void GraphLRUBufRead(unsigned int sectorNumber);
#endif
		unsigned int GraphFinalize(); 
		bool cache_readAdjEdgeList_page(unsigned int LPN, unsigned int Number, char **data_pointer);
		void AccessAuxMemory(unsigned int index, AuxDataValueType &value, bool isRead);
		bool cache_readNumber(unsigned int LPN, unsigned int Offset, unsigned int Number, AuxDataValueType &value, bool isRead);
		void AccessBufMemory(unsigned int index, unsigned int &value, bool isRead);
		bool cache_readNumber_buf(unsigned int LPN, unsigned int Offset, unsigned int Number, unsigned int &value, bool isRead);
#if(VERTEX_CACHE_MAP == 1)
		bool cache_readAdjEdgeList_vertex(unsigned int vID, char *host_buffer);
		void cache_writeAdjEdgeList_vertex(unsigned int vID, char *host_buffer);
#endif//VERTEX_CACHE_MAP
#if(ADD_UPDATES_TO_BUFFER == 1)
		void AddToBuffer(char *bufferFile, char *mapFile);
		std::vector<unsigned int> bufferRowPtrArray;//Need to initialize this, you can initialize this where you initialize other things
		std::vector<unsigned int> bufferColumnIndexPtrArray;
		unsigned int AccessLinkedListBuffer(unsigned int vertexId, vector<unsigned int> &EdgeList);
		void MergeBuffer(unsigned int NumOfPagesToMerge);
#endif//ADD_UPDATES_TO_BUFFER

};
