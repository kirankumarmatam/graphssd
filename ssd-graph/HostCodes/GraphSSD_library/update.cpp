#include "graphFiltering.h"
#include "header.h"

extern map<unsigned int, unsigned int> vertexMapping;
//extern std::map<unsigned int, char *> Storage;
extern unsigned int GraphLPN;
extern unsigned int currentGTTIndex_edge;
extern struct GTTTable1 *gtt1;
extern unsigned int NumNodes, NumEdges;
unsigned int NumOfExtraPages = 0;
unsigned int NumOfPagesAccessed = 0;
#define DEBUG_UPDATE_FUNCTION 0
#define MEASURE_UPDATE_TIME 1

#if(Update_edges_from_file == 0)
void SSDInterface::AddEdge(unsigned int numOfRandomEdges)
{
	std::vector<unsigned int> randomEdgesSrcVertex;
	randomEdgesSrcVertex.reserve(numOfRandomEdges);
	std::vector<unsigned int> randomEdgesDestVertex;
	randomEdgesDestVertex.reserve(numOfRandomEdges);
	for(unsigned int i=0; i < numOfRandomEdges; i++)
	{
		randomEdgesSrcVertex.push_back(rand() % NumNodes);
		randomEdgesDestVertex.push_back(rand() % NumNodes);
	}
	sort(randomEdgesSrcVertex.begin(), randomEdgesSrcVertex.end());

	unsigned int addTopPointer = 0;

	while(addTopPointer < numOfRandomEdges)
	{
#elif(Update_edges_from_file == 1)
#if(Update_using_buffer == 1)
		void SSDInterface::AddEdge(std::vector<std::pair<unsigned int, unsigned int> > &MergeVector)
		{
#elif(Update_using_buffer == 0)
			void SSDInterface::AddEdge(const char *addEdgeFilePtr)
			{
#endif//Update_using_buffer
#if(DEBUG_UPDATE_FUNCTION == 1)
				cout << "GTL starts" << endl;
				for(unsigned int i = 0; i <= currentGTTIndex_edge; i++)
					cout << "i = " << i << " LPN = " << gtt1->GTTPointer1[i].LPN << " vertex = " << gtt1->GTTPointer1[i].vertexId << endl;
				cout << "GTL ends" << endl;
#endif//DEBUG_UPDATE_FUNCTION
#if(MEASURE_UPDATE_TIME == 1)
				struct timespec update_time_start, update_time_end;
				double update_time_elapsed;
				struct timespec update_gtl_time_start, update_gtl_time_end;
				double update_gtl_time_elapsed_seconds = 0;
				double update_gtl_time_elapsed_nanoSeconds = 0;
				clock_gettime(CLOCK_MONOTONIC, &update_time_start);
#endif//MEASURE_UPDATE_TIME
				//Get the file name here
#if(Update_using_buffer == 0)
				FILE *addEdgeFile = fopen(addEdgeFilePtr, "r");
#endif//Update_using_buffer
				unsigned int srcVertexForAddEdge, destVertexForAddEdge;
#if(Update_using_buffer == 0)
				cout << "Opened addEdgeFile" << endl;
				while(fscanf(addEdgeFile, "%u %u\n", &srcVertexForAddEdge, &destVertexForAddEdge) != EOF)
				{
#elif(Update_using_buffer == 1)
					std::vector<std::pair<unsigned int, unsigned int> >::iterator it = MergeVector.begin();
					while(it != MergeVector.end())
					{
						srcVertexForAddEdge = it->first;
						destVertexForAddEdge = it->second;
#endif//Update_using_buffer
#endif//Update_edges_from_file
						unsigned int gttVidIndex, gttVidNumber;
#if(Update_edges_from_file == 0)
						binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, randomEdgesSrcVertex[addTopPointer], &gttVidIndex, &gttVidNumber);
#elif(Update_edges_from_file == 1)
						//				cout << srcVertexForAddEdge << " " << destVertexForAddEdge << endl;
						binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, srcVertexForAddEdge, &gttVidIndex, &gttVidNumber);
#endif//Update_edges_from_file
						unsigned int positionToAdd = gttVidIndex + gttVidNumber - 1;
						unsigned int nextVertex;
						//		char *currentPage = Storage[gtt1->GTTPointer1[positionToAdd].LPN]; //Read from storage here
						char *currentPage = new char [SSD_PAGE_SIZE];
						NumOfPagesAccessed++;
						ReadFromSSDN(gtt1->GTTPointer1[positionToAdd].LPN * (SSD_PAGE_SIZE/PAGE_SIZE), (SSD_PAGE_SIZE/PAGE_SIZE));
						memcpy((void *)currentPage, (void *)data_nvme_command, SSD_PAGE_SIZE);
						if(positionToAdd < currentGTTIndex_edge)
							nextVertex = gtt1->GTTPointer1[positionToAdd + 1].vertexId;
						else if (positionToAdd == currentGTTIndex_edge)
						{
							nextVertex = -1;
						}

						unsigned int verticesInCurrentPage = ((unsigned int *)currentPage)[SSD_PAGE_SIZE / (sizeof(unsigned int)) - 1];
						unsigned int pageTopPointer = 0, currentPageVertex, currentAddVertex;
#if(Update_edges_from_file == 0)
						char *temporaryPage = new char [SSD_PAGE_SIZE + numOfRandomEdges * sizeof(unsigned int)];
#elif(Update_edges_from_file == 1)
						char *temporaryPage = new char [SSD_PAGE_SIZE + (NumEdges / 1000) * sizeof(unsigned int)];
#endif//Update_edges_from_file
						char *temporaryOffsetStorage = new char [3*verticesInCurrentPage * sizeof(unsigned int)];
						unsigned int currentTempPageOffset = 0;
						while(1)
						{
#if(Update_edges_from_file == 0)
							if(addTopPointer < numOfRandomEdges)
							{
								currentAddVertex = randomEdgesSrcVertex[addTopPointer];
							}
							else {
								currentAddVertex = -1;
							}
							if((pageTopPointer >= verticesInCurrentPage) && ((currentAddVertex >= nextVertex) || (addTopPointer >= numOfRandomEdges)))
							{
								break;
							}
#elif(Update_edges_from_file == 1)
							currentAddVertex = srcVertexForAddEdge;
							if((pageTopPointer >= verticesInCurrentPage) && (currentAddVertex >= nextVertex))
							{
								break;
							}
#endif//Update_edges_from_file
							if(pageTopPointer < verticesInCurrentPage)
							{
								currentPageVertex = ((unsigned int *)currentPage)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (pageTopPointer * 3)];
							} else 
							{
								currentPageVertex = -1;
							}

							//	cout << "A ca = " << currentAddVertex << " ap = " << addTopPointer << " pp = " << pageTopPointer << " cp = " << currentPageVertex << endl;

							if(currentPageVertex <= currentAddVertex) {
								((unsigned int *)temporaryOffsetStorage)[3*pageTopPointer] = currentPageVertex;
								((unsigned int *)temporaryOffsetStorage)[3*pageTopPointer + 1] = currentTempPageOffset;
								unsigned int temp_offset = ((unsigned int *)currentPage)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (pageTopPointer * 3 + 1)];
								unsigned int temp_number = ((unsigned int *)currentPage)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (pageTopPointer * 3 + 2)];
								((unsigned int *)temporaryOffsetStorage)[3*pageTopPointer + 2] = temp_number;
								memcpy((void *)(temporaryPage + currentTempPageOffset), (void *)(currentPage + temp_offset), sizeof(unsigned int) * temp_number);
								pageTopPointer++;
								currentTempPageOffset += temp_number * sizeof(unsigned int);
							} else if(currentPageVertex > currentAddVertex) {
								//				cout << ((unsigned int *)temporaryOffsetStorage)[3*(pageTopPointer - 1)] << " " << currentAddVertex << endl;
								assert(((unsigned int *)temporaryOffsetStorage)[3*(pageTopPointer - 1)] == currentAddVertex);
#if(Update_edges_from_file == 0)
								memcpy((void *)(temporaryPage + currentTempPageOffset), &(randomEdgesDestVertex[addTopPointer]), sizeof(unsigned int));
#elif(Update_edges_from_file == 1)
								memcpy((void *)(temporaryPage + currentTempPageOffset), &(destVertexForAddEdge), sizeof(unsigned int));
#endif//Update_edges_from_file
								((unsigned int *)temporaryOffsetStorage)[3*(pageTopPointer-1) + 2] += 1;
								currentTempPageOffset += sizeof(unsigned int);
#if(Update_edges_from_file == 0)
								addTopPointer++;
#elif(Update_edges_from_file == 1)
#if(Update_using_buffer == 0)
								if(fscanf(addEdgeFile, "%u %u\n", &srcVertexForAddEdge, &destVertexForAddEdge) == EOF)
								{
									srcVertexForAddEdge = -1;
									//				currentAddVertex = -1;
									//							break;
								}
#elif(Update_using_buffer == 1)
								it++;
								if(it == MergeVector.end())
								{
									srcVertexForAddEdge = -1;
								}
								else {
									srcVertexForAddEdge = it->first;
									destVertexForAddEdge = it->second;
								}
#endif//Update_using_buffer
#endif//Update_edges_from_file
							}
						}

#if(Update_split_half == 1)					
						unsigned int maxSSDpages = ceil( ((currentTempPageOffset + verticesInCurrentPage * 3 * sizeof(unsigned int))*1.0) / SSD_PAGE_SIZE);
						unsigned int maxFreeSpaceInApage = (maxSSDpages * SSD_PAGE_SIZE - (currentTempPageOffset + verticesInCurrentPage * 3 * sizeof(unsigned int))) / maxSSDpages;
						assert(maxFreeSpaceInApage <= SSD_PAGE_SIZE);
#endif//Update_split_half

						unsigned int temp_numOfFreeElementsAvailableInTheBuffer = 0, temp_bufferPageTopPointer = 0, numberOfPages = 0;
						char *pageToActOn;
						for(unsigned int i=0; i < verticesInCurrentPage; i++)
						{
							unsigned int valuesRemainingToAdd = ((unsigned int *)temporaryOffsetStorage)[3*i+2];
							unsigned int numOfItemsToCopy;
							do {
#if(Update_split_half == 0)
								if ((valuesRemainingToAdd + 3) > temp_numOfFreeElementsAvailableInTheBuffer)
#elif(Update_split_half == 1)
									if(((valuesRemainingToAdd + 3) > temp_numOfFreeElementsAvailableInTheBuffer) || (maxFreeSpaceInApage > temp_numOfFreeElementsAvailableInTheBuffer*sizeof(unsigned int)))
#endif //Update_split_half
									{
										if(numberOfPages == 0)
										{
											pageToActOn = currentPage;
											((unsigned int *)pageToActOn)[SSD_PAGE_SIZE / (sizeof(unsigned int)) - 1] = 0;
										} else {
											//					Storage[gtt1->GTTPointer1[positionToAdd+1].LPN] = pageToActOn;//Write to storage here
											memcpy((void *)data_nvme_command, (void *)pageToActOn, SSD_PAGE_SIZE);
											if(gtt1->GTTPointer1[positionToAdd].LPN == 159500)
											{
												cout << "Writing at 159500" << endl;
											}
											WriteToSSDN(gtt1->GTTPointer1[positionToAdd].LPN * (SSD_PAGE_SIZE/PAGE_SIZE), (SSD_PAGE_SIZE/PAGE_SIZE));
											pageToActOn = new char [SSD_PAGE_SIZE];
#if(MEASURE_UPDATE_TIME == 1)
											clock_gettime(CLOCK_MONOTONIC, &update_gtl_time_start);
#endif//MEASURE_UPDATE_TIME
											for(unsigned int j = currentGTTIndex_edge; j > positionToAdd; j--)
											{
												gtt1->GTTPointer1[j+1].LPN = gtt1->GTTPointer1[j].LPN;
												gtt1->GTTPointer1[j+1].vertexId = gtt1->GTTPointer1[j].vertexId;
											}
#if(MEASURE_UPDATE_TIME == 1)
											clock_gettime(CLOCK_MONOTONIC, &update_gtl_time_end);
											update_gtl_time_elapsed_seconds = ((double)update_gtl_time_end.tv_sec - (double)update_gtl_time_start.tv_sec);
											update_gtl_time_elapsed_nanoSeconds += ((double)update_gtl_time_end.tv_nsec - (double)update_gtl_time_start.tv_nsec);
#endif//MEASURE_UPDATE_TIME
											currentGTTIndex_edge += 1;
											GraphLPN++;
											gtt1->GTTPointer1[positionToAdd+1].LPN = GraphLPN;
											gtt1->GTTPointer1[positionToAdd+1].vertexId = ((unsigned int *)temporaryOffsetStorage)[3*i];
											positionToAdd += 1;
											((unsigned int *)pageToActOn)[SSD_PAGE_SIZE / (sizeof(unsigned int)) - 1] = 0;
										}
										numberOfPages++;
										//								cout << "numberOfPages = " << numberOfPages << endl;
										temp_numOfFreeElementsAvailableInTheBuffer = (SSD_PAGE_SIZE / (sizeof(unsigned int))) - 1;
										temp_bufferPageTopPointer = 0;
										/*
										   For now leave this
										   maxSSDpages = ceil((currentTempPageOffset + (verticesInCurrentPage - i) * 3 * sizeof(unsigned int)) / SSD_PAGE_SIZE);
										   maxFreeSpaceInAPage = (maxSSDpages * SSD_PAGE_SIZE - (valuesRemainingToAdd + verticesInCurrentPage * 3 * sizeof(unsigned int)));
										 */
									}

								numOfItemsToCopy = (valuesRemainingToAdd + 3) < temp_numOfFreeElementsAvailableInTheBuffer ? valuesRemainingToAdd : temp_numOfFreeElementsAvailableInTheBuffer - 3;
								memcpy(pageToActOn + temp_bufferPageTopPointer * sizeof(unsigned int), temporaryPage + ((unsigned int *)temporaryOffsetStorage)[3*i+1] + ((((unsigned int *)temporaryOffsetStorage)[3*i+2] - valuesRemainingToAdd) * sizeof(unsigned int)), numOfItemsToCopy * sizeof(unsigned int));
								unsigned int numberofVerticesCurrently = ((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 1];
								((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (numberofVerticesCurrently * 3)] = ((unsigned int *)temporaryOffsetStorage)[3*i];
								if(((unsigned int *)temporaryOffsetStorage)[3*i] == 280180)
								{
									cout << "vertex id = 280180" << endl;
								}
								else if(((unsigned int *)temporaryOffsetStorage)[3*i] == 259949)
								{
									cout << "vertex id = 259949" << endl;
								}
								((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (numberofVerticesCurrently * 3 + 1)] = temp_bufferPageTopPointer * sizeof(unsigned int);
								((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (numberofVerticesCurrently * 3 + 2)] = numOfItemsToCopy;
								((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 1]++;
								valuesRemainingToAdd = valuesRemainingToAdd - numOfItemsToCopy;
								temp_bufferPageTopPointer += numOfItemsToCopy;
								temp_numOfFreeElementsAvailableInTheBuffer = temp_numOfFreeElementsAvailableInTheBuffer - numOfItemsToCopy - 3;
							} while (valuesRemainingToAdd > 0);
						}
						NumOfExtraPages += (numberOfPages -1);
						memcpy((void *)data_nvme_command, (void *)pageToActOn, SSD_PAGE_SIZE);
						if(gtt1->GTTPointer1[positionToAdd].LPN == 159500)
						{
							cout << "Writing at 159500" << endl;
						}
						WriteToSSDN(gtt1->GTTPointer1[positionToAdd].LPN * (SSD_PAGE_SIZE/PAGE_SIZE), 4); // Do we need to add positionToAdd here?
						free(currentPage);
						free(temporaryPage);
						free(temporaryOffsetStorage);
					}
#if(MEASURE_UPDATE_TIME == 1)
					clock_gettime(CLOCK_MONOTONIC, &update_time_end);
					update_time_elapsed = ((double)update_time_end.tv_sec - (double)update_time_start.tv_sec) - update_gtl_time_elapsed_seconds;
					update_time_elapsed += (((double)update_time_end.tv_nsec - (double)update_time_start.tv_nsec) - update_gtl_time_elapsed_nanoSeconds) / 1000000000.0;
					fprintf(stdout, "\n");
					fprintf(stdout, "UPDATE_TIME_RD: %lf\n", update_time_elapsed);
					fprintf(stdout, "UPDATE_TIME_RD: sec = %lf nanoSec = %lf gtl_sec = %lf gtl_nanoSec = %lf\n", ((double)update_time_end.tv_sec - (double)update_time_start.tv_sec), ((double)update_time_end.tv_nsec - (double)update_time_start.tv_nsec), update_gtl_time_elapsed_seconds, update_gtl_time_elapsed_nanoSeconds);
#endif//MEASURE_UPDATE_TIME
					cout << "NumOfExtraPages = " << NumOfExtraPages << " NumOfPagesAccessed = " << NumOfPagesAccessed << endl;
#if(DEBUG_UPDATE_FUNCTION == 1)
					cout << "GTL starts" << endl;
					for(unsigned int i = 0; i <= currentGTTIndex_edge; i++)
						cout << "i = " << i << " LPN = " << gtt1->GTTPointer1[i].LPN << " vertex = " << gtt1->GTTPointer1[i].vertexId << endl;
					cout << "GTL ends" << endl;
					//			exit(0);
#endif//DEBUG_UPDATE_FUNCTION

#if(Update_edges_from_file == 0)
					randomEdgesSrcVertex.clear();
					randomEdgesDestVertex.clear();
#elif(Update_edges_from_file == 1)
#if(Update_using_buffer == 0)
					fclose(addEdgeFile);
#endif//Update_using_buffer
#endif//Update_edges_from_file
				}
