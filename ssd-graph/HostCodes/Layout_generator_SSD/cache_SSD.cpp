#include "graphFiltering.h"
#define CacheType 2
#include "lruCache_SSD.h"

extern map<unsigned int, unsigned int > vertexMapping;
extern void * data_nvme_command;
extern unsigned int numOfNodes;
extern unsigned int fd;

std::map<unsigned int, char *>hostCache;
std::vector<unsigned int> randomEvictionVector;

unsigned int MaxCacheSize;
unsigned int CacheUtilized;
unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
unsigned int TotNumOfRequestsPerAccess_host = 0, TotNumOfRequestsPerAccess_SSD = 0;

void cache_readAdjEdgeList(unsigned int LPN, unsigned int Number, char **data_pointer)
{
	numOfCacheAccesses++;
	TotNumOfRequestsPerAccess_host += (Number - (LPN % (4)) + 3) / 4 + ((LPN % 4 == 0) ? 0 : 1);
	int miss = 0;
	for(unsigned int i=0; i<Number; i++)
	{
		if(hostCache.find(LPN+i) == hostCache.end())
		{
			miss = 1;
			break;
		}
	}
#if (VERBOSE == 1)
	cout << "LPN = " << LPN <<" Number = " << Number << endl;
	cout << "miss = " << miss << endl;
#endif
//Allocate and free memory for data_pointer
	if(miss == 0)
	{
		numOfCacheHits++;
		//push the values in to the EdgeList
		for(unsigned int i=0; i < Number; i++)
		{
			data_pointer[i] = hostCache[LPN+i];
#if(CacheType == 2)
			update_lruCache(LPN+i);
#endif //CacheType
		}
	}
	else {
		numOfCacheMisses++;
		TotNumOfRequestsPerAccess_SSD += (Number - (LPN % (4)) + 3) / 4 + ((LPN % 4 == 0) ? 0 : 1);
		struct nvme_user_io io;
		io.opcode = nvme_cmd_read;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long)0;
		io.addr = (unsigned long)data_nvme_command;
		io.slba = LPN;
		io.nblocks = Number - 1;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;
		int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		if (err < 0)
			cout << "Error in executing nvme command!!" << endl;
		if (err)
		{
			fprintf(stderr, "nvme read status:%x\n", err);
			exit(0);
		}

		int freeSpace = MaxCacheSize - CacheUtilized;
		int numOfAdjElements = 4096 * Number;
#if (VERBOSE == 1)
		cout << "freeSpace = " << freeSpace << " CacheUtilized = " << CacheUtilized << " LPN =  "<< LPN << " numOfAdjElements = " << numOfAdjElements << " EdgeListSpace = " << (Number + numOfAdjElements) << endl;
		for(unsigned int i = 0; i < Number * 4096 / sizeof(unsigned int); i++)
			cout << ((unsigned int *)data_nvme_command)[i] << " ";
		cout << endl;
#endif
		while(freeSpace < (Number + numOfAdjElements))
		{
			numOfCacheEvictions++;
#if(CacheType == 1)
			int indexForEviction = rand() % randomEvictionVector.size();
			std::map<unsigned int, char *>::iterator it1 = hostCache.find(randomEvictionVector[indexForEviction]);
#elif(CacheType == 2)
			int EvictedLPN = evict_lruCache();
			std::map<unsigned int, char *>::iterator it1 = hostCache.find(EvictedLPN);
#endif //CacheType
 			assert(it1 != hostCache.end());
			freeSpace += (1 + 4096);
			CacheUtilized -= (1 + 4096);
			free(it1->second);
			hostCache.erase(it1);
#if(CacheType == 1)
			randomEvictionVector[indexForEviction] = randomEvictionVector.back();
			randomEvictionVector.pop_back();
#endif //CacheType == 2
		}
		for(unsigned int i=0; i < Number; i++)
		{
			if(hostCache.find(LPN + i) == hostCache.end())
			{
				hostCache[LPN+i];
				hostCache[LPN+i] = new char [4096];
				memcpy((void *)hostCache[LPN+i], (void *)((char *)data_nvme_command + 4096 * i), 4096);
#if(CacheType == 1)
				randomEvictionVector.push_back(LPN+i);
#endif//CacheType
				CacheUtilized += (1 + 4096);
			}
			data_pointer[i] = hostCache[LPN+i];
#if(CacheType == 2)
			update_lruCache(LPN+i);
#endif//CacheType
		}
	}
	return;
}
//Need to take care of the cases where number of adjacent vertices for a vertex are zero

//writeAdjEdgeList -- no need to support it now
