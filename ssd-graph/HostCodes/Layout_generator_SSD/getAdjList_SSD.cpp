#include "graphFiltering.h"
#define PAGE_PER_CMD 1
#define PERFORM_GET_EDGE 1
#define NUM_OF_GET_EDGE_REQUESTS 500
#define FLUSH_SSD_BUFFER 1
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192

unsigned int numOfNodes;
unsigned int numOfEdges;
unsigned int fd;
void *data_nvme_command;

#if (CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if (CALCULATE_OVERHEAD_TIME == 1)
       struct timespec time_start_overhead, time_end_overhead;
       double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

int main(int argc, char **argv)
{
	struct timespec time_start, time_end;
	double time_elapsed;

	if(argc < 3)
	{
		cout << "sudo ./a.out /dev/nvme0n1 dataSet.txt";
		return 0;
	}

	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);//This need to match with the input file!
	numOfNodes = G->GetNodes();
	numOfEdges = G->GetEdges();
	map<unsigned int, unsigned int> vertexMapping;
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);

	//	static const char *perrstr;
	//	int err, fd;
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		cout << "Error opening file!!" << endl;
	if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		return 0;
	}

	InitializeGraphDataStructures(G->GetNodes());
#if (FLUSH_SSD_BUFFER == 1)
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1

#if (CALCULATE_DATA_FETCHED == 1)
baseline_data_fetched = 0;
adjacencyList_data_fetched = 0;
#endif //CALCULATE_DATA_FETCHED == 1

#if (CALCULATE_OVERHEAD_TIME == 1)
time_elapsed_overhead = 0;
#endif // CALCULATE_OVERHEAD_TIME

	vector<unsigned int> EdgeList;
#if (PERFORM_GET_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0;
	while(i < NUM_OF_GET_EDGE_REQUESTS)
	{
#if (VERBOSE == 1)
		cout << "i = " << i << endl;
#endif
		TNGraph::TNodeI NI = G->GetRndNI();
		if(NI.GetOutDeg() != 0)
		{
			GetAdjList(vertexMapping[NI.GetId()], EdgeList);
			i++;
		}
		EdgeList.clear();
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#if(CALCULATE_DATA_FETCHED == 1)
	cout << "baseline data fetched = " << baseline_data_fetched  << " graphSSD data fetched = " << adjacencyList_data_fetched << endl;
#endif // CALCULATE_DATA_FETCHED == 1
#if(CALCULATE_OVERHEAD_TIME == 1)
	cout << "IO overhead time = " << time_elapsed_overhead << endl;
#endif //CALCULATE_OVERHEAD_TIME == 1
#endif //PERFORM_GET_EDGE

	FinalizeGraphDataStructures();

	return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
