#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <vector>
#include <list>

#include <time.h>

using namespace std;
#define SLBA 200
#define SLBA_FLUSH_BUFFER 5145631

#define CALCULATE_DATA_FETCHED  0
#define CALCULATE_OVERHEAD_TIME 1

#define useCache 1
#define VERBOSE 0
#define IsUndirectedGraph 1
#define SortAndMap 1

void GetAdjList(unsigned int vID, vector<unsigned int> &EdgeList);
void WriteToSSD(unsigned int j);
void InitializeGraphDataStructures(unsigned int NumNodes);
void FinalizeGraphDataStructures();
void cache_readAdjEdgeList(unsigned int LPN, unsigned int Number, char **data_pointer);
