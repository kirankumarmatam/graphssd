#include "lruCache_SSD.h"

std::vector<unsigned int> LRU_vector;

void delete_lruCache(unsigned int LPN)
{
	for(std::vector<unsigned int>::iterator it=LRU_vector.begin(); it != LRU_vector.end(); it++)
	{
		if(*it == LPN)
		{
			LRU_vector.erase(it);
			break;
		}
	}
}

void update_lruCache(unsigned int LPN)
{
	delete_lruCache(LPN);
	LRU_vector.insert(LRU_vector.begin(), LPN);
}

unsigned int evict_lruCache()
{
	unsigned int LPN = LRU_vector.back();
	LRU_vector.pop_back();
	return LPN;
}
