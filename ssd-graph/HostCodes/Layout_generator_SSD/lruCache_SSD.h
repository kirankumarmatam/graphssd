#include <vector>

void delete_lruCache(unsigned int LPN);
void update_lruCache(unsigned int LPN);
unsigned int evict_lruCache();
