#include "header.h"

#define SLBA 200
#define PRINT_CSR 0
#define CHECK_ERR 0
#define STORE_AT_SSD 1
#define PAGES_PER_CMD 32

int main(int argc, char **argv)
{
	if(argc < 4)
	{
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt output_flash.txt" << endl;
		return 0; 
	}

    cout << "Loading using SNAP library" << endl;
#if(IsUndirectedGraph == 1)
            PUNGraph G1 = TSnap::LoadEdgeList<PUNGraph>(argv[2], 0, 1);
                    PNGraph G = TSnap::ConvertGraph<PNGraph>(G1);
#elif(IsUndirectedGraph == 0)
                            PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
#endif//IsUndirectedGraph
    cout << "Loaded using SNAP library" << endl;
    //	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
	map<unsigned int, unsigned int> vertexMapping;
#if(SortAndMap == 1)
    std::vector<unsigned int> SortVector;
    for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
    {
        SortVector.push_back(NI.GetId());
    }
    std::sort(SortVector.begin(), SortVector.end());
    unsigned int nodeIndex = 0;
    for(unsigned int i = 0; i < SortVector.size(); i++)
    {
        vertexMapping[SortVector[i]] = nodeIndex++;
    }
    assert(vertexMapping.size() == nodeIndex);
#elif(SortAndMap == 0)
    unsigned int nodeIndex = 0;
    for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
    {
        vertexMapping[NI.GetId()] = nodeIndex++;
    }
    assert(vertexMapping.size() == nodeIndex);
#endif//SortAndMap

    cout << "Gathered vertex mapping information" << endl;
	assert(vertexMapping.size() == nodeIndex);
	assert(vertexMapping.size() == G->GetNodes());

	unsigned int *Array = (unsigned int *) calloc((unsigned int)(G->GetNodes())+1+(unsigned int)(G->GetEdges()), sizeof(unsigned int));
    if(Array == NULL)
    {
        cout << "Allocating csr array failed, size requested = " << G->GetNodes()+1+G->GetEdges() << " No. of nodes = " << G->GetNodes() << " No. of edges = " << (unsigned int)G->GetEdges() << endl;
        exit(0);
    }
    cout << "Allocated array for csr" << endl;
	Array[0] = 0;
	unsigned int numOfEdgesInCSR = 0;
#if(SortAndMap == 0)
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
#elif(SortAndMap == 1)
        for(unsigned int k = 0; k < SortVector.size(); k++)
        {
            TNGraph::TNodeI NI = G->GetNI(SortVector[k]);
#endif//SortAndMap
		unsigned int i = vertexMapping[NI.GetId()];
		unsigned int MaxNeighborsPerVertex = (NI.GetOutDeg() < (16*4096 / sizeof(unsigned int))) ? NI.GetOutDeg() : (16*4096 / sizeof(unsigned int));
		numOfEdgesInCSR += MaxNeighborsPerVertex;
		Array[i+1] = Array[i] + MaxNeighborsPerVertex;
        std::vector<unsigned int> EdgeList;
        for(unsigned int j = 0; j < MaxNeighborsPerVertex; j++)
        {
            EdgeList.push_back(vertexMapping[NI.GetOutNId(j)]);
        }
        std::sort(EdgeList.begin(), EdgeList.end());
        for(unsigned int j = 0; j < MaxNeighborsPerVertex; j++)
        {
            Array[G->GetNodes() + 1 + Array[i]+j] = EdgeList[j];
        }
        EdgeList.clear();
	}
#if(SortAndMap == 1)
        SortVector.clear();
#endif//SortAndMap
    cout << "Loaded graph into CSR array" << endl;

//	static const char *perrstr;
	int err, fd;

//	struct timespec time_start, time_end;
//	double time_elapsed;
//	perrstr = argv[1];
	void * buffer;

	if (posix_memalign(&buffer, 4096, REQUEST_SIZE)) {
		fprintf(stderr, "can not allocate io payload\n");
		return 0;
	}

	ofstream myFlashFile;
	
	myFlashFile.open(argv[3], fstream::in | fstream::out | fstream::trunc);
	if (!myFlashFile.is_open())
                cout << " Cannot open flash output file!" << endl;
	
    cout << "Opened file for writing" << endl;
	unsigned int pageCount = 0;
#if (STORE_AT_SSD == 1)
	unsigned int NumNodes = G->GetNodes();
	myFlashFile.write((char *)(&NumNodes), sizeof(unsigned int));
	myFlashFile.write((char *)(&numOfEdgesInCSR), sizeof(unsigned int));
	unsigned int i = 0;
	for(i = 0; i < (G->GetNodes() + 1 + numOfEdgesInCSR); i += (4096*PAGES_PER_CMD/sizeof(unsigned int)))
	{
        if(i % (4096*PAGES_PER_CMD * 1024*10) == 0)
            cout << "Wrote around 1 more GB of CSR matrix" << endl;
		unsigned int valuesRemaining = (G->GetNodes() + 1 + numOfEdgesInCSR) - i;
		if(valuesRemaining < (4096*PAGES_PER_CMD/sizeof(unsigned int)))
		{
			memcpy(buffer, (void *)(Array+i), valuesRemaining * sizeof(unsigned int));
		}
		else 
		{
			memcpy(buffer, (void *)(Array+i), 4096*PAGES_PER_CMD);
		}

		myFlashFile.write((char *)buffer, PAGES_PER_CMD * 4096);
	}
	myFlashFile.close();
    cout << "Wrote CSR matrix for file" << endl;
#endif
#if (CHECK_ERR == 1)
	struct nvme_user_io io;
        pageCount = 0;
	unsigned int err_count = 0;
        for(unsigned int i = 0; i < (G->GetNodes() + 1 + numOfEdgesInCSR); i += (4096/sizeof(unsigned int)))
        {
                unsigned int valuesRemaining = (G->GetNodes() + 1 + numOfEdgesInCSR) - i;
                io.opcode = nvme_cmd_read;
                io.flags = 0;
                io.control = 0;
                io.metadata = (unsigned long) 0;
                io.addr = (unsigned long) buffer;
                io.slba = SLBA + pageCount;
                io.nblocks = 0;
                io.dsmgmt = 0;
                io.reftag = 0;
                io.apptag = 0;
                io.appmask = 0;

                err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
                //              if (err < 0)
                //                      goto perror;
		if (err)
			fprintf(stderr, "nvme write status:%x\n", err);
		pageCount++;

		if(valuesRemaining < (4096/sizeof(unsigned int)))
		{
			for(unsigned int j = 0; j < valuesRemaining; j++)
			{
				if(*((unsigned int *)buffer + j) != Array[i+j])
					err_count += 1;
			}
		}
		else 
		{
			for(unsigned int j = 0; j < (4096 / sizeof(unsigned int)); j++)
			{
				if(*((unsigned int *)buffer + j) != Array[i+j])
					err_count += 1;
			}
		}
        }
	cout << "Error rate = " << (err_count * 1.0) / (G->GetNodes() + 1 + numOfEdgesInCSR) << endl;
#endif //CHECK_ERR

#if (PRINT_CSR == 1)
	for(int i = 0; i < (G->GetNodes() + 1 + numOfEdgesInCSR); i += 1)
	{
		cout << i << " " << Array[i] << endl;
	}
	cout << endl;
#endif
	free(Array);
	return 0;
}
