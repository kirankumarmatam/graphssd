#include "../StoreGraphToAFile/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
unsigned int NUMBER_OF_REQUESTS;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	fstream myFile;
	myFile.open(argv[4], fstream::in);
	unsigned int GTLsize;
	myFile.read((char *)(&GTLsize), sizeof(unsigned int));
	cout << "GTLsize = " << GTLsize << endl;
	myFile.close();
	return 0;
}
