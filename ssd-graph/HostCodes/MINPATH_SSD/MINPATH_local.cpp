#include "../Baseline_library/header.h"
#include <limits>

using namespace std;

unsigned int numOfNodes;
unsigned int numOfEdges;

int main(int argc, char **argv)
{

    PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[1], 0, 1);
    numOfNodes = G->GetNodes();
    numOfEdges = G->GetEdges();

    vector<unsigned int> EdgeList;
    unsigned int EdgeWeight,CurrentDistance;

    list<unsigned int> updatedQueue;
    for(int numOfrun = 0; numOfrun < 1; numOfrun++)
    {
//		unsigned int root = rand() % G->GetNodes(), reqElement = rand() % G->GetNodes();
//		unsigned int root = 0, reqElement = 100000;

        unsigned int root = 0;

        unsigned int *distance = (unsigned int *)malloc(numOfNodes*sizeof(unsigned int));
        fill_n(distance,numOfNodes,numeric_limits<unsigned int>::max());
        //fill_n(distance,numOfNodes,100);
        distance[root]=0;
        updatedQueue.push_back(root); // Need to get ROOT as input element, root element needs to be < NumVertices
        bool *updated = (bool *)calloc(numOfNodes, sizeof(bool));

        while(!updatedQueue.empty())
        {
            unsigned int topElement = updatedQueue.front();

            updatedQueue.pop_front();
            CurrentDistance=distance[topElement];
            //GetAdjList(topElement, EdgeList);
            TNGraph::TNodeI NI = G->GetNI(topElement);
            for (int e = 0; e < NI.GetOutDeg(); e++) 
            {
                EdgeList.push_back(NI.GetOutNId(e));
            }
            
            for(unsigned int i=0; i < EdgeList.size(); i++)
            {
#if(STORE_LARGE_SCALE == 1)
                assert(EdgeList[i] < numOfNodes);
#else
                assert(EdgeList[i] < vertexMapping.size());
#endif//STORE_LARGE_SCALE
                //GetEdgeWeight(topElement,EdgeList[i],EdgeWeight);
				        EdgeWeight=1;
                if (CurrentDistance+EdgeWeight<distance[EdgeList[i]])
                {
                    distance[EdgeList[i]]=CurrentDistance+EdgeWeight;
                    if (updated[EdgeList[i]]==0)
                    {
                        updatedQueue.push_back(EdgeList[i]);
                        updated[EdgeList[i]] = 1;
                    }
                }
            }
            fill_n(updated,numOfNodes,false);
            EdgeList.clear();

        }
        for (unsigned int i=0;i<numOfNodes;i++)
        {
            cout << distance[i] << endl;
        }
        updatedQueue.clear();
        free(updated);
		free(distance);
    }

    return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
