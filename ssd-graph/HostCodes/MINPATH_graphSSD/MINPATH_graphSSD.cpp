#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1


#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
extern unsigned int cache_percentage;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;

int main(int argc, char **argv)
{
    SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
    SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

#if(LOAD_GRAPH_FROM_FILE == 0)
    NumNodes = G->GetNodes(), NumEdges = G->GetEdges();
#endif
    cout << NumNodes << " " << NumEdges << endl;
#if(useCache==1)
    if(((NumNodes + NumEdges) * sizeof(unsigned int)) < (1024*1024))
        MaxCacheSize = 1024 * 1024;
    else MaxCacheSize = (((NumNodes + NumEdges) * sizeof(unsigned int)) * cache_percentage) / 100;
#endif

    struct timespec time_start, time_end;
    double time_elapsed;

    vector<unsigned int> EdgeList;
    unsigned int EdgeWeight,CurrentDistance;

    unsigned int total_number_of_requests = 1;
//	srand(time(NULL));
//	unsigned int root = 0, reqElement = 100000;
//	cout << root << " " << reqElement << endl;
    clock_gettime(CLOCK_MONOTONIC, &time_start);
//	unsigned int root = 0, reqElement = 2;
    list<unsigned int> updatedQueue;
    for(int numOfrun = 0; numOfrun < total_number_of_requests; numOfrun++)
    {
//		cout << numOfrun << endl;
//	unsigned int root = rand() % G->GetNodes(), reqElement = rand() % G->GetNodes();

        unsigned int root = 0;
#if(AuxMemoryInStorage == 0)
#if(LOAD_GRAPH_FROM_FILE == 1)
        bool *updated = (bool *)calloc(NumNodes, sizeof(bool));
        unsigned int *distance = (unsigned int *)malloc(NumNodes*sizeof(unsigned int));
        fill_n(distance,NumNodes,numeric_limits<unsigned int>::max());
        distance[root]=0;
#else
        bool *updated = (bool *)calloc(vertexMapping.size(), sizeof(bool));
        unsigned int *distance = (unsigned int *)malloc(vertexMapping.size()*sizeof(unsigned int));
        fill_n(distance,vertexMapping.size(),numeric_limits<unsigned int>::max());
#endif
#else
        for(unsigned int i=0; i < NumNodes; i++)
        {
            AuxDataValueType value = 0;
            SSDInstance.AccessAuxMemory(i, value, 0);
        }
        for(unsigned int i =NumNodes; i < 2*NumNodes; i++)
        {
		AuxDataValueType value;
		if (i==NumNodes+root)
		{
			value = 0;
			SSDInstance.AccessAuxMemory(i, value, (bool)0);
		}
		else
		{
			value = numeric_limits<unsigned int>::max();
			SSDInstance.AccessAuxMemory(i, value, (bool)0);
            }

        }

#endif//AuxMemoryInStorage
        updatedQueue.push_back(root); // Need to get ROOT as input element, root element needs to be < NumVertices


        unsigned int temp_debugging = 0;
#if(STOP_SIMULATION == 1)
        unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1
        while(!updatedQueue.empty())
        {
//			cout << "E" <<endl ;
            unsigned int topElement = updatedQueue.front();
#if (VERBOSE == 1)
            cout << topElement << ", " << endl; ;
#endif
#if(AuxMemoryInStorage == 0)
            CurrentDistance=distance[topElement];
#else
            AuxDataValueType distance_value;
            SSDInstance.AccessAuxMemory(topElement+NumNodes, distance_value, 1);
            CurrentDistance=distance_value;
#endif
//			cout << "E1" <<endl ;
            SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
#if (VERBOSE_PRINT_ADJLIST == 1)
            cout << topElement << " :";
            for(unsigned int i=0; i < EdgeList.size(); i++)
            {
                cout << " " << EdgeList[i];
            }
            cout << endl;
#endif //VERBOSE_PRINT_ADJLIST
//			cout << "E" <<endl ;
            updatedQueue.pop_front();
            for(unsigned int i=0; i < EdgeList.size(); i++)
            {
#if(AuxMemoryInStorage == 0)
#if(LOAD_GRAPH_FROM_FILE == 1)
                assert(EdgeList[i] < NumNodes);
#else
                assert(EdgeList[i] < vertexMapping.size());
#endif
//Keep EdgeWeight == 1??
                if (CurrentDistance+EdgeWeight<distance[EdgeList[i]])
                {
                    distance[EdgeList[i]]=CurrentDistance+EdgeWeight;
                    if (updated[EdgeList[i]]==0)
                    {
                        updatedQueue.push_back(EdgeList[i]);
                        updated[EdgeList[i]] = 1;
                    }
                }
#else
                //GetEdgeWeight(topElement,EdgeList[i],EdgeWeight);
                EdgeWeight=1;
                AuxDataValueType distance_value;
                SSDInstance.AccessAuxMemory(EdgeList[i]+NumNodes, distance_value, 1);
                if (CurrentDistance+EdgeWeight<distance_value)
                {
                    distance_value=CurrentDistance+EdgeWeight;
                    SSDInstance.AccessAuxMemory(EdgeList[i]+NumNodes, distance_value, 0);
                    AuxDataValueType update_value;
                    SSDInstance.AccessAuxMemory(EdgeList[i], update_value, 1);
                    if (update_value==0)
                    {
                        updatedQueue.push_back(EdgeList[i]);
                        update_value = 1;
                        SSDInstance.AccessAuxMemory(EdgeList[i], update_value, 0);
                    }
                }
#endif // AuxMemoryInStorage
            }
            //		cout << "Searching" << endl;
            //		cout << endl;
            for(unsigned int i=0; i < EdgeList.size(); i++)
            {
#if(AuxMemoryInStorage == 0)
                updated[EdgeList[i]] = 0;
#else
                AuxDataValueType update_value=0;
                SSDInstance.AccessAuxMemory(EdgeList[i], update_value, 0);
#endif
            }

            EdgeList.clear();
            //		temp_debugging++;
            //		if(temp_debugging == 12)
            //			break;
#if(STOP_SIMULATION == 1)
            number_requests_executed++;
            //		cout << number_requests_executed << endl;
            if(number_requests_executed == NUMBER_OF_REQUESTS)
                break;
#endif//STOP_SIMULATION == 1
        }
#if(AuxMemoryInStorage == 0)
        free(updated);
        free(distance);
#endif
        updatedQueue.clear();
    }
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
    time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
    SSDInstance.FinalizeGraphDataStructures();
#if(useCache == 1)
    cout << "MaxCacheSize = " << MaxCacheSize << " numOfCacheHits = " <<  numOfCacheHits << " numOfCacheEvictions = " << numOfCacheEvictions << " numOfCacheAccesses = " << numOfCacheAccesses << " numOfCacheMisses = " <<  numOfCacheMisses << " TotRequests_SSD = " << TotRequests_SSD << " AvgNumOfRequestsPerAccess_SSD = " << (double) numOfCacheMisses / TotRequests_SSD  << " TotRequests_host = " << TotRequests_host << " TotNumOfRequestsPerAccess_host = " << (double)numOfCacheAccesses / TotRequests_host << endl;
#endif//useCache
    return 0;
}
