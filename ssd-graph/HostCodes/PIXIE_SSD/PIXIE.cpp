#include "../Baseline_library/graphFiltering.h"
#include <limits>
#include <functional>
#define PAGE_PER_CMD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 0
#define FLUSH_SSD_BUFFER 1
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define STORE_LARGE_SCALE 0

extern unsigned int numOfNodes;
extern unsigned int numOfEdges;
extern unsigned int fd;
extern void *data_nvme_command;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
struct timespec time_start_overhead, time_end_overhead;
double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

template <typename Sequence, typename BinaryPredicate>
struct IndexCompareT {
  IndexCompareT(const Sequence& seq, const BinaryPredicate comp)
    : seq_(seq), comp_(comp) { }
  bool operator()(const size_t a, const size_t b) const
  {
    return comp_(seq_[a], seq_[b]);
  }
  const Sequence seq_;
  const BinaryPredicate comp_;
};

template <typename Sequence, typename BinaryPredicate>
IndexCompareT<Sequence, BinaryPredicate>
IndexCompare(const Sequence& seq, const BinaryPredicate comp)
{
  return IndexCompareT<Sequence, BinaryPredicate>(seq, comp);
}

template <typename Sequence, typename BinaryPredicate>
std::vector<unsigned int> ArgSort(const Sequence& seq, BinaryPredicate func)
{
  std::vector<unsigned int> index(seq.size());
  for (unsigned int i = 0; i < index.size(); i++)
    index[i] = i;

  std::sort(index.begin(), index.end(), IndexCompare(seq, func));

  return index;
}


int main(int argc, char **argv)
{
    set_context(argc, argv);

    vector<unsigned int> EdgeList;
    unsigned int alpha=5;
    unsigned int N=20;
    unsigned int Nout=10;
    unsigned int q=rand()%N;

    struct timespec time_start, time_end;
    double time_elapsed;
    clock_gettime(CLOCK_MONOTONIC, &time_start);
//#if(AuxMemoryInStorage == 0)
    vector<unsigned int> V;
    for (unsigned int i =0; i < N; i++)
    {
        V.push_back(0);
    }
/*#else
    for(unsigned int i =0; i < N; i++)
    {
        AuxDataValueType value = 0;
        AccessAuxMemory(i, value, (bool)0);
    }
#endif*/
    for(int numOfrun = 0; numOfrun < 1; numOfrun++)
    {

#if(STOP_SIMULATION == 1)
        unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1
//        srand(time(NULL));

        unsigned int totSteps=0;
        unsigned int currSteps=alpha;
        while (totSteps<N)
        {
            unsigned int currPin=q;
            for(unsigned int i =0; i < currSteps; i++)
            {
                EdgeList.clear();
                GetAdjList(currPin, EdgeList);
                unsigned int length=EdgeList.size();
                unsigned int random_neighbour=rand()%length;
                currPin=EdgeList[random_neighbour];

                EdgeList.clear();
                GetAdjList(currPin, EdgeList);
                length=EdgeList.size();
                random_neighbour=rand()%length;
                currPin=EdgeList[random_neighbour];

//#if(AuxMemoryInStorage == 0)
                V[currPin]++;
/*#else
                AuxDataValueType tmp = 0;
                AccessAuxMemory(i, tmp, (bool)1);
                tmp++;
                AccessAuxMemory(i, tmp, (bool)0);
#endif//AuxMemoryInStorage*/
            }
            totSteps+=currSteps;
        }
        vector<unsigned int> idxs = ArgSort(V, greater<unsigned int>());
        cout << "Query pin is " << q <<endl;
        for (unsigned int i=0;i<Nout;i++)
        {
            cout << "node " << idxs[i] << " has been visited for " << V[idxs[i]] << " times" <<endl;
        }
    }
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
    time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
    FinalizeGraphDataStructures();
#if(useCache == 1)
    printTime();
#endif


//perror:
//	perror(perrstr);
//	return 1;

}
