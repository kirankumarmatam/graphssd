#include "../Baseline_library/header.h"
#include <limits>
#include <functional>
#define INS 3
#define TENTAINS 2
#define NOTINS 1
#define UNKNOWN 0

#define ISCOLORED 128
#define NOTMIN 4
#define NEIGHBORINS 8
using namespace std;

unsigned int numOfNodes;
unsigned int numOfEdges;

template <typename Sequence, typename BinaryPredicate>
struct IndexCompareT {
  IndexCompareT(const Sequence& seq, const BinaryPredicate comp)
    : seq_(seq), comp_(comp) { }
  bool operator()(const size_t a, const size_t b) const
  {
    return comp_(seq_[a], seq_[b]);
  }
  const Sequence seq_;
  const BinaryPredicate comp_;
};

template <typename Sequence, typename BinaryPredicate>
IndexCompareT<Sequence, BinaryPredicate>
IndexCompare(const Sequence& seq, const BinaryPredicate comp)
{
  return IndexCompareT<Sequence, BinaryPredicate>(seq, comp);
}

template <typename Sequence, typename BinaryPredicate>
std::vector<unsigned int> ArgSort(const Sequence& seq, BinaryPredicate func)
{
  std::vector<unsigned int> index(seq.size());
  for (unsigned int i = 0; i < index.size(); i++)
    index[i] = i;

  std::sort(index.begin(), index.end(), IndexCompare(seq, func));

  return index;
}
int main(int argc, char **argv)
{
    PUNGraph G1 = TSnap::LoadEdgeList<PUNGraph>(argv[1], 0, 1);
    PNGraph G = TSnap::ConvertGraph<PNGraph>(G1);
    numOfNodes = G->GetNodes();
    numOfEdges = G->GetEdges();

    unsigned int alpha=5;
    unsigned int N=20;
    unsigned int Nout=10;
    unsigned q=rand()%N;
    vector<int> V;
    for (unsigned int i =0; i < N; i++)
    {
        V.push_back(0);
    }
    for(int numOfrun = 0; numOfrun < 1; numOfrun++)
    {

#if(STOP_SIMULATION == 1)
        unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1
//        srand(time(NULL));

        unsigned totSteps=0;
        unsigned currSteps=alpha;
        while (totSteps<N)
        {
            unsigned currPin=q;
            for(unsigned int i =0; i < currSteps; i++)
            {
                TNGraph::TNodeI NI = G->GetNI(currPin);
                unsigned int e=rand()%NI.GetOutDeg();
                unsigned int random_neighbour=NI.GetOutNId(e);
                currPin=random_neighbour;

                NI = G->GetNI(currPin);
                e=rand()%NI.GetOutDeg();
                random_neighbour=NI.GetOutNId(e);
                currPin=random_neighbour;

                V[currPin]++;
            }
            totSteps+=currSteps;
        }
        vector<unsigned int> idxs = ArgSort(V, greater<unsigned int>());
        cout << "Query pin is " << q <<endl;
        for (unsigned int i=0;i<Nout;i++)
        {
            cout << "node " << idxs[i] << " has been visited for " << V[idxs[i]] << " times" <<endl;
        }
    }

    return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
