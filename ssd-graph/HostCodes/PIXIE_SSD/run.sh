#!/bin/bash
FILES=/Datasets/Benchmarks/SNAP/Run/*
sh compile_storeInCSR.sh
sh compile_pixieSSD.sh.sh
for i in $FILES
do
	echo $i
	sudo ./storeInCSR /dev/nvme0n1 $i
	sudo ./PIXIE /dev/nvme0n1 $i
done
