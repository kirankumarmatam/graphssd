#include "../GraphSSD_library/graphFiltering.h"
#include <functional>
#define STORE_GRAPH_AT_SSD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
extern unsigned int cache_percentage;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;

template <typename Sequence, typename BinaryPredicate>
struct IndexCompareT
{
    IndexCompareT(const Sequence& seq, const BinaryPredicate comp)
        : seq_(seq), comp_(comp) { }
    bool operator()(const size_t a, const size_t b) const
    {
        return comp_(seq_[a], seq_[b]);
    }
    const Sequence seq_;
    const BinaryPredicate comp_;
};

template <typename Sequence, typename BinaryPredicate>
IndexCompareT<Sequence, BinaryPredicate>
IndexCompare(const Sequence& seq, const BinaryPredicate comp)
{
    return IndexCompareT<Sequence, BinaryPredicate>(seq, comp);
}

template <typename Sequence, typename BinaryPredicate>
std::vector<unsigned int> ArgSort(const Sequence& seq, BinaryPredicate func)
{
    std::vector<unsigned int> index(seq.size());
    for (unsigned int i = 0; i < index.size(); i++)
        index[i] = i;

    std::sort(index.begin(), index.end(), IndexCompare(seq, func));

    return index;
}

int main(int argc, char **argv)
{
    SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
    SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

#if(LOAD_GRAPH_FROM_FILE == 0)
    NumNodes = G->GetNodes(), NumEdges = G->GetEdges();
#endif
    cout << NumNodes << " " << NumEdges << endl;

    struct timespec time_start, time_end;
    double time_elapsed;

    vector<unsigned int> EdgeList;
    unsigned int alpha=5;
    unsigned int N=20;
    unsigned int Nout=10;
    unsigned int q=rand()%N;

    clock_gettime(CLOCK_MONOTONIC, &time_start);
    list<unsigned int> updatedQueue;

    unsigned int temp_debugging = 0;
#if(STOP_SIMULATION == 1)
    unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1

//	srand(time(NULL));
    /*#if(AuxMemoryInStorage == 0)
    #if(LOAD_GRAPH_FROM_FILE == 1)
    	short int *degree = (short int *)calloc(NumNodes, sizeof(short int));
    #else
    	short int *degree = (short int *)calloc(vertexMapping.size(), sizeof(short int));
    #endif
    #else
    	AuxDataValueType value = 0;
    	for(unsigned int i=0; i < NumNodes; i++)
    	{
    		SSDInstance.AccessAuxMemory(i, value, 0);
    	}
    #endif//AuxMemoryInStorage*/
    vector<unsigned int> V;
    for (unsigned int i =0; i < N; i++)
    {
        V.push_back(0);
    }
    unsigned int totSteps=0;
    unsigned int currSteps=alpha;
    while (totSteps<N)
    {
        unsigned int currPin=q;
        for(unsigned int i =0; i < currSteps; i++)
        {
            EdgeList.clear();
            SSDInstance.GetAdjListUseNvmeCommands(i, EdgeList);
            unsigned int length=EdgeList.size();
            unsigned int random_neighbour=rand()%length;
            currPin=EdgeList[random_neighbour];

            EdgeList.clear();
            SSDInstance.GetAdjListUseNvmeCommands(i, EdgeList);
            length=EdgeList.size();
            random_neighbour=rand()%length;
            currPin=EdgeList[random_neighbour];

//#if(AuxMemoryInStorage == 0)
            V[currPin]++;
            /*#else
                            AuxDataValueType tmp = 0;
                            AccessAuxMemory(i, tmp, (bool)1);
                            tmp++;
                            AccessAuxMemory(i, tmp, (bool)0);
            #endif//AuxMemoryInStorage*/
        }
        totSteps+=currSteps;
    }
    vector<unsigned int> idxs = ArgSort(V, greater<unsigned int>());
    cout << "Query pin is " << q <<endl;
    for (unsigned int i=0; i<Nout; i++)
    {
        cout << "node " << idxs[i] << " has been visited for " << V[idxs[i]] << " times" <<endl;
    }

    clock_gettime(CLOCK_MONOTONIC, &time_end);
    time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
    time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
    SSDInstance.FinalizeGraphDataStructures();
#if(useCache == 1)
    SSDInstance.printTime();
#endif//useCache
    return 0;
}
