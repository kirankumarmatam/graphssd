#!/bin/bash
FILES=/home/ossd/GraphSSD/Benchmarks/SNAP/MediumGraphs/*
sh compile_bfsSSD.sh
for i in $FILES
do
	echo $i
	sudo ../Baseline_library/storeInCSR /dev/nvme0n1 $i 10000
	sudo ./BFS /dev/nvme0n1 $i 10000
done
