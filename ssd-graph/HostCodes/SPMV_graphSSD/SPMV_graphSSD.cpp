#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_BFS 1
#define VERBOSE_BFS 1
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1
#define PRINT_LEVEL_NUMBER 1
#define READ_REQUESTS_FROM_FILE 0 // This is incomplete
#define READ_REQUEST_AS_ARGUMENT 1

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
unsigned int cache_percentage = 5;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	cout << G->GetNodes() << " " << G->GetEdges() << endl;
#if(useCache==1)
	if(((G->GetNodes() + G->GetEdges()) * sizeof(unsigned int)) < (1024*1024))
		MaxCacheSize = 1024 * 1024;
	else MaxCacheSize = (((G->GetNodes() + G->GetEdges()) * sizeof(unsigned int)) * cache_percentage) / 100;
#endif

	struct timespec time_start, time_end;
	double time_elapsed;

	vector<unsigned int> EdgeList;

#if (PERFORM_BFS == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	for(unsigned int vid = 0; vid < G->GetNodes(); vid++)
	{
		SSDInstance.GetAdjListUseNvmeCommands(vid, EdgeList);
		EdgeList.clear();
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	SSDInstance.FinalizeGraphDataStructures();	
#if(useCache == 1)
	cout << "MaxCacheSize = " << MaxCacheSize << " numOfCacheHits = " <<  numOfCacheHits << " numOfCacheEvictions = " << numOfCacheEvictions << " numOfCacheAccesses = " << numOfCacheAccesses << " numOfCacheMisses = " <<  numOfCacheMisses << " TotRequests_SSD = " << TotRequests_SSD << " AvgNumOfRequestsPerAccess_SSD = " << (double) numOfCacheMisses / TotRequests_SSD  << " TotRequests_host = " << TotRequests_host << " TotNumOfRequestsPerAccess_host = " << (double)numOfCacheAccesses / TotRequests_host << endl;
#endif//useCache
#endif //PERFORM_BFS
return 0;
}
