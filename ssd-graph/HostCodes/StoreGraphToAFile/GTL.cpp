#include "graphFiltering.h"
#include "header.h"

extern map<unsigned int, unsigned int > vertexMapping;

#if(UseFTL == 3)
extern std::map<unsigned int, char *> Storage;
extern unsigned int currentGTTIndex_edge;
extern unsigned int GraphLPN;
extern struct GTTTable1 *gtt1;
#endif

#define SECTOR_SIZE 4096
#if(UseFTL == 3)
void SSDInterface::WriteGraphAndDelete()
{
	for(std::map<unsigned int, char *>::iterator it = Storage.begin(); it != Storage.end(); it++)
	{
		memcpy((void *)data_nvme_command, (void *)it->second, SSD_PAGE_SIZE);
		WriteToSSDN( it->first * (SSD_PAGE_SIZE / SECTOR_SIZE), (SSD_PAGE_SIZE / SECTOR_SIZE));
	}

	myFlashFile.seekp(ios_base::beg);
	unsigned int NumOfSSDPagesWritten = currentGTTIndex_edge + 1;
        myFlashFile.write((char *)(&NumOfSSDPagesWritten), sizeof(unsigned int));

	unsigned int *GTLArray = new unsigned int [2*currentGTTIndex_edge + 2];

	for(unsigned int i = 0; i <= currentGTTIndex_edge; i++)
	{
		GTLArray[2*i] = gtt1->GTTPointer1[i].vertexId;
		GTLArray[2*i+1] = gtt1->GTTPointer1[i].LPN;
	}
	myGTLFile.write((char *)(&currentGTTIndex_edge), sizeof(unsigned int));
	myGTLFile.write((char *)(&GraphLPN), sizeof(unsigned int));
	myGTLFile.write((char *)GTLArray, sizeof(unsigned int) * 2 * (currentGTTIndex_edge + 1));	
	free(GTLArray);
	

#if(UseCache_GetPagesFromSSD == 0)
	GraphFinalize();
#endif//UseCache_GetPagesFromSSD
	return;
}
#endif

void SSDInterface::WriteToSSDN(unsigned int SLBA, unsigned int numBlocks)
{
	myFlashFile.write((char *)data_nvme_command, numBlocks * SECTOR_SIZE);
//	if(SLBA == 200)
//	{
//		cout << ((unsigned int *)data_nvme_command)[SSD_PAGE_SIZE/4 - 1] << endl;
//	}
	if (myFlashFile.bad())//This executes i.e the file fails to write
	{
		cout << "numBlocks = " << numBlocks << " SECTOR_SIZE = " << SECTOR_SIZE << endl;
		cout << "Failed = " << myFlashFile.bad() << " " << myFlashFile.eof() << endl;
		exit(0);
	}
}

void SSDInterface::InitializeGraphDataStructures(unsigned int NumNodes)
{
#if(UseFTL == 3)
		GraphInitialize();
#endif
}

void SSDInterface::AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList)
{
	if(UseFTL == 3)
	{
		unsigned int num = EdgeList.size();
		if(EdgeList.size() * sizeof(unsigned int) > 16*4096)
			num = 16*4096 / (sizeof(unsigned int));
		memcpy((void *)data_nvme_command, EdgeList.data(), num*sizeof(unsigned int));
		putAdjEdgeList(vID,(char *)data_nvme_command, num);
#if(STORE_LARGE_GRAPH)
		if(Storage.size() > 1)
		{
			for(std::map<unsigned int, char *>::iterator it = Storage.begin(); it != Storage.end(); it++)
			{
				if((it->first != GraphLPN))//Need to extend it to GraphLPNWeight if adding edges
				{
					memcpy((void *)data_nvme_command, (void *)it->second, SSD_PAGE_SIZE);
					WriteToSSDN( it->first * (SSD_PAGE_SIZE / SECTOR_SIZE), (SSD_PAGE_SIZE / SECTOR_SIZE));
					free(it->second);
					Storage.erase(it);
				}
			}			
		}
#endif
		return;
	}
}

void SSDInterface::AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList)
{
	assert(vID < vertexMapping.size());
	vertexToAdjListMap[vID];
	vertexToAdjListMap[vID] = EdgeList;
}
