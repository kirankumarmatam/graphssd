#include "header.h"
#include "graphFiltering.h"

unsigned int GraphLPN;
unsigned int GraphLPNWeight;
char *Test_variable_size_buffer;
unsigned int numOfFreeElementsAvailableInTheBuffer;
unsigned int numOfFreeElementsAvailableInTheBufferWeight;
unsigned int bufferPageTopPointer;
unsigned int bufferPageTopPointerWeight;
unsigned int currentGTTIndex_edge;
unsigned int currentGTTIndex_weight;
struct GTLTable1 *gtlTable1;
struct CompletionQueues *gtlCompletionQueues;
struct CompletionQueuePtrs *gtlCompletionQueuePtrs;

struct GTTTable1 *gtt1;
struct GTTTable1Weight *gtt1_weight;
struct VertexCompletionInformation vertexCompletionInformation;

#if(useCache == 1)
	unsigned int TotRequests_host = 0, TotRequests_SSD = 0;
#endif

unsigned int SSDInterface::putAdjEdgeList(unsigned int vID, char *host_buffer, unsigned int getNumAdjVertices)
{
	vertexCompletionInformation.vertexId = vID;
	bufferingPage_loadToSSD(host_buffer, getNumAdjVertices);
	vertexCompletionInformation.flags = 2;
	vertexCompletionInformation.host_addr = host_buffer;
	vertexCompletionInformation.isEdge = 0;
	vertexCompletionInformation.NumOfAdjVertices = getNumAdjVertices;
	vertexCompletionInformation.numberOfVerticesRemainingToFetch = getNumAdjVertices;
	bufferingPage_loadToNAND(getNumAdjVertices, 1);
	return 0;
}

unsigned int SSDInterface::bufferingPage_loadToSSD(char *host_buffer, unsigned int getNumAdjVertices)
{
	if(getNumAdjVertices == 0) return 0;
	assert(getNumAdjVertices <= (16*4096 / sizeof(unsigned int)));
	unsigned int numItems = 2*getNumAdjVertices;
	memcpy((void *)Test_variable_size_buffer, (void *)host_buffer, numItems*sizeof(unsigned int));
	return 0;
}

unsigned int SSDInterface::bufferingPage_loadToNAND(unsigned int getNumAdjVertices, unsigned int isEdges)
{
	unsigned int valuesRemainingToAdd = getNumAdjVertices;
	unsigned int numOfItemsToCopy;
	do {
//		printf("b_l = %d\n", valuesRemainingToAdd);
		if (isEdges == 1) {
			if ((valuesRemainingToAdd + 3) > numOfFreeElementsAvailableInTheBuffer)
			{
				GraphLPN++;
				numOfFreeElementsAvailableInTheBuffer = (SSD_PAGE_SIZE / (sizeof(unsigned int))) - 1;
				bufferPageTopPointer = 0;
				currentGTTIndex_edge += 1;
				gtt1->GTTPointer1[currentGTTIndex_edge].vertexId = vertexCompletionInformation.vertexId;
				gtt1->GTTPointer1[currentGTTIndex_edge].LPN = GraphLPN;
			}
		} else {
			if ((valuesRemainingToAdd + 3) > numOfFreeElementsAvailableInTheBufferWeight) {
				GraphLPNWeight++;
				numOfFreeElementsAvailableInTheBufferWeight = (SSD_PAGE_SIZE / (sizeof(unsigned int))) - 1;
				bufferPageTopPointerWeight = 0;
				currentGTTIndex_weight += 1;
				gtt1_weight->GTTPointer1[currentGTTIndex_weight].vertexId = vertexCompletionInformation.vertexId;
				gtt1_weight->GTTPointer1[currentGTTIndex_weight].LPN = GraphLPNWeight;
			}
		}

		if (isEdges == 1) {
			numOfItemsToCopy = (valuesRemainingToAdd + 3) < numOfFreeElementsAvailableInTheBuffer ?	valuesRemainingToAdd : numOfFreeElementsAvailableInTheBuffer - 3;
		} else {
			numOfItemsToCopy = (valuesRemainingToAdd + 3) < numOfFreeElementsAvailableInTheBufferWeight ?	valuesRemainingToAdd :	numOfFreeElementsAvailableInTheBufferWeight - 3;
		}
		if (isEdges == 1) {
			GraphLRUBufWrite(GraphLPN, numOfItemsToCopy * sizeof(unsigned int),	bufferPageTopPointer * sizeof(unsigned int), (getNumAdjVertices - valuesRemainingToAdd) * sizeof(unsigned int), gtt1->GTTPointer1[currentGTTIndex_edge].vertexId);
		} else {
			GraphLRUBufWrite(GraphLPNWeight, numOfItemsToCopy * sizeof(unsigned int), bufferPageTopPointerWeight * sizeof(unsigned int), (2 * getNumAdjVertices - valuesRemainingToAdd) * sizeof(unsigned int), gtt1_weight->GTTPointer1[currentGTTIndex_weight].vertexId);
		}

		valuesRemainingToAdd = valuesRemainingToAdd - numOfItemsToCopy;
		if (isEdges == 1) {
			bufferPageTopPointer += numOfItemsToCopy;
			numOfFreeElementsAvailableInTheBuffer = numOfFreeElementsAvailableInTheBuffer - numOfItemsToCopy - 3;
		} else {
			bufferPageTopPointerWeight += numOfItemsToCopy;
			numOfFreeElementsAvailableInTheBufferWeight = numOfFreeElementsAvailableInTheBufferWeight - numOfItemsToCopy - 3;
		}
	} while (valuesRemainingToAdd > 0);
	return 0;
}

unsigned int SSDInterface::GraphInitialize()
{
	Test_variable_size_buffer = new char [1024*1024];
	gtt1 = new GTTTable1;
	gtt1_weight = new GTTTable1Weight;
	gtlCompletionQueues = new CompletionQueues;
	gtlCompletionQueuePtrs = new CompletionQueuePtrs;
	GraphLPN = 50-1;
	GraphLPNWeight = 786407-1;
	numOfFreeElementsAvailableInTheBuffer = 0;
	bufferPageTopPointer = 0;
	numOfFreeElementsAvailableInTheBufferWeight = 0;
	bufferPageTopPointerWeight = 0;
	currentGTTIndex_edge = -1;
	currentGTTIndex_weight = -1;
	int i,j;
	for(i=0; i < MAX_CHANNEL_NUM; i++)
	{
		for(j=0; j < MAX_WAY_NUM; j++)
		{
			(gtlCompletionQueuePtrs->completionQueuePointer[i][j]).front = 0;
			(gtlCompletionQueuePtrs->completionQueuePointer[i][j]).rear = 0;
		}
	}
	vertexCompletionInformation.flags = 0;
	vertexCompletionInformation.vertexId = -1;
	return 0;
}
extern std::map<unsigned int, char *> Storage;
unsigned int SSDInterface::GraphFinalize()
{
	free(Test_variable_size_buffer);
	free(gtt1);
	free(gtt1_weight);
	free(gtlCompletionQueues);
	free(gtlCompletionQueuePtrs);
	for(std::map<unsigned int, char *>::iterator it = Storage.begin(); it != Storage.end(); it++)	
	{
		free(it->second);
	}
	Storage.clear();
	return 0;
}
