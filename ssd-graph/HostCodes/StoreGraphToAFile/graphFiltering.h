#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <algorithm>

#include <time.h>

using namespace std;

#define UseFTL 3
#define useCache 1
#define SLBA_FLUSH_BUFFER 5145631
#define STORE_LARGE_GRAPH 1
#define CHECK_ERROR 0
#define UseCache_GetPagesFromSSD 1
#define STORE_UPDATE_EDGES 0
#define IsUndirectedGraph 0
#define SortAndMap 1

class SSDInterface
{	
	public:
		ofstream myFlashFile;
		fstream myGTLFile;

		void InitializeGraphDataStructures(unsigned int NumNodes);
		void FinalizeGraphDataStructures();
		void WriteGraphAndDelete();
		int store_graph(int argc, char **argv);
		void ClearQueues();
		void cache_readAdjEdgeList(unsigned int vID, vector<unsigned int> &EdgeList);
		void GetEdgeWeightNvmeCommands(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight);
		void AddVertexWeightUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights);
		void GetAdjListUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList);
		void AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList);

		void AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList);
		void GetAdjListNoGTL(unsigned int vID, vector<unsigned int> &EdgeList);
		void WriteToSSD(unsigned int j);
		void WriteToSSDN(unsigned int SLBA, unsigned int numBlocks);
		void AddVertexWeight(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights)
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				AddVertexWeightUseNvmeCommands(vID, EdgeList, EdgeWeights);
			}
		}
		void GetEdgeWeight(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight)
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				GetEdgeWeightNvmeCommands(srcVId, destVId, edgeWeight);
			}
		}

                map<unsigned int, vector<unsigned int> > vertexToAdjListMap;
		void *data_nvme_command;
		int fd;
		unsigned int PAGE_SIZE;

		SSDInterface()
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				PAGE_SIZE = 4096;
				if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
					fprintf(stderr, "cannot allocate io payload for data_wr\n");
				}
			}
		}

		unsigned int putAdjEdgeList(unsigned int vID, char *host_buffer, unsigned int numAdjVertices); 
		unsigned int getAdjEdgeList(unsigned int vID, char *host_buffer); 
		unsigned int getEdgeWeight(unsigned int vID, char *host_buffer, unsigned int destEdge); 
		unsigned int getTwoHopAdjEdgeList(unsigned int vID, char *host_buffer); 
		unsigned int bufferingPage_loadToSSD(char *host_buffer, unsigned int numOfElementsToLoad); 
		unsigned int bufferingPage_loadToNAND(unsigned int getNumAdjVertices, unsigned int isEdge); 
		unsigned int putAdjEdgeList_weight(unsigned int vID, char *host_buffer, unsigned int numAdjVertices); 
		unsigned int GraphInitialize(); 
		unsigned int getEdgesToSSDDram_initiateLoading(unsigned int gttVidIndex, unsigned int gttVidNumber, unsigned int isEdge); 
		unsigned int binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number); 
		unsigned int CopyFunction_InPage(char * devAddr, char * Test_variable_size_buffer, unsigned int currentTopPointer, unsigned int vertexId); 
		unsigned int findEdge_inBuffer(char * Test_variable_size_buffer, char * Test_variable_size_buffer_weight, unsigned int destEdge, unsigned int NumberOfAdjVertices); 
		void process_completionRequest(unsigned int chNo, unsigned int wayNo, unsigned int front, unsigned int isLruBufHit); 
		void process_completionRequest_write(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit); 
		void GraphLRUBufWrite(unsigned int curSect, unsigned int Len, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int lowestVertexId); 
#if(useCache == 1 && UseCache_GetPagesFromSSD == 1)
		bool GraphLRUBufRead(unsigned int sectorNumber);
#else
		void GraphLRUBufRead(unsigned int sectorNumber);
#endif
		unsigned int GraphFinalize(); 
		bool cache_readAdjEdgeList_page(unsigned int LPN, unsigned int Number, char **data_pointer);
};
