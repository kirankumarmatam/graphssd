#include "graphFiltering.h"
#include "header.h"
#define VERBOSE 0
#define FLUSH_SSD_BUFFER 1
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192

map<unsigned int, unsigned int> vertexMapping;

#if(IsUndirectedGraph == 1)
PUNGraph G1;
PNGraph G;
#elif(IsUndirectedGraph == 0)
PNGraph G;
#endif//IsUndirectedGraph

#if(STORE_UPDATE_EDGES == 1)
    bool myComparator(std::pair<unsigned int, unsigned int> comparisionObject1, std::pair<unsigned int, unsigned int> comparisionObject2)
    {
        return (comparisionObject1.first < comparisionObject2.first);
    }
#endif //STORE_UPDATE_EDGES

int SSDInterface::store_graph(int argc, char **argv)
{
	if(argc < 5)
	{
		cout << "Usage is ./a.out /dev/nvme0n1 inputDataSet.txt outputFile_flash.txt outputFile_gtl.txt" << endl;
		return 0;
	}
#if(IsUndirectedGraph == 1)
    G1 = TSnap::LoadEdgeList<PUNGraph>(argv[2], 0, 1);
    G = TSnap::ConvertGraph<PNGraph>(G1);
#elif(IsUndirectedGraph == 0)
                            G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
#endif//IsUndirectedGraph
#if(SortAndMap == 1)
    std::vector<unsigned int> SortVector;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
        SortVector.push_back(NI.GetId());
	}
    std::sort(SortVector.begin(), SortVector.end());
    unsigned int nodeIndex = 0;
    for(unsigned int i = 0; i < SortVector.size(); i++)
    {
		vertexMapping[SortVector[i]] = nodeIndex++;
    }
	assert(vertexMapping.size() == nodeIndex);
#elif(SortAndMap == 0)
    unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);
#endif//SortAndMap

#if(SortAndMap == 1)
    FILE *myMappingFile = fopen(argv[6], "w");   
    for(unsigned int i = 0; i < SortVector.size(); i++)
    {
        fprintf(myMappingFile, "%u %u\n", SortVector[i], vertexMapping[SortVector[i]]);
    }
    fclose(myMappingFile);
#elif(SortAndMap == 0)
    FILE *myMappingFile = fopen(argv[6], "w");   
    unsigned int mappingNum = 0;
    for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
    {
        fprintf(myMappingFile, "%u %u\n", NI.GetId(), vertexMapping[NI.GetId()]);
    }
    fclose(myMappingFile);
#endif//SortAndMap

	InitializeGraphDataStructures(G->GetNodes());

	myFlashFile.open(argv[3], fstream::in | fstream::out | fstream::trunc);
	if (!myFlashFile.is_open())
		cout << " Cannot open flash output file!" << endl;

	myFlashFile.write((char *)(&argc), sizeof(unsigned int));
	if (myFlashFile.fail())
	{
		cout <<"In store_graph" << endl;
		cout << "Failed = " << myFlashFile.fail() << endl;
		exit(0);
	}

	cout <<"GetNodes " << G->GetNodes() << endl;
	vector<unsigned int> EdgeList;

#if(SortAndMap == 1)
    for (unsigned int i = 0; i < SortVector.size(); i++)
  	{
        TNGraph::TNodeI NI = G->GetNI(SortVector[i]);
#if(VERBOSE == 1)
		cout << "W " << vertexMapping[NI.GetId()] << endl;
#endif
        if(NI.GetId() % 10000000 == 0)
            cout << "Printed 10M nodes into flash file" << endl;
//		for (int e = 0; e < NI.GetOutDeg(); e++)
		for (int e = 0; e < NI.GetOutDeg() && e < (16*4096 / sizeof(unsigned int)); e++)
		{
			assert(vertexMapping.find(NI.GetOutNId(e)) != vertexMapping.end());
			EdgeList.push_back(vertexMapping[NI.GetOutNId(e)]);
		}
        std::sort(EdgeList.begin(), EdgeList.end());
		AddVertexUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
		EdgeList.clear();
	}
    SortVector.clear();
#elif(SortAndMap == 0)
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
#if(VERBOSE == 1)
		cout << "W " << vertexMapping[NI.GetId()] << endl;
#endif
        if(NI.GetId() % 10000000 == 0)
            cout << "Printed 10M nodes into flash file" << endl;
//		for (int e = 0; e < NI.GetOutDeg(); e++)
		for (int e = 0; e < NI.GetOutDeg() && e < (16*4096 / sizeof(unsigned int)); e++)
		{
			assert(vertexMapping.find(NI.GetOutNId(e)) != vertexMapping.end());
			EdgeList.push_back(vertexMapping[NI.GetOutNId(e)]);
		}
        std::sort(EdgeList.begin(), EdgeList.end());
		AddVertexUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
		EdgeList.clear();
	}
#endif

    cout << " Almost finished writing flash to file" << endl;
	//Open file here, write that file here
	myGTLFile.open(argv[4], fstream::in | fstream::out | fstream::trunc);
	unsigned int NumNodes = G->GetNodes(), NumEdges = G->GetEdges();
	myGTLFile.write((char *)(&NumNodes), sizeof(unsigned int));
	myGTLFile.write((char *)(&NumEdges), sizeof(unsigned int));
    cout << "Wrote node and edge info to gtl file" << endl;
	if (!myGTLFile.is_open())
		cout << " Cannot open flash output file!" << endl;

#if(STORE_UPDATE_EDGES == 1)
    std::vector< std::pair<unsigned int, unsigned int> > randomEdges;
    unsigned int i=0;
    unsigned int numOfRandomEdges = NumEdges / 10;
    while(i < numOfRandomEdges)//also need to make sure that the graph is not fully connected
    {
        unsigned int srcNode = rand() % NumNodes;
        unsigned int destNode = rand() % NumNodes;
        if(G->IsNode(srcNode) && G->IsNode(destNode))
        {
            if(!G->IsEdge(srcNode, destNode))
            {
                randomEdges.push_back(std::make_pair(srcNode, destNode));
                i++;
            }
        }
    }
    sort(randomEdges.begin(), randomEdges.end(), myComparator);
    FILE *myUpdateFile;
    myUpdateFile = fopen(argv[5], "w");
    if (myUpdateFile != NULL)
        cout << " Cannot open update output file!" << endl;
    for(std::vector<std::pair<unsigned int, unsigned int> >::iterator it = randomEdges.begin(); it != randomEdges.end(); it++)
    {
        fprintf(myUpdateFile, "%d %d\n", it->first, it->second);
    }
    fclose(myUpdateFile);
#endif //STORE_UPDATE_EDGES

	WriteGraphAndDelete();
    cout << "Finished writing to flash and gtl file" << endl;
	
	myGTLFile.close();
        myFlashFile.close();

	return 0;
}
