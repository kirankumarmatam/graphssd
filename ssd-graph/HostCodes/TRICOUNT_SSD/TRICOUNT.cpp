#include "../Baseline_library/graphFiltering.h"
#include <limits>
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 0
#define FLUSH_SSD_BUFFER 1
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192

extern unsigned int numOfNodes;
extern unsigned int numOfEdges;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
struct timespec time_start_overhead, time_end_overhead;
double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

int main(int argc, char **argv)
{
	set_context(argc, argv);

	vector<unsigned int> EdgeList,NextEdgeList,EdgeListLow,NextEdgeListLow,CommonList;
    unsigned int TotalCount=0;
    struct timespec time_start, time_end;
    double time_elapsed;
#if(MEASURE_IO_TIME == 1)
    struct timespec time_start_io, time_end_io;
    double time_elapsed_io_iter;
    double time_elapsed_io = 0;
#endif // MEASURE_IO_TIME == 1
    clock_gettime(CLOCK_MONOTONIC, &time_start);

    for(int numOfrun = 0; numOfrun < 1; numOfrun++)
    {

#if(STOP_SIMULATION == 1)
        unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1

        for(unsigned int i =0; i < numOfNodes; i++)
        {
		if(i >= 7000000)
			break;
		if(i % 100000 == 0)
			cout << "i = " << i << endl;
		EdgeList.clear();
#if (MEASURE_IO_TIME == 1)
			clock_gettime(CLOCK_MONOTONIC, &time_start_io);
			GetAdjList(i, EdgeList);
			clock_gettime(CLOCK_MONOTONIC, &time_end_io);
			time_elapsed_io_iter = ((double)time_end_io.tv_sec - (double)time_start_io.tv_sec);
			time_elapsed_io_iter += ((double)time_end_io.tv_nsec - (double)time_start_io.tv_nsec) / 1000000000.0;
			time_elapsed_io += time_elapsed_io_iter;
#endif // MEASURE_IO_TIME == 1
            EdgeListLow.clear();
            for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
            {
                if (*it<i)
                {
                    EdgeListLow.push_back(*it);
                }
            }
//            sort(EdgeListLow.begin(), EdgeListLow.end());
            for (vector<unsigned int>::iterator it = EdgeListLow.begin() ; it != EdgeListLow.end(); it++)
            {
		    NextEdgeList.clear();
                GetAdjList(*it, NextEdgeList);
                NextEdgeListLow.clear();
                for (vector<unsigned int>::iterator ir = NextEdgeList.begin() ; ir != NextEdgeList.end(); ir++)
                {
                    if (*ir<*it)
                    {
                        NextEdgeListLow.push_back(*ir);
                    }
                }
//                sort(NextEdgeListLow.begin(), NextEdgeListLow.end());
                CommonList.clear();
                set_intersection(EdgeListLow.begin(),EdgeListLow.end(),NextEdgeListLow.begin(),NextEdgeListLow.end(),back_inserter(CommonList));
                TotalCount+=CommonList.size();
            }
#if(STOP_SIMULATION == 1)
            number_requests_executed++;
//			cout << number_requests_executed << endl;
            if(number_requests_executed == NUMBER_OF_REQUESTS)
                break;
#endif//STOP_SIMULATION == 1

        }
        cout << TotalCount << endl;
        TotalCount=0;
    }
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
    time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#if(MEASURE_IO_TIME == 1)
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD_IO: %lf\n", time_elapsed_io);
#endif // MEASURE_IO_TIME == 1
   FinalizeGraphDataStructures();
#if(useCache == 1)
    printTime();
#endif


//perror:
//	perror(perrstr);
//	return 1;
}
