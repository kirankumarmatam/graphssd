#include "../Baseline_library/graphFiltering.h"
#include <limits>
#define PAGE_PER_CMD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 0
#define FLUSH_SSD_BUFFER 1
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define STORE_LARGE_SCALE 0

extern unsigned int numOfNodes;
extern unsigned int numOfEdges;
extern unsigned int fd;
extern void *data_nvme_command;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
struct timespec time_start_overhead, time_end_overhead;
double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotNumOfRequestsPerAccess_host, TotNumOfRequestsPerAccess_SSD;
extern unsigned int MaxCacheSize;
extern unsigned int cache_percentage;
#endif

int main(int argc, char **argv)
{
    if(argc < 4)
    {
#if(SORE_LARGE_SCALE == 1)
        cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt output_flash.txt" << endl;
#else
        cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt number_of_requests" << endl;
#endif//STORE_LARGE_SCALE
        return 0;
    }
#if(STOP_SIMULATION == 1)
    NUMBER_OF_REQUESTS = atoi(argv[3]);
#endif//STOP_SIMULATION == 1

#if(STORE_LARGE_SCALE == 0)
    PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
    numOfNodes = G->GetNodes();
    numOfEdges = G->GetEdges();
#else
    fstream myFlashFile;
    myFlashFile.open(argv[3]);
    myFlashFile.read((char *)(&numOfNodes), sizeof(unsigned int));
    myFlashFile.read((char *)(&numOfEdges), sizeof(unsigned int));
    cout << "numOfNodes = " << numOfNodes << " numOfEdges = " << numOfEdges << endl;
    myFlashFile.close();
#endif//STORE_LARGE_SCALE == 1

#if(useCache==1)
    if(((numOfNodes + numOfEdges) * sizeof(unsigned int)) < (1024*1024))
        MaxCacheSize = 1024 * 1024;
    else MaxCacheSize = (((numOfNodes + numOfEdges) * sizeof(unsigned int)) * cache_percentage) / 100;
#endif

//	static const char *perrstr;
//	int err, fd;
    fd = open(argv[1], O_RDWR);
    if (fd < 0)
        cout << "Error opening file!!" << endl;
    if ( posix_memalign(&data_nvme_command, 4096, 32*4096) )
    {
        fprintf(stderr, "cannot allocate io payload for data_wr\n");
        return 0;
    }
#if(STORE_LARGE_SCALE == 1)
    InitializeGraphDataStructures(numOfNodes);
#else
    InitializeGraphDataStructures(G->GetNodes());
#endif//STORE_LARGE_SCALE
#if (FLUSH_SSD_BUFFER == 1)
    int j;
    for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
    {
        WriteToSSD(j);
    }
#endif //FLUSH_SSD_BUFFER == 1

    vector<unsigned int> EdgeList,NextEdgeList,EdgeListLow,NextEdgeListLow,CommonList;
    unsigned int TotalCount=0;
    struct timespec time_start, time_end;
    double time_elapsed;
#if(MEASURE_IO_TIME == 1)
    struct timespec time_start_io, time_end_io;
    double time_elapsed_io_iter;
    double time_elapsed_io = 0;
#endif // MEASURE_IO_TIME == 1
    clock_gettime(CLOCK_MONOTONIC, &time_start);
    list<unsigned int> updatedQueue;

    for(int numOfrun = 0; numOfrun < 1; numOfrun++)
    {
//		unsigned int root = rand() % G->GetNodes();
//		unsigned int root = 0;

        for(unsigned int i =0; i < numOfNodes; i++)
        {
#if (MEASURE_IO_TIME == 1)
			clock_gettime(CLOCK_MONOTONIC, &time_start_io);
			GetAdjList(topElement, EdgeList);
			clock_gettime(CLOCK_MONOTONIC, &time_end_io);
			time_elapsed_io_iter = ((double)time_end_io.tv_sec - (double)time_start_io.tv_sec);
			time_elapsed_io_iter += ((double)time_end_io.tv_nsec - (double)time_start_io.tv_nsec) / 1000000000.0;
			time_elapsed_io += time_elapsed_io_iter;
#endif // MEASURE_IO_TIME == 1
            GetAdjList(i, EdgeList);
            EdgeListLow.clear();
            for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
            {
                if (*it<i)
                {
                    EdgeListLow.push_back(*it);
                }
            }
            sort(EdgeListLow.begin(), EdgeListLow.end());
            for (it = EdgeListLow.begin() ; it != EdgeListLow.end(); it++)
            {
                GetAdjList(*it, NextEdgeList);
                NextEdgeListLow.clear();
                for (vector<unsigned int>::iterator ir = EdgeList.begin() ; ir != EdgeList.end(); ir++)
                {
                    if (*ir<*it)
                    {
                        NextEdgeListLow.push_back(*ir);
                    }
                }
                sort(NextEdgeListLow.begin(), NextEdgeListLow.end());
                CommonList.clear();
                set_intersection(EdgeListLow.begin(),EdgeListLow.end(),NextEdgeListLow.begin(),NextEdgeListLow.end(),back_inserter(CommonList));
                TotalCount+=CommonList.size();
            }
        }
    }
#if(STOP_SIMULATION == 1)
        unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1

        while(!updatedQueue.empty())
        {
            unsigned int topElement = updatedQueue.front();
#if (VERBOSE == 1)
            cout << topElement << ",";
#endif
            updatedQueue.pop_front();
#if(AuxMemoryInStorage == 0)
            CurrentDistance=distance[topElement];
#else
            AuxDataValueType distance_value;
            AccessAuxMemory(topElement+numOfNodes, distance_value, 1);
            CurrentDistance=distance_value;
#endif
#if (MEASURE_IO_TIME == 1)
            clock_gettime(CLOCK_MONOTONIC, &time_start_io);
            GetAdjList(topElement, EdgeList);
            clock_gettime(CLOCK_MONOTONIC, &time_end_io);
            time_elapsed_io_iter = ((double)time_end_io.tv_sec - (double)time_start_io.tv_sec);
            time_elapsed_io_iter += ((double)time_end_io.tv_nsec - (double)time_start_io.tv_nsec) / 1000000000.0;
            time_elapsed_io += time_elapsed_io_iter;
#endif // MEASURE_IO_TIME == 1
#if (VERBOSE_PRINT_ADJLIST == 1)
            cout << topElement << " :";
            for(unsigned int i=0; i < EdgeList.size(); i++)
            {
                cout << " " << EdgeList[i];
            }
            cout << endl;
#endif //VERBOSE_PRINT_ADJLIST

            for(unsigned int i=0; i < EdgeList.size(); i++)
            {
#if(STORE_LARGE_SCALE == 1)
                assert(EdgeList[i] < numOfNodes);
#else
                assert(EdgeList[i] < vertexMapping.size());
#endif//STORE_LARGE_SCALE
#if(AuxMemoryInStorage == 0)
                if (CurrentDistance+EdgeWeight<distance[EdgeList[i]])
                {
                    distance[EdgeList[i]]=CurrentDistance+EdgeWeight;
                    if (updated[EdgeList[i]]==0)
                    {
                        updatedQueue.push_back(EdgeList[i]);
                        updated[EdgeList[i]] = 1;
                    }
                }
#else
                //GetEdgeWeight(topElement,EdgeList[i],EdgeWeight);
                EdgeWeight=1;
                AuxDataValueType distance_value;
                AccessAuxMemory(EdgeList[i]+numOfNodes, distance_value, 1);
                if (CurrentDistance+EdgeWeight<distance_value)
                {
                    distance_value=CurrentDistance+EdgeWeight;
                    AccessAuxMemory(EdgeList[i]+numOfNodes, distance_value, 0);
                    AuxDataValueType update_value;
                    AccessAuxMemory(EdgeList[i], update_value, 1);
                    if (update_value==0)
                    {
                        updatedQueue.push_back(EdgeList[i]);
                        update_value = 1;
                        AccessAuxMemory(EdgeList[i], update_value, 0);
                    }
                }
#endif // AuxMemoryInStorage

            }
            for(unsigned int i=0; i < EdgeList.size(); i++)
            {
#if(AuxMemoryInStorage == 0)
                updated[EdgeList[i]] = 0;
#else
                AuxDataValueType update_value=0;
                AccessAuxMemory(EdgeList[i], update_value, 0);
#endif
            }
            EdgeList.clear();
#if(STOP_SIMULATION == 1)
            number_requests_executed++;
//			cout << number_requests_executed << endl;
            if(number_requests_executed == NUMBER_OF_REQUESTS)
                break;
#endif//STOP_SIMULATION == 1

        }
        updatedQueue.clear();
#if(AuxMemoryInStorage == 0)
        free(updated);
        free(distance);
#endif
    }
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
    time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#if(MEASURE_IO_TIME == 1)
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD_IO: %lf\n", time_elapsed_io);
#endif // MEASURE_IO_TIME == 1
#if(useCache == 1)
    cout << "MaxCacheSize = " << MaxCacheSize << " numOfCacheHits = " <<  numOfCacheHits << " numOfCacheEvictions = " << numOfCacheEvictions << " numOfCacheAccesses = " << numOfCacheAccesses << " numOfCacheMisses = " <<  numOfCacheMisses << " AvgNumOfRequestsPerAccess_SSD = " << (double)TotNumOfRequestsPerAccess_SSD / numOfCacheMisses  << " TotNumOfRequestsPerAccess_host = " << (double)TotNumOfRequestsPerAccess_host / numOfCacheAccesses << endl;
#endif
    FinalizeGraphDataStructures();


//perror:
//	perror(perrstr);
//	return 1;
}
