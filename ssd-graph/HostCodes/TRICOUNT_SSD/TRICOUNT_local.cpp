#include "../Baseline_library/header.h"
#include <limits>

using namespace std;

unsigned int numOfNodes;
unsigned int numOfEdges;

int main(int argc, char **argv)
{

    PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[1], 0, 1);
    numOfNodes = G->GetNodes();
    numOfEdges = G->GetEdges();

    vector<unsigned int> EdgeList,NextEdgeList,EdgeListLow,NextEdgeListLow,CommonList;
    unsigned int TotalCount=0;
    list<unsigned int> updatedQueue;
    for(int numOfrun = 0; numOfrun < 1; numOfrun++)
    {
//		unsigned int root = rand() % G->GetNodes(), reqElement = rand() % G->GetNodes();
//		unsigned int root = 0, reqElement = 100000;
        for(unsigned int i =0; i < numOfNodes; i++)
        {
            //GetAdjList(i, EdgeList);
            TNGraph::TNodeI NI = G->GetNI(i);
            EdgeList.clear();
            for (int e = 0; e < NI.GetOutDeg(); e++)
            {
                EdgeList.push_back(NI.GetOutNId(e));
            }
            EdgeListLow.clear();
            for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
            {
                if (*it<i)
                {
                    EdgeListLow.push_back(*it);
                }
            }
            sort(EdgeListLow.begin(), EdgeListLow.end());
            for (vector<unsigned int>::iterator it = EdgeListLow.begin() ; it != EdgeListLow.end(); it++)
            {
                //GetAdjList(*it, NextEdgeList);
                TNGraph::TNodeI NI = G->GetNI(*it);
                NextEdgeList.clear();
                for (int e = 0; e < NI.GetOutDeg(); e++)
                {
                    NextEdgeList.push_back(NI.GetOutNId(e));
                }
                NextEdgeListLow.clear();
                for (vector<unsigned int>::iterator ir = NextEdgeList.begin() ; ir != NextEdgeList.end(); ir++)
                {
                    if (*ir<*it)
                    {
                        NextEdgeListLow.push_back(*ir);
                    }
                }
                sort(NextEdgeListLow.begin(), NextEdgeListLow.end());
                CommonList.clear();
                set_intersection(EdgeListLow.begin(),EdgeListLow.end(),NextEdgeListLow.begin(),NextEdgeListLow.end(),back_inserter(CommonList));
                TotalCount+=CommonList.size();
            }

        }
        cout << TotalCount << endl;
        TotalCount=0;
    }

    return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
