#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;

int main(int argc, char **argv)
{
    SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
    SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

#if(LOAD_GRAPH_FROM_FILE == 0)
    NumNodes = G->GetNodes(), NumEdges = G->GetEdges();
#endif
    cout << NumNodes << " " << NumEdges << endl;

    struct timespec time_start, time_end;
    double time_elapsed;

    vector<unsigned int> EdgeList,NextEdgeList,EdgeListLow,NextEdgeListLow,CommonList;
    unsigned int TotalCount=0;

    unsigned int total_number_of_requests = 1;
//	srand(time(NULL));
//	unsigned int root = 0, reqElement = 100000;
//	cout << root << " " << reqElement << endl;
    clock_gettime(CLOCK_MONOTONIC, &time_start);
//	unsigned int root = 0, reqElement = 2;
    list<unsigned int> updatedQueue;
    for(int numOfrun = 0; numOfrun < total_number_of_requests; numOfrun++)
    {
//		cout << numOfrun << endl;
//	unsigned int root = rand() % G->GetNodes(), reqElement = rand() % G->GetNodes();

        unsigned int temp_debugging = 0;
#if(STOP_SIMULATION == 1)
        unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1

        for(unsigned int i =0; i < NumNodes; i++)
        {
		if(i >= 7000000)
			break;
		if(i % 100000 == 0)
			cout << "i = " << i << endl;
		EdgeList.clear();
            SSDInstance.GetAdjListUseNvmeCommands(i, EdgeList);
            EdgeListLow.clear();
            for (vector<unsigned int>::iterator it = EdgeList.begin() ; it != EdgeList.end(); it++)
            {
                if (*it<i)
                {
                    EdgeListLow.push_back(*it);
                }
            }
//            sort(EdgeListLow.begin(), EdgeListLow.end());
            for (vector<unsigned int>::iterator it = EdgeListLow.begin() ; it != EdgeListLow.end(); it++)
            {
		    NextEdgeList.clear();
                SSDInstance.GetAdjListUseNvmeCommands(*it, NextEdgeList);
                NextEdgeListLow.clear();
                for (vector<unsigned int>::iterator ir = NextEdgeList.begin() ; ir != NextEdgeList.end(); ir++)
                {
                    if (*ir<*it)
                    {
                        NextEdgeListLow.push_back(*ir);
                    }
                }
//                sort(NextEdgeListLow.begin(), NextEdgeListLow.end());
                CommonList.clear();
                set_intersection(EdgeListLow.begin(),EdgeListLow.end(),NextEdgeListLow.begin(),NextEdgeListLow.end(),back_inserter(CommonList));
                TotalCount+=CommonList.size();
            }
#if(STOP_SIMULATION == 1)
            number_requests_executed++;
            //		cout << number_requests_executed << endl;
            if(number_requests_executed == NUMBER_OF_REQUESTS)
                break;
#endif//STOP_SIMULATION == 1
        }
        cout << TotalCount << endl;
        TotalCount=0;
    }
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
    time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
    SSDInstance.FinalizeGraphDataStructures();
#if(useCache == 1)
    SSDInstance.printTime();
#endif//useCache
    return 0;
}
