#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_BFS 1
#define VERBOSE_BFS 0
#define VERBOSE_PRINT_ADJLIST 1
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1
#define PRINT_LEVEL_NUMBER 0
#define READ_REQUESTS_FROM_FILE 0 // This is incomplete
#define READ_REQUEST_AS_ARGUMENT 1

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;
extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	cout << NumNodes << " " << NumEdges << endl;

	vector<unsigned int> EdgeList;

	for(unsigned int topElement=0; topElement < 6; topElement++)
	{
		SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
#if (VERBOSE_PRINT_ADJLIST == 1)
		cout << topElement << " :";
		for(unsigned int i=0; i < EdgeList.size(); i++)
		{
			cout << " " << EdgeList[i];
		}
		cout << endl;
#endif //VERBOSE_PRINT_ADJLIST
		EdgeList.clear();
	}

#if(ADD_UPDATES_TO_BUFFER)
	SSDInstance.AddToBuffer(argv[7]);
#endif // ADD_UPDATES_TO_BUFFER

	for(unsigned int topElement=0; topElement < 6; topElement++)
	{
		SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
#if (VERBOSE_PRINT_ADJLIST == 1)
		cout << topElement << " :";
		for(unsigned int i=0; i < EdgeList.size(); i++)
		{
			cout << " " << EdgeList[i];
		}
		cout << endl;
#endif //VERBOSE_PRINT_ADJLIST
		EdgeList.clear();
	}

	SSDInstance.FinalizeGraphDataStructures();

	return 0;
}
