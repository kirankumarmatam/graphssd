#include "graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_BFS 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 1
#define STOP_SIMULATION 1
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1

int main(int argc, char **argv)
{
	struct timespec time_start, time_end;
	double time_elapsed;

	SSDInterface SSDInstance;
#if (UseFTL == 2)
	SSDInstance.fd = open(argv[1], O_RDWR);
	if (SSDInstance.fd < 0)
		cout << "Error opening file!!" << endl;
#endif

	if(argc < 4)
	{
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt number_of_requests" << endl;
		return 0;
	}
#if(STOP_SIMULATION == 1)
	NUMBER_OF_REQUESTS = atoi(argv[3]);
#endif//STOP_SIMULATION == 1

	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
	map<unsigned int, unsigned int> vertexMapping;
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);

	SSDInstance.InitializeGraphDataStructures(G->GetNodes());
	cout <<"GetNodes " << G->GetNodes() << endl;
	vector<unsigned int> EdgeList;
#if (STORE_GRAPH_AT_SSD == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int temp_debugging = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		for (int e = 0; e < NI.GetOutDeg(); e++)
		{
			if(vertexMapping[NI.GetId()] == 79620)
				cout << vertexMapping[NI.GetOutNId(e)] << endl;
			assert(vertexMapping.find(NI.GetOutNId(e)) != vertexMapping.end());
			EdgeList.push_back(vertexMapping[NI.GetOutNId(e)]);
		}
		SSDInstance.AddVertexUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
		EdgeList.clear();
//		temp_debugging++;
//		cout << "Entering here\n";
//		if(temp_debugging == 3)
//			break;
	}
//	return 0;
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_WR: %lf\n", time_elapsed);
#endif //STORE_GRAPH_AT_SSD

#if (FLUSH_SSD_BUFFER == 1)
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		SSDInstance.WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1

	SSDInstance.ClearQueues();
#if (PERFORM_BFS == 1)
//	srand(time(NULL));
//	unsigned int root = 0, reqElement = 100000;
//	cout << root << " " << reqElement << endl;
	clock_gettime(CLOCK_MONOTONIC, &time_start);
//	unsigned int root = 0, reqElement = 2;
		list<unsigned int> bfsQueue;
		bool elementFound = 0;
	for(int numOfrun = 0; numOfrun < 1; numOfrun++)
	{
		cout << numOfrun << endl;
//	unsigned int root = rand() % G->GetNodes(), reqElement = rand() % G->GetNodes();
	unsigned int root = 0, reqElement = 4000;
		bfsQueue.push_back(root); // Need to get ROOT as input element, root element needs to be < NumVertices
		elementFound = 0;
		bool *visited = (bool *)calloc(vertexMapping.size(), sizeof(bool));
		temp_debugging = 0;
#if(STOP_SIMULATION == 1)
		unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1
		while(!bfsQueue.empty())
		{
//			cout << "E" <<endl ;
			unsigned int topElement = bfsQueue.front();
#if (VERBOSE == 1)
			cout << topElement << ",";
#endif
			if(topElement == reqElement) // requiredElement is input to the program
			{
				cout << "Found the required element" << endl;
				elementFound = 1;
				break;
			}
//			cout << "E1" <<endl ;
			SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
#if (VERBOSE_PRINT_ADJLIST == 1)
			cout << topElement << " :";
			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
				cout << " " << EdgeList[i];
			}
			cout << endl;
#endif //VERBOSE_PRINT_ADJLIST
//			cout << "E" <<endl ;
			bfsQueue.pop_front();
			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
				assert(EdgeList[i] < vertexMapping.size());
				if(visited[EdgeList[i]] == 0)
				{
					bfsQueue.push_back(EdgeList[i]);
					visited[EdgeList[i]] = 1;
				}
			}
			//		cout << "Searching" << endl;
			//		cout << endl;
			EdgeList.clear();
			//		temp_debugging++;
			//		if(temp_debugging == 12)
			//			break;
#if(STOP_SIMULATION == 1)
			number_requests_executed++;
			//		cout << number_requests_executed << endl;
			if(number_requests_executed == NUMBER_OF_REQUESTS)
				break;
#endif//STOP_SIMULATION == 1
		}
		free(visited);
		bfsQueue.clear();
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	SSDInstance.FinalizeGraphDataStructures();	
	if(elementFound == 0)
		cout << "Did not find the required element" << endl;
#endif //PERFORM_BFS
	return 0;
}
