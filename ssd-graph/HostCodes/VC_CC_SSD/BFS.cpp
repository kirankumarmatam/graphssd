#include "../Baseline_library/graphFiltering.h"
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define READ_REQUEST_AS_ARGUMENT 1

extern unsigned int numOfNodes, numOfEdges;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
       struct timespec time_start_overhead, time_end_overhead;
       double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

#if(useCache == 1)
       extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
       extern unsigned int TotNumOfRequestsPerAccess_host, TotNumOfRequestsPerAccess_SSD;
       extern unsigned int MaxCacheSize;
#endif

       extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;
       extern PNGraph G;

int main(int argc, char **argv)
{
	set_context(argc, argv);

	vector<unsigned int> EdgeList;
	struct timespec time_start, time_end;
	double time_elapsed;
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	list<unsigned int> updatedQueue;
	for(int numOfrun = 0; numOfrun < 1; numOfrun++)
	{
		unsigned int *componentNum = (unsigned int *)malloc(numOfNodes*sizeof(unsigned int));
		for(unsigned int i = 0 ; i < numOfNodes; i++)
		{
			componentNum[i] = i;
			updatedQueue.push_back(i);
		}
		while(!updatedQueue.empty())
		{
			unsigned int topElement = updatedQueue.front();
#if (VERBOSE == 1)
			cout << topElement << ", " << endl;
#endif
			EdgeList.clear();
			GetAdjList(topElement, EdgeList);
			updatedQueue.pop_front();
			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
#if(LOAD_GRAPH_FROM_FILE == 1)
				assert(EdgeList[i] < numOfNodes);
#else
				assert(EdgeList[i] < vertexMapping.size());
#endif
				if (componentNum[topElement] < componentNum[EdgeList[i]])
				{
					componentNum[EdgeList[i]] = componentNum[topElement];
					updatedQueue.push_back(EdgeList[i]);
				}
			}
		}
		updatedQueue.clear();
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	FinalizeGraphDataStructures();
#if(useCache == 1)
	printTime();
#endif

	return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
