#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1


#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
extern unsigned int cache_percentage;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;

int main(int argc, char **argv)
{
    SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
    SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

    struct timespec time_start, time_end;
    double time_elapsed;

    vector<unsigned int> EdgeList;

    unsigned int total_number_of_requests = 1;
    clock_gettime(CLOCK_MONOTONIC, &time_start);
    list<unsigned int> updatedQueue;
    for(int numOfrun = 0; numOfrun < total_number_of_requests; numOfrun++)
    {
        unsigned int *componentNum = (unsigned int *)malloc(NumNodes*sizeof(unsigned int));
	for(unsigned int i = 0 ; i < NumNodes; i++)
	{
		componentNum[i] = i;
        	updatedQueue.push_back(i);
	}
        unsigned int temp_debugging = 0;
        while(!updatedQueue.empty())
        {
            unsigned int topElement = updatedQueue.front();
#if (VERBOSE == 1)
            cout << topElement << ", " << endl;
#endif
            EdgeList.clear();
            SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
            updatedQueue.pop_front();
            for(unsigned int i=0; i < EdgeList.size(); i++)
            {
#if(LOAD_GRAPH_FROM_FILE == 1)
                assert(EdgeList[i] < NumNodes);
#else
                assert(EdgeList[i] < vertexMapping.size());
#endif
                if (componentNum[topElement] < componentNum[EdgeList[i]])
                {
			componentNum[EdgeList[i]] = componentNum[topElement];
                        updatedQueue.push_back(EdgeList[i]);
                }
            }
        }
#if(AuxMemoryInStorage == 0)
        free(updated);
        free(distance);
#endif
        updatedQueue.clear();
    }
    clock_gettime(CLOCK_MONOTONIC, &time_end);
    time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
    time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
    fprintf(stdout, "\n");
    fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
    SSDInstance.FinalizeGraphDataStructures();
    SSDInstance.printTime();
    return 0;
}
