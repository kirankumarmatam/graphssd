#include "../Baseline_library/graphFiltering.h"
#define VERBOSE 0
#define VERBOSE_PRINT_ADJLIST 0
#define MEASURE_IO_TIME	1
#define STOP_SIMULATION 0
#define FLUSH_SSD_BUFFER 1
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define READ_REQUEST_AS_ARGUMENT 0
#define STORE_LARGE_SCALE 0

unsigned int numOfNodes;
unsigned int numOfEdges;
unsigned int fd;
void *data_nvme_command;

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
       struct timespec time_start_overhead, time_end_overhead;
       double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

#if(useCache == 1)
       extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
       extern unsigned int TotNumOfRequestsPerAccess_host, TotNumOfRequestsPerAccess_SSD;
       extern unsigned int MaxCacheSize;
       unsigned int cache_percentage = 5;
#endif

int main(int argc, char **argv)
{
	if(argc < 4)
	{
#if(STORE_LARGE_SCALE == 1)
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt output_flash.txt" << endl;
#else
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt number_of_requests" << endl;
#endif//STORE_LARGE_SCALE
		return 0;
	}
#if(STOP_SIMULATION == 1)
	        NUMBER_OF_REQUESTS = atoi(argv[3]);
#endif//STOP_SIMULATION == 1

#if(STORE_LARGE_SCALE == 0)
	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
	numOfNodes = G->GetNodes();
	numOfEdges = G->GetEdges();
#else
	fstream myFlashFile;
	myFlashFile.open(argv[3]);
	myFlashFile.read((char *)(&numOfNodes), sizeof(unsigned int));
	myFlashFile.read((char *)(&numOfEdges), sizeof(unsigned int));
	cout << "numOfNodes = " << numOfNodes << " numOfEdges = " << numOfEdges << endl;
	myFlashFile.close();
#endif//STORE_LARGE_SCALE == 1

#if(useCache==1)
	if(((numOfNodes + numOfEdges) * sizeof(unsigned int)) < (1024*1024))
		MaxCacheSize = 1024 * 1024;
	else MaxCacheSize = (((numOfNodes + numOfEdges) * sizeof(unsigned int)) * cache_percentage) / 100;
#endif

//	static const char *perrstr;
//	int err, fd;
	fd = open(argv[1], O_RDWR);
        if (fd < 0)
                cout << "Error opening file!!" << endl;
	if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		return 0;
	}
#if(STORE_LARGE_SCALE == 1)
	InitializeGraphDataStructures(numOfNodes);
#else
	InitializeGraphDataStructures(G->GetNodes());
#endif//STORE_LARGE_SCALE
#if (FLUSH_SSD_BUFFER == 1)
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1

        vector<unsigned int> EdgeList;
	struct timespec time_start, time_end;
	double time_elapsed;
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	for(int numOfrun = 0; numOfrun < 1; numOfrun++)
	{
		//Write WCC code here
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#if(useCache == 1)
	cout << "MaxCacheSize = " << MaxCacheSize << " numOfCacheHits = " <<  numOfCacheHits << " numOfCacheEvictions = " << numOfCacheEvictions << " numOfCacheAccesses = " << numOfCacheAccesses << " numOfCacheMisses = " <<  numOfCacheMisses << " AvgNumOfRequestsPerAccess_SSD = " << (double)TotNumOfRequestsPerAccess_SSD / numOfCacheMisses  << " TotNumOfRequestsPerAccess_host = " << (double)TotNumOfRequestsPerAccess_host / numOfCacheAccesses << endl;
#endif
	FinalizeGraphDataStructures();

	return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
