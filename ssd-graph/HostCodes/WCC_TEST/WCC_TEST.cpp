//Remember: all the other include statements you need (like map, time) are in the graphFiltering header file
#include "../Baseline_library/graphFiltering.h"

extern unsigned int numOfNodes, numOfEdges;

//NOTE: STOP_SIMULATION if/else directives not included
//NOTE: READ_REQUEST_AS_ARGUMENT not included
//NOTE: MEASURE_IO_TIME not inlcuded
//NOTE: STORE_LARGE_SCALE not included
//NOTE: VERBOSE not included


#define DEBUG 0

#if(CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if(CALCULATE_OVERHEAD_TIME == 1)
       struct timespec time_start_overhead, time_end_overhead;
       double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

#if(useCache == 1)
       extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
       extern unsigned int TotNumOfRequestsPerAccess_host, TotNumOfRequestsPerAccess_SSD;
       extern unsigned int MaxCacheSize;
#endif


int main(int argc, char **argv)
{

	set_context(argc, argv);

	vector<unsigned int> EdgeList;

	//initialize timing variables
	struct timespec time_start, time_end;
	double time_elapsed;

	clock_gettime(CLOCK_MONOTONIC, &time_start);

	//queue for bfs
	list<unsigned int> bfsQueue;

	//current component number and eventually the number of components
	unsigned int compNum = 1;

	//the next root - assuming node numbers can fit in unsigned int
	unsigned int root = 0;

	unsigned int nodeCounter = 0;

	//arrays stored in one aux mem array
	//first visited then components then nodeInComp
#if(AuxMemoryInStorage == 0)
	bool *visited = (bool *)calloc(numOfNodes, sizeof(bool));
	//For finding the component with the most nodes
	unsigned int *nodeInComp = (unsigned int *)calloc(numOfNodes, sizeof(unsigned int));
	//array of component numbers indexed by nodeID
	unsigned int *components = (unsigned int *)calloc(numOfNodes, sizeof(unsigned int)); 
#else
	//Assuming numOfNodes < Maximum unsigned int 
	for(unsigned int i = 0; i < numOfNodes; i++)
	{
#if(DEBUG==1)
		if((i % 10000000) == 0)
			cout << "Initalized " << i << endl;
#endif//DEBUG
		AuxDataValueType value = 0;
		AccessAuxMemory(i, value, (bool)0);
	}
	unsigned int *components = (unsigned int *)calloc(numOfNodes, sizeof(unsigned int)); 
#endif	

	unsigned int node_progress = 0;
	for(int numOfrun = 0; numOfrun < 1; numOfrun++)
	{

	while(root < numOfNodes){
#if(DEBUG==1)
		cout << "root = " << root << endl;
#endif//DEBUG

			//push root onto bfsQueue
			bfsQueue.push_back(root);
#if(AuxMemoryInStorage == 0)
			components[root] = compNum;
			visited[root] = 1;
#else
			//CHECK FOR OVERFLOW ETC.
			AuxDataValueType value = 1;
			AccessAuxMemory(root, value, 0);
			components[root] = compNum;
#endif //AuxMemoryInStorage
			nodeCounter++;

			//BFS code
			while(!bfsQueue.empty()){
				unsigned int topElement = bfsQueue.front();
				bfsQueue.pop_front();

#if(DEBUG == 1)
				if((nodeCounter % 10000) == 0)
				{
					cout << "nodeCounter = " << nodeCounter << " topElement =  " << topElement << endl;
				}
#endif//DEBUG

				GetAdjList(topElement, EdgeList); 


				for(unsigned int i = 0; i < EdgeList.size(); i++){
#if(AuxMemoryInStorage == 0)					
					if(visited[EdgeList[i]] == 0){
						bfsQueue.push_back(EdgeList[i]);
						components[EdgeList[i]] = compNum;
						visited[EdgeList[i]] = 1;
						nodeCounter++;
					}
#else	
					AccessAuxMemory(EdgeList[i], value, 1);
					if(value == 0)
					{
						bfsQueue.push_back(EdgeList[i]);
						value = 1;
						AccessAuxMemory(EdgeList[i], value, 0);
						components[EdgeList[i]] = compNum;
						nodeCounter++;
					}
#endif //AuxMemoryInStorage
				}
				EdgeList.clear();
			}

			bfsQueue.clear();

			while(components[node_progress] != 0 && node_progress < numOfNodes){
				node_progress++;
			}
#if(AuxMemoryInStorage == 0)
			nodeInComp[compNum] = nodeCounter;
#endif
			compNum = compNum + 1;
#if(DEBUG == 1)
			cout << "node_progress = " << node_progress << endl;
#endif//DEBUG
			root = node_progress;
			nodeCounter = 0;
		}
#if(AuxMemoryInStorage == 0)
		free(components);
		free(visited);
#elif(AuxMemoryInStorage == 1)
		free(components);
#endif
	}
#if(AuxMemoryInStorage == 0)
	unsigned int maxComp = 0;

	for(unsigned int i_maxComp = 0; i_maxComp < numOfNodes; i_maxComp++){
		if(nodeInComp[i_maxComp] > maxComp){
			maxComp = nodeInComp[i_maxComp];
		}
	}
#endif //AuxMemoryInStorage

	compNum = compNum - 1;
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\nTIME_RD: %lf\n", time_elapsed);
	fprintf(stdout, "Number of components: %u\n", compNum);
#if(AuxMemoryInStorage == 0)
	fprintf(stdout, "Largest component: %u\n", maxComp);
#endif//AuxMemoryInStorage

	FinalizeGraphDataStructures();
#if(useCache == 1)
	printTime();
#endif

	return 0;

	//perror:
	//	perror(perrstr);
	//	return 1;
}
