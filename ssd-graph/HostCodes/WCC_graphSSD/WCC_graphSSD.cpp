#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1
#define DEBUG 0

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;
extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif //STORE_GRAPH_AT_SSD

	cout << NumNodes << " " << NumEdges << endl;

	struct timespec time_start, time_end;
	double time_elapsed;

	//edge list for bfs
	vector<unsigned int> EdgeList;

	//start time
	clock_gettime(CLOCK_MONOTONIC, &time_start);

	//queue for bfs
	list<unsigned int> bfsQueue;

	unsigned int nodeCounter = 0;
	unsigned int compNum = 1;

	//the next root - assuming node numbers can fit in unsigned int
	unsigned int root = 0;


	//arrays stored in one aux mem array
	//first visited then components then nodeInComp
#if(AuxMemoryInStorage == 0)
#if(LOAD_GRAPH_FROM_FILE == 1)
	bool *visited = (bool *)calloc(NumNodes, sizeof(bool));
	//For finding the component with the most nodes
	unsigned int *nodeInComp = (unsigned int *)calloc(NumNodes, sizeof(unsigned int));
	//array of component numbers indexed by nodeID
	unsigned int *components = (unsigned int *)calloc(NumNodes, sizeof(unsigned int)); 
#else
	bool *visited = (bool *)calloc(vertexMapping.size(), sizeof(bool));
	//For finding the component with the most nodes
	unsigned int *nodeInComp = (unsigned int *)calloc(vertexMapping.size(), sizeof(unsigned int));
	//array of component numbers indexed by nodeID
	unsigned int *components = (unsigned int *)calloc(vertexMapping.size(), sizeof(unsigned int)); 
#endif // LOAD_GRAPH_FROM_FILE
#else

	AuxDataValueType value = 0;
	//Assuming NumNodes*3 < Maximum unsigned int 
	for(unsigned int i = 0; i < NumNodes; i++)
	{
		if((i % 10000000) == 0)
		{
			cout << "i = " << i << endl;
		}
		SSDInstance.AccessAuxMemory(i, value, 0);
	}
	//array of component numbers indexed by nodeID
	unsigned int *components = (unsigned int *)calloc(NumNodes, sizeof(unsigned int)); 
#endif	

	unsigned int node_progress = 0;
	for(int numOfrun = 0; numOfrun < 1; numOfrun++)
	{

		while(root < NumNodes){
			//push root onto bfsQueue
			bfsQueue.push_back(root);
#if(AuxMemoryInStorage == 0)
			components[root] = compNum;
			visited[root] = 1;
#else
			//CHECK FOR OVERFLOW ETC.
			value = 1;
			SSDInstance.AccessAuxMemory(root, value, 0);
			components[root] = compNum;
#endif //AuxMemoryInStorage
			nodeCounter++;
			//BFS code
			while(!bfsQueue.empty()){
				unsigned int topElement = bfsQueue.front();
#if(DEBUG == 1)
						if((nodeCounter % 1) == 0)
			{
				cout << "nodeCounter = " << nodeCounter << " node_progress = " << node_progress<< endl;
			}
#endif//DEBUG

	bfsQueue.pop_front();

				//getadj list
				SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);


				for(unsigned int i = 0; i < EdgeList.size(); i++){

#if(LOAD_GRAPH_FROM_FILE == 1)
					assert(EdgeList[i] < NumNodes);
#else
					assert(EdgeList[i] < vertexMapping.size());
#endif

#if(AuxMemoryInStorage == 0)					
					if(visited[EdgeList[i]] == 0){
						bfsQueue.push_back(EdgeList[i]);
						components[EdgeList[i]] = compNum;
						visited[EdgeList[i]] = 1;
						nodeCounter++;
					}
#else
					SSDInstance.AccessAuxMemory(EdgeList[i], value, 1);
					if(value == 0)
					{
						bfsQueue.push_back(EdgeList[i]);
						value = 1;
						SSDInstance.AccessAuxMemory(EdgeList[i], value, 0);
						components[EdgeList[i]] = compNum;
						nodeCounter++;
					}


#endif //AuxMemoryInStorage
				}
				EdgeList.clear();
			}

			bfsQueue.clear();

			while(components[node_progress] != 0 && node_progress < NumNodes)
			{
				node_progress++;
			}
			compNum = compNum + 1;
#if(AuxMemoryInStorage == 0)
			nodeInComp[compNum] = nodeCounter;
#endif//AuxMemoryInStorage
			root = node_progress;
			nodeCounter = 0;
		}
#if(AuxMemoryInStorage == 0)
		free(components);
		free(visited);
#elif(AuxMemoryInStorage == 1)
		free(components);
#endif
	}
#if(AuxMemoryInStorage == 0)
	unsigned int maxComp = 0;

	for(unsigned int i_maxComp = 0; i_maxComp < NumNodes; i_maxComp++){
		if(nodeInComp[i_maxComp] > maxComp){
			maxComp = nodeInComp[i_maxComp];
		}
	}
#endif//AuxMemoryInStorage

	compNum = compNum - 1;
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\nTIME_RD: %lf\n", time_elapsed);
	fprintf(stdout, "Number of components: %u\n", compNum);
#if(AuxMemoryInStorage == 0)
	fprintf(stdout, "Largest component: %u\n", maxComp);
#endif//AuxMemoryInStorage

	SSDInstance.FinalizeGraphDataStructures();
#if(useCache == 1)
	SSDInstance.printTime();
#endif //useCache

	return 0;

	//perror:
	//	perror(perrstr);
	//	return 1;
}
