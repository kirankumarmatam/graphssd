#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_GET_EDGE 1
#define VERBOSE 0
#define NUM_OF_GET_EDGE_REQUESTS 500

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	cout << NumNodes << " " << NumEdges << endl;

	struct timespec time_start, time_end;
	double time_elapsed;

	vector<unsigned int> EdgeList;

#if (CHECK_ERROR == 1)
vector<unsigned int> EdgeList_check;
#endif //CHECK_ERROR == 1

#if (PERFORM_GET_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0;
	while(i < NUM_OF_GET_EDGE_REQUESTS)
	{
		unsigned int nodeId = rand() % NumNodes;
//		TNGraph::TNodeI NI = G->GetRndNI();
//		if(NI.GetOutDeg() != 0)
//		{
			i++;
//#if(VERBOSE == 1)
//			cout << i << " " << vertexMapping[NI.GetId()]  <<endl;
//#endif
			SSDInstance.GetAdjEdgeWeightsUseNvmeCommands(nodeId, EdgeList);
#if (CHECK_ERROR == 1)
			SSDInstance.GetAdjListNoGTL(nodeId, EdgeList_check);
			unsigned int j;
		//	assert((EdgeList_check.size() == EdgeList.size()) || (EdgeList_check.size() > (16*4096/sizeof(unsigned int))));
			if((EdgeList_check.size() != EdgeList.size()) && (EdgeList_check.size() < (16*4096/sizeof(unsigned int))))
			{
				cout << "vID = " << nodeId <<  " Sizes check = " << EdgeList_check.size() << " ssd =  " << EdgeList.size() << endl;
				for(j = 0; j < EdgeList_check.size(); j++)
			{
#if(VERBOSE == 1)
				cout << EdgeList_check[j] << " ";
#endif
			}
				cout << endl;
			for(j = 0; j < EdgeList.size(); j++)
			{
#if(VERBOSE == 1)
				cout << EdgeList[j] << " ";
#endif
			}
				cout << endl;

			}
			for(j = 0; j < EdgeList.size(); j++)
			{
				if(EdgeList[j] != EdgeList_check[j])
				{
					cout << "E " << EdgeList_check[j] << " " << EdgeList[j] << endl;
					sort(EdgeList.begin(), EdgeList.begin()+EdgeList.size());
					sort(EdgeList_check.begin(), EdgeList_check.begin()+EdgeList.size());
					for(unsigned int k=0; k < EdgeList.size(); k++)
						assert(EdgeList[k] == EdgeList_check[k]);
					break;
				}
#if(VERBOSE == 1)
				cout << EdgeList[j] << " ";
#endif
			}
#if(VERBOSE == 1)
			cout << endl;
#endif
#endif // CHECK_ERROR == 1
//		}
		EdgeList.clear();
#if (CHECK_ERROR == 1)
		EdgeList_check.clear();
#endif
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#endif //PERFORM_GET_EDGE

	SSDInstance.FinalizeGraphDataStructures();
#if(PERFORM_GET_EDGE == 1)
	if(useCache == 1)
		SSDInstance.printTime();
#endif
	return 0;
}
