#include "graphFiltering.h"

extern void * data_nvme_command;
extern map<unsigned int, unsigned int > vertexMapping;
extern unsigned int numOfNodes;
extern unsigned int fd;
#if(CALCULATE_DATA_FETCHED == 1)
extern unsigned int baseline_data_fetched;
extern unsigned int adjacencyList_data_fetched;
#endif // CALCULATE_DATA_FETCHED == 1

#if (CALCULATE_OVERHEAD_TIME == 1)
       extern struct timespec time_start_overhead, time_end_overhead;
       extern double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

void GetAdjList(unsigned int vID, vector<unsigned int> &EdgeList)
{

#if(CALCULATE_OVERHEAD_TIME == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start_overhead);
#endif // CALCULATE_OVERHEAD_TIME == 1
	unsigned int numOfElementsInAPage = 4096 / sizeof(unsigned int);
	assert(vID < vertexMapping.size());
	unsigned int slba = SLBA + (vID / (numOfElementsInAPage));
	unsigned int nblocks;
	if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
	{
		nblocks = 1;
	}
	else 
	{
		nblocks = 0;
	}
#if(CALCULATE_DATA_FETCHED ==1)
	baseline_data_fetched += (nblocks+1) * 4096;
#endif // CALCULATE_DATA_FETCHED == 1

	char **data_pointer = new char * [nblocks];
	cache_readAdjEdgeList(slba, nblocks, data_pointer);

	unsigned int *RowPtr = (unsigned int *)malloc(sizeof(unsigned int) * 2);
	RowPtr[0] = ((unsigned int *)data_pointer[0])[vID % numOfElementsInAPage];
	if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
	{
		RowPtr[1] = ((unsigned int *)data_pointer[1])[0];
	}
	free(data_pointer);

#if(CALCULATE_OVERHEAD_TIME == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_end_overhead);
	time_elapsed_overhead += ((double)time_end_overhead.tv_sec - (double)time_start_overhead.tv_sec);
	time_elapsed_overhead += ((double)time_end_overhead.tv_nsec - (double)time_start_overhead.tv_nsec) / 1000000000.0;
#endif //CALCULATE_OVERHEAD_TIME == 1

	if(RowPtr[0] == RowPtr[1]) 
		return;

	slba = SLBA + (numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage;
	nblocks = (RowPtr[1] / numOfElementsInAPage) - (RowPtr[0] / numOfElementsInAPage) + 1;

#if(CALCULATE_DATA_FETCHED ==1)
	baseline_data_fetched += (nblocks+1) * 4096;
#endif // CALCULATE_DATA_FETCHED == 1
	data_pointer = new char * [nblocks];
	cache_readAdjEdgeList(slba, nblocks, data_pointer);
#if(CALCULATE_DATA_FETCHED ==1)
	adjacencyList_data_fetched += ((RowPtr[1] - RowPtr[0] + 1) * sizeof(unsigned int));
#endif // CALCULATE_DATA_FETCHED == 1

	unsigned int startingElement = (numOfNodes + 1 + RowPtr[0]) % numOfElementsInAPage;
	for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
//		EdgeList.push_back(*((unsigned int *)data_nvme_command + startingElement + i));
		EdgeList.push_back(((unsigned int *)(data_pointer[(startingElement + i) / numOfElementsInAPage]))[(startingElement + i) % numOfElementsInAPage]);

	free(data_pointer);
	return;
}

void WriteToSSD(unsigned int j)
{
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA_FLUSH_BUFFER + j;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}

	return;

	//perror:
	//      perror(perrstr);
	//      return 1;
}

void InitializeGraphDataStructures(unsigned int NumNodes)
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 4;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = NumNodes;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}

void FinalizeGraphDataStructures()
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 6;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = 0;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}
