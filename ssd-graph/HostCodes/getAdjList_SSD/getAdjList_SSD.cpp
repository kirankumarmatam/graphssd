#include "../Baseline_library/graphFiltering.h"
#define PAGE_PER_CMD 1
#define PERFORM_GET_EDGE 1
#define NUM_OF_GET_EDGE_REQUESTS 500
#define FLUSH_SSD_BUFFER 1
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192

extern unsigned int numOfNodes, numOfEdges;

#if (CALCULATE_DATA_FETCHED == 1)
unsigned int baseline_data_fetched;
unsigned int adjacencyList_data_fetched;
#endif //CALCULATE_DATA_FETCHED == 1

#if (CALCULATE_OVERHEAD_TIME == 1)
       struct timespec time_start_overhead, time_end_overhead;
       double time_elapsed_overhead;
#endif // CALCULATE_OVERHEAD_TIME == 1

#if(useCache == 1)
       extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
       extern unsigned int TotNumOfRequestsPerAccess_host, TotNumOfRequestsPerAccess_SSD;
       extern unsigned int MaxCacheSize;
#endif
       extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;
       extern map<unsigned int, unsigned int > vertexMapping;
       extern PNGraph G;

int main(int argc, char **argv)
{
	struct timespec time_start, time_end;
	double time_elapsed;

	set_context(argc, argv);

#if (CALCULATE_DATA_FETCHED == 1)
baseline_data_fetched = 0;
adjacencyList_data_fetched = 0;
#endif //CALCULATE_DATA_FETCHED == 1

#if (CALCULATE_OVERHEAD_TIME == 1)
time_elapsed_overhead = 0;
#endif // CALCULATE_OVERHEAD_TIME

	vector<unsigned int> EdgeList;
#if (PERFORM_GET_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0;
	while(i < NUM_OF_GET_EDGE_REQUESTS)
	{
#if (VERBOSE == 1)
		cout << "i = " << i << endl;
#endif
		unsigned int nodeId = rand() % numOfNodes;
//		if(NI.GetOutDeg() != 0)
//		{
//			GetAdjList(vertexMapping[NI.GetId()], EdgeList);
			GetAdjList(nodeId, EdgeList);
			i++;
//		}
		EdgeList.clear();
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#if(CALCULATE_DATA_FETCHED == 1)
	cout << "baseline data fetched = " << baseline_data_fetched  << " graphSSD data fetched = " << adjacencyList_data_fetched << endl;
#endif // CALCULATE_DATA_FETCHED == 1
#if(CALCULATE_OVERHEAD_TIME == 1)
	cout << "IO overhead time = " << time_elapsed_overhead << endl;
#endif //CALCULATE_OVERHEAD_TIME == 1
#endif //PERFORM_GET_EDGE
	FinalizeGraphDataStructures();
#if(useCache == 1)
	printTime();
#endif

	return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
