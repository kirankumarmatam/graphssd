#include "header.h"

#define SLBA 200
#define PRINT_CSR 0
#define CHECK_ERR 1
#define STORE_AT_SSD 1
int main(int argc, char **argv)
{
	if(argc < 3)
	{
		cout << "sudo ./a.out /dev/nvme0n1 dataSet.txt";
		return 0;
	}

	       PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
	//      PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
//	        PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/web-Google.txt", 0, 1);      
		//      PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/test_graph.txt", 0, 1);      

	map<unsigned int, unsigned int> vertexMapping;
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);
	assert(vertexMapping.size() == G->GetNodes());

	FILE *file = fopen("../../Benchmarks/SNAP/csr_facebook_combined.txt", "w+");
	if(file == NULL)
		cout << "Error in open file" << endl;
	unsigned int *Array = (unsigned int *) malloc(sizeof(unsigned int) * (G->GetNodes()+1+G->GetEdges()));
	Array[0] = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		unsigned int i = vertexMapping[NI.GetId()];
		Array[i+1] = Array[i] + NI.GetOutDeg();
		for(int j = 0; j < NI.GetOutDeg(); j++)
		{
			Array[G->GetNodes() + 1 + Array[i]+j] = vertexMapping[NI.GetOutNId(j)];
		}
	}

//	static const char *perrstr;
	int err, fd;
	struct nvme_user_io io;

//	struct timespec time_start, time_end;
//	double time_elapsed;

	if (argc < 2) {
		fprintf(stderr, "Usage: %s <device>\n", argv[0]);
		return 1;
	}

//	perrstr = argv[1];
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
	{
		printf("Error in commandline\n");
		//      goto perror;
	}
	void * buffer;

	if (posix_memalign(&buffer, 4096, REQUEST_SIZE)) {
		fprintf(stderr, "can not allocate io payload\n");
		return 0;
	}

	unsigned int pageCount = 0;
#if (STORE_AT_SSD == 1)
	for(int i = 0; i < (G->GetNodes() + 1 + G->GetEdges()); i += (4096/sizeof(unsigned int)))
	{
		unsigned int valuesRemaining = (G->GetNodes() + 1 + G->GetEdges()) - i;
		if(valuesRemaining < (4096/sizeof(unsigned int)))
		{
			memcpy(buffer, (void *)(Array+i), valuesRemaining * sizeof(unsigned int));
		}
		else 
		{
			memcpy(buffer, (void *)(Array+i), 4096);
		}

		io.opcode = nvme_cmd_write;
		io.flags = 0;
		io.control = 0;
		io.metadata = (unsigned long) 0;
		io.addr = (unsigned long) buffer;
		io.slba = SLBA + pageCount;
		io.nblocks = 0;
		io.dsmgmt = 0;
		io.reftag = 0;
		io.apptag = 0;
		io.appmask = 0;

		err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
		//              if (err < 0)
		//                      goto perror;
		if (err)
		{
			fprintf(stderr, "nvme write status:%x\n", err);
			exit(0);
		}
		pageCount++;
	}
#endif
#if (CHECK_ERR == 1)
        pageCount = 0;
	unsigned int err_count = 0;
        for(int i = 0; i < (G->GetNodes() + 1 + G->GetEdges()); i += (4096/sizeof(unsigned int)))
        {
                unsigned int valuesRemaining = (G->GetNodes() + 1 + G->GetEdges()) - i;
                io.opcode = nvme_cmd_read;
                io.flags = 0;
                io.control = 0;
                io.metadata = (unsigned long) 0;
                io.addr = (unsigned long) buffer;
                io.slba = SLBA + pageCount;
                io.nblocks = 0;
                io.dsmgmt = 0;
                io.reftag = 0;
                io.apptag = 0;
                io.appmask = 0;

                err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
                //              if (err < 0)
                //                      goto perror;
		if (err)
		{
			fprintf(stderr, "nvme write status:%x\n", err);
			exit(0);
		}
		pageCount++;

		if(valuesRemaining < (4096/sizeof(unsigned int)))
		{
			for(unsigned int j = 0; j < valuesRemaining; j++)
			{
				if(*((unsigned int *)buffer + j) != Array[i+j])
					err_count += 1;
			}
		}
		else 
		{
			for(unsigned int j = 0; j < (4096 / sizeof(unsigned int)); j++)
			{
				if(*((unsigned int *)buffer + j) != Array[i+j])
					err_count += 1;
			}
		}
        }
	cout << "Error rate = " << (err_count * 1.0) / (G->GetNodes() + 1 + G->GetEdges()) << endl;
#endif //CHECK_ERR

#if (PRINT_CSR == 1)
	for(int i = 0; i < (G->GetNodes() + 1 + G->GetEdges()); i += 1)
	{
		cout << i << " " << Array[i] << endl;
	}
	cout << endl;
#endif
	return 0;
}
