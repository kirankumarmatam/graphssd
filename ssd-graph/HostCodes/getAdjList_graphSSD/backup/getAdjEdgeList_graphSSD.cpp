#include "graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_GET_EDGE 1
#define VERBOSE 0
#define FLUSH_SSD_BUFFER 0
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define CHECK_ERROR 0

map<unsigned int, unsigned int> vertexMapping;
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;

int main(int argc, char **argv)
{
	struct timespec time_start, time_end;
	double time_elapsed;

	SSDInterface SSDInstance;
#if (UseFTL == 2|| UseFTL == 3)
	SSDInstance.fd = open(argv[1], O_RDWR);
	if (SSDInstance.fd < 0)
		cout << "Error opening file!!" << endl;
#endif

	if(argc < 3)
	{
		cout << "Usage is ./a.out inputDataSet.txt" << endl;
		return 0;
	}
	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);

	SSDInstance.InitializeGraphDataStructures(G->GetNodes());
	cout <<"GetNodes " << G->GetNodes() << endl;
	vector<unsigned int> EdgeList;
#if (STORE_GRAPH_AT_SSD == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
//	unsigned int temp_debugging = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
#if (VERBOSE == 1)
		cout << "W " << vertexMapping[NI.GetId()] << endl;
#endif
//		for (int e = 0; e < NI.GetOutDeg(); e++)
		for (int e = 0; e < NI.GetOutDeg() && e < (16*4096 / sizeof(unsigned int)); e++)
		{
			assert(vertexMapping.find(NI.GetOutNId(e)) != vertexMapping.end());
			EdgeList.push_back(vertexMapping[NI.GetOutNId(e)]);
		}
		SSDInstance.AddVertexUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
#if (CHECK_ERROR == 1)
		SSDInstance.AddVertexNoGTL(vertexMapping[NI.GetId()], EdgeList);
#endif // CHECK_ERROR == 1
		EdgeList.clear();
//		temp_debugging++;
//		if(temp_debugging == 100)
//			break;
	}
#if(UseFTL == 3)
	SSDInstance.WriteGraphAndDelete();
#endif
	
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_WR: %lf\n", time_elapsed);
#endif //STORE_GRAPH_AT_SSD

#if (FLUSH_SSD_BUFFER == 1)
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		SSDInstance.WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1

	SSDInstance.ClearQueues();

#if (CHECK_ERROR == 1)
vector<unsigned int> EdgeList_check;
#endif //CHECK_ERROR == 1

#if (PERFORM_GET_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0;
	while(i < NUM_OF_GET_EDGE_REQUESTS)
	{
		TNGraph::TNodeI NI = G->GetRndNI();
		if(NI.GetOutDeg() != 0)
		{
			i++;
//#if(VERBOSE == 1)
//			cout << i << " " << vertexMapping[NI.GetId()]  <<endl;
//#endif
			SSDInstance.GetAdjListUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
#if (CHECK_ERROR == 1)
			SSDInstance.GetAdjListNoGTL(vertexMapping[NI.GetId()], EdgeList_check);
			unsigned int j;
		//	assert((EdgeList_check.size() == EdgeList.size()) || (EdgeList_check.size() > (16*4096/sizeof(unsigned int))));
			if((EdgeList_check.size() != EdgeList.size()) && (EdgeList_check.size() < (16*4096/sizeof(unsigned int))))
			{
				cout << "vID = " << vertexMapping[NI.GetId()] <<  " Sizes check = " << EdgeList_check.size() << " ssd =  " << EdgeList.size() << endl;
				for(j = 0; j < EdgeList_check.size(); j++)
			{
#if(VERBOSE == 1)
				cout << EdgeList_check[j] << " ";
#endif
			}
				cout << endl;
			for(j = 0; j < EdgeList.size(); j++)
			{
#if(VERBOSE == 1)
				cout << EdgeList[j] << " ";
#endif
			}
				cout << endl;

			}
			for(j = 0; j < EdgeList.size(); j++)
			{
				if(EdgeList[j] != EdgeList_check[j])
				{
					cout << "E " << EdgeList_check[j] << " " << EdgeList[j] << endl;
					sort(EdgeList.begin(), EdgeList.begin()+EdgeList.size());
					sort(EdgeList_check.begin(), EdgeList_check.begin()+EdgeList.size());
					for(unsigned int k=0; k < EdgeList.size(); k++)
						assert(EdgeList[k] == EdgeList_check[k]);
					break;
				}
#if(VERBOSE == 1)
				cout << EdgeList[j] << " ";
#endif
			}
#if(VERBOSE == 1)
			cout << endl;
#endif
#endif // CHECK_ERROR == 1
		}
		EdgeList.clear();
#if (CHECK_ERROR == 1)
		EdgeList_check.clear();
#endif

	}
	if(useCache == 1)
		cout << "numOfCacheHits = " <<  numOfCacheHits << " numOfCacheEvictions = " << numOfCacheEvictions << " numOfCacheAccesses = " << numOfCacheAccesses << " numOfCacheMisses = " <<  numOfCacheMisses << endl;
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#endif //PERFORM_GET_EDGE

	SSDInstance.FinalizeGraphDataStructures();
	return 0;
}
