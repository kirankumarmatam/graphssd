#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <vector>
#include <list>
#include <algorithm>

#include <time.h>
#include "header.h"

using namespace std;

#define UseFTL 3
#define useCache 0
#define NUM_OF_GET_EDGE_REQUESTS 500
//#define NUM_OF_GET_EDGE_REQUESTS 100000000
#define SLBA_FLUSH_BUFFER 5145631

class SSDInterface
{	
	public:
		void InitializeGraphDataStructures(unsigned int NumNodes);
		void FinalizeGraphDataStructures();
		void WriteGraphAndDelete();
		void ClearQueues();
		void cache_readAdjEdgeList(unsigned int vID, vector<unsigned int> &EdgeList);
		void GetEdgeWeightNvmeCommands(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight);
		void AddVertexWeightUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights);
		void GetAdjListUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList);
		void AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList);

		void AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList);
		void GetAdjListNoGTL(unsigned int vID, vector<unsigned int> &EdgeList);
		void WriteToSSD(unsigned int j);
		void AddVertexWeight(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights)
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				AddVertexWeightUseNvmeCommands(vID, EdgeList, EdgeWeights);
			}
		}
		void GetEdgeWeight(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight)
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				GetEdgeWeightNvmeCommands(srcVId, destVId, edgeWeight);
			}
		}

                map<unsigned int, vector<unsigned int> > vertexToAdjListMap;
		void *data_nvme_command;
		int fd;
		unsigned int PAGE_SIZE;

		SSDInterface()
		{
			if(UseFTL == 2 || UseFTL == 3)
			{
				PAGE_SIZE = 4096;
				if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
					fprintf(stderr, "cannot allocate io payload for data_wr\n");
				}
			}
		}
};
