#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_BFS 1
#define VERBOSE_BFS 0
#define VERBOSE_PRINT_VERTEX 0
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1
#define PRINT_LEVEL_NUMBER 0
#define READ_REQUESTS_FROM_FILE 0 // This is incomplete
#define READ_REQUEST_AS_ARGUMENT 1
#define INITIAL 0
#define ADD_UPDATES 1
#define MERGE_BUFFER 1
#define AFTER_UPDATES 1

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;
extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;
extern unsigned int NumNodesNew;

extern std::set<unsigned int> newlyAddedOrUpdatedVertices;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	cout << NumNodes << " " << NumEdges << endl;

	unsigned int numOfQueries = atoi(argv[9]);
	cout << "numOfQueries = " << numOfQueries << endl;

	vector<unsigned int> EdgeList;

/*	vector <unsigned int> QueriesPerformed;
	QueriesPerformed.push_back(18);
	QueriesPerformed.push_back(142);
	QueriesPerformed.push_back(212);
	QueriesPerformed.push_back(213);
*/
	unsigned int topElement;
#if(INITIAL == 1)
	for(unsigned int var=0; var < numOfQueries; var++)
	{
//		cout << "var = " << var << endl;
		topElement = rand() % NumNodes;
//		topElement = QueriesPerformed[var];
//		QueriesPerformed.push_back(topElement);
		SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
#if (VERBOSE_PRINT_ADJLIST == 1)
		cout << topElement << " :";
		for(unsigned int i=0; i < EdgeList.size(); i++)
		{
			cout << " " << EdgeList[i];
		}
		cout << endl;
#endif //VERBOSE_PRINT_ADJLIST
		EdgeList.clear();
	}

//	SSDInstance.FlushCaches();
	SSDInstance.FinalizeGraphDataStructures();
	SSDInstance.printTime();

cout << "Finished initial querying" << endl;
#endif//INITIAL
	

#if(ADD_UPDATES)
#if(ADD_FROM_MULTIPLE_FILES == 0)
	SSDInstance.AddToBuffer(argv[7], argv[8]);
#elif(ADD_FROM_MULTIPLE_FILES == 1)
	for(unsigned int i = 0; i <= 9; i++)
	{
		std::string initialString = argv[7] + std::to_string(i) + ".net";
		char *C = new char[initialString.size()+1];
		strcpy(C, initialString.c_str());
		SSDInstance.AddToBuffer(C, argv[8]);
		free(C);
	}
#endif//ADD_FROM_MULTIPLE_FILES

#if(FLUSH_SSD_BUFFER == 1)
	for(unsigned int j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		SSDInstance.WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1
cout << "Finished after ADD_UPDATES" << endl;
#endif // ADD_UPDATES

#if(MERGE_BUFFER == 1)
	SSDInstance.MergeBuffer(1839);
#if(FLUSH_SSD_BUFFER == 1)
	for(unsigned int j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		SSDInstance.WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1
	SSDInstance.FinalizeGraphDataStructures();
	SSDInstance.printTime();
cout << "Finished merge buffer" << endl;
#endif//MERGE_BUFFER

#if(ADDED_WITHOUT_TESTING == 1)
unsigned int query;
if(rand() % 100 >= 95)
{
		query = newlyAddedOrUpdatedVertices[rand() % newlyAddedOrUpdatedVertices.size()];
}
else {
	query = rand() % NumNodes;
	while(newlyAddedOrUpdatedVertices.find(query) != newlyAddedOrUpdatedVertices.end())
	{
		query = rand() % NumNodes;
	}
}
#endif//ADDED_WITHOUT_TESTING

#if(AFTER_UPDATES == 1)
	SSDInstance.FinalizeGraphDataStructures();
	SSDInstance.printTime();

std::vector<unsigned int> newlyAddedOrUpdatedVerticesSorted;
for(std::set<unsigned int>::iterator it = newlyAddedOrUpdatedVertices.begin(); it != newlyAddedOrUpdatedVertices.end(); it++)
{
	newlyAddedOrUpdatedVerticesSorted.push_back(*it);
}
sort(newlyAddedOrUpdatedVerticesSorted.begin(), newlyAddedOrUpdatedVerticesSorted.end());
/*for(std::vector<unsigned int>::iterator it = newlyAddedOrUpdatedVerticesSorted.begin(); it != newlyAddedOrUpdatedVerticesSorted.end(); it++)
{
	cout << *it << endl;
}*/
	for(unsigned int var=0; var < numOfQueries; var++)
	{
		if(rand() % 100 < 95)
		{
			topElement = newlyAddedOrUpdatedVerticesSorted[rand() % newlyAddedOrUpdatedVertices.size()];
		}
		else {
			topElement = rand() % NumNodes;
			while(newlyAddedOrUpdatedVertices.find(topElement) != newlyAddedOrUpdatedVertices.end())
			{
				topElement = rand() % NumNodes;
			}
		}
#if(VERBOSE_PRINT_VERTEX == 1)
		cout << "topE = " << topElement << endl;
#endif//VERBOSE_PRINT_VERTEX
//		topElement = QueriesPerformed[var];
		SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
#if (VERBOSE_PRINT_ADJLIST == 1)
		cout << topElement << " :";
		for(unsigned int i=0; i < EdgeList.size(); i++)
		{
			cout << " " << EdgeList[i];
		}
		cout << endl;
#endif //VERBOSE_PRINT_ADJLIST
		EdgeList.clear();
	}
cout << "Finished after update querying" << endl;
#endif//AFTER_UPDATES

	SSDInstance.FinalizeGraphDataStructures();
	SSDInstance.printTime();

	return 0;
}
