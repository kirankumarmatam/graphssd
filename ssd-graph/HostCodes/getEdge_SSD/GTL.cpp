#include "graphFiltering.h"

extern void * data_nvme_command;
extern map<unsigned int, unsigned int > vertexMapping;
extern unsigned int numOfNodes;
extern unsigned int numOfEdges;
extern unsigned int fd;
#define SEPARATE_WEIGHT_SLBA 1
#define SEPARATE_VERTEX_SLBA 1
void GetEdgeWeight(unsigned int vID, unsigned int destEdge, unsigned int &EdgeWeight)
{
	unsigned int numOfElementsInAPage = 4096 / sizeof(unsigned int);
	assert(vID < vertexMapping.size());
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
#if (SEPARATE_VERTEX_SLBA == 1)
	io.slba = SLBA_VERTEX + (vID / (numOfElementsInAPage));
#elif (SEPARATE_VERTEX_SLBA == 0)
	io.slba = SLBA + (vID / (numOfElementsInAPage));
#endif
	if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
	{
		io.nblocks = 1;
	}
	else 
	{
		io.nblocks = 0;
	}
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
        if (err < 0)
                cout << "Error in executing nvme command" << endl;
	if (err)
	{
		fprintf(stderr, "nvme read status:%x\n", err);
		exit(0);
	}

	unsigned int *RowPtr = (unsigned int *)malloc(sizeof(unsigned int) * 2);
	memcpy(RowPtr, (unsigned int *)data_nvme_command + (vID % numOfElementsInAPage), 2*sizeof(unsigned int));

	if(RowPtr[0] == RowPtr[1]) 
		return;
	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
#if(SEPARATE_VERTEX_SLBA == 1)
	io.slba = SLBA + (RowPtr[0]) / numOfElementsInAPage;
#elif (SEPARATE_VERTEX_SLBA == 0)
	io.slba = SLBA + (numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage;
#endif
	io.nblocks = (RowPtr[1] / numOfElementsInAPage) - (RowPtr[0] / numOfElementsInAPage) + 1;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
	{
		fprintf(stderr, "nvme read status:%x\n", err);
		exit(0);
	}

#if(SEPARATE_VERTEX_SLBA == 1)
	unsigned int startingElement = (RowPtr[0]) % numOfElementsInAPage;
#elif(SEPARATE_VERTEX_SLBA == 0)
	unsigned int startingElement = (numOfNodes + 1 + RowPtr[0]) % numOfElementsInAPage;
#endif
	unsigned int edgeWeightIndex = 0;
	for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
	{
		if(*((unsigned int *)data_nvme_command + startingElement + i) == destEdge)
		{
			edgeWeightIndex = i;
			break;
		}
	}

	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
#if (SEPARATE_WEIGHT_SLBA == 1)
	io.slba = SLBA_WEIGHT + (RowPtr[0] + edgeWeightIndex) / numOfElementsInAPage;
#elif (SEPARATE_WEIGHT_SLBA == 0)
	io.slba = SLBA_WEIGHT + (numOfNodes + 1 + RowPtr[0] + numOfEdges + edgeWeightIndex) / numOfElementsInAPage;
#endif // SEPARATE_WEIGHT_SLBA == 1
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
	{
		fprintf(stderr, "nvme read status:%x\n", err);
		exit(0);
	}

#if (SEPARATE_WEIGHT_SLBA == 1)
	startingElement = (RowPtr[0] + edgeWeightIndex) % numOfElementsInAPage;
#elif (SEPARATE_WEIGHT_SLBA == 0)
	startingElement = (numOfNodes + 1 + RowPtr[0] + numOfEdges + edgeWeightIndex) % numOfElementsInAPage;
#endif // SEPARATE_WEIGHT_SLBA == 1

	EdgeWeight = *((unsigned int *) data_nvme_command + startingElement);
	return;

//perror:
//	perror(perrstr);
//	return 1;
}

void GetAdjList(unsigned int vID, vector<unsigned int> &EdgeList)
{
	unsigned int numOfElementsInAPage = 4096 / sizeof(unsigned int);
	assert(vID < vertexMapping.size());
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA + (vID / (numOfElementsInAPage));
	if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
	{
		io.nblocks = 1;
	}
	else 
	{
		io.nblocks = 0;
	}
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
        if (err < 0)
                cout << "Error in executing nvme command" << endl;
	if (err)
		fprintf(stderr, "nvme read status:%x\n", err);

	unsigned int *RowPtr = (unsigned int *)malloc(sizeof(unsigned int) * 2);
	memcpy(RowPtr, (unsigned int *)data_nvme_command + (vID % numOfElementsInAPage), 2*sizeof(unsigned int));

	if(RowPtr[0] == RowPtr[1]) 
		return;
	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA + (numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage;
	io.nblocks = (RowPtr[1] / numOfElementsInAPage) - (RowPtr[0] / numOfElementsInAPage) + 1;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
		fprintf(stderr, "nvme read status:%x\n", err);

	unsigned int startingElement = (numOfNodes + 1 + RowPtr[0]) % numOfElementsInAPage;
	for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
		EdgeList.push_back(*((unsigned int *)data_nvme_command + startingElement + i));
/*	cout << vID << " :";
	for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
		cout << " " << *((unsigned int *)data_nvme_command + startingElement + i);
	cout << endl;
*/
//perror:
//	perror(perrstr);
//	return 1;
}
void WriteToSSD(unsigned int j)
{
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA_FLUSH_BUFFER + j;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
        if (err < 0)
                cout << "Error in executing nvme command" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}

	return;

//perror:
//	perror(perrstr);
//	return 1;
}

void InitializeGraphDataStructures(unsigned int NumNodes)
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 4;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = NumNodes;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}

void FinalizeGraphDataStructures()
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 6;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = 0;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}

