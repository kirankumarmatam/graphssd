#include "graphFiltering.h"
#define PAGE_PER_CMD 1
#define PERFORM_GET_EDGE 1
#define NUM_OF_GET_EDGE_REQUESTS 400
#define FLUSH_SSD_BUFFER 1
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192

unsigned int numOfNodes;
unsigned int numOfEdges;
unsigned int fd;
void *data_nvme_command;

int main(int argc, char **argv)
{
	struct timespec time_start, time_end;
	double time_elapsed;
	if(argc < 3)
	{
		cout << "Usage ./a.out /dev/nvme0n1 dataSet.txt" << endl;
		return 0;
	}

	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);//This need to match with the input file!
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/web-Google.txt", 0, 1);
	//      PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/test_graph.txt", 0, 1);
	numOfNodes = G->GetNodes();
	numOfEdges = G->GetEdges();
	map<unsigned int, unsigned int> vertexMapping;
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);

	//	static const char *perrstr;
	//	int err, fd;
	fd = open(argv[1], O_RDWR);
	if (fd < 0)
		cout << "Error opening file!!" << endl;
	if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		return 0;
	}

	InitializeGraphDataStructures(G->GetNodes());
#if (FLUSH_SSD_BUFFER == 1)
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1

#if (PERFORM_GET_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0, edgeWeight, destEdge;
	while(i < NUM_OF_GET_EDGE_REQUESTS)
	{
		TNGraph::TNodeI NI = G->GetRndNI();
		if(NI.GetOutDeg() != 0)
		{
			destEdge = vertexMapping[NI.GetOutNId(rand() % NI.GetOutDeg())];
//			                      cout << "srcEdge = " << vertexMapping[NI.GetId()] << " destEdge = " << destEdge << endl; 
			GetEdgeWeight(vertexMapping[NI.GetId()], destEdge, edgeWeight);
//			                      cout << "edge weight = " << edgeWeight << endl;
			if(destEdge != edgeWeight)
				cout << "Incorrect Edge Weight!!" << endl;
			i++;
			//                      cout << i << endl;
		}
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#endif //PERFORM_GET_EDGE

	FinalizeGraphDataStructures();
	return 0;

//perror:
//	perror(perrstr);
//	return 1;
}
