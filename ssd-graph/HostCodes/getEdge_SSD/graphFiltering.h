#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <vector>
#include <list>

#include <time.h>

using namespace std;
#define SLBA_VERTEX 2000000
#define SLBA 200
#define SLBA_WEIGHT 3145631
#define SLBA_FLUSH_BUFFER 5145631

void GetAdjList(unsigned int vID, vector<unsigned int> &EdgeList);
void GetEdgeWeight(unsigned int vID, unsigned int destEdge, unsigned int &EdgeWeight);
void WriteToSSD(unsigned int j);
void InitializeGraphDataStructures(unsigned int NumNodes);
void FinalizeGraphDataStructures();
