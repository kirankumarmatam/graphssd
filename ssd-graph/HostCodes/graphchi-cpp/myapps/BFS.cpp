/**
 * @file
 * @author  Aapo Kyrola <akyrola@cs.cmu.edu>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Copyright [2012] [Aapo Kyrola, Guy Blelloch, Carlos Guestrin / Carnegie Mellon University]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

 *
 * @section DESCRIPTION
 *
 * Template for GraphChi applications. To create a new application, duplicate
 * this template.
 */



#include <string>

#include "graphchi_basic_includes.hpp"

#define PRINTRESULT 0

using namespace graphchi;
using namespace std;

/**
  * Type definitions. Remember to create suitable graph shards using the
  * Sharder-program.
  */
typedef vid_t VertexDataType;
typedef vid_t EdgeDataType;

vid_t target_id;
vid_t root_id;

/**
  * GraphChi programs need to subclass GraphChiProgram<vertex-type, edge-type>
  * class. The main logic is usually in the update function.
  */
struct BFSProgram : public GraphChiProgram<VertexDataType, EdgeDataType>
{

    bool converged;
    bool found;
    vid_t level;
    /**
     *  Vertex update function.
     */
    void update(graphchi_vertex<VertexDataType, EdgeDataType> &vertex, graphchi_context &gcontext)
    {

        if (gcontext.iteration == 0)
        {
            /* On first iteration, initialize vertex (and its edges). This is usually required, because
               on each run, GraphChi will modify the data files. To start from scratch, it is easiest
               do initialize the program in code. Alternatively, you can keep a copy of initial data files. */
            // vertex.set_data(init_value);
            vid_t tmp_data;
            level=0;
            if (root_id==target_id)
            {
                found=true;
            }
            else
            {
                found=false;
            }
            if (vertex.id()==root_id)
            {
                tmp_data=0;
            }
            else
            {
                tmp_data=0xffffffff;
            }
            vertex.set_data(tmp_data);
            for(int i=0; i < vertex.num_outedges(); i++)
            {
                // Do something
                vertex.outedge(i)->set_data(tmp_data);
            }
        }
        else if (gcontext.iteration!=gcontext.last_iteration)
        {
            /* Do computation */
            vid_t curlevel=vertex.get_data();
            /* Loop over in-edges (example) */
            for(int i=0; i < vertex.num_inedges(); i++)
            {
                // Do something
                //    value += vertex.inedge(i).get_data();
                if (curlevel<level)
                {
                    break;
                }
                EdgeDataType tmp=vertex.inedge(i)->get_data();
                if (tmp==(level-1))
                {
                    vertex.set_data(level);
                    for(int j=0; j < vertex.num_outedges(); j++)
                    {
                        // Do something
                        // vertex.outedge(i).set_data(x)
                        vertex.outedge(j)->set_data(level);
                    }
                    converged = false;
                    if (vertex.id()==target_id)
                    {
                        found=true;
                    }
                    break;
                }
            }

        }
        else
        {
            cout << "v: " << vertex.id() << " level: " << vertex.get_data() << endl;
        }
    }

    /**
     * Called before an iteration starts.
     */
    void before_iteration(int iteration, graphchi_context &gcontext)
    {
        converged = iteration > 0;
    }

    /**
     * Called after an iteration has finished.
     */
    void after_iteration(int iteration, graphchi_context &gcontext)
    {
        level++;
        if (found)
        {
            std::cout << "Found!" << std::endl;
#if(PRINTRESULT==1)
            if (iteration!=gcontext.last_iteration)
            {
                gcontext.set_last_iteration(iteration+1);
            }
#else
            gcontext.set_last_iteration(iteration);
#endif
        }
        if (converged)
        {
            std::cout << "Converged!" << std::endl;
#if(PRINTRESULT==1)
            if (iteration!=gcontext.last_iteration)
            {
                gcontext.set_last_iteration(iteration+1);
            }
#else
            gcontext.set_last_iteration(iteration);
#endif
        }
    }

    /**
     * Called before an execution interval is started.
     */
    void before_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
    {
    }

    /**
     * Called after an execution interval has finished.
     */
    void after_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
    {
    }

};

int main(int argc, const char ** argv)
{
    /* GraphChi initialization will read the command line
       arguments and the configuration file. */
    graphchi_init(argc, argv);

    /* Metrics object for keeping track of performance counters
       and other information. Currently required. */
    metrics m("BFS");

    /* Basic arguments for application */
    std::string filename = get_option_string("file");  // Base filename
    int niters           = get_option_int("niters", 9999); // Number of iterations
    bool scheduler       = get_option_int("scheduler", 0); // Whether to use selective scheduling

    root_id       = get_option_int("root", 0);
    target_id       = get_option_int("target", 0);


    /* Detect the number of shards or preprocess an input to create them */
    int nshards          = convert_if_notexists<EdgeDataType>(filename,
                           get_option_string("nshards", "auto"));

    /* Run */
    BFSProgram program;
    graphchi_engine<VertexDataType, EdgeDataType> engine(filename, nshards, scheduler, m);
    engine.run(program, niters);

    /* Report execution metrics */
    metrics_report(m);
    return 0;
}
