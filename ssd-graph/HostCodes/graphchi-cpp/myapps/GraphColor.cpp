/**
 * @file
 * @author  Aapo Kyrola <akyrola@cs.cmu.edu>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Copyright [2012] [Aapo Kyrola, Guy Blelloch, Carlos Guestrin / Carnegie Mellon University]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

 *
 * @section DESCRIPTION
 *
 * Template for GraphChi applications. To create a new application, duplicate
 * this template.
 */



#include <string>

#include "graphchi_basic_includes.hpp"
#include "graphchi_types.hpp"

#define INFINI 0xffffffff

#define INS 3
#define TENTAINS 2
#define NOTINS 1
#define UNKNOWN 0
#define PRINTRESULT 0

using namespace graphchi;
using namespace std;

template <typename T>
struct GraphColorVertex
{
    bool IsColored;
    bool IsCaled;
    unsigned char type;
    T degree;
#if(PRINTRESULT==1)
    T color;
#endif
};
/**
  * Type definitions. Remember to create suitable graph shards using the
  * Sharder-program.
  */
typedef GraphColorVertex<vid_t> VertexDataType;
typedef GraphColorEdge<vid_t> EdgeDataType; // modify src/graphchi_types.hpp and src/preprocessing/conversions.hpp

/**
  * GraphChi programs need to subclass GraphChiProgram<vertex-type, edge-type>
  * class. The main logic is usually in the update function.
  */
struct GraphColorProgram : public GraphChiProgram<VertexDataType, EdgeDataType>
{
    bool converged;
    /**
     *  Vertex update function.
     */
    vid_t TotalColor;
    vid_t step;
    bool HasUnknown;
    bool fired;
    void update(graphchi_vertex<VertexDataType, EdgeDataType> &vertex, graphchi_context &gcontext)
    {
        if (gcontext.iteration == 0)
        {
            /* On first iteration, initialize vertex (and its edges). This is usually required, because
               on each run, GraphChi will modify the data files. To start from scratch, it is easiest
               do initialize the program in code. Alternatively, you can keep a copy of initial data files. */
            // vertex.set_data(init_value);
            VertexDataType v;
            EdgeDataType e;
            v.IsColored=0;
            v.type=UNKNOWN;
            v.degree=0;
#if(PRINTRESULT==1)
            v.color=0;
#endif
            v.IsCaled=0;

            e.ADDD=1;
            e.DECD=0;
            e.NINS=0;
            e.SID=0;
            e.NeighborId=vertex.id();
            vertex.set_data(v);
            srand(time(NULL));
            for(int i=0; i < vertex.num_outedges(); i++)
            {
                // Do something
                vertex.outedge(i)->set_data(e);
            }
            vertex.set_data(v);
            converged=0;
            TotalColor=0;
            step=0;
            HasUnknown=1;
            fired=0;
        }
        else if (gcontext.iteration!=gcontext.last_iteration)
        {
            /* Do computation */
            VertexDataType v=vertex.get_data();
            EdgeDataType e;
            switch(step)
            {
            case 1 :
            {
                if(!v.IsColored && (v.type==UNKNOWN))
                {
                    if (!v.IsCaled)
                    {
                        for(int i=0; i < vertex.num_inedges(); i++)
                        {
                            // Do something
                            e=vertex.inedge(i)->get_data();
                            if (e.ADDD)
                            {
                                v.degree++;
                            }
                            if (e.DECD)
                            {
                                v.degree--;
                            }
                        }
                        v.IsCaled=1;
                    }
                    float p=(float)rand()/(float)(RAND_MAX);
                    if (p*2*v.degree<1.0)
                    {
                        v.type=TENTAINS;
                        for(int i=0; i < vertex.num_outedges(); i++)
                        {
                            // Do something
                            e=vertex.outedge(i)->get_data();
                            e.SID=1;
                            e.NINS=0;
                            vertex.outedge(i)->set_data(e);
                        }
                        fired=1;
                    }
                    else
                    {
                        for(int i=0; i < vertex.num_outedges(); i++)
                        {
                            // Do something
                            e=vertex.outedge(i)->get_data();
                            e.NINS=0;
                            vertex.outedge(i)->set_data(e);
                        }
                    }
                    vertex.set_data(v);
                }
                converged=0;
                break;
            }
            case 2 :
            {
                if(!v.IsColored && (v.type==TENTAINS))
                {
                    bool IsMin=1;
                    for(int i=0; i < vertex.num_inedges(); i++)
                    {
                        // Do something
                        e=vertex.inedge(i)->get_data();
                        if (e.SID && e.NeighborId<vertex.id())
                        {
                            IsMin=0;
                        }
                    }
                    if (IsMin)
                    {
                        v.type=INS;
                        for(int i=0; i < vertex.num_outedges(); i++)
                        {
                            // Do something
                            e=vertex.outedge(i)->get_data();
                            e.NINS=1;
                            vertex.outedge(i)->set_data(e);
                        }
                    }
                    else
                    {
                        v.type=UNKNOWN;
                    }
                    vertex.set_data(v);
                }
                converged=0;
                break;
            }
            case 3 :
            {
                if(!v.IsColored && (v.type==UNKNOWN))
                {
                    bool NIsInS=0;
                    for(int i=0; i < vertex.num_inedges(); i++)
                    {
                        // Do something
                        e=vertex.inedge(i)->get_data();
                        if (e.NINS)
                        {
                            NIsInS=1;
                        }
                    }
                    if (NIsInS)
                    {
                        v.type=NOTINS;
                        e.SID=0;
                        e.ADDD=0;
                        e.DECD=1;
                        e.NINS=0;
                        for(int i=0; i < vertex.num_outedges(); i++)
                        {
                            // Do something
                            e=vertex.outedge(i)->get_data();
                            e.ADDD=0;
                            e.DECD=1;
                            e.SID=0;
                            vertex.outedge(i)->set_data(e);
                        }
                    }
                    else
                    {
                        HasUnknown=1;
                        for(int i=0; i < vertex.num_outedges(); i++)
                        {
                            // Do something
                            e=vertex.outedge(i)->get_data();
                            e.ADDD=0;
                            e.SID=0;
                            vertex.outedge(i)->set_data(e);
                        }
                    }
                    vertex.set_data(v);
                }
                converged=0;
                break;
            }
            case 4 :
            {
                if(!v.IsColored)
                {
                    if (v.type==INS)
                    {
#if(PRINTRESULT==1)
                        v.color=TotalColor+1;
#endif
                        v.IsColored=1;
                        e.SID=0;
                        e.ADDD=0;
                        e.DECD=0;
                        e.NINS=0;
                    }
                    else
                    {
                        v.type=UNKNOWN;
                        e.SID=0;
                        e.ADDD=1;
                        e.DECD=0;
                        e.NINS=0;
                        converged=0;
                    }
                    v.degree=0;
                    v.IsCaled=0;
                    for(int i=0; i < vertex.num_outedges(); i++)
                    {
                        // Do something
                        vertex.outedge(i)->set_data(e);
                    }
                }
                vertex.set_data(v);
                break;
            }
            }

            /* Loop over all edges (ignore direction) */
            //for(int i=0; i < vertex.num_edges(); i++) {
            // vertex.edge(i).get_data()
            //}

            // v.set_data(new_value);
        }
        else
        {
#if(PRINTRESULT==1)
            VertexDataType v=vertex.get_data();
            cout << "v: " << vertex.id() << " color is: " << v.color << endl;
#endif
        }
    }

    /**
     * Called before an iteration starts.
     */
    void before_iteration(int iteration, graphchi_context &gcontext)
    {
        converged = iteration > 0;
        fired=0;
        HasUnknown=0;

    }

    /**
     * Called after an iteration has finished.
     */
    void after_iteration(int iteration, graphchi_context &gcontext)
    {
        switch (step)
        {
        case 0:
        {
            step=1;
            break;
        }
        case 1:
        {
            if (fired)
            {
                step=2;
            }
            else
            {
                step=1;
            }
            break;
        }
        case 2:
        {
            step=3;
            break;
        }
        case 3:
        {
            if (HasUnknown)
            {
                step=1;
            }
            else
            {
                step=4;
            }
            break;
        }
        case 4:
        {
            step=1;
            TotalColor++;
            break;
        }
        }

        if (converged)
        {
            std::cout << "Total number of colors is " << TotalColor <<std::endl;
#if(PRINTRESULT==1)
            if (iteration!=gcontext.last_iteration)
            {
                gcontext.set_last_iteration(iteration+1);
            }
#else
            gcontext.set_last_iteration(iteration);
#endif
        }
    }

    /**
     * Called before an execution interval is started.
     */
    void before_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
    {
    }

    /**
     * Called after an execution interval has finished.
     */
    void after_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
    {
    }

};

int main(int argc, const char ** argv)
{
    /* GraphChi initialization will read the command line
       arguments and the configuration file. */
    graphchi_init(argc, argv);

    /* Metrics object for keeping track of performance counters
       and other information. Currently required. */
    metrics m("GraphColor");

    /* Basic arguments for application */
    std::string filename = get_option_string("file");  // Base filename
    int niters           = get_option_int("niters", 1000); // Number of iterations
    bool scheduler       = get_option_int("scheduler", 0); // Whether to use selective scheduling

    /* Detect the number of shards or preprocess an input to create them */
    int nshards          = convert_if_notexists<EdgeDataType>(filename,
                           get_option_string("nshards", "auto"));

    /* Run */
    GraphColorProgram program;
    graphchi_engine<VertexDataType, EdgeDataType> engine(filename, nshards, scheduler, m);
    engine.run(program, niters);

    /* Report execution metrics */
    metrics_report(m);
    return 0;
}
