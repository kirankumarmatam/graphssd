/**
 * @file
 * @author  Aapo Kyrola <akyrola@cs.cmu.edu>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Copyright [2012] [Aapo Kyrola, Guy Blelloch, Carlos Guestrin / Carnegie Mellon University]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

 *
 * @section DESCRIPTION
 *
 * Demonstration for streaming graph updates. This application reads from a file
 * list of edges and adds them into the graph continuously. Simultaneously, pagerank
 * is computed for the evolving graph.
 *
 * This code includes a fair amount of code for demo purposes. To be cleaned
 * eventually.
 */

#include <string>
#include <fstream>
#include <cmath>

#define GRAPHCHI_DISABLE_COMPRESSION


#include "graphchi_basic_includes.hpp"
#include "engine/dynamic_graphs/graphchi_dynamicgraph_engine.hpp"
#include "util/toplist.hpp"

/* HTTP admin tool */
//#include "httpadmin/chi_httpadmin.hpp"
//#include "httpadmin/plotter.hpp"

using namespace graphchi;

#define THRESHOLD 1e-1f
#define RANDOMRESETPROB 0.15f


typedef float VertexDataType;
typedef float EdgeDataType;

graphchi_dynamicgraph_engine<float, float> * dyngraph_engine;
std::string streaming_graph_file;


struct DoNothing : public GraphChiProgram<VertexDataType, EdgeDataType>
{

    /**
     * Called before an iteration starts.
     */
    void before_iteration(int iteration, graphchi_context &gcontext)
    {
    }

    /**
     * Called after an iteration has finished.
     */
    void after_iteration(int iteration, graphchi_context &gcontext)
    {
    }

    /**
     * Called before an execution interval is started.
     */
    void before_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &ginfo)
    {
    }


    /**
     * Pagerank update function.
     */
    void update(graphchi_vertex<VertexDataType, EdgeDataType> &v, graphchi_context &ginfo)
    {
    }

};



bool running = true;

/**
  * Function executed by a separate thread that streams
  * graph from a file.
  */
void * dynamic_graph_reader(void * info);
void * dynamic_graph_reader(void * info)
{
    std::cout << "Start sleeping..." << std::endl;
    usleep(50000);
    std::cout << "End sleeping..." << std::endl;

    logstream(LOG_INFO) << "Going to stream from: " << streaming_graph_file << std::endl;
    FILE * f = fopen(streaming_graph_file.c_str(), "r");
    if (f == NULL)
    {
        logstream(LOG_ERROR) << "Could not open file for streaming: " << streaming_graph_file <<
                             " error: " << strerror(errno) << std::endl;
    }
    assert(f != NULL);


    size_t c = 0;
    // Used for flow control
    timeval last, now;
    gettimeofday(&last, NULL);

    vid_t from;
    vid_t to;
    char s[1024];

    while(fgets(s, 1024, f) != NULL)
    {
        FIXLINE(s);
        /* Read next line */
        char delims[] = "\t ";
        char * t;
        t = strtok(s, delims);
        from = atoi(t);
        t = strtok(NULL, delims);
        to = atoi(t);

        if (from == to)
        {
            // logstream(LOG_WARNING) << "Self-edge in stream: " << from << " <-> " << to << std::endl;
            continue;
        }

        bool success=false;
        while (!success)
        {
            success = dyngraph_engine->add_edge(from, to, 0.0f);
        }
        dyngraph_engine->add_task(from);
    }
    fclose(f);
    dyngraph_engine->finish_after_iters(1);
    return NULL;
}


int main(int argc, const char ** argv)
{
    graphchi_init(argc, argv);
    metrics m("streaming");

    /* Parameters */
    std::string filename    = get_option_string("file"); // Base filename
    int niters              = 100000;                    // End of computation to be determined programmatically
    // Pagerank can be run with or without selective scheduling
    bool scheduler          = false;

    /* Process input file (the base graph) - if not already preprocessed */
    int nshards             = convert_if_notexists<EdgeDataType>(filename, get_option_string("nshards", "auto"));

    /* Streaming input graph - must be in edge-list format */
    streaming_graph_file = get_option_string_interactive("streaming_graph_file",
                           "Pathname to graph file to stream edges from");

    /* Create the engine object */
    dyngraph_engine = new graphchi_dynamicgraph_engine<float, float>(filename, nshards, scheduler, m);
    dyngraph_engine->set_modifies_inedges(false); // Improves I/O performance.

    /* Start streaming thread */
    pthread_t strthread;
    int ret = pthread_create(&strthread, NULL, dynamic_graph_reader, NULL);
    assert(ret>=0);

    /* Start HTTP admin */
    /* start_httpadmin< graphchi_dynamicgraph_engine<float, float> >(dyngraph_engine);
     register_http_request_handler(new IntervalTopRequest());


     pthread_t plotterthr;
     ret = pthread_create(&plotterthr, NULL, plotter_thread, NULL);
     assert(ret>=0);
     */

    /* Run the engine */
    DoNothing program;
    dyngraph_engine->run(program, niters);


    running = false;

    /* Output top ranked vertices */
    metrics_report(m);
    return 0;
}





