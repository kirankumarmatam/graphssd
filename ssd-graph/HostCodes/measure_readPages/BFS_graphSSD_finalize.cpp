#include "../GraphSSD_library/graphFiltering.h"

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
	unsigned int fd = open(argv[1], O_RDWR);
	if (fd < 0)
		cout << "Error opening file!!" << endl;
	SSDInstance.fd = fd;
	SSDInstance.FinalizeGraphDataStructures();
	SSDInstance.printTime();
	return 0;
}
