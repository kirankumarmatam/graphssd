// ***********************************************
// kiran: copied from nvme_rw for test
// ***********************************************

#include <linux/nvme.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include <time.h>
#define EN_CHECK_ERR 1
#define EN_WR (1 | EN_CHECK_ERR)
#define SECTOR_SIZE 4096
#define LBA_BASE1 200
#define NUMBER_OF_SSD_BUFFER_SECTORS 4096
#define NUM_OF_TEST_VARIABLES 2
#define LBA_BASE1_END (LBA_BASE1 + NUMBER_OF_SSD_BUFFER_SECTORS * NUM_OF_TEST_VARIABLES)
#define LBA_BASE2 30000
#define LBA_BASE2_END (LBA_BASE2 + NUMBER_OF_SSD_BUFFER_SECTORS * NUM_OF_TEST_VARIABLES)
#define PAGE_PER_CMD1 1
#define PAGE_PER_CMD2 4

int err, fd;
void ssd_command(unsigned long data_wr, unsigned isWrite, unsigned int slba, unsigned int nblocks)
{
	struct nvme_user_io io;
	if(isWrite == 1) io.opcode = nvme_cmd_write;
	else io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_wr;
	io.slba = slba;
	io.nblocks = nblocks - 1;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);
}

void write_vector(unsigned long data_wr, char *data_write, unsigned int LBA_BASE, unsigned int LBA_BASE_END, unsigned int PAGE_PER_CMD)
{
	unsigned int i=0;
	for(i=0; i < (LBA_BASE_END - LBA_BASE) / PAGE_PER_CMD; i++)
	{

		memcpy((void *)data_wr, (void *)(data_write+i*PAGE_PER_CMD * SECTOR_SIZE), PAGE_PER_CMD*SECTOR_SIZE);
	return;
		ssd_command((unsigned long)data_wr, 1, LBA_BASE + i * PAGE_PER_CMD, PAGE_PER_CMD);
	}
}

void read_vector(unsigned long data_rd, unsigned int LBA_BASE, unsigned int LBA_BASE_END, unsigned int PAGE_PER_CMD, unsigned int denominator)
{
	// for time measurement
	struct timespec time_start, time_end;
	double time_elapsed;
#if(EN_CHECK_ERR)
	unsigned long n_byte = 0;
	unsigned long n_err = 0;
#endif	// EN_CHECK_ERR
	unsigned int i=0;
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	for (i = 0; i < (LBA_BASE_END - LBA_BASE) / PAGE_PER_CMD; i++) {
		ssd_command((unsigned long)data_rd, 0, LBA_BASE + i * PAGE_PER_CMD, PAGE_PER_CMD);
#if(EN_CHECK_ERR)
	unsigned int j=0;
	for (j = 0; j < (PAGE_PER_CMD * SECTOR_SIZE); j++) {
		n_byte++;
		n_err += ( ((char *)data_rd)[j]!=(char)(j%denominator) ) ? 1: 0;
	}
#endif	// EN_CHECK_ERR
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf, bw=%lf B/s\n", time_elapsed, (double)SECTOR_SIZE*(LBA_BASE_END-LBA_BASE)/time_elapsed);
#if(EN_CHECK_ERR)
	fprintf(stdout, "ERR: %ld / %ld = %lf \n", n_err, n_byte, (double)n_err/n_byte);
#endif
}

int main(int argc, char **argv)
{
	printf("Entering here 0\n");
	static const char *perrstr;
	void *data_wr;
	void *data_rd;
	if ( posix_memalign(&data_wr, SECTOR_SIZE, PAGE_PER_CMD2 * SECTOR_SIZE) ) {
		fprintf(stderr, "cannot allocate io payload for data_wr\n");
		return 0;
	}
	if ( posix_memalign(&data_rd, SECTOR_SIZE, PAGE_PER_CMD2 * SECTOR_SIZE) ) {
		fprintf(stderr, "cannot allocate io payload for data_rd\n");
		return 0;
	}

	char data_write1[(LBA_BASE1_END - LBA_BASE1) * SECTOR_SIZE];
	char data_write2[(LBA_BASE2_END - LBA_BASE2) * SECTOR_SIZE];

	unsigned int i = 0;
	unsigned int j = 0;
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <device>\n", argv[0]);
		return 1;
	}

	printf("Entering here 1\n");
	perrstr = argv[1];
	fd = open(argv[1], O_RDWR);
	printf("fd = %d\r\n", fd);
	if (fd < 0)
	{
		return 0;
		goto perror;
	}

	printf("Entering here 2\n");

#if(EN_WR == 1)
	for (i = 0; i < (LBA_BASE2_END - LBA_BASE2) * SECTOR_SIZE; i++) {
		data_write2[i] = (char)(i%64);
	}
	write_vector((unsigned long)data_wr, data_write2, LBA_BASE2, LBA_BASE2_END, PAGE_PER_CMD2);
	for (i = 0; i < (LBA_BASE1_END - LBA_BASE1) * SECTOR_SIZE; i++) {
		data_write1[i] = (char)(i%256);
	}
	write_vector((unsigned long)data_wr, data_write1, LBA_BASE1, LBA_BASE1_END, PAGE_PER_CMD1);
#endif//EN_WR
return 0;
read_vector((unsigned long)data_rd, LBA_BASE1, LBA_BASE1_END, PAGE_PER_CMD1, 256);
read_vector((unsigned long)data_rd, LBA_BASE2, LBA_BASE2_END, PAGE_PER_CMD2, 64);
	return 0;

 perror:
	perror(perrstr);
	return 1;
}
