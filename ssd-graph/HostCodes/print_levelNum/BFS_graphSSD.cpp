#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_BFS 1
#define VERBOSE_BFS 1
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1
#define PRINT_LEVEL_NUMBER 1
#define PRINT_LABEL_MAPPING 1
#define READ_REQUESTS_FROM_FILE 0 // This is incomplete
#define READ_REQUEST_AS_ARGUMENT 1

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;
extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	cout << NumNodes << " " << NumEdges << endl;

	struct timespec time_start, time_end;
	double time_elapsed;

	vector<unsigned int> EdgeList;

#if (PERFORM_BFS == 1)

#if(PRINT_LABEL_MAPPING == 1)
	std::map<unsigned int, unsigned int> labelMappingGraphToFile;
	unsigned int fileLabel, graphSSDLabel;
	FILE *mapFilePtr = fopen(argv[7], "r");
	while(fscanf(mapFilePtr, "%u %u", &fileLabel, &graphSSDLabel) != EOF)
	{
		labelMappingGraphToFile[graphSSDLabel] = fileLabel;
	}
	fclose(mapFilePtr);
#endif//PRINT_LABEL_MAPPING

	clock_gettime(CLOCK_MONOTONIC, &time_start);
#if(PRINT_LEVEL_NUMBER == 1)
	list<std::pair <unsigned int, unsigned int> > bfsQueue;
#else
	list<unsigned int> bfsQueue;
#endif
	bool elementFound = 0;
#if(READ_REQUEST_AS_ARGUMENT == 1)
	unsigned int root = 0, reqElement = atoi(argv[6]);
#else
	unsigned int root = 0, reqElement = 4000;
#endif

#if(PRINT_LEVEL_NUMBER == 1)
	bfsQueue.push_back(std::make_pair(root,0)); // Need to get ROOT as input element, root element needs to be < NumVertices
#else
	bfsQueue.push_back(root); // Need to get ROOT as input element, root element needs to be < NumVertices
#endif
	elementFound = 0;
#if(AuxMemoryInStorage == 0)
#if(LOAD_GRAPH_FROM_FILE == 1)
	bool *visited = (bool *)calloc(NumNodes, sizeof(bool));
#else
	bool *visited = (bool *)calloc(vertexMapping.size(), sizeof(bool));
#endif
#else
	AuxDataValueType value = 0;
	for(unsigned int i=0; i < NumNodes; i++)
	{
		SSDInstance.AccessAuxMemory(i, value, 0);
	}
#endif//AuxMemoryInStorage
	unsigned int temp_debugging = 0;
#if(STOP_SIMULATION == 1)
	unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1
	while(!bfsQueue.empty())
	{
#if(PRINT_LEVEL_NUMBER == 1)
		std::pair<unsigned int, unsigned int> topElementPair = bfsQueue.front();
		unsigned int topElement = topElementPair.first;
#if (VERBOSE_BFS == 1)
#if(PRINT_LABEL_MAPPING == 1)
		cout << topElementPair.first << ", " << labelMappingGraphToFile[topElementPair.first] << ", " << topElementPair.second << endl;
#else
		cout << topElementPair.first << ", " << topElementPair.second << endl; ;
#endif//PRINT_LABEL_MAPPING
#endif
#else
		unsigned int topElement = bfsQueue.front();
#if (VERBOSE_BFS == 1)
		cout << topElement << ", " << endl; ;
#endif
#endif
		if(topElement == reqElement) // requiredElement is input to the program
		{
			cout << "Found the required element" << endl;
			elementFound = 1;
			break;
		}
		SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
#if (VERBOSE_PRINT_ADJLIST == 1)
		cout << topElement << " :";
		for(unsigned int i=0; i < EdgeList.size(); i++)
		{
			cout << " " << EdgeList[i];
		}
		cout << endl;
#endif //VERBOSE_PRINT_ADJLIST
		bfsQueue.pop_front();
		for(unsigned int i=0; i < EdgeList.size(); i++)
		{
#if(LOAD_GRAPH_FROM_FILE == 1)
			assert(EdgeList[i] < NumNodes);
#else
			assert(EdgeList[i] < vertexMapping.size());
#endif
#if(AuxMemoryInStorage == 0)
			if(visited[EdgeList[i]] == 0)
#else
				SSDInstance.AccessAuxMemory(EdgeList[i], value, 1);
			if(value == 0)
#endif//AuxMemoryInStorage
			{
#if(PRINT_LEVEL_NUMBER == 1)
				bfsQueue.push_back(std::make_pair(EdgeList[i], topElementPair.second+1));
#else
				bfsQueue.push_back(EdgeList[i]);
#endif
#if(AuxMemoryInStorage == 0)
				visited[EdgeList[i]] = 1;
#else
				value = 1;
				SSDInstance.AccessAuxMemory(EdgeList[i], value, 0);
#endif//AuxMemoryInStorage == 1
			}
		}
		EdgeList.clear();
#if(STOP_SIMULATION == 1)
		number_requests_executed++;
		if(number_requests_executed == NUMBER_OF_REQUESTS)
			break;
#endif//STOP_SIMULATION == 1
	}
#if(AuxMemoryInStorage == 0)
	free(visited);
#endif
#if(PRINT_LABEL_MAPPING == 1)
	labelMappingGraphToFile.clear();
#endif//PRINT_LABEL_MAPPING
	bfsQueue.clear();
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	SSDInstance.FinalizeGraphDataStructures();	
	if(elementFound == 0)
		cout << "Did not find the required element" << endl;
#if(useCache == 1)
	SSDInstance.printTime();
#endif//useCache
#endif //PERFORM_BFS
	return 0;
}
