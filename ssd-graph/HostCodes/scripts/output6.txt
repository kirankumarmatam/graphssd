facebook_combined.txt
numOfSSDPages = 25
4039 88234

TIME_BINARY_SEARCH: 736745245.000000 736744473.000000 0.000001

TIME_COPY: -121616.737645

TIME_POST_PROCESS: -121616.737671

TIME_BINARY_SEARCH: 737722954.000000 737722513.000000 0.000000

TIME_COPY: -121616.738616

TIME_POST_PROCESS: -121616.738640

TIME_RD: 0.029056
MaxCacheSize = 1048576 numOfCacheAccesses = 500 numOfCacheAccesses_aux = 0 numOfCacheMisses = 25 numOfCacheMisses_aux = 0 numOfNANDPageReads = 25 TotRequests_SSD = 25 AvgNumOfRequestsPerAccess_SSD = 1
Time_RD (in sec): 0.023075
4039 88234

TIME_COPY: -121627.016985

TIME_POST_PROCESS: -121627.016999

TIME_COPY: -121627.017616

TIME_POST_PROCESS: -121627.018540

TIME_RD: 0.043954
MaxCacheSize = 1048576 numOfCacheAccesses = 1000 numOfCacheAccesses_aux = 0 numOfCacheMisses = 50 numOfCacheMisses_aux = 0 numOfNANDPageReads = 50 TotRequests_SSD = 50 AvgNumOfRequestsPerAccess_SSD = 1
Time_RD (in sec): 0.04615
4039 88234

TIME_BINARY_SEARCH: 821323660.000000 821322683.000000 0.000001

TIME_COPY: -121639.822250

TIME_POST_PROCESS: -121639.822270

TIME_BINARY_SEARCH: 822614231.000000 822613841.000000 0.000000

TIME_COPY: -121639.822625

TIME_POST_PROCESS: -121639.822640

TIME_RD: 0.070042
Number of components: 1
Largest component: 3829
MaxCacheSize = 1048576 numOfCacheAccesses = 113856 numOfCacheAccesses_aux = 110027 numOfCacheMisses = 37 numOfCacheMisses_aux = 12 numOfNANDPageReads = 29 TotRequests_SSD = 37 AvgNumOfRequestsPerAccess_SSD = 1
Time_RD (in sec): 0.0414467
4039 88234

TIME_BINARY_SEARCH: 718876321.000000 718875504.000000 0.000001

TIME_COPY: -121652.719782

TIME_POST_PROCESS: -121652.719800

TIME_BINARY_SEARCH: 720358621.000000 720358044.000000 0.000001

TIME_COPY: -121652.720374

TIME_POST_PROCESS: -121652.720395

TIME_RD: 0.096173
MaxCacheSize = 1048576 numOfCacheHits = 199609 numOfCacheEvictions = 0 numOfCacheAccesses = 199642 numOfCacheMisses = 33 TotRequests_SSD = 33 AvgNumOfRequestsPerAccess_SSD = 1 TotRequests_host = 199642 TotNumOfRequestsPerAccess_host = 1
sx-stackoverflow.txt
numOfSSDPages = 506608
65608366 1806067135

TIME_BINARY_SEARCH: 934204031.000000 934202379.000000 0.000002

TIME_COPY: -122012.935090

TIME_POST_PROCESS: -122012.935102

TIME_BINARY_SEARCH: 935116847.000000 935116136.000000 0.000001

TIME_COPY: -122012.935954

TIME_POST_PROCESS: -122012.935968

TIME_RD: 0.495645
MaxCacheSize = 374335100 numOfCacheAccesses = 500 numOfCacheAccesses_aux = 0 numOfCacheMisses = 497 numOfCacheMisses_aux = 0 numOfNANDPageReads = 497 TotRequests_SSD = 497 AvgNumOfRequestsPerAccess_SSD = 1
Time_RD (in sec): 0.449291
65608366 1806067135

TIME_COPY: -122023.330984

TIME_POST_PROCESS: -122023.330999

TIME_COPY: -122023.331610

TIME_POST_PROCESS: -122023.332431

TIME_RD: 0.779502
MaxCacheSize = 374335100 numOfCacheAccesses = 1000 numOfCacheAccesses_aux = 0 numOfCacheMisses = 994 numOfCacheMisses_aux = 0 numOfNANDPageReads = 990 TotRequests_SSD = 994 AvgNumOfRequestsPerAccess_SSD = 1
Time_RD (in sec): 0.895082
65608366 1806067135
BaselineResults
facebook_combined.txt
sx-stackoverflow.txt

 === REPORT FOR sharder() ===
[Timings]
edata_flush:		0.00085 s
execute_sharding:		0.004839 s
finish_shard.sort:		0.000778 s
preprocessing:		0.009309 s
shard_final:		0.002832 s
[Other]
app:	sharder
Converged!
Total number of different labels (components/communities): 1
List of labels was written to file: /Datasets/Benchmarks/SNAP/GraphChi/facebook_combined.txt.components
1. label: 0, size: 4039

 === REPORT FOR connected-components() ===
[Numeric]
cachesize_mb:		0
compression:		1
execthreads:		8
loadthreads:		4
membudget_mb:		375
niothreads:		2
niters:		5
nshards:		1
nvertices:		4039
scheduler:		0
serialized-updates:		20195	(count: 5, min: 4039, max: 4039, avg: 4039)
stripesize:		1.07374e+09
updates:		20195
work:		441170
[Timings]
execute-updates:		0.014714s	 (count: 5, min: 0.001748s, max: 0.004329, avg: 0.0029428s)
inmem-exec:		0.014724s	 (count: 5, min: 0.00175s, max: 0.004332, avg: 0.0029448s)
iomgr_init:		4.2e-05 s
label-analysis:		0.050193 s
memoryshard_create_edges:		0.002949 s
preada_now:		0.000134s	 (count: 3, min: 8e-06s, max: 0.000117, avg: 4.46667e-05s)
pwritea_now:		1.4e-05 s
runtime:		0.070703 s
stripedio_wait_for_reads:		0.050569 s
stripedio_wait_for_writes:		1e-06s	 (count: 3, min: 0s, max: 1e-06, avg: 3.33333e-07s)
[Other]
app:	connected-components
engine:	default
file:	/Datasets/Benchmarks/SNAP/GraphChi/facebook_combined.txt

 === REPORT FOR sharder() ===
[Numeric]
niothreads:		2
stripesize:		1.07374e+09
subwindow:		5e+06
[Timings]
blockload:		11.5612s	 (count: 17189, min: 2e-06s, max: 0.51773, avg: 0.000672594s)
degrees.runtime:		77.4582 s
edata_flush:		15.2002s	 (count: 6956, min: 0.001225s, max: 0.017903, avg: 0.0021852s)
execute_sharding:		326.346 s
finish_shard.sort:		44.9606s	 (count: 148, min: 0.216748s, max: 0.463199, avg: 0.303788s)
memoryshard_create_edges:		59.1666s	 (count: 149, min: 0.128703s, max: 0.499039, avg: 0.397091s)
preada_now:		13.7757s	 (count: 17781, min: 1e-06s, max: 0.517725, avg: 0.000774741s)
preprocessing:		267.232 s
read_next_vertices:		69.5488s	 (count: 21903, min: 0s, max: 0.525872, avg: 0.00317531s)
shard_final:		85.5721s	 (count: 148, min: 0.46552s, max: 3.09041, avg: 0.57819s)
stream_ahead:		73.5341s	 (count: 149, min: 0.128739s, max: 1.29487, avg: 0.493517s)
stream_ahead.0:		1.29487 s
stream_ahead.1:		1.21895 s
stream_ahead.10:		0.775903 s
stream_ahead.100:		0.421819 s
stream_ahead.101:		0.420575 s
stream_ahead.102:		0.419566 s
stream_ahead.103:		0.423571 s
stream_ahead.104:		0.424719 s
stream_ahead.105:		0.395526 s
stream_ahead.106:		0.377049 s
stream_ahead.107:		0.373928 s
stream_ahead.108:		0.378672 s
stream_ahead.109:		0.382268 s
stream_ahead.11:		0.754272 s
stream_ahead.110:		0.37621 s
stream_ahead.111:		0.378472 s
stream_ahead.112:		0.374325 s
stream_ahead.113:		0.372904 s
stream_ahead.114:		0.387816 s
stream_ahead.115:		0.373655 s
stream_ahead.116:		0.380646 s
stream_ahead.117:		0.371425 s
stream_ahead.118:		0.376211 s
stream_ahead.119:		0.378844 s
stream_ahead.12:		0.723768 s
stream_ahead.120:		0.383022 s
stream_ahead.121:		0.380217 s
stream_ahead.122:		0.381153 s
stream_ahead.123:		0.384412 s
stream_ahead.124:		0.383953 s
stream_ahead.125:		0.380451 s
stream_ahead.126:		0.380267 s
stream_ahead.127:		0.387448 s
stream_ahead.128:		0.370794 s
stream_ahead.129:		0.372685 s
stream_ahead.13:		0.782994 s
stream_ahead.130:		0.367207 s
stream_ahead.131:		0.364724 s
stream_ahead.132:		0.364245 s
stream_ahead.133:		0.359002 s
stream_ahead.134:		0.350833 s
stream_ahead.135:		0.345231 s
stream_ahead.136:		0.347987 s
stream_ahead.137:		0.342521 s
stream_ahead.138:		0.339724 s
stream_ahead.139:		0.331638 s
stream_ahead.14:		0.780104 s
stream_ahead.140:		0.326826 s
stream_ahead.141:		0.320838 s
stream_ahead.142:		0.297879 s
stream_ahead.143:		0.304521 s
stream_ahead.144:		0.287686 s
stream_ahead.145:		0.272174 s
stream_ahead.146:		0.291897 s
stream_ahead.147:		0.537856s	 (count: 2, min: 0.128739s, max: 0.409117, avg: 0.268928s)
stream_ahead.15:		0.695416 s
stream_ahead.16:		0.681684 s
stream_ahead.17:		0.743247 s
stream_ahead.18:		0.680266 s
stream_ahead.19:		0.693825 s
stream_ahead.2:		1.16625 s
stream_ahead.20:		0.692248 s
stream_ahead.21:		0.682348 s
stream_ahead.22:		0.667633 s
stream_ahead.23:		0.662769 s
stream_ahead.24:		0.665209 s
stream_ahead.25:		0.672092 s
stream_ahead.26:		0.694535 s
stream_ahead.27:		0.593851 s
stream_ahead.28:		0.574597 s
stream_ahead.29:		0.564546 s
stream_ahead.3:		1.22078 s
stream_ahead.30:		0.612773 s
stream_ahead.31:		0.584305 s
stream_ahead.32:		0.498606 s
stream_ahead.33:		0.508559 s
stream_ahead.34:		0.571997 s
stream_ahead.35:		0.454076 s
stream_ahead.36:		0.53405 s
stream_ahead.37:		0.515117 s
stream_ahead.38:		0.491937 s
stream_ahead.39:		0.477251 s
stream_ahead.4:		1.02581 s
stream_ahead.40:		0.463398 s
stream_ahead.41:		0.450166 s
stream_ahead.42:		0.391431 s
stream_ahead.43:		0.483162 s
stream_ahead.44:		0.472792 s
stream_ahead.45:		0.437761 s
stream_ahead.46:		0.465468 s
stream_ahead.47:		0.778757 s
stream_ahead.48:		0.472473 s
stream_ahead.49:		0.450156 s
stream_ahead.5:		0.981568 s
stream_ahead.50:		0.434359 s
stream_ahead.51:		0.441183 s
stream_ahead.52:		0.431363 s
stream_ahead.53:		0.447362 s
stream_ahead.54:		0.433964 s
stream_ahead.55:		0.43492 s
stream_ahead.56:		0.434391 s
stream_ahead.57:		0.436819 s
stream_ahead.58:		0.415467 s
stream_ahead.59:		0.438904 s
stream_ahead.6:		1.0152 s
stream_ahead.60:		0.449833 s
stream_ahead.61:		0.442022 s
stream_ahead.62:		0.441275 s
stream_ahead.63:		0.422108 s
stream_ahead.64:		0.434379 s
stream_ahead.65:		0.426476 s
stream_ahead.66:		0.427691 s
stream_ahead.67:		0.4394 s
stream_ahead.68:		0.449128 s
stream_ahead.69:		0.449427 s
stream_ahead.7:		0.939548 s
stream_ahead.70:		0.445262 s
stream_ahead.71:		0.406951 s
stream_ahead.72:		0.425327 s
stream_ahead.73:		0.415944 s
stream_ahead.74:		0.416801 s
stream_ahead.75:		0.400356 s
stream_ahead.76:		0.440559 s
stream_ahead.77:		0.434099 s
stream_ahead.78:		0.463812 s
stream_ahead.79:		0.434007 s
stream_ahead.8:		0.88908 s
stream_ahead.80:		0.42329 s
stream_ahead.81:		0.411444 s
stream_ahead.82:		0.419151 s
stream_ahead.83:		0.422462 s
stream_ahead.84:		0.422407 s
stream_ahead.85:		0.418847 s
stream_ahead.86:		0.404092 s
stream_ahead.87:		0.411446 s
stream_ahead.88:		0.408019 s
stream_ahead.89:		0.405613 s
stream_ahead.9:		0.882797 s
stream_ahead.90:		0.399555 s
stream_ahead.91:		0.400314 s
stream_ahead.92:		0.398953 s
stream_ahead.93:		0.406695 s
stream_ahead.94:		0.411966 s
stream_ahead.95:		0.410433 s
stream_ahead.96:		0.417887 s
stream_ahead.97:		0.413941 s
stream_ahead.98:		0.418134 s
stream_ahead.99:		0.420066 s
[Other]
app:	sharder
Converged!

 === REPORT FOR BFS() ===
[Numeric]
cachesize_mb:		0
compression:		1
execthreads:		4
loadthreads:		4
membudget_mb:		375
niothreads:		2
niters:		2
nshards:		148
nvertices:		1.24836e+08
scheduler:		0
serialized-updates:		4.71874e+07	(count: 556, min: 84, max: 491957, avg: 84869.5)
stripesize:		1.07374e+09
updates:		2.49672e+08
work:		7.22427e+09
[Timings]
blockload:		13.3043s	 (count: 34378, min: 4e-06s, max: 0.01645, avg: 0.000387001s)
commit:		1.21409s	 (count: 476, min: 0.002189s, max: 0.004341, avg: 0.00255061s)
commit_thr:		83.776s	 (count: 25914, min: 0.002229s, max: 0.374445, avg: 0.00323285s)
execute-updates:		22.8432s	 (count: 556, min: 0.00018s, max: 0.119208, avg: 0.0410849s)
iomgr_init:		3.1e-05 s
memoryshard_create_edges:		95.934s	 (count: 556, min: 0.030364s, max: 0.306899, avg: 0.172543s)
memshard_commit:		1.77158s	 (count: 296, min: 0.003522s, max: 0.053178, avg: 0.00598506s)
preada_now:		29.2467s	 (count: 36674, min: 3e-06s, max: 0.019557, avg: 0.000797479s)
pwritea_now:		3.38385s	 (count: 1618, min: 1.3e-05s, max: 0.004536, avg: 0.00209138s)
read_next_vertices:		245.236s	 (count: 81732, min: 0s, max: 0.170406, avg: 0.00300049s)
runtime:		170.87 s
stripedio_wait_for_reads:		5.55426s	 (count: 556, min: 0s, max: 0.252749, avg: 0.00998967s)
stripedio_wait_for_writes:		26.2772s	 (count: 299, min: 0s, max: 0.474184, avg: 0.0878836s)
[Other]
app:	BFS
engine:	default
file:	/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt

 === REPORT FOR sharder() ===
[Numeric]
niothreads:		2
stripesize:		1.07374e+09
subwindow:		5e+06
[Timings]
blockload:		15.4973s	 (count: 18794, min: 2e-06s, max: 0.375155, avg: 0.000824586s)
degrees.runtime:		76.2685 s
edata_flush:		30.3443s	 (count: 13865, min: 0.001554s, max: 0.003854, avg: 0.00218855s)
execute_sharding:		401.807 s
finish_shard.sort:		55.7148s	 (count: 295, min: 0.13135s, max: 0.375892, avg: 0.188864s)
memoryshard_create_edges:		54.3606s	 (count: 296, min: 0.062181s, max: 0.292484, avg: 0.183651s)
preada_now:		17.4276s	 (count: 19550, min: 1e-06s, max: 0.375151, avg: 0.000891437s)
preprocessing:		281.706 s
read_next_vertices:		78.6056s	 (count: 87024, min: 0s, max: 0.378184, avg: 0.000903263s)
shard_final:		114.026s	 (count: 295, min: 0.303277s, max: 1.12824, avg: 0.386528s)
stream_ahead:		72.3734s	 (count: 296, min: 0.062221s, max: 0.961897, avg: 0.244505s)
stream_ahead.0:		0.961897 s
stream_ahead.1:		0.727543 s
stream_ahead.10:		0.619299 s
stream_ahead.100:		0.19937 s
stream_ahead.101:		0.203818 s
stream_ahead.102:		0.195478 s
stream_ahead.103:		0.163449 s
stream_ahead.104:		0.187707 s
stream_ahead.105:		0.189126 s
stream_ahead.106:		0.197712 s
stream_ahead.107:		0.198848 s
stream_ahead.108:		0.194919 s
stream_ahead.109:		0.197109 s
stream_ahead.11:		0.557602 s
stream_ahead.110:		0.19582 s
stream_ahead.111:		0.194049 s
stream_ahead.112:		0.158912 s
stream_ahead.113:		0.18479 s
stream_ahead.114:		0.199166 s
stream_ahead.115:		0.191703 s
stream_ahead.116:		0.19198 s
stream_ahead.117:		0.196867 s
stream_ahead.118:		0.184181 s
stream_ahead.119:		0.166366 s
stream_ahead.12:		0.563295 s
stream_ahead.120:		0.188551 s
stream_ahead.121:		0.183558 s
stream_ahead.122:		0.195873 s
stream_ahead.123:		0.186299 s
stream_ahead.124:		0.182099 s
stream_ahead.125:		0.18759 s
stream_ahead.126:		0.190661 s
stream_ahead.127:		0.192735 s
stream_ahead.128:		0.169364 s
stream_ahead.129:		0.197277 s
stream_ahead.13:		0.555264 s
stream_ahead.130:		0.192207 s
stream_ahead.131:		0.196152 s
stream_ahead.132:		0.192131 s
stream_ahead.133:		0.19355 s
stream_ahead.134:		0.188801 s
stream_ahead.135:		0.189739 s
stream_ahead.136:		0.196424 s
stream_ahead.137:		0.194497 s
stream_ahead.138:		0.200426 s
stream_ahead.139:		0.185956 s
stream_ahead.14:		0.550806 s
stream_ahead.140:		0.194759 s
stream_ahead.141:		0.191626 s
stream_ahead.142:		0.197535 s
stream_ahead.143:		0.198624 s
stream_ahead.144:		0.193815 s
stream_ahead.145:		0.196084 s
stream_ahead.146:		0.186431 s
stream_ahead.147:		0.194166 s
stream_ahead.148:		0.215016 s
stream_ahead.149:		0.213926 s
stream_ahead.15:		0.506445 s
stream_ahead.150:		0.211125 s
stream_ahead.151:		0.204237 s
stream_ahead.152:		0.175728 s
stream_ahead.153:		0.211646 s
stream_ahead.154:		0.210575 s
stream_ahead.155:		0.169541 s
stream_ahead.156:		0.206443 s
stream_ahead.157:		0.210237 s
stream_ahead.158:		0.210801 s
stream_ahead.159:		0.206547 s
stream_ahead.16:		0.518465 s
stream_ahead.160:		0.208536 s
stream_ahead.161:		0.209884 s
stream_ahead.162:		0.187073 s
stream_ahead.163:		0.188024 s
stream_ahead.164:		0.208296 s
stream_ahead.165:		0.210649 s
stream_ahead.166:		0.167141 s
stream_ahead.167:		0.204267 s
stream_ahead.168:		0.204335 s
stream_ahead.169:		0.202696 s
stream_ahead.17:		0.501576 s
stream_ahead.170:		0.202184 s
stream_ahead.171:		0.200941 s
stream_ahead.172:		0.19824 s
stream_ahead.173:		0.199233 s
stream_ahead.174:		0.19926 s
stream_ahead.175:		0.195887 s
stream_ahead.176:		0.200124 s
stream_ahead.177:		0.192292 s
stream_ahead.178:		0.178818 s
stream_ahead.179:		0.193332 s
stream_ahead.18:		0.509187 s
stream_ahead.180:		0.159329 s
stream_ahead.181:		0.192573 s
stream_ahead.182:		0.192136 s
stream_ahead.183:		0.188685 s
stream_ahead.184:		0.191077 s
stream_ahead.185:		0.187378 s
stream_ahead.186:		0.192002 s
stream_ahead.187:		0.180386 s
stream_ahead.188:		0.179141 s
stream_ahead.189:		0.17379 s
stream_ahead.19:		0.490982 s
stream_ahead.190:		0.177138 s
stream_ahead.191:		0.173711 s
stream_ahead.192:		0.175747 s
stream_ahead.193:		0.179791 s
stream_ahead.194:		0.1779 s
stream_ahead.195:		0.187254 s
stream_ahead.196:		0.168265 s
stream_ahead.197:		0.168114 s
stream_ahead.198:		0.164233 s
stream_ahead.199:		0.164969 s
stream_ahead.2:		0.712484 s
stream_ahead.20:		0.469541 s
stream_ahead.200:		0.165983 s
stream_ahead.201:		0.166726 s
stream_ahead.202:		0.159594 s
stream_ahead.203:		0.165812 s
stream_ahead.204:		0.165149 s
stream_ahead.205:		0.164812 s
stream_ahead.206:		0.16083 s
stream_ahead.207:		0.160753 s
stream_ahead.208:		0.165876 s
stream_ahead.209:		0.156501 s
stream_ahead.21:		0.43727 s
stream_ahead.210:		0.158853 s
stream_ahead.211:		0.159797 s
stream_ahead.212:		0.160165 s
stream_ahead.213:		0.162745 s
stream_ahead.214:		0.163146 s
stream_ahead.215:		0.159552 s
stream_ahead.216:		0.164962 s
stream_ahead.217:		0.167547 s
stream_ahead.218:		0.168424 s
stream_ahead.219:		0.16471 s
stream_ahead.22:		0.395452 s
stream_ahead.220:		0.159852 s
stream_ahead.221:		0.163456 s
stream_ahead.222:		0.169441 s
stream_ahead.223:		0.39999 s
stream_ahead.224:		0.174758 s
stream_ahead.225:		0.176464 s
stream_ahead.226:		0.175919 s
stream_ahead.227:		0.175996 s
stream_ahead.228:		0.176911 s
stream_ahead.229:		0.175086 s
stream_ahead.23:		0.410557 s
stream_ahead.230:		0.16887 s
stream_ahead.231:		0.164255 s
stream_ahead.232:		0.163224 s
stream_ahead.233:		0.16298 s
stream_ahead.234:		0.162833 s
stream_ahead.235:		0.159594 s
stream_ahead.236:		0.164025 s
stream_ahead.237:		0.163409 s
stream_ahead.238:		0.166038 s
stream_ahead.239:		0.163872 s
stream_ahead.24:		0.386065 s
stream_ahead.240:		0.169124 s
stream_ahead.241:		0.164014 s
stream_ahead.242:		0.165546 s
stream_ahead.243:		0.167043 s
stream_ahead.244:		0.165223 s
stream_ahead.245:		0.168005 s
stream_ahead.246:		0.169063 s
stream_ahead.247:		0.165513 s
stream_ahead.248:		0.165548 s
stream_ahead.249:		0.167989 s
stream_ahead.25:		0.395199 s
stream_ahead.250:		0.168518 s
stream_ahead.251:		0.171782 s
stream_ahead.252:		0.165062 s
stream_ahead.253:		0.168454 s
stream_ahead.254:		0.165874 s
stream_ahead.255:		0.164552 s
stream_ahead.256:		0.159918 s
stream_ahead.257:		0.160269 s
stream_ahead.258:		0.163042 s
stream_ahead.259:		0.162337 s
stream_ahead.26:		0.368728 s
stream_ahead.260:		0.161769 s
stream_ahead.261:		0.16119 s
stream_ahead.262:		0.161298 s
stream_ahead.263:		0.160012 s
stream_ahead.264:		0.159289 s
stream_ahead.265:		0.158803 s
stream_ahead.266:		0.160176 s
stream_ahead.267:		0.158401 s
stream_ahead.268:		0.153426 s
stream_ahead.269:		0.153678 s
stream_ahead.27:		0.430934 s
stream_ahead.270:		0.151653 s
stream_ahead.271:		0.152265 s
stream_ahead.272:		0.152282 s
stream_ahead.273:		0.152038 s
stream_ahead.274:		0.147846 s
stream_ahead.275:		0.148627 s
stream_ahead.276:		0.149005 s
stream_ahead.277:		0.146467 s
stream_ahead.278:		0.146877 s
stream_ahead.279:		0.143056 s
stream_ahead.28:		0.434318 s
stream_ahead.280:		0.143536 s
stream_ahead.281:		0.142726 s
stream_ahead.282:		0.142515 s
stream_ahead.283:		0.133921 s
stream_ahead.284:		0.133256 s
stream_ahead.285:		0.135828 s
stream_ahead.286:		0.136561 s
stream_ahead.287:		0.146005 s
stream_ahead.288:		0.129593 s
stream_ahead.289:		0.124706 s
stream_ahead.29:		0.436804 s
stream_ahead.290:		0.12423 s
stream_ahead.291:		0.124148 s
stream_ahead.292:		0.141449 s
stream_ahead.293:		0.181932 s
stream_ahead.294:		0.256456s	 (count: 2, min: 0.062221s, max: 0.194235, avg: 0.128228s)
stream_ahead.3:		0.836876 s
stream_ahead.30:		0.412338 s
stream_ahead.31:		0.436583 s
stream_ahead.32:		0.420495 s
stream_ahead.33:		0.411013 s
stream_ahead.34:		0.42439 s
stream_ahead.35:		0.399824 s
stream_ahead.36:		0.418272 s
stream_ahead.37:		0.395189 s
stream_ahead.38:		0.401796 s
stream_ahead.39:		0.378359 s
stream_ahead.4:		0.747816 s
stream_ahead.40:		0.380253 s
stream_ahead.41:		0.378722 s
stream_ahead.42:		0.373928 s
stream_ahead.43:		0.347523 s
stream_ahead.44:		0.370509 s
stream_ahead.45:		0.341742 s
stream_ahead.46:		0.352664 s
stream_ahead.47:		0.340376 s
stream_ahead.48:		0.367943 s
stream_ahead.49:		0.365041 s
stream_ahead.5:		0.739939 s
stream_ahead.50:		0.368928 s
stream_ahead.51:		0.383451 s
stream_ahead.52:		0.336503 s
stream_ahead.53:		0.364467 s
stream_ahead.54:		0.364673 s
stream_ahead.55:		0.36758 s
stream_ahead.56:		0.359271 s
stream_ahead.57:		0.34868 s
stream_ahead.58:		0.346894 s
stream_ahead.59:		0.328702 s
stream_ahead.6:		0.756375 s
stream_ahead.60:		0.319489 s
stream_ahead.61:		0.310978 s
stream_ahead.62:		0.303168 s
stream_ahead.63:		0.275002 s
stream_ahead.64:		0.27067 s
stream_ahead.65:		0.255529 s
stream_ahead.66:		0.277433 s
stream_ahead.67:		0.268884 s
stream_ahead.68:		0.251278 s
stream_ahead.69:		0.257043 s
stream_ahead.7:		0.706863 s
stream_ahead.70:		0.234533 s
stream_ahead.71:		0.220863 s
stream_ahead.72:		0.239722 s
stream_ahead.73:		0.250907 s
stream_ahead.74:		0.554411 s
stream_ahead.75:		0.215511 s
stream_ahead.76:		0.225827 s
stream_ahead.77:		0.23186 s
stream_ahead.78:		0.227968 s
stream_ahead.79:		0.230027 s
stream_ahead.8:		0.603954 s
stream_ahead.80:		0.22802 s
stream_ahead.81:		0.214361 s
stream_ahead.82:		0.216536 s
stream_ahead.83:		0.200037 s
stream_ahead.84:		0.216374 s
stream_ahead.85:		0.21174 s
stream_ahead.86:		0.212237 s
stream_ahead.87:		0.210183 s
stream_ahead.88:		0.212909 s
stream_ahead.89:		0.206946 s
stream_ahead.9:		0.636986 s
stream_ahead.90:		0.210331 s
stream_ahead.91:		0.207211 s
stream_ahead.92:		0.205929 s
stream_ahead.93:		0.205166 s
stream_ahead.94:		0.200551 s
stream_ahead.95:		0.201954 s
stream_ahead.96:		0.208879 s
stream_ahead.97:		0.200928 s
stream_ahead.98:		0.197474 s
stream_ahead.99:		0.1701 s
[Other]
app:	sharder
Converged!

 === REPORT FOR MinPath() ===
[Numeric]
cachesize_mb:		0
compression:		1
execthreads:		4
loadthreads:		4
membudget_mb:		375
niothreads:		2
niters:		2
nshards:		295
nvertices:		1.24836e+08
scheduler:		0
serialized-updates:		4.17463e+07	(count: 722, min: 55, max: 414775, avg: 57820.4)
stripesize:		1.07374e+09
updates:		2.49672e+08
work:		7.22427e+09
[Timings]
blockload:		13.7848s	 (count: 37588, min: 5e-06s, max: 0.025173, avg: 0.000366733s)
commit:		2.73829s	 (count: 1220, min: 0.001572s, max: 0.004369, avg: 0.0022445s)
commit_thr:		156.804s	 (count: 52796, min: 0.002224s, max: 0.036453, avg: 0.00296999s)
execute-updates:		24.383s	 (count: 722, min: 0.000123s, max: 0.092206, avg: 0.0337715s)
iomgr_init:		4e-05 s
memoryshard_create_edges:		95.2939s	 (count: 722, min: 0.024064s, max: 0.204637, avg: 0.131986s)
memshard_commit:		2.04493s	 (count: 590, min: 0.001724s, max: 0.049773, avg: 0.00346598s)
preada_now:		24.3666s	 (count: 40544, min: 3e-06s, max: 0.340819, avg: 0.000600991s)
pwritea_now:		5.11746s	 (count: 2597, min: 6e-06s, max: 0.00447, avg: 0.00197053s)
read_next_vertices:		274.937s	 (count: 212268, min: 0s, max: 0.133676, avg: 0.00129524s)
runtime:		213.137 s
stripedio_wait_for_reads:		7.57452s	 (count: 722, min: 0s, max: 0.181471, avg: 0.010491s)
stripedio_wait_for_writes:		60.5594s	 (count: 593, min: 0s, max: 0.261803, avg: 0.102124s)
[Other]
app:	MinPath
engine:	default
file:	/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt
Converged!
Total number of different labels (components/communities): 1
List of labels was written to file: /Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt.components
1. label: 101, size: 65608366

 === REPORT FOR connected-components() ===
[Numeric]
cachesize_mb:		0
compression:		1
execthreads:		8
loadthreads:		4
membudget_mb:		375
niothreads:		2
niters:		10
nshards:		148
nvertices:		1.24836e+08
scheduler:		0
serialized-updates:		2.35937e+08	(count: 2780, min: 84, max: 491957, avg: 84869.5)
stripesize:		1.07374e+09
updates:		1.24836e+09
work:		3.61213e+10
[Timings]
blockload:		96.7738s	 (count: 171890, min: 5e-06s, max: 0.057637, avg: 0.000562999s)
commit:		8.09838s	 (count: 2380, min: 0.002186s, max: 0.022538, avg: 0.00340268s)
commit_thr:		590.384s	 (count: 129570, min: 0.002224s, max: 0.326131, avg: 0.00455649s)
execute-updates:		309.135s	 (count: 2780, min: 0.002695s, max: 0.27266, avg: 0.1112s)
iomgr_init:		3.8e-05 s
label-analysis:		1.05183 s
memoryshard_create_edges:		545.593s	 (count: 2780, min: 0.030635s, max: 0.365649, avg: 0.196256s)
memshard_commit:		12.3927s	 (count: 1480, min: 0.003658s, max: 0.068318, avg: 0.00837348s)
preada_now:		188.489s	 (count: 183370, min: 4e-06s, max: 0.152575, avg: 0.00102792s)
pwritea_now:		23.9236s	 (count: 9480, min: 9e-06s, max: 0.032709, avg: 0.00252358s)
read_next_vertices:		1373.85s	 (count: 408660, min: 0s, max: 0.178058, avg: 0.00336185s)
runtime:		1194.89 s
stripedio_wait_for_reads:		23.7514s	 (count: 2780, min: 0s, max: 0.4432, avg: 0.00854365s)
stripedio_wait_for_writes:		187.431s	 (count: 1491, min: 0s, max: 0.916136, avg: 0.125708s)
[Other]
app:	connected-components
engine:	default
file:	/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt
