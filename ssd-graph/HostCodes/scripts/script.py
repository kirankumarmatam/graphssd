#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_to_run = ["facebook_combined.txt", "sx-stackoverflow.txt"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/facebook_flash.txt", "ProcessedGraphs/GraphSSDLayout/sx-stackoverflow_flash.txt"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/facebook_gtl.txt", "ProcessedGraphs/GraphSSDLayout/sx-stackoverflow_gtl.txt"]
Output_file = "/home/ossd/GraphSSD/HostCodes/output.txt"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("getAdjList_graphSSD")
	command = "sh compile.sh;sudo ./getAdjList /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 >> "+Output_file
	print command
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system(command)
	os.chdir("../getAdjEdgeWeights_graphSSD")
	os.system("sh compile.sh;sudo ./getEdgeWeights /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 >> "+Output_file)
	os.chdir("../BFS_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 >> "+Output_file)
	os.chdir("../WCC_graphSSD")
	os.system("sh compile.sh;sudo ./WCC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 >> "+Output_file)
	os.chdir("../MINPATH_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 >> "+Output_file)


os.system("echo BaselineResults >> "+Output_file)

Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/facebook_csr.txt", "ProcessedGraphs/CSRLayout/sx-stackoverflow_flash.txt"]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo Baseline_library/storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	os.chdir("getAdjList_SSD")
	os.system("sh compile_getAdjList.sh;sudo ./getAdjEdgeList /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.chdir("../getAdjEdgeWeights_SSD")
	os.system("sh compile_getEdgeWeightsSSD.sh; sudo ./getEdgeWeights /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.chdir("../BFS_SSD")
	os.system("sh compile_bfsSSD.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.chdir("../WCC_TEST")
	os.system("sh compile_wccSSD.sh; sudo ./WCC_TEST /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.chdir("../MINPATH_SSD")
	os.system("sh compile_minpathSSD.sh; sudo ./MINPATH /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)

Graphs_graphChi = ["/Datasets/Benchmarks/SNAP/GraphChi/facebook_combined.txt", "/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make myapps/BFS")
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 4000 filetype edgelist >> " + Output_file)
	os.system("make myapps/MinPath")
	os.system("./bin/myapps/MinPath file "+ Graphs_graphChi[i] + " root 0 filetype edgelist >> " + Output_file)
	os.system("make example_apps/connectedcomponents")
	os.system("./bin/example_apps/connectedcomponents file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("make myapps/streaming")
	os.system("./bin/myapps/streaming file "+ Graphs_graphChi[i] + " filetype edgelist streaming_graph_file temp.txt >> " + Output_file)

