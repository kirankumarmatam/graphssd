#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_updated_to_run = ["graph500_128M_updated.net"]
Graphs_updated_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_updated_flash.net"]
Graphs_updated_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_updated_gtl.net"]
Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
for i in range(len(Graphs_updated_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("BFS_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_updated_to_run[i]+" "+Dataset_directory+Graphs_updated_flash_to_run[i]+" "+Dataset_directory+Graphs_updated_gtl_to_run[i]+" 1 4000  >> "+Output_file)

Graphs_to_run = ["graph500_128M.net"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_flash.net"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_gtl.net"]
Graphs_update_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_update.net"]

for i in range(len(Graphs_to_run)):
	os.chdir("../update_graphSSD");
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 "+Dataset_directory+Graphs_update_to_run[i]+"  >> "+Output_file)

