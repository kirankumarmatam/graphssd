#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_to_run = ["sx-stackoverflow.txt"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/sx-stackoverflow_flash.txt"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/sx-stackoverflow_gtl.txt"]
Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("BFS_graphSSD")
	os.system("sed -i 's/AuxMemoryInStorage 1$/AuxMemoryInStorage 0/g' ../GraphSSD_library/graphFiltering.h")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 1111  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 7750  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 4000  >> "+Output_file)
	os.system("sed -i 's/AuxMemoryInStorage 0$/AuxMemoryInStorage 1/g' ../GraphSSD_library/graphFiltering.h")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 1111  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 7750  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 4000  >> "+Output_file)
	os.system("sed -i 's/AuxDataValueType bool$/AuxDataValueType unsigned int/g' ../GraphSSD_library/graphFiltering.h")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 1111  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 7750  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 4000  >> "+Output_file)
	os.system("sed -i 's/AuxDataValueType unsigned int$/AuxDataValueType bool/g' ../GraphSSD_library/graphFiltering.h")
	os.system("sed -i 's/cache_percentage = 5/cache_percentage = 10/g' ../GraphSSD_library/store_graph.cpp")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 4000  >> "+Output_file)
	os.system("sed -i 's/cache_percentage = 10/cache_percentage = 5/g' ../GraphSSD_library/store_graph.cpp")

