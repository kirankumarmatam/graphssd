#!/bin/bash

import os

Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)

Graphs_graphChi = ["/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make myapps/BFS")
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 1111 filetype edgelist >> " + Output_file)
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 7750 filetype edgelist >> " + Output_file)
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 4000 filetype edgelist >> " + Output_file)

