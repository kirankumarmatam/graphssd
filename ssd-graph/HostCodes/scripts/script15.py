#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)

Graphs_to_run = ["MediumGraphs/sx-stackoverflow.net", "MediumGraphs/com-orkut.all.cmty.net"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/sx_flash.net", "ProcessedGraphs/GraphSSDLayout/com_orkut_flash.net"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/sx_gtl.net", "ProcessedGraphs/GraphSSDLayout/com_orkut_gtl.net"]
Graphs_update_to_run = ["ProcessedGraphs/GraphSSDLayout/sx_update.net", "ProcessedGraphs/GraphSSDLayout/com_orkut_update.net"]

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/update_graphSSD_debug");
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 "+Dataset_directory+Graphs_update_to_run[i]+"  >> "+Output_file)

Graphs_to_run = ["graph500_128M.net"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_flash.net"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_gtl.net"]
Graphs_update_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_update.net"]

#Graphs_to_run = ["facebook_combined.txt"]
#Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/facebook_flash.txt"]
#Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/facebook_gtl.txt"]
#Graphs_update_to_run = ["ProcessedGraphs/GraphSSDLayout/graph500_128M_update.net"]

#Graphs_to_run = ["soc-LiveJournal1.net"]
#Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/soc_flash.net"]
#Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/soc_gtl.net"]
#Graphs_update_to_run = ["ProcessedGraphs/GraphSSDLayout/soc_update.net"]

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/update_graphSSD_incremental");
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 "+Dataset_directory+Graphs_update_to_run[i]+"  >> "+Output_file)

