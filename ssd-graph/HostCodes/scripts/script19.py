#!/bin/bash

import os

Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
os.system("cp -R /Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt* /home/ossd/ssd")
os.system("cp -R /Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.net /home/ossd/ssd;mv /home/ossd/ssd/com-friendster.ungraph.net /home/ossd/ssd/com-friendster.ungraph.txt")
os.system("cgcreate -g memory,cpu:graphChiGroup")
os.system("cgset -r memory.limit_in_bytes=$((1024*1024*1024)) graphChiGroup")

Graphs_graphChi = ["/home/ossd/ssd/com-friendster.ungraph.txt"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make myapps/BFS")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 1111 filetype edgelist >> " + Output_file)
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 7750 filetype edgelist >> " + Output_file)
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 4000 filetype edgelist >> " + Output_file)

