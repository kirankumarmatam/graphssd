#!/bin/bash

import os

Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
os.system("cp -R /Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt* /home/ossd/ssd")
os.system("cp -R /Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.net /home/ossd/ssd;mv /home/ossd/ssd/com-friendster.ungraph.net /home/ossd/ssd/com-friendster.ungraph.txt")

Graphs_graphChi = ["/home/ossd/ssd/com-friendster.ungraph.txt"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make myapps/BFS")
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 101 target 1212 filetype edgelist >> " + Output_file)
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 101 target 7851 filetype edgelist >> " + Output_file)
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 101 target 4101 filetype edgelist >> " + Output_file)

