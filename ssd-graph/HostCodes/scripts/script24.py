#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_to_run = ["com-orkut.all.cmty.txt"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/com_orkut_undirected_sorted_flash.net"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/com_orkut_undirected_sorted_gtl.net"]
Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("BFS_graphSSD")
#	os.system("sed -i 's/cache_percentage = 5/cache_percentage = 40/g' ../GraphSSD_library/store_graph.cpp")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 1111  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 7750  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 4000  >> "+Output_file)
	os.chdir("../WCC_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 >> "+Output_file)
	os.chdir("../TRICOUNT_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0  >> "+Output_file)
#	os.system("sed -i 's/cache_percentage = 40/cache_percentage = 5/g' ../GraphSSD_library/store_graph.cpp")

os.system("echo BaselineResults >> "+Output_file)

Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/Undirected_graphs/com-orkut.all.cmty_undirected_sorted_flash.txt"]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo Baseline_library/storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	os.chdir("BFS_SSD")
#	os.system("sed -i 's/AuxDataValueType unsigned int/AuxDataValueType bool/g' ../Baseline_library/graphFiltering.h")
#	os.system("sed -i 's/cache_percentage = 5/cache_percentage = 40/g' ../Baseline_library/GTL.cpp")
	os.system("sh compile_bfsSSD.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 1111  >> "+Output_file)
	os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 7750  >> "+Output_file)
	os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 4000  >> "+Output_file)
	os.chdir("../WCC_TEST")
	os.system("sh compile_wccSSD.sh;sudo ./WCC_TEST /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.chdir("../TRICOUNT_SSD")
	os.system("sh compile_tricountSSD.sh;sudo ./TRICOUNT /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
#	os.system("sed -i 's/cache_percentage = 40/cache_percentage = 5/g' ../Baseline_library/GTL.cpp")
#	os.system("sed -i 's/AuxDataValueType bool/AuxDataValueType unsigned int/g' ../Baseline_library/graphFiltering.h")
