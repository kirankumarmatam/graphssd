#!/bin/bash

import os

Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
os.system("cp conf/graphchi_template.cnf conf/graphchi.cnf")
os.system("sed -i 's/MEM_BUDGET/200/g' conf/graphchi.cnf")
os.system("make myapps/BFS;./bin/myapps/BFS file /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt root 2 target 2163 filetype edgelist >> temp.txt")
os.system("cp -R /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt* /home/ossd/ssd")

os.system("cgcreate -g memory,cpu:graphChiGroup")
os.system("cgset -r memory.limit_in_bytes=$((250*1024*1024)) graphChiGroup")

Graphs_graphChi = ["/home/ossd/ssd/graph500_32M.txt"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make myapps/BFS")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 2 target 2163 filetype edgelist >> " + Output_file)
	os.system("make myapps/BFS")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 2 target 15336 filetype edgelist >> " + Output_file)
	os.system("make myapps/BFS")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 2 target 7917 filetype edgelist >> " + Output_file)
	os.system("make example_apps/connectedcomponents")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/connectedcomponents file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("make myapps/MinPath")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/MinPath file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("rm -r /home/ossd/ssd/*")

os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
os.system("cp conf/graphchi_template.cnf conf/graphchi.cnf")
os.system("sed -i 's/MEM_BUDGET/28/g' conf/graphchi.cnf")
os.system("./bin/myapps/BFS file /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net root 2 target 2163 filetype edgelist >> temp.txt")
os.system("cp -R /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net* /home/ossd/ssd")

os.system("cgset -r memory.limit_in_bytes=$((40*1024*1024)) graphChiGroup")
Graphs_graphChi = ["/home/ossd/ssd/soc-LiveJournal1.net"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make example_apps/trianglecounting")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/trianglecounting file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)

