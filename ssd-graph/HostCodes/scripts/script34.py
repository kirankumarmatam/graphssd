#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_flash.net","ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_flash.txt"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_gtl.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_gtl.txt"]
Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("BFS_graphSSD")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
#	os.system("sed -i 's/cache_percentage = 5/cache_percentage = 40/g' ../GraphSSD_library/store_graph.cpp")
	if(i == 0):
		os.system("echo BFS_graphSSD >> " + Output_file)
		os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 1  >> "+Output_file)
		os.system("echo BFS_graphSSD >> " + Output_file)
		os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 135  >> "+Output_file)
		os.system("echo BFS_graphSSD >> " + Output_file)
		os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 112  >> "+Output_file)
	elif(i == 1):
		os.system("echo BFS_graphSSD >> " + Output_file)
		os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 898614  >> "+Output_file)
		os.system("echo BFS_graphSSD >> " + Output_file)
		os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 8348 >> "+Output_file)
		os.system("echo BFS_graphSSD >> " + Output_file)
		os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 27530 >> "+Output_file)
	os.chdir("../WCC_graphSSD")
	os.system("echo WCC_graphSSD >> " + Output_file)
	os.system("sh compile.sh;sudo ./WCC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 >> "+Output_file)
	os.system("echo GRAPHCOLOR_graphSSD >> " + Output_file)
	os.system("sed -i 's/AuxDataValueType bool/AuxDataValueType short int/g' ../GraphSSD_library/graphFiltering.h")
	os.chdir("../GRAPHCOLOR_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0  >> "+Output_file)
	os.system("sed -i 's/AuxDataValueType short int/AuxDataValueType bool/g' ../GraphSSD_library/graphFiltering.h")

os.system("echo BaselineResults >> "+Output_file)

Graphs_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt"]
Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/Undirected_graphs/soc-LiveJournal1_undirected_sorted_flash.net", "ProcessedGraphs/CSRLayout/Undirected_graphs/graph500_32M_basline_undirected_sorted_flash.txt"]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo Baseline_library/storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	os.chdir("BFS_SSD")
#	os.system("sed -i 's/AuxDataValueType unsigned int/AuxDataValueType bool/g' ../Baseline_library/graphFiltering.h")
#	os.system("sed -i 's/cache_percentage = 5/cache_percentage = 40/g' ../Baseline_library/GTL.cpp")
#adjust macros
	if(i == 0):
		os.system("echo BFS_SSD >> " + Output_file)
		os.system("sh compile_bfsSSD.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 1  >> "+Output_file)
		os.system("echo BFS_SSD >> " + Output_file)
		os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 135  >> "+Output_file)
		os.system("echo BFS_SSD >> " + Output_file)
		os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 112  >> "+Output_file)
	elif(i == 1):
		os.system("echo BFS_SSD >> " + Output_file)
		os.system("sh compile_bfsSSD.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 898614  >> "+Output_file)
		os.system("echo BFS_SSD >> " + Output_file)
		os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 8348 >> "+Output_file)
		os.system("echo BFS_SSD >> " + Output_file)
		os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 27530 >> "+Output_file)
	os.chdir("../WCC_TEST")
	os.system("echo WCC_TEST >> " + Output_file)
	os.system("sh compile_wccSSD.sh;sudo ./WCC_TEST /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.chdir("../GRAPHCOLOR_SSD")
	os.chdir("echo GRAPHCOLOR_SSD")
	os.system("sed -i 's/AuxDataValueType bool/AuxDataValueType short int/g' ../Baseline_library/graphFiltering.h")
	os.system("sh compile_graphcolorSSD.sh;sudo ./GRAPHCOLOR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.system("sed -i 's/AuxDataValueType short int/AuxDataValueType bool/g' ../Baseline_library/graphFiltering.h")
