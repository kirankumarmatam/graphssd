#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_flash.net","ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_flash.txt"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_gtl.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_gtl.txt"]
Graphs_mapping_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_mapping.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_mapping.txt"]
Graphs_output_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_levelNumber.net", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/graph500_32M_undirected_sorted_levelNumber.txt"]
Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("print_levelNum")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sed -i 's/cache_percentage = 5/cache_percentage = 100/g' ../GraphSSD_library/store_graph.cpp")
	os.system("sed -i 's/AuxMemoryInStorage 1/AuxMemoryInStorage 0/g' ../GraphSSD_library/graphFiltering.h")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 11111111 "+Dataset_directory+Graphs_mapping_to_run[i] + " >> "+Dataset_directory+Graphs_output_to_run[i])
	os.system("sed -i 's/cache_percentage = 100/cache_percentage = 5/g' ../GraphSSD_library/store_graph.cpp")
	os.system("sed -i 's/AuxMemoryInStorage 0/AuxMemoryInStorage 1/g' ../GraphSSD_library/graphFiltering.h")
