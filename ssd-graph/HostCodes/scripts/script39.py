#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)

os.system("echo BaselineResults >> "+Output_file)

Graphs_to_run = ["roadNet-CA.txt"]
Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/Undirected_graphs/roadNet-CA_undirected_sorted_flash.txt"]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo Baseline_library/storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	os.chdir("BFS_SSD")
	os.system("echo BFS_SSD >> " + Output_file)
	os.system("sh compile_bfsSSD.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 1  >> "+Output_file)
	os.system("echo BFS_SSD >> " + Output_file)
	os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 1795625 >> "+Output_file)
	os.system("echo BFS_SSD >> " + Output_file)
	os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 1787037 >> "+Output_file)
	os.system("echo BFS_SSD >> " + Output_file)
	os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 111111111 >> "+Output_file)
	os.chdir("../WCC_TEST")
	os.system("echo WCC_TEST >> " + Output_file)
	os.system("sh compile_wccSSD.sh;sudo ./WCC_TEST /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)

Graphs_to_run = ["roadNet-CA.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_flash.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_flash.net"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/Undirected_graphs/roadNet-CA_gtl.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/soc_undirected_sorting_gtl.net" ]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("TRICOUNT_graphSSD")
	os.system("echo TRICOUNT_graphSSD >> "+ Output_file)
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1  >> "+Output_file)

os.system("echo BaselineResults >> "+Output_file)

Graphs_to_run = ["roadNet-CA.txt", "ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net"]
Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/Undirected_graphs/roadNet-CA_undirected_sorted_flash.txt", "ProcessedGraphs/CSRLayout/Undirected_graphs/soc-LiveJournal1_undirected_sorted_flash.net"]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo Baseline_library/storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	os.chdir("TRICOUNT_SSD")
	os.system("echo TRICOUNT >> " + Output_file)
        os.system("sh compile_tricountSSD.sh;sudo ./TRICOUNT /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)

