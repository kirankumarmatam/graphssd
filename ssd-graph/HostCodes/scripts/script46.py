#!/bin/bash

import os

Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)

os.system("cgcreate -g memory,cpu:graphChiGroup")

os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
os.system("cp conf/graphchi_template.cnf conf/graphchi.cnf")
os.system("sed -i 's/MEM_BUDGET/28/g' conf/graphchi.cnf")
os.system("cgset -r memory.limit_in_bytes=$((40*1024*1024)) graphChiGroup")
os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net root 2 target 2163 filetype edgelist >> temp.txt")
os.system("cp -R /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net* /home/ossd/ssd")

Graphs_graphChi = ["/home/ossd/ssd/soc-LiveJournal1.net"]

os.system("echo soc-LiveJournal1 connectedcomponents >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_initial /dev/nvme0n1 >> " + Output_file)
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make example_apps/connectedcomponents")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/connectedcomponents file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("rm -r /home/ossd/ssd/*")
os.system("echo soc-LiveJournal1 connectedcomponents >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_final /dev/nvme0n1 >> " + Output_file)

os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
os.system("cp conf/graphchi_template.cnf conf/graphchi.cnf")
os.system("sed -i 's/MEM_BUDGET/28/g' conf/graphchi.cnf")
os.system("cgset -r memory.limit_in_bytes=$((40*1024*1024)) graphChiGroup")
os.system("cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net root 2 target 2163 filetype edgelist >> temp.txt")
os.system("cp -R /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net* /home/ossd/ssd")

Graphs_graphChi = ["/home/ossd/ssd/soc-LiveJournal1.net"]
os.system("echo soc-LiveJournal1 trianglecounting >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_initial /dev/nvme0n1 >> " + Output_file)
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make example_apps/trianglecounting")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/trianglecounting file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("rm -r /home/ossd/ssd/*")
os.system("echo soc-LiveJournal1 trianglecounting >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_final /dev/nvme0n1 >> " + Output_file)


os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
os.system("cp conf/graphchi_template.cnf conf/graphchi.cnf")
os.system("sed -i 's/MEM_BUDGET/2/g' conf/graphchi.cnf")
os.system("cgset -r memory.limit_in_bytes=$((5*1024*1024)) graphChiGroup")
os.system("make myapps/BFS;cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file /Datasets/Benchmarks/SNAP/roadNet-CA.txt root 2 target 2163 filetype edgelist >> temp.txt")
os.system("cp -R /Datasets/Benchmarks/SNAP/roadNet-CA.txt* /home/ossd/ssd")


os.system("echo roadNet triangleCounting >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_initial /dev/nvme0n1 >> " + Output_file)
Graphs_graphChi = ["/home/ossd/ssd/roadNet-CA.txt"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make example_apps/trianglecounting")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/trianglecounting file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("rm -r /home/ossd/ssd/*")
os.system("echo roadNet triangleCounting >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_final /dev/nvme0n1 >> " + Output_file)

os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
os.system("cp conf/graphchi_template.cnf conf/graphchi.cnf")
os.system("sed -i 's/MEM_BUDGET/2/g' conf/graphchi.cnf")
os.system("cgset -r memory.limit_in_bytes=$((5*1024*1024)) graphChiGroup")
os.system("make myapps/BFS;cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file /Datasets/Benchmarks/SNAP/roadNet-CA.txt root 2 target 2163 filetype edgelist >> temp.txt")
os.system("cp -R /Datasets/Benchmarks/SNAP/roadNet-CA.txt* /home/ossd/ssd")


os.system("echo roadNet connectedcomponents >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_initial /dev/nvme0n1 >> " + Output_file)
Graphs_graphChi = ["/home/ossd/ssd/roadNet-CA.txt"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make example_apps/connectedcomponents")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/connectedcomponents file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("rm -r /home/ossd/ssd/*")
os.system("echo roadNet connectedcomponents >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_final /dev/nvme0n1 >> " + Output_file)

os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
os.system("cp conf/graphchi_template.cnf conf/graphchi.cnf")
os.system("sed -i 's/MEM_BUDGET/225/g' conf/graphchi.cnf")
os.system("cgset -r memory.limit_in_bytes=$((250*1024*1024)) graphChiGroup")
os.system("make myapps/BFS;cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt root 2 target 2163 filetype edgelist >> temp.txt")
os.system("cp -R /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/graph500_32M.txt* /home/ossd/ssd")


Graphs_graphChi = ["/home/ossd/ssd/graph500_32M.txt"]
os.system("echo graph500_32M connectedcomponents >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_initial /dev/nvme0n1 >> " + Output_file)
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make example_apps/connectedcomponents")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/connectedcomponents file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("rm -r /home/ossd/ssd/*")
os.system("echo graph500_32M connectedcomponents >> " + Output_file)
os.system("/home/ossd/GraphSSD/HostCodes/measure_readPages/graphFiltering_final /dev/nvme0n1 >> " + Output_file)
