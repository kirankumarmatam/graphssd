#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_to_run = ["sx-stackoverflow.txt"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/sx-stackoverflow_flash.txt"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/sx-stackoverflow_gtl.txt"]
Output_file = "/home/ossd/GraphSSD/HostCodes/output.txt"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("BFS_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 1111  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 7750  >> "+Output_file)
	os.system("sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 4000  >> "+Output_file)
	os.system("sed -i 's/cache_percentage = 5/cache_percentage = 10/g' ../GraphSSD_library/store_graph.cpp")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 4000  >> "+Output_file)
	os.system("sed -i 's/cache_percentage = 10/cache_percentage = 5/g' ../GraphSSD_library/store_graph.cpp")

os.system("echo BaselineResults >> "+Output_file)

Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/sx-stackoverflow_flash.txt"]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo Baseline_library/storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	os.chdir("BFS_SSD")
	os.system("sh compile_bfsSSD.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 1111  >> "+Output_file)
	os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 7750  >> "+Output_file)
	os.system("sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 4000  >> "+Output_file)
	os.system("sed -i 's/cache_percentage = 5/cache_percentage = 10/g' ../Baseline_library/GTL.cpp")
	os.system("sh compile_bfsSSD.sh; sudo ./BFS /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" 4000  >> "+Output_file)
	os.system("sed -i 's/cache_percentage = 10/cache_percentage = 5/g' ../Baseline_library/GTL.cpp")

Graphs_graphChi = ["/Datasets/Benchmarks/SNAP/GraphChi/com-friendster.ungraph.txt"]
for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make myapps/BFS")
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 1111 filetype edgelist >> " + Output_file)
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 7750 filetype edgelist >> " + Output_file)
	os.system("./bin/myapps/BFS file "+ Graphs_graphChi[i] + " root 0 target 4000 filetype edgelist >> " + Output_file)

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.chdir("WCC_graphSSD")
	os.system("sed -i 's/AuxDataValueType bool/AuxDataValueType unsigned int/g' ../GraphSSD_library/graphFiltering.h")
	os.system("sh compile.sh;sudo ./WCC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 >> "+Output_file)
	os.system("sed -i 's/AuxDataValueType unsigned int/AuxDataValueType bool/g' ../GraphSSD_library/graphFiltering.h")


