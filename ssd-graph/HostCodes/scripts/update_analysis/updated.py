import numpy as np
import math

def read_file(fname='youtube_add_result.txt'):
    f=open(fname,'r')
    i=0
    iniv=[]
    addv=[]
    adde=[]
    for line in f.readlines():
        if line[0:3]=='Ext':
            line=filter(lambda ch:ch in '0123456789 ',line)
            nums=[] 
            nums.extend(line.strip().split("   "))
            newv=int(nums[0])
            newe=int(nums[1])
            continue
        elif line[0:3]!='Num':
            continue
        line=filter(lambda ch:ch in '0123456789 ',line)
        nums=[] 
        nums.extend(line.strip().split("   "))
        iniv.append(int(nums[1]))
        addv.append(int(nums[2]))
        adde.append(int(nums[3]))
    f.close()
    return iniv,addv,adde,newv,newe

def add_page(iniv,addv,adde,newv,newe):
    newp=int(math.ceil((newv+newe)*4.0/(16*1024)))
    v=int(math.ceil((newv+0.0)/newp))
    for i in range(0,newp-1):
        iniv.append(v)
        addv.append(0)
        adde.append(0)
    iniv.append(newv-v*(newp-1))
    addv.append(0)
    adde.append(0)
    return iniv,addv,adde,0,0

def merge(iniv,addv,adde,pages,threshold):
    idx=(-np.array(addv)).argsort()[:pages].tolist()
    for i in idx:
        if adde[i]>threshold:
            tmp1=(iniv[i]+addv[i])/2
            tmp2=iniv[i]+addv[i]-tmp1
            iniv[i]=tmp1
            addv[i]=0
            adde[i]=0
            iniv.append(tmp2)
            addv.append(0)
            adde.append(0)
        else:
            iniv[i]+=addv[i]
            addv[i]=0
            adde[i]=0
    return iniv,addv,adde
def cal_g(iniv,addv,newv,total_access):
    iniv_a=np.array(iniv)
    addv_a=np.array(addv)
    clean_id=np.where(addv_a==0)[0].tolist()
    total_v=np.sum(iniv_a)+newv
    updated_v=np.sum(addv_a)
    clean_v=np.sum(iniv_a[clean_id])
    dirty_v=total_v-updated_v-clean_v-newv
    return (newv*2+updated_v*3+clean_v*1+dirty_v*2.0)/total_v*total_access
    
def cal_b(iniv,addv,newv,total_access):
    iniv_a=np.array(iniv)
    addv_a=np.array(addv)
    clean_id=np.where(addv_a==0)[0].tolist()
    total_v=np.sum(iniv_a)+newv
    updated_v=np.sum(addv_a)
    clean_v=total_v-updated_v
    return (updated_v*3+clean_v*2.0)/total_v*total_access

if __name__ == '__main__':
    iniv=[]
    addv=[]
    adde=[]
    total_access=1000
    per=0.1
    threshold=20000

    g=[total_access]
    b=[total_access*2]
    total_pages=[]
    
    iniv,addv,adde,newv,newe=read_file('youtube_add_result.txt')

    g.append(cal_g(iniv,addv,newv,total_access))
    b.append(cal_b(iniv,addv,newv,total_access))
    initial_pages=len(iniv)
    total_pages.append(initial_pages)
    total_pages.append(initial_pages)
    total_pages_b=math.ceil((newv+newe+np.sum(adde))*4.0/(16*1024))+initial_pages
    upated_pages=len(np.where(np.array(addv)!=0)[0])

    iniv,addv,adde,newv,newe=add_page(iniv,addv,adde,newv,newe)
    g.append(cal_g(iniv,addv,newv,total_access))
    total_pages.append(len(iniv))
    addde_pages=len(iniv)-initial_pages

    total_upated_pages=upated_pages+addde_pages
    batch=int(math.floor(total_upated_pages*per))
    n=int(math.ceil(total_upated_pages/(batch+0.0)))
    for i in range (0,n):
        if i!=(n-1):
            b_tmp=batch
        else:
            b_tmp=total_upated_pages-(n-1)*batch
        iniv,addv,adde=merge(iniv,addv,adde,b_tmp,threshold)
        total_pages.append(len(iniv))
        g.append(cal_g(iniv,addv,newv,total_access))
    print total_upated_pages
    print b
    print total_pages_b
    print g
    print total_pages
