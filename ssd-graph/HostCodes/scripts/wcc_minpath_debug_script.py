#!/bin/bash

import os

Dataset_directory = "/Datasets/Benchmarks/SNAP/"
Graphs_to_run = ["GraphChi/com-friendster.ungraph.net"]
Graphs_flash_to_run = ["ProcessedGraphs/GraphSSDLayout/sx-stackoverflow_flash.txt"]
Graphs_gtl_to_run = ["ProcessedGraphs/GraphSSDLayout/sx-stackoverflow_gtl.txt"]
Output_file = "/home/ossd/GraphSSD/HostCodes/output.txt"
os.system("rm "+Output_file)
os.system("touch "+Output_file)
os.system("echo BaselineResults >> "+Output_file)

Graphs_flash_to_run_baseline = ["ProcessedGraphs/CSRLayout/sx-stackoverflow_flash.txt"]
for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.system("sudo Baseline_library/storeInCSR /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i])
	os.system("sed -i 's/AuxDataValueType bool$/AuxDataValueType unsigned int/g' Baseline_library/graphFiltering.h")
	os.chdir("WCC_TEST")
	os.system("sh compile_wccSSD.sh; sudo ./WCC_TEST /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.chdir("../MINPATH_SSD")
	os.system("sh compile_minpathSSD.sh; sudo ./MINPATH /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.system("sed -i 's/AuxMemoryInStorage 1$/AuxMemoryInStorage 0/g' ../Baseline_library/graphFiltering.h")
	os.chdir("../WCC_TEST")
	os.system("sh compile_wccSSD.sh; sudo ./WCC_TEST /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.chdir("../MINPATH_SSD")
	os.system("sh compile_minpathSSD.sh; sudo ./MINPATH /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run_baseline[i]+" >> "+Output_file)
	os.system("sed -i 's/AuxMemoryInStorage 0$/AuxMemoryInStorage 1/g' ../Baseline_library/graphFiltering.h")

for i in range(len(Graphs_to_run)):
	os.chdir("/home/ossd/GraphSSD/HostCodes")
	os.system("echo "+Graphs_to_run[i]+" >> "+Output_file)
	os.chdir("WCC_graphSSD")
	os.system("sh compile.sh;sudo ./WCC /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 1 >> "+Output_file)
	os.chdir("../MINPATH_graphSSD")
	os.system("sh compile.sh;sudo ./graphFiltering /dev/nvme0n1 "+Dataset_directory+Graphs_to_run[i]+" "+Dataset_directory+Graphs_flash_to_run[i]+" "+Dataset_directory+Graphs_gtl_to_run[i]+" 0 >> "+Output_file)

