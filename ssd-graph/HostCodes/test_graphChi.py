#!/bin/bash

import os

Output_file = "/home/ossd/GraphSSD/HostCodes/output.net"
os.system("rm "+Output_file)
os.system("touch "+Output_file)

os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
os.system("cp conf/graphchi_template.cnf conf/graphchi.cnf")
os.system("sed -i 's/MEM_BUDGET/28/g' conf/graphchi.cnf")

os.system("cgcreate -g memory,cpu:graphChiGroup")
os.system("cgset -r memory.limit_in_bytes=$((40*1024*1024)) graphChiGroup")


os.system("make myapps/BFS;cgexec -g memory,cpu:graphChiGroup ./bin/myapps/BFS file /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net root 2 target 2163 filetype edgelist >> temp.txt")
os.system("cp -R /Datasets/Benchmarks/SNAP/ProcessedGraphs/GraphSSDLayout/Undirected_graphs/GraphChi/soc-LiveJournal1.net* /Datasets/test")

Graphs_graphChi = ["/Datasets/test/soc-LiveJournal1.net"]

for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make example_apps/connectedcomponents")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/connectedcomponents file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)

for i in range(len(Graphs_graphChi)):
	os.chdir("/home/ossd/GraphSSD/HostCodes/graphchi-cpp")
	os.system("make example_apps/trianglecounting")
	os.system("cgexec -g memory,cpu:graphChiGroup ./bin/example_apps/trianglecounting file "+ Graphs_graphChi[i] + " filetype edgelist >> " + Output_file)
	os.system("rm -r /Datasets/test/*")
