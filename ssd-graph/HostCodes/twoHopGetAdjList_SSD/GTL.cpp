#include "graphFiltering.h"

extern void * data_nvme_command;
extern map<unsigned int, unsigned int > vertexMapping;
extern unsigned int numOfNodes;
extern unsigned int fd;
void GetAdjList(unsigned int vID, vector<unsigned int> &EdgeList)
{
	unsigned int numOfElementsInAPage = 4096 / sizeof(unsigned int);
	assert(vID < vertexMapping.size());
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA + (vID / (numOfElementsInAPage));
	if((vID % numOfElementsInAPage) == (numOfElementsInAPage - 1))
	{
		io.nblocks = 1;
	}
	else 
	{
		io.nblocks = 0;
	}
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
        if (err < 0)
                cout << "Error in executing nvme command" << endl;
	if (err)
		fprintf(stderr, "nvme read status:%x\n", err);

	unsigned int *RowPtr = (unsigned int *)malloc(sizeof(unsigned int) * 2);
	memcpy(RowPtr, (unsigned int *)data_nvme_command + (vID % numOfElementsInAPage), 2*sizeof(unsigned int));

	if(RowPtr[0] == RowPtr[1]) 
		return;
	io.opcode = nvme_cmd_read;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA + (numOfNodes + 1 + RowPtr[0]) / numOfElementsInAPage;
	io.nblocks = (RowPtr[1] / numOfElementsInAPage) - (RowPtr[0] / numOfElementsInAPage) + 1;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
		fprintf(stderr, "nvme read status:%x\n", err);

	unsigned int startingElement = (numOfNodes + 1 + RowPtr[0]) % numOfElementsInAPage;
	for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
		EdgeList.push_back(*((unsigned int *)data_nvme_command + startingElement + i));
/*	cout << vID << " :";
	for(unsigned int i=0; i < (RowPtr[1] - RowPtr[0]); i++)
		cout << " " << *((unsigned int *)data_nvme_command + startingElement + i);
	cout << endl;
*/
//perror:
//	perror(perrstr);
//	return 1;
}

void WriteToSSD(unsigned int j)
{
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA_FLUSH_BUFFER + j;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);

	return;

	//perror:
	//      perror(perrstr);
	//      return 1;
}
