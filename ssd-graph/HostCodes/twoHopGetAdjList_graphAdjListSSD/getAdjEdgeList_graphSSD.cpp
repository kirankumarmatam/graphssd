#include "graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_GET_EDGE 1
#define VERBOSE 0
#define FLUSH_SSD_BUFFER 1
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define CHECK_ERROR 1

int main(int argc, char **argv)
{
	struct timespec time_start, time_end;
	double time_elapsed;

	SSDInterface SSDInstance;
#if (UseFTL == 2)
	SSDInstance.fd = open(argv[1], O_RDWR);
	if (SSDInstance.fd < 0)
		cout << "Error opening file!!" << endl;
#endif

	if(argc < 3)
	{
		cout << "Usage is ./a.out inputDataSet.txt" << endl;
		return 0;
	}
	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/web-Google.txt", 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/test_graph.txt", 0, 1);
	map<unsigned int, unsigned int> vertexMapping;
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);

	SSDInstance.InitializeGraphDataStructures(G->GetNodes());
	cout <<"GetNodes " << G->GetNodes() << endl;
	vector<unsigned int> EdgeList;
#if (STORE_GRAPH_AT_SSD == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int temp_debugging = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		for (int e = 0; e < NI.GetOutDeg(); e++)
		{
			assert(vertexMapping.find(NI.GetOutNId(e)) != vertexMapping.end());
			EdgeList.push_back(vertexMapping[NI.GetOutNId(e)]);
		}
		SSDInstance.AddVertexUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
		EdgeList.clear();
//		temp_debugging++;
//		cout << "Entering here\n";
//		if(temp_debugging == 100)
//			break;
	}
//	return 0;
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_WR: %lf\n", time_elapsed);
#endif //STORE_GRAPH_AT_SSD

#if (FLUSH_SSD_BUFFER == 1)
	int j;
	for(j = 0; j < NUMBER_OF_PAGES_IN_SSD_BUFFER; j++)
	{
		SSDInstance.WriteToSSD(j);
	}
#endif //FLUSH_SSD_BUFFER == 1

	SSDInstance.ClearQueues();

#if (PERFORM_GET_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0;
	TNGraph::TNodeI NI1;
	vector<unsigned int> EdgeList1;
	while(i < NUM_OF_GET_EDGE_REQUESTS)
	{
		TNGraph::TNodeI NI = G->GetRndNI();
		if(NI.GetOutDeg() != 0)
		{
			i++;
			SSDInstance.GetAdjListUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
#if(PRINT_EDGE_LIST == 1)
			cout << i << endl;
			cout << EdgeList.size() << endl;
			for(int j=0; j < EdgeList.size(); j++)
			{
				cout << EdgeList[j] << " ";
			}
#endif //PRINT_EDGE_LIST == 1
			for(int j = 0; j < EdgeList.size(); j++)
			{
				NI1 = G->GetNI(NI.GetOutNId(j));
				if(NI1.GetOutDeg() != 0)
				{
					SSDInstance.GetAdjListUseNvmeCommands(vertexMapping[NI1.GetId()], EdgeList1);
#if(PRINT_EDGE_LIST == 1)
					cout << "\n" << EdgeList1.size() << endl;
					for(int k=0; k < EdgeList1.size(); k++)
					{
						cout << EdgeList1[k] << " ";
					}
#endif //PRINT_EDGE_LIST == 1
					EdgeList1.clear();
				}
			}
#if(PRINT_EDGE_LIST == 1)
			cout << endl;
#endif // PRINT_EDGE_LIST == 1
			EdgeList.clear();
		}
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#endif //PERFORM_GET_EDGE

	return 0;
}
