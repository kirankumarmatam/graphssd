#include <stdio.h>
#include <map>
#include <vector>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <iostream>
#include "stdafx.h"
#include <math.h>
#include <list>
#include <linux/nvme.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <vector>
#include <list>

#include <time.h>

using namespace std;

#define UseFTL 2
#define NUM_OF_GET_EDGE_REQUESTS 1000
//#define NUM_OF_GET_EDGE_REQUESTS 10
#define SLBA_FLUSH_BUFFER 5145631

class SSDInterface
{	
	public:
		void InitializeGraphDataStructures(unsigned int NumNodes);
		void FinalizeGraphDataStructures();
		void ClearQueues();
		void GetEdgeWeightNvmeCommands(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight);
		void AddVertexWeightUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights);
		void GetAdjListUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList);
		void GetTwoHopAdjListUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList);
		void AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList);

		void AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList);
		void GetAdjListNoGTL(unsigned int vID, vector<unsigned int> &EdgeList);
		void WriteToSSD(unsigned int j);
		void AddVertexWeight(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights)
		{
			if(UseFTL == 2)
			{
				AddVertexWeightUseNvmeCommands(vID, EdgeList, EdgeWeights);
			}
		}
		void GetEdgeWeight(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight)
		{
			if(UseFTL == 2)
			{
				GetEdgeWeightNvmeCommands(srcVId, destVId, edgeWeight);
			}
		}

               map<unsigned int, vector<unsigned int> > vertexToAdjListMap;
		void *data_nvme_command;
		int fd;
		unsigned int PAGE_SIZE;

		SSDInterface()
		{
			if(UseFTL == 2)
			{
				PAGE_SIZE = 4096;
				if ( posix_memalign(&data_nvme_command, 4096, 32*4096) ) {
					fprintf(stderr, "cannot allocate io payload for data_wr\n");
				}
			}
		}
};
