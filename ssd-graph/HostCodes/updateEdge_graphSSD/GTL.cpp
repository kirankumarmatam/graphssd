#include "graphFiltering.h"

extern map<unsigned int, unsigned int > vertexMapping;

void SSDInterface::InitializeGraphDataStructures(unsigned int NumNodes)
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 4;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = NumNodes;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);
}

void SSDInterface::AddVertexWeightUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights)
{
	assert(EdgeList.size()*sizeof(unsigned int) <= 16*4096);//Check if we need to increase the buffer size
	unsigned int num = EdgeList.size();
//	memcpy((void *)data_nvme_command, &num, sizeof(unsigned int));
	memcpy((void *)data_nvme_command, EdgeList.data(), EdgeList.size()*sizeof(unsigned int));
	memcpy((void *)((unsigned int *)data_nvme_command + EdgeList.size()), EdgeWeights.data(), EdgeWeights.size()*sizeof(unsigned int));
//	cout << data_nvme_command << " " << (unsigned int *)data_nvme_command + 1 << endl;
/*	cout << "vID = " << vID << " edgeListSize = " << EdgeList.size() << endl;
	for(int i=0; i < 2*EdgeList.size(); i++)
		cout <<*((unsigned int *)data_nvme_command + i) << " ";
	cout << endl;
*/	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 5;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;//Transfer number of vertices to write in metadata (reserved)?
	io.addr = (unsigned long)data_nvme_command;
//	cout << "io.addr = " << io.addr << endl;
	io.slba = vID;     // use slba for passing the vertex Id
	io.nblocks = ceil((sizeof(unsigned int)*(EdgeList.size()+EdgeWeights.size()+1)*1.0)/PAGE_SIZE);
	io.dsmgmt = 0;
//	io.reftag = 0;
	io.reftag = num;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);
}

void SSDInterface::GetEdgeWeightNvmeCommands(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight)
{
	assert(vID < vertexMapping.size());
	struct nvme_user_io io;
	io.opcode = nvme_cmd_read;
	io.flags = 5;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = srcVId;     // use slba for passing the vertex Id
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = destVId;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme read status:%x\n", err);
	
	edgeWeight = *((unsigned int *)data_nvme_command);
}

void SSDInterface::UpdateEdgeWeightNvmeCommands(unsigned int srcVId, unsigned int destVId, unsigned int edgeWeight)
{
	assert(vID < vertexMapping.size());
	struct nvme_user_io io;
	io.opcode = nvme_cmd_read;
	io.flags = 6;//Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = srcVId;     // use slba for passing the vertex Id
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = destVId;
	io.apptag = (edgeWeight & 0x0000FFFF);
	io.appmask = (edgeWeight & 0xFFFF0000);
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme read status:%x\n", err);
}
