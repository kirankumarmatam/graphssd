#include "graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_UPDATE_EDGE 1
#define VERBOSE 0

int main(int argc, char **argv)
{
	struct timespec time_start, time_end;
	double time_elapsed;

	SSDInterface SSDInstance;
#if (UseFTL == 2)
	SSDInstance.fd = open(argv[1], O_RDWR);
	if (SSDInstance.fd < 0)
		cout << "Error opening file!!" << endl;
#endif

	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/facebook_combined.txt", 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/web-Google.txt", 0, 1);
//	PNGraph G = TSnap::LoadEdgeList<PNGraph>("../../Benchmarks/SNAP/test_graph.txt", 0, 1);
	map<unsigned int, unsigned int> vertexMapping;
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);

	SSDInstance.InitializeGraphDataStructures(G->GetNodes());
	cout <<"GetNodes " << G->GetNodes() << endl;
	vector<unsigned int> EdgeList;
	vector<unsigned int> EdgeWeights;
#if (STORE_GRAPH_AT_SSD == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int temp_debugging = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		for (int e = 0; e < NI.GetOutDeg(); e++)
		{
			assert(vertexMapping.find(NI.GetOutNId(e)) != vertexMapping.end());
			EdgeList.push_back(vertexMapping[NI.GetOutNId(e)]);
			EdgeWeights.push_back(vertexMapping[NI.GetOutNId(e)]);
		}
		SSDInstance.AddVertexWeight(vertexMapping[NI.GetId()], EdgeList, EdgeWeights);
		EdgeList.clear();
		EdgeWeights.clear();
//		temp_debugging++;
//		cout << "Entering here\n";
//		if(temp_debugging == 100)
//			break;
	}
//	return 0;
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_WR: %lf\n", time_elapsed);
#endif //STORE_GRAPH_AT_SSD

#if (PERFORM_UPDATE_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0, edgeWeight, destEdge;
	while(i < NUM_OF_UPDATE_EDGE_REQUESTS)
	{
		TNGraph::TNodeI NI = G->GetRndNI();
		if(NI.GetOutDeg() != 0)
		{
			destEdge = vertexMapping[NI.GetOutNId(rand() % NI.GetOutDeg())];
//			cout << "srcEdge = " << vertexMapping[NI.GetId()] << " destEdge = " << destEdge << endl; 
			SSDInstance.UpdateEdgeWeight(vertexMapping[NI.GetId()], destEdge, (2*destEdge));
			SSDInstance.GetEdgeWeight(vertexMapping[NI.GetId()], destEdge, edgeWeight);
//			cout << "edge weight = " << edgeWeight << endl;
			if(2*destEdge != edgeWeight)
				cout << "Incorrect Edge Weight!!" << endl;
			i++;
		}
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#endif //PERFORM_UPDATE_EDGE
	return 0;
}
