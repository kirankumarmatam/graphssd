#include "../GraphSSD_library/graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_BFS 1
#define VERBOSE_BFS 1
#define VERBOSE_PRINT_ADJLIST 0
#define STOP_SIMULATION 0
unsigned int NUMBER_OF_REQUESTS;
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define FLUSH_SSD_BUFFER 1
#define PRINT_LEVEL_NUMBER 0
#define READ_REQUESTS_FROM_FILE 0 // This is incomplete
#define READ_REQUEST_AS_ARGUMENT 0

#if(useCache == 1)
extern unsigned int numOfCacheHits, numOfCacheEvictions, numOfCacheAccesses, numOfCacheMisses;
extern unsigned int TotRequests_host, TotRequests_SSD;
extern unsigned int MaxCacheSize;
#endif

extern map<unsigned int, unsigned int> vertexMapping;
extern PNGraph G;
extern unsigned int NumNodes, NumEdges;
extern unsigned int numOfNANDPageWrites, numOfNANDPageReads;

int main(int argc, char **argv)
{
	SSDInterface SSDInstance;
#if(STORE_GRAPH_AT_SSD == 1)
	SSDInstance.store_graph(argc, argv);
#endif // STORE_GRAPH_AT_SSD

	cout << NumNodes << " " << NumEdges << endl;

	struct timespec time_start, time_end;
	double time_elapsed;

	vector<unsigned int> EdgeList;

#if(Update_edges_from_file == 0)
		SSDInstance.AddEdge(100000);
#elif(Update_edges_from_file == 1)
		SSDInstance.AddEdge(argv[6]);
#endif//Update_edges_from_file

#if (PERFORM_BFS == 1)

#if(READ_REQUESTS_FROM_FILE == 1)
	fstream request_file;
	request_file.open(argv[3]);
	unsigned int total_number_of_requests;
	fscanf(request_file,"%d\n",&total_number_of_requests);
#else
	unsigned int total_number_of_requests = 1;
#endif //READ_REQUESTS_FROM_FILE

//	srand(time(NULL));
//	unsigned int root = 0, reqElement = 100000;
//	cout << root << " " << reqElement << endl;
	clock_gettime(CLOCK_MONOTONIC, &time_start);
//	unsigned int root = 0, reqElement = 2;
#if(PRINT_LEVEL_NUMBER == 1)
		list<std::pair <unsigned int, unsigned int> > bfsQueue;
#else
		list<unsigned int> bfsQueue;
#endif
		bool elementFound = 0;
	for(int numOfrun = 0; numOfrun < total_number_of_requests; numOfrun++)
	{
//		cout << numOfrun << endl;
//	unsigned int root = rand() % G->GetNodes(), reqElement = rand() % G->GetNodes();
#if(READ_REQUESTS_FROM_FILE == 1)
		unsigned int root = 0, reqElement;
		fscanf(request_file, "%d\n", &reqElement;
#elif(READ_REQUEST_AS_ARGUMENT == 1)
		unsigned int root = 0, reqElement = atoi(argv[3]);
#else
		unsigned int root = 0, reqElement = 4000;
#endif

#if(PRINT_LEVEL_NUMBER == 1)
		bfsQueue.push_back(std::make_pair(root,0)); // Need to get ROOT as input element, root element needs to be < NumVertices
#else
		bfsQueue.push_back(root); // Need to get ROOT as input element, root element needs to be < NumVertices
#endif
		elementFound = 0;
#if(AuxMemoryInStorage == 0)
#if(LOAD_GRAPH_FROM_FILE == 1)
		bool *visited = (bool *)calloc(NumNodes, sizeof(bool));
#else
		bool *visited = (bool *)calloc(vertexMapping.size(), sizeof(bool));
#endif
		assert(visited != NULL);
#else
		AuxDataValueType value = 0;
		for(unsigned int i=0; i < NumNodes; i++)
		{
			SSDInstance.AccessAuxMemory(i, value, 0);
		}
#endif//AuxMemoryInStorage
		unsigned int temp_debugging = 0;
#if(STOP_SIMULATION == 1)
		unsigned int number_requests_executed = 0;
#endif // STOP_SIMULATION == 1
		while(!bfsQueue.empty())
		{
//			cout << "E" <<endl ;
#if(PRINT_LEVEL_NUMBER == 1)
			std::pair<unsigned int, unsigned int> topElementPair = bfsQueue.front();
			unsigned int topElement = topElementPair.first;
#if (VERBOSE_BFS == 1)
			cout << topElementPair.first << ", " << topElementPair.second << endl; ;
#endif
#else
		unsigned int topElement = bfsQueue.front();
#if (VERBOSE_BFS == 1)
			cout << topElement << ", " << endl; ;
#endif
#endif
			assert(topElement <= NumNodes);
			if(topElement == reqElement) // requiredElement is input to the program
			{
				cout << "Found the required element" << endl;
				elementFound = 1;
				break;
			}
//			cout << "E1" <<endl ;
			SSDInstance.GetAdjListUseNvmeCommands(topElement, EdgeList);
#if (VERBOSE_PRINT_ADJLIST == 1)
			cout << topElement << " :";
			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
				cout << " " << EdgeList[i];
			}
			cout << endl;
#endif //VERBOSE_PRINT_ADJLIST
//			cout << "E" <<endl ;
			bfsQueue.pop_front();
			for(unsigned int i=0; i < EdgeList.size(); i++)
			{
#if(LOAD_GRAPH_FROM_FILE == 1)
				assert(EdgeList[i] < NumNodes);
#else
				assert(EdgeList[i] < vertexMapping.size());
#endif
#if(AuxMemoryInStorage == 0)
				if(visited[EdgeList[i]] == 0)
#else
				SSDInstance.AccessAuxMemory(EdgeList[i], value, 1);
				if(value == 0)
#endif//AuxMemoryInStorage
				{
#if(PRINT_LEVEL_NUMBER == 1)
					bfsQueue.push_back(std::make_pair(EdgeList[i], topElementPair.second+1));
					if(topElementPair.second >= 10)
						exit(0);
#else
					bfsQueue.push_back(EdgeList[i]);
#endif
#if(AuxMemoryInStorage == 0)
					visited[EdgeList[i]] = 1;
#else
					value = 1;
					SSDInstance.AccessAuxMemory(EdgeList[i], value, 0);
#endif//AuxMemoryInStorage == 1
				}
			}
			//		cout << "Searching" << endl;
			//		cout << endl;
			EdgeList.clear();
			//		temp_debugging++;
			//		if(temp_debugging == 12)
			//			break;
#if(STOP_SIMULATION == 1)
			number_requests_executed++;
			//		cout << number_requests_executed << endl;
			if(number_requests_executed == NUMBER_OF_REQUESTS)
				break;
#endif//STOP_SIMULATION == 1
		}
#if(AuxMemoryInStorage == 0)
		free(visited);
#endif
		bfsQueue.clear();
	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
	SSDInstance.FinalizeGraphDataStructures();	
	if(elementFound == 0)
		cout << "Did not find the required element" << endl;
#if(useCache == 1)
	SSDInstance.printTime();
#endif//useCache
#endif //PERFORM_BFS
	return 0;
}
