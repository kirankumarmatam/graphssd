#include "graphFiltering.h"

extern map<unsigned int, unsigned int > vertexMapping;

void SSDInterface::InitializeGraphDataStructures(unsigned int NumNodes)
{
	GraphInitialize();
	return;
}

void SSDInterface::AddVertexUseNvmeCommands(unsigned int vID, vector<unsigned int> EdgeList)
{

//	assert(EdgeList.size()*sizeof(unsigned int) <= 32*4096);
	unsigned int num = EdgeList.size();
	if(EdgeList.size() * sizeof(unsigned int) > 16*4096)
		num = 16*4096 / (sizeof(unsigned int));
	memcpy((void *)data_nvme_command, EdgeList.data(), num*sizeof(unsigned int));
	putAdjEdgeList(vID,(char *)data_nvme_command, num);
	return;
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 2;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = vID;     // use slba for passing the vertex Id
//	io.nblocks = ceil((sizeof(unsigned int)*(EdgeList.size()+1)*1.0)/PAGE_SIZE);
	io.nblocks = ceil((sizeof(unsigned int)*(num+1)*1.0)/PAGE_SIZE);
	io.dsmgmt = 0;
	io.reftag = num;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;

	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}

void SSDInterface::AddVertexNoGTL(unsigned int vID, vector<unsigned int> EdgeList)
{
	assert(vID < vertexMapping.size());
	vertexToAdjListMap[vID];
	vertexToAdjListMap[vID] = EdgeList;
}

void SSDInterface::GetAdjListUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList)
{
	assert(vID < vertexMapping.size());
	*((unsigned int *)data_nvme_command) = 0;
	getAdjEdgeList(vID, (char *)data_nvme_command);

	assert(EdgeList.size()*sizeof(unsigned int) <= 32*4096);
	for(unsigned int i=0; i < *((unsigned int *)data_nvme_command); i++)
	{
		//              cout << "Edge " << i << " = " << *((unsigned int *)data_nvme_command + i + (4096/sizeof(unsigned int)) ) << endl;
		//              cout << "Edge " << i << " = " << *((unsigned int *)data_nvme_command + i + 1) << endl;
		//              EdgeList.push_back(*((unsigned int *)data_nvme_command + i + (4096/sizeof(unsigned int)) ));
		EdgeList.push_back(*((unsigned int *)data_nvme_command + i + 1) );
	}
	//      if(EdgeList.size() == 0)return;
	/*      cout << vID << " :";
		for(unsigned int i=0; i < *((unsigned int *)data_nvme_command); i++)
		{
		cout << " " << *((unsigned int *)data_nvme_command + i + 1);
		}
		cout << endl;
	 */}
void SSDInterface::GetAdjListNoGTL(unsigned int vID, vector<unsigned int> &EdgeList)
{
	assert(vID < vertexMapping.size());
	for(unsigned int i=0; i<vertexToAdjListMap[vID].size(); i++)
		EdgeList.push_back(vertexToAdjListMap[vID][i]);
}

void SSDInterface::AddVertexWeightUseNvmeCommands(unsigned int vID, vector<unsigned int> &EdgeList, vector<unsigned int> &EdgeWeights)
{
	assert(EdgeList.size()*sizeof(unsigned int) <= 16*4096);//Check if we need to increase the buffer size
	unsigned int num = EdgeList.size();
//	memcpy((void *)data_nvme_command, &num, sizeof(unsigned int));
	memcpy((void *)data_nvme_command, EdgeList.data(), EdgeList.size()*sizeof(unsigned int));
	memcpy((void *)((unsigned int *)data_nvme_command + EdgeList.size()), EdgeWeights.data(), EdgeWeights.size()*sizeof(unsigned int));
//	cout << data_nvme_command << " " << (unsigned int *)data_nvme_command + 1 << endl;
/*	cout << "vID = " << vID << " edgeListSize = " << EdgeList.size() << endl;
	for(int i=0; i < 2*EdgeList.size(); i++)
		cout <<*((unsigned int *)data_nvme_command + i) << " ";
	cout << endl;
*/	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 5;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;//Transfer number of vertices to write in metadata (reserved)?
	io.addr = (unsigned long)data_nvme_command;
//	cout << "io.addr = " << io.addr << endl;
	io.slba = vID;     // use slba for passing the vertex Id
	io.nblocks = ceil((sizeof(unsigned int)*(EdgeList.size()+EdgeWeights.size()+1)*1.0)/PAGE_SIZE);
	io.dsmgmt = 0;
//	io.reftag = 0;
	io.reftag = num;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);
}

void SSDInterface::GetEdgeWeightNvmeCommands(unsigned int srcVId, unsigned int destVId, unsigned int & edgeWeight)
{
	assert(srcVId < vertexMapping.size());
	struct nvme_user_io io;
	io.opcode = nvme_cmd_read;
	io.flags = 5;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = srcVId;     // use slba for passing the vertex Id
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = destVId;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
		fprintf(stderr, "nvme read status:%x\n", err);
	
	edgeWeight = *((unsigned int *)data_nvme_command);
}

void SSDInterface::WriteToSSD(unsigned int j)
{
	struct nvme_user_io io;
	unsigned int err;
	io.opcode = nvme_cmd_write;
	io.flags = 0;
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = SLBA_FLUSH_BUFFER + j;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command" << endl;
	if (err)
		fprintf(stderr, "nvme write status:%x\n", err);

	return;

	//perror:
	//      perror(perrstr);
	//      return 1;
}

void SSDInterface::FinalizeGraphDataStructures()
{
	GraphFinalize();
	return;
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 6;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = 0;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}

void SSDInterface::ClearQueues()
{
	struct nvme_user_io io;
	io.opcode = nvme_cmd_write;
	io.flags = 7;//2; Initialize with correct flags
	io.control = 0;
	io.metadata = (unsigned long)0;
	io.addr = (unsigned long)data_nvme_command;
	io.slba = 0;
	io.nblocks = 0;
	io.dsmgmt = 0;
	io.reftag = 0;
	io.apptag = 0;
	io.appmask = 0;
	int err = ioctl(fd, NVME_IOCTL_SUBMIT_IO, &io);
	if (err < 0)
		cout << "Error in executing nvme command!!" << endl;
	if (err)
	{
		fprintf(stderr, "nvme write status:%x\n", err);
		exit(0);
	}
}
