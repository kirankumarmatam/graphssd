#include "header.h"

unsigned int CopyFunction_InPage(char * devAddr, char * Test_variable_size_buffer, unsigned int currentTopPointer, unsigned int vertexId)
{
	unsigned int lowestVertexId = ((unsigned int *)devAddr)[(SSD_PAGE_SIZE / sizeof(unsigned int)) - 2];
	if(vertexId == 29438)
		cout << "lowestVertexId = " << lowestVertexId << endl;
	unsigned int distance = vertexId - lowestVertexId;
	if(vertexId == 29438)
		cout << "distance = " << distance << endl;
	unsigned int offset = ((unsigned int *)devAddr)[(SSD_PAGE_SIZE / sizeof(unsigned int)) - (3*distance + 3)];
	if(vertexId == 29438)
		cout << "offset = " << offset << endl;
	unsigned int number = ((unsigned int *)devAddr)[(SSD_PAGE_SIZE / sizeof(unsigned int)) - (3*distance + 4)];
	if(vertexId == 29438)
		cout << "number = " << number << endl;
	if(vertexId == 29438)
	{
		cout << "R " << endl;
		for(unsigned int i=0; i < number; i++)
		{
			cout << ((unsigned int *)(devAddr + offset))[i] << " ";
		}
		cout << endl;
	}
	memcpy((void *)(Test_variable_size_buffer + currentTopPointer), (void *)(devAddr + offset), number * sizeof(unsigned int));
	return number;
}

unsigned int binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number)
{
        while (l < r)
        {
                int m = l + (r-l)/2;
//		printf("%d %d %d\n", l, m, r);

                if (arr[m].vertexId == x) 
                {
                        unsigned int start = m;
                        m = m-1;
                        while((m >= 0) && (arr[m].vertexId == x))
                        {
                                start = m;
                                m = m-1;
                        }
                        m = start;
                        unsigned int count=0;
                        while(((unsigned int)m <= r) && (arr[m].vertexId == x))
                        {
                                count++;
                                m++;
                        }
                        *index = start;
                        *number = count;
                        return 0;
                }
                else if (arr[m].vertexId < x) 
                {
                        if(x < arr[m+1].vertexId)
                        {
                                *index = m;
                                *number = 1;
                                return 0;
                        }
                        else if(x > arr[m+1].vertexId)
                        {
                                l = m+1;
                        }
                        else if(x == arr[m+1].vertexId)
                        {
                                unsigned int start = m+1;
                                m = start;
                                unsigned int count = 0;
                                while(((unsigned int)m <= r) && (arr[m].vertexId == x))
                                {
                                        count++;
                                        m++;
                                }
                                *index = start;
                                *number = count;
                                return 0;
                        }
                }
                else if(arr[m].vertexId > x) 
                {
                        r = m;
                }
        }
        if(l == r)
        {
                if(arr[l].vertexId <= x)
                {
                        *index = l;
                        *number = 1;
                        return 0;
                }
                else {
                        return 1;
                }
        }

        return 1;
}

unsigned int findEdge_inBuffer(char *Test_variable_size_buffer, char *Test_variable_size_buffer_weight, unsigned int destEdge, unsigned int NumberOfAdjVertices)
{
	unsigned int i;
	for(i=0; i < NumberOfAdjVertices; i++)
	{
		if(destEdge == ((unsigned int *)Test_variable_size_buffer)[i])
		{
			return ((unsigned int *)Test_variable_size_buffer_weight)[i];
		}
	}
	return -1;
}
