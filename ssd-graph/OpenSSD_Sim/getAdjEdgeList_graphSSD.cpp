#include "graphFiltering.h"
#define STORE_GRAPH_AT_SSD 1
#define PERFORM_GET_EDGE 1
#define VERBOSE 0
#define FLUSH_SSD_BUFFER 1
#define NUMBER_OF_PAGES_IN_SSD_BUFFER 8192
#define CHECK_ERROR 1
#define AddEdges 1

map<unsigned int, unsigned int> vertexMapping;

int main(int argc, char **argv)
{
	struct timespec time_start, time_end;
	double time_elapsed;

	SSDInterface SSDInstance;

	if(argc < 3)
	{
		cout << "Usage is ./a.out inputDataSet.txt" << endl;
		return 0;
	}
	PNGraph G = TSnap::LoadEdgeList<PNGraph>(argv[2], 0, 1);
	unsigned int nodeIndex = 0;
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
		vertexMapping[NI.GetId()] = nodeIndex++;
	}
	assert(vertexMapping.size() == nodeIndex);

	SSDInstance.InitializeGraphDataStructures(G->GetNodes());
	cout <<"GetNodes " << G->GetNodes() << endl;
	vector<unsigned int> EdgeList;
#if (STORE_GRAPH_AT_SSD == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	for (TNGraph::TNodeI NI = G->BegNI(); NI < G->EndNI(); NI++)
	{
#if (VERBOSE == 1)
		cout << "W " << vertexMapping[NI.GetId()] << endl;
#endif
		if(vertexMapping[NI.GetId()] == 29438)
			cout << "W " << vertexMapping[NI.GetId()] << " : " <<  endl;
//		for (int e = 0; e < NI.GetOutDeg(); e++)
		for (int e = 0; (e < NI.GetOutDeg()) && (e < (16*4096 / sizeof(unsigned int))); e++)
		{
			assert(vertexMapping.find(NI.GetOutNId(e)) != vertexMapping.end());
			EdgeList.push_back(vertexMapping[NI.GetOutNId(e)]);
			if(vertexMapping[NI.GetId()] == 29438)
				cout << vertexMapping[NI.GetOutNId(e)] << " ";
		}
		if(vertexMapping[NI.GetId()] == 29438)
			cout << endl;
		SSDInstance.AddVertexUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
		SSDInstance.AddVertexNoGTL(vertexMapping[NI.GetId()], EdgeList);
		EdgeList.clear();
//		temp_debugging++;
//		cout << "Entering here\n";
//		if(temp_debugging == 100)
//			break;
	}
//	return 0;
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_WR: %lf\n", time_elapsed);
#endif //STORE_GRAPH_AT_SSD

#if (CHECK_ERROR == 1)
vector<unsigned int> EdgeList_check;
#endif //CHECK_ERROR == 1

#if (AddEdges == 1)
SSDInstance.AddEdge();
#endif

#if (PERFORM_GET_EDGE == 1)
	clock_gettime(CLOCK_MONOTONIC, &time_start);
	unsigned int i=0;
	while(i < NUM_OF_GET_EDGE_REQUESTS)
	{
		TNGraph::TNodeI NI = G->GetRndNI();
	//	if(NI.GetOutDeg() != 0)
	//	{
			i++;
//#if (VERBOSE == 1)
//#endif
			if(vertexMapping[NI.GetId()] == 29438)
				cout << "R " << vertexMapping[NI.GetId()] << " : ";
			SSDInstance.GetAdjListUseNvmeCommands(vertexMapping[NI.GetId()], EdgeList);
#if (CHECK_ERROR == 1)
			SSDInstance.GetAdjListNoGTL(vertexMapping[NI.GetId()], EdgeList_check);
			unsigned int j;
			cout << "C " << i << " " << vertexMapping[NI.GetId()]  << endl;
			assert(EdgeList_check.size() == EdgeList.size());
			for(j = 0; j < EdgeList.size(); j++)
			{
				if(vertexMapping[NI.GetId()] == 29438)
					cout << EdgeList_check[j] << " " << EdgeList[j] << " ";
				assert(EdgeList[j] == EdgeList_check[j]);
#if (VERBOSE == 1)
				cout << EdgeList[j] << " ";
#endif
			}
#if (VERBOSE == 1)
			cout << endl;
#endif
#endif // CHECK_ERROR == 1
	//	}
		EdgeList.clear();
#if (CHECK_ERROR == 1)
		EdgeList_check.clear();
#endif

	}
	clock_gettime(CLOCK_MONOTONIC, &time_end);
	time_elapsed = ((double)time_end.tv_sec - (double)time_start.tv_sec);
	time_elapsed += ((double)time_end.tv_nsec - (double)time_start.tv_nsec) / 1000000000.0;
	fprintf(stdout, "\n");
	fprintf(stdout, "TIME_RD: %lf\n", time_elapsed);
#endif //PERFORM_GET_EDGE

	SSDInstance.FinalizeGraphDataStructures();
	return 0;
}
