#include "header.h"
extern unsigned int GraphLPN;
extern char * Test_variable_size_buffer;
extern unsigned int numOfFreeElementsAvailableInTheBuffer;
extern unsigned int bufferPageTopPointer;
extern unsigned int currentGTTIndex_edge;
extern unsigned int currentGTTIndex_weight;
extern struct CompletionQueues *gtlCompletionQueues;
extern struct CompletionQueuePtrs *gtlCompletionQueuePtrs;

extern struct GTTTable1 *gtt1;
extern struct GTTTable1 *gtt1_weight;

extern struct VertexCompletionInformation vertexCompletionInformation;
std::map<unsigned int, char *> Storage;

void process_completionRequest(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit)
{
	if(!(vertexCompletionInformation.flags == 5 || vertexCompletionInformation.flags == 2 || vertexCompletionInformation.flags == 8))
	{
	        return;
	}
	if(vertexCompletionInformation.flags == 2)
	{
		unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
		if(front == rear)
		{
			printf("ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n", chNo, wayNo);
		}
		char * devAddr = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
		gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front = (front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

		if(vertexCompletionInformation.vertexId == 29438)
			cout << "R LPN = " << gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].LPN << " destPtr = " << (vertexCompletionInformation.NumOfAdjVertices + 1) << endl;

		vertexCompletionInformation.NumOfAdjVertices += CopyFunction_InPage(devAddr, Test_variable_size_buffer, (vertexCompletionInformation.NumOfAdjVertices + 1) * sizeof(unsigned int), vertexCompletionInformation.vertexId);

		vertexCompletionInformation.numberOfVerticesRemainingToFetch -= 1;

		if(vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0)
		{
			if (vertexCompletionInformation.NumOfAdjVertices != 0) {
				memcpy((void *)Test_variable_size_buffer, &(vertexCompletionInformation.NumOfAdjVertices), sizeof(unsigned int));
				unsigned int numItems = (vertexCompletionInformation.NumOfAdjVertices+1);
				memcpy((void *)(vertexCompletionInformation.host_addr), (void *)Test_variable_size_buffer, numItems * sizeof(unsigned int));
			}
			vertexCompletionInformation.flags = 0;
		}
	} else if (vertexCompletionInformation.flags == 5) {
		unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
		if (front == rear) {
			printf("ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n", chNo, wayNo);
		}
		char * devAddr = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
		unsigned int LPN = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].LPN;
		gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front = (front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

		if(LPN <= GraphLPN)
		{
			vertexCompletionInformation.NumOfAdjVertices += CopyFunction_InPage( devAddr, Test_variable_size_buffer, vertexCompletionInformation.NumOfAdjVertices * sizeof(unsigned int), vertexCompletionInformation.vertexId);
		} else if(LPN > GraphLPN){
			vertexCompletionInformation.NumOfAdjVertices_weight += CopyFunction_InPage(devAddr, Test_variable_size_buffer + 512*1024, vertexCompletionInformation.NumOfAdjVertices_weight * sizeof(unsigned int), vertexCompletionInformation.vertexId);
		}

		vertexCompletionInformation.numberOfVerticesRemainingToFetch -= 1;

		if (vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0) {
			if (vertexCompletionInformation.NumOfAdjVertices != 0) {
				*((unsigned int *) Test_variable_size_buffer) = findEdge_inBuffer(Test_variable_size_buffer, Test_variable_size_buffer + 512*1024, vertexCompletionInformation.destEdge, vertexCompletionInformation.NumOfAdjVertices);
				unsigned int numItems = 1;
				memcpy((void *)vertexCompletionInformation.host_addr, (void *)Test_variable_size_buffer, sizeof(unsigned int) * numItems);
				}
			}
			vertexCompletionInformation.flags = 0;
		}
	else if(vertexCompletionInformation.flags == 8)
	{
		unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
		if(front == rear)
		{
			printf("ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n", chNo, wayNo);
		}
		char * devAddr = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
		gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front = (front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

//		printf("1.1.1 process_completionRequest %d %d %d %d\r\n", vertexCompletionInformation.vertexId, gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].LPN, vertexCompletionInformation.NumOfAdjVertices, vertexCompletionInformation.numberOfVerticesRemainingToFetch);

		vertexCompletionInformation.NumOfAdjVertices += CopyFunction_InPage(devAddr, Test_variable_size_buffer, (vertexCompletionInformation.NumOfAdjVertices + 1) * sizeof(unsigned int), vertexCompletionInformation.vertexId);

		vertexCompletionInformation.numberOfVerticesRemainingToFetch -= 1;

		if(vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0)
		{
			if(vertexCompletionInformation.hopNum == 0 || vertexCompletionInformation.NumOfAdjVertices == 0)
			{
				if (vertexCompletionInformation.NumOfAdjVertices != 0) {
					memcpy((void *)Test_variable_size_buffer, &(vertexCompletionInformation.NumOfAdjVertices), sizeof(unsigned int));
					unsigned int numItems = (vertexCompletionInformation.NumOfAdjVertices+1);
					memcpy((void *)vertexCompletionInformation.host_addr, (void *)Test_variable_size_buffer, sizeof(unsigned int) * numItems);
				}
				vertexCompletionInformation.flags = 0;
			}
			else if (vertexCompletionInformation.hopNum == 1) {//Incorrect here, vertexCompletionInformation.vertexId is a shared variable
				unsigned int firstHopNeighbors = vertexCompletionInformation.NumOfAdjVertices;
				vertexCompletionInformation.numberOfVerticesRemainingToFetch = vertexCompletionInformation.NumOfAdjVertices;
				vertexCompletionInformation.hopNum = 0;
				unsigned int i = 0;
				for (i = 0; i < firstHopNeighbors; i++) {
					vertexCompletionInformation.vertexId = ((unsigned int *) Test_variable_size_buffer)[i + 1];
					unsigned int gttVidIndex, gttVidNumber;
					binarySearch(gtt1->GTTPointer1, 0, currentGTTIndex_edge, vertexCompletionInformation.vertexId, &gttVidIndex, &gttVidNumber);
					vertexCompletionInformation.numberOfVerticesRemainingToFetch += (gttVidNumber - 1);
					vertexCompletionInformation.isEdge = 1;
					getEdgesToSSDDram_initiateLoading(gttVidIndex, gttVidNumber, 0);
				}
			}
		}
	}
	return;
}

void process_completionRequest_write(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit)
{
	if(!(vertexCompletionInformation.flags == 5 || vertexCompletionInformation.flags == 2))
	{
	        return;
	}
	unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
	unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
	if(front == rear)
	{
		printf("ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n", chNo, wayNo);
	}
	char * devAddr = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
	unsigned int destPageOffset = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].destPageOffset;
	unsigned int srcPageOffset = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].srcPageOffset;
	unsigned int Number = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].Number;
	unsigned int lowestVertexId = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].lowestVertexId;
	gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front = (front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

	memcpy((void *)(devAddr+destPageOffset), (void *)(Test_variable_size_buffer+srcPageOffset), Number*sizeof(unsigned int));
	unsigned int vertexId = vertexCompletionInformation.vertexId;
	((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3)] = vertexId;
	((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3 + 1)] = destPageOffset;
	((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3 + 2)] = Number;
	if(lowestVertexId == vertexId)
	{
		((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 1] = 1;
	}
	else {
		((unsigned int *)devAddr)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 1] += 1;
	}
	vertexCompletionInformation.numberOfVerticesRemainingToFetch -= Number;

	if(vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0)
	{
		if(vertexCompletionInformation.NumOfAdjVertices == 0 && vertexCompletionInformation.isEdge == 1)
		{
			vertexCompletionInformation.isEdge = 0;
			return;
		}
		vertexCompletionInformation.flags = 0;
		return;
	}
	return;
}

void GraphLRUBufRead(unsigned int sectorNumber)
{
	unsigned int tempLpn;

	tempLpn = sectorNumber;

	if(Storage.find(tempLpn) == Storage.end())
		assert(0);

	unsigned int dieNo = tempLpn % DIE_NUM;
	unsigned int chNo = dieNo % CHANNEL_NUM;
	unsigned int wayNo = dieNo / CHANNEL_NUM;
	unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
	unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
	if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
	{
		printf("ERROR (GraphPmRead): COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", chNo, wayNo);
	}
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].devAddr = Storage[tempLpn];
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].LPN = tempLpn;
	gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);

	process_completionRequest(chNo, wayNo, 0, 1);
}

void GraphLRUBufWrite(unsigned int curSect, unsigned int Len, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int lowestVertexId)
{
	unsigned int tempLpn, dieNo;

	tempLpn = curSect;

	if(Storage.find(tempLpn) == Storage.end())
	{
		Storage[tempLpn];
		Storage[tempLpn] = new char [SSD_PAGE_SIZE];
	}

	dieNo = tempLpn % DIE_NUM;
	unsigned int chNo = dieNo % CHANNEL_NUM;
	unsigned int wayNo = dieNo / CHANNEL_NUM;
	unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
	unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
	if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
	{
		printf("ERROR: COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", chNo, wayNo);
	}
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].devAddr = Storage[tempLpn];
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].destPageOffset = destPageOffset;
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].srcPageOffset = srcPageOffset;
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].Number = Len / sizeof(unsigned int);
	gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].lowestVertexId = lowestVertexId;
	gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);

	process_completionRequest_write(chNo, wayNo, 0, 1);

	return;
}
