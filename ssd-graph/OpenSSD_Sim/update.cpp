#include "graphFiltering.h"

extern map<unsigned int, unsigned int> vertexMapping;
extern std::map<unsigned int, char *> Storage;
extern unsigned int GraphLPN;
extern unsigned int currentGTTIndex_edge;
extern struct GTTTable1 *gtt1;

void SSDInterface::AddEdge()
{
	unsigned int numOfRandomEdges = 1;
	std::vector<unsigned int> randomEdgesSrcVertex;
	randomEdgesSrcVertex.reserve(numOfRandomEdges);
	std::vector<unsigned int> randomEdgesDestVertex;
	randomEdgesDestVertex.reserve(numOfRandomEdges);
	for(unsigned int i=0; i < numOfRandomEdges; i++)
	{
		randomEdgesSrcVertex.push_back(rand() % vertexMapping.size());
		randomEdgesDestVertex.push_back(rand() % vertexMapping.size());
	}
	sort(randomEdgesSrcVertex.begin(), randomEdgesSrcVertex.end());

	unsigned int addTopPointer = 0;

	while(addTopPointer < numOfRandomEdges)
	{
		unsigned int gttVidIndex, gttVidNumber;
		binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, randomEdgesSrcVertex[addTopPointer], &gttVidIndex, &gttVidNumber);
		unsigned int positionToAdd = gttVidIndex + gttVidNumber - 1;
		unsigned int nextVertex;
		char *currentPage = Storage[gtt1->GTTPointer1[positionToAdd].LPN];
		if(positionToAdd < currentGTTIndex_edge)
			nextVertex = gtt1->GTTPointer1[positionToAdd + 1].vertexId;
		else if (positionToAdd == currentGTTIndex_edge)
			nextVertex = -1;

		unsigned int verticesInCurrentPage = ((unsigned int *)currentPage)[SSD_PAGE_SIZE / (sizeof(unsigned int)) - 1];
		unsigned int pageTopPointer = 0, currentPageVertex, currentAddVertex;
		char *temporaryPage = new char [SSD_PAGE_SIZE + numOfRandomEdges * sizeof(unsigned int)];
		char *temporaryOffsetStorage = new char [3*verticesInCurrentPage * sizeof(unsigned int)];
		unsigned int currentTempPageOffset = 0;
		while(1)
		{
			if(addTopPointer < numOfRandomEdges)
			{
				currentAddVertex = randomEdgesSrcVertex[addTopPointer];
			}
			else {
				currentAddVertex = -1;
			}
			if((pageTopPointer >= verticesInCurrentPage) && ((currentAddVertex >= nextVertex) || (addTopPointer >= numOfRandomEdges)))
			{
				break;
			}
			if(pageTopPointer < verticesInCurrentPage)
			{
				currentPageVertex = ((unsigned int *)currentPage)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (pageTopPointer * 3)];
			} else 
			{
				currentPageVertex = -1;
			}

//	cout << "A ca = " << currentAddVertex << " ap = " << addTopPointer << " pp = " << pageTopPointer << " cp = " << currentPageVertex << endl;

			if(currentPageVertex <= currentAddVertex) {
				((unsigned int *)temporaryOffsetStorage)[3*pageTopPointer] = currentPageVertex;
				((unsigned int *)temporaryOffsetStorage)[3*pageTopPointer + 1] = currentTempPageOffset;
				unsigned int temp_offset = ((unsigned int *)currentPage)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (pageTopPointer * 3 + 1)];
				unsigned int temp_number = ((unsigned int *)currentPage)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (pageTopPointer * 3 + 2)];
				((unsigned int *)temporaryOffsetStorage)[3*pageTopPointer + 2] = temp_number;
				memcpy((void *)(temporaryPage + currentTempPageOffset), (void *)(currentPage + temp_offset), sizeof(unsigned int) * temp_number);
				pageTopPointer++;
				currentTempPageOffset += temp_number * sizeof(unsigned int);
			} else if(currentPageVertex > currentAddVertex) {
//				cout << ((unsigned int *)temporaryOffsetStorage)[3*(pageTopPointer - 1)] << " " << currentAddVertex << endl;
				assert(((unsigned int *)temporaryOffsetStorage)[3*(pageTopPointer - 1)] == currentAddVertex);
				memcpy((void *)(temporaryPage + currentTempPageOffset), &(randomEdgesDestVertex[addTopPointer]), sizeof(unsigned int));
				((unsigned int *)temporaryOffsetStorage)[3*(pageTopPointer-1) + 2] += 1;
				currentTempPageOffset += sizeof(unsigned int);
				addTopPointer++;
			}
		}

		unsigned int temp_numOfFreeElementsAvailableInTheBuffer = 0, temp_bufferPageTopPointer = 0, numberOfPages = 0;
		char *pageToActOn;
		for(unsigned int i=0; i < verticesInCurrentPage; i++)
		{
			unsigned int valuesRemainingToAdd = ((unsigned int *)temporaryOffsetStorage)[3*i+2];
			unsigned int numOfItemsToCopy;
			do {
				if ((valuesRemainingToAdd + 3) > temp_numOfFreeElementsAvailableInTheBuffer)
				{
					if(numberOfPages == 0)
					{
						pageToActOn = currentPage;
						((unsigned int *)pageToActOn)[SSD_PAGE_SIZE / (sizeof(unsigned int)) - 1] = 0;
					} else {
						pageToActOn = new char [SSD_PAGE_SIZE];
						for(unsigned int j = currentGTTIndex_edge; j > positionToAdd; j--)
						{
							gtt1->GTTPointer1[j+1].LPN = gtt1->GTTPointer1[j].LPN;
							gtt1->GTTPointer1[j+1].vertexId = gtt1->GTTPointer1[j].vertexId;
						}
						currentGTTIndex_edge += 1;
						GraphLPN++;
						gtt1->GTTPointer1[positionToAdd+1].LPN = GraphLPN;
						gtt1->GTTPointer1[positionToAdd+1].vertexId = ((unsigned int *)temporaryOffsetStorage)[3*i];
						((unsigned int *)pageToActOn)[SSD_PAGE_SIZE / (sizeof(unsigned int)) - 1] = 0;
						Storage[gtt1->GTTPointer1[positionToAdd+1].LPN] = pageToActOn;
					}
					numberOfPages++;
					temp_numOfFreeElementsAvailableInTheBuffer = (SSD_PAGE_SIZE / (sizeof(unsigned int))) - 1;
					temp_bufferPageTopPointer = 0;
				}

				numOfItemsToCopy = (valuesRemainingToAdd + 3) < temp_numOfFreeElementsAvailableInTheBuffer ? valuesRemainingToAdd : temp_numOfFreeElementsAvailableInTheBuffer - 3;
				memcpy(pageToActOn + temp_bufferPageTopPointer * sizeof(unsigned int), temporaryPage + ((unsigned int *)temporaryOffsetStorage)[3*i+1] + ((((unsigned int *)temporaryOffsetStorage)[3*i+2] - valuesRemainingToAdd) * sizeof(unsigned int)), numOfItemsToCopy * sizeof(unsigned int));
				unsigned int numberofVerticesCurrently = ((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 1];
				((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (numberofVerticesCurrently * 3)] = ((unsigned int *)temporaryOffsetStorage)[3*i];
				((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (numberofVerticesCurrently * 3 + 1)] = temp_bufferPageTopPointer * sizeof(unsigned int);
				((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 2 - (numberofVerticesCurrently * 3 + 2)] = numOfItemsToCopy;
				((unsigned int *)pageToActOn)[(SSD_PAGE_SIZE/sizeof(unsigned int)) - 1]++;
				valuesRemainingToAdd = valuesRemainingToAdd - numOfItemsToCopy;
				temp_bufferPageTopPointer += numOfItemsToCopy;
				temp_numOfFreeElementsAvailableInTheBuffer = temp_numOfFreeElementsAvailableInTheBuffer - numOfItemsToCopy - 3;
			} while (valuesRemainingToAdd > 0);
		}
		free(temporaryPage);
		free(temporaryOffsetStorage);
	}
	randomEdgesSrcVertex.clear();
	randomEdgesDestVertex.clear();
}
