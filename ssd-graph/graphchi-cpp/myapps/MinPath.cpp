/**
 * @file
 * @author  Aapo Kyrola <akyrola@cs.cmu.edu>
 * @version 1.0
 *
 * @section LICENSE
 *
 * Copyright [2012] [Aapo Kyrola, Guy Blelloch, Carlos Guestrin / Carnegie Mellon University]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.

 *
 * @section DESCRIPTION
 *
 * Template for GraphChi applications. To create a new application, duplicate
 * this template.
 */



#include <string>

#include "graphchi_basic_includes.hpp"
#include "graphchi_types.hpp"

#define INFINI 0xffffffff
#define PRINTRESULT 0

using namespace graphchi;
using namespace std;

/**
  * Type definitions. Remember to create suitable graph shards using the
  * Sharder-program.
  */
typedef vid_t VertexDataType;
typedef WeightedEdge<vid_t> EdgeDataType; // modify src/graphchi_types.hpp and src/preprocessing/conversions.hpp

vid_t root_id;

/**
  * GraphChi programs need to subclass GraphChiProgram<vertex-type, edge-type>
  * class. The main logic is usually in the update function.
  */
struct MinPathProgram : public GraphChiProgram<VertexDataType, EdgeDataType>
{

    bool converged;
    /**
     *  Vertex update function.
     */
    void update(graphchi_vertex<VertexDataType, EdgeDataType> &vertex, graphchi_context &gcontext)
    {

        if (gcontext.iteration == 0)
        {
            /* On first iteration, initialize vertex (and its edges). This is usually required, because
               on each run, GraphChi will modify the data files. To start from scratch, it is easiest
               do initialize the program in code. Alternatively, you can keep a copy of initial data files. */
            // vertex.set_data(init_value);
            vid_t tmp_data;
            if (vertex.id()==root_id)
            {
                tmp_data=0;
            }
            else
            {
                tmp_data=INFINI;
            }
            vertex.set_data(tmp_data);
            for(int i=0; i < vertex.num_outedges(); i++)
            {
                // Do something
                EdgeDataType edge=vertex.outedge(i)->get_data();
                edge.data=tmp_data;
                vertex.outedge(i)->set_data(edge);
            }
        }
        else if (gcontext.iteration!=gcontext.last_iteration)
        {
            /* Do computation */

            /* Loop over in-edges (example) */
            vid_t curmin=vertex.get_data();
            vid_t diatance=curmin;
            EdgeDataType edge;
            for(int i=0; i < vertex.num_inedges(); i++)
            {
                // Do something
                edge=vertex.inedge(i)->get_data();
                if (edge.data!=INFINI)
                {
                    vid_t tmp=edge.data+edge.weight;
                    if (tmp<curmin && tmp>=edge.data)
                    {
                        curmin=tmp;
                    }
                }
            }
            if (curmin < diatance)
            {
                converged = false;
                vertex.set_data(curmin);
                /* Loop over out-edges (example) */
                for(int i=0; i < vertex.num_outedges(); i++)
                {
                    // Do something
                    // vertex.outedge(i).set_data(x)
                    edge=vertex.outedge(i)->get_data();
                    edge.data=curmin;
                    vertex.outedge(i)->set_data(edge);
                }
            }
            /* Loop over all edges (ignore direction) */
            //for(int i=0; i < vertex.num_edges(); i++) {
            // vertex.edge(i).get_data()
            //}

            // v.set_data(new_value);
        }
        else
        {

            cout << "v: " << vertex.id() << " daitance: " << vertex.get_data() << endl;
        }
    }

    /**
     * Called before an iteration starts.
     */
    void before_iteration(int iteration, graphchi_context &gcontext)
    {
        converged = iteration > 0;
    }

    /**
     * Called after an iteration has finished.
     */
    void after_iteration(int iteration, graphchi_context &gcontext)
    {
        if (converged)
        {
            std::cout << "Converged!" << std::endl;
#if(PRINTRESULT==1)
            if (iteration!=gcontext.last_iteration)
            {
                gcontext.set_last_iteration(iteration+1);
            }
#else
            gcontext.set_last_iteration(iteration);
#endif
        }
    }

    /**
     * Called before an execution interval is started.
     */
    void before_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
    {
    }

    /**
     * Called after an execution interval has finished.
     */
    void after_exec_interval(vid_t window_st, vid_t window_en, graphchi_context &gcontext)
    {
    }

};

int main(int argc, const char ** argv)
{
    /* GraphChi initialization will read the command line
       arguments and the configuration file. */
    graphchi_init(argc, argv);

    /* Metrics object for keeping track of performance counters
       and other information. Currently required. */
    metrics m("MinPath");

    /* Basic arguments for application */
    std::string filename = get_option_string("file");  // Base filename
    int niters           = get_option_int("niters", 100); // Number of iterations
    bool scheduler       = get_option_int("scheduler", 0); // Whether to use selective scheduling

    root_id       = get_option_int("root", 0);
    /* Detect the number of shards or preprocess an input to create them */
    int nshards          = convert_if_notexists<EdgeDataType>(filename,
                           get_option_string("nshards", "auto"));

    /* Run */
    MinPathProgram program;
    graphchi_engine<VertexDataType, EdgeDataType> engine(filename, nshards, scheduler, m);
    engine.run(program, niters);

    /* Report execution metrics */
    metrics_report(m);
    return 0;
}
