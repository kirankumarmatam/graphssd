# This folder has log files

## Vivado install tips

1. Please read doc/install_vivado.pdf and follow the instructions.
 - You can run vivado as a normal user without root permission
 - Select Vivado System Edition with Xilinx SDK and Cable driver

2. Add your account to "dialout" group by editing /etc/group
 - You can use vivado and minicom without root permission

3. minicom or xsdk serial port settings
 - Serial Device         : /dev/ttyUSB1
 - Hardware Flow Control : No

## OpenSSD page read/write performance check

1. 400MB (100,0000 pages) test

        $ TIME_WR: 16.522699, bw=24790139.211171 B/s
        $ TIME_RD: 7.983777, bw=51304040.168488 B/s
        $ ERR: 0 / 409600000 = 0.000000

2. 4GB (1,000,000 pages) test

        $ TIME_WR: 170.174776, bw=24069372.069767 B/s
        $ TIME_RD: 79.921708, bw=51250156.263319 B/s
        $ ERR: 0 / 4096000000 = 0.000000

No error is detected. Good to use for research. :-)
