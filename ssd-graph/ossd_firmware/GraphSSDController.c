#include "GraphSSDController.h"
#include	"memory_map.h"
#include "GraphUtil.h"
#if (GRAPH_SSD_ == 1)
unsigned int GraphLPN;
extern unsigned int Test_variable_size_buffer;
unsigned int currentRetListAddress;
unsigned int numOfFreeElementsAvailableInTheBuffer;
unsigned int bufferPageTopPointer;
unsigned int currentGTTIndex_edge;
unsigned int currentGTTIndex_weight;
struct GTLTable1 *gtlTable1;
struct CompletionQueues *gtlCompletionQueues;
struct CompletionQueuePtrs *gtlCompletionQueuePtrs;

struct GTTTable1 *gtt1;
struct GTTTable1 *gtt1_weight;

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
struct VertexCompletionInformation vertexCompletionInformation;
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1

#if (TEST_COMPLETION_STATUS == 1)
unsigned int currentRetListAddressCompletionStatus;
#endif //TEST_COMPLETION_STATUS == 1

#if (USE_DEFINE_ == 0)
unsigned int MAX_NUM_OF_VERTICES;
unsigned int GTL_POINTER1_ADDR;
#endif

void Graph_Initialize(unsigned int maxNumOfVertices)
{
#if(USE_DEFINE_ == 0)
	MAX_NUM_OF_VERTICES = maxNumOfVertices;
	GTL_POINTER1_ADDR = (GTL_TABLE_1 + sizeof(struct GTLPointer1) * MAX_NUM_OF_VERTICES);
	gtlTable1->GTLPoiniter1 = (struct GTLPointer1 *)GTL_TABLE_1;
	xil_printf("GTLPointer1 = %d\r\n", gtlTable1->GTLPoiniter1);
#endif
	GraphLPN = 50-1;// Also initialized in init_ftl.c
	currentRetListAddress = 0;// Also initialized in init_ftl.c
}

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER == 1 | GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)

//#if (TEST_COMPLETION_STATUS == 1)
/*void * malloc_completionStatus(unsigned int numOfBytes)
{
	unsigned int Address = NAND_FETCH_REQUEST + currentRetListAddressCompletionStatus;//Set these
	currentRetListAddressCompletionStatus += numOfBytes;
	return (void *)Address;
}*/

void process_completionRequest(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	if(!(vertexCompletionInformation.flags == 5 || vertexCompletionInformation.flags == 2 || vertexCompletionInformation.flags == 8))
	{
	        return;
	}
	if(vertexCompletionInformation.flags == 2)
	{
		unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
		if(front == rear)
		{
			xil_printf("ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n", chNo, wayNo);
		}
		unsigned int devAddr = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
		gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front = (front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

		vertexCompletionInformation.NumOfAdjVertices += CopyFunction_InPage(devAddr, Test_variable_size_buffer, (vertexCompletionInformation.NumOfAdjVertices + 1) * sizeof(unsigned int), vertexCompletionInformation.vertexId);

		/*unsigned int lowestVertexId = ((unsigned int *)devAddr)[(PAGE_SIZE / sizeof(unsigned int)) - 2];
		unsigned int distance = vertexCompletionInformation.vertexId - lowestVertexId;
		unsigned int offset = ((unsigned int *)devAddr)[(PAGE_SIZE / sizeof(unsigned int)) - (3*distance + 3)];
		unsigned int number = ((unsigned int *)devAddr)[(PAGE_SIZE / sizeof(unsigned int)) - (3*distance + 4)];
		xil_printf("R 1.1.1 %d %d %d %d\r\n", lowestVertexId, distance, offset, number);
		memcpy((void *)(Test_variable_size_buffer + (vertexCompletionInformation.NumOfAdjVertices + 1)*sizeof()), (void *)(devAddr + offset), number * sizeof(unsigned int));
		vertexCompletionInformation.NumOfAdjVertices += number;
*/
		vertexCompletionInformation.numberOfVerticesRemainingToFetch -= 1;
//		xil_printf("1.1.2 %d %d\r\n", vertexCompletionInformation.NumOfAdjVertices, vertexCompletionInformation.numberOfVerticesRemainingToFetch);

		if(vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0)
		{
//			xil_printf("R 1.1.3 %d\r\n", Test_variable_size_buffer);
			memcpy((void *)Test_variable_size_buffer, &(vertexCompletionInformation.NumOfAdjVertices), sizeof(unsigned int));
			unsigned int numItems = (vertexCompletionInformation.NumOfAdjVertices+1);
			if (numItems != 0) {
				if (numItems < (SECTOR_SIZE_FTL / sizeof(unsigned int))) {
					copyFromDeviceToHost(Test_variable_size_buffer,
							vertexCompletionInformation.prp1,
							vertexCompletionInformation.prp0,
							ceil((numItems * sizeof(unsigned int) * 1.0) / 16)
									* 16);
				} else {
					int i = 0;
					for (i = 0;
							i
									< ceil(
											(numItems * sizeof(unsigned int)
													* 1.0) / SECTOR_SIZE_FTL);
							i++) {
						if(i >= 256)
							xil_printf("ERROR! i = %d numItems = %d\r\n", i, numItems);
						set_auto_tx_dma(vertexCompletionInformation.cmdSlotTag,
								i,
								Test_variable_size_buffer + i * SECTOR_SIZE_FTL);
					}
					check_auto_tx_dma_done();
				}
			}
			vertexCompletionInformation.flags = 0;
			NVME_COMPLETION nvmeCPL;
			nvmeCPL.dword[0] = 0;
			nvmeCPL.specific = 0x0;
			set_auto_nvme_cpl(vertexCompletionInformation.cmdSlotTag,
					nvmeCPL.specific, nvmeCPL.statusFieldWord);
		}
	} else if (vertexCompletionInformation.flags == 5) {
		unsigned int front =
				gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
		unsigned int rear =
				gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
		if (front == rear) {
			xil_printf(
					"ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n",
					chNo, wayNo);
		}
		unsigned int devAddr =
				gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
		unsigned int LPN = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].LPN;
		gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front =
				(front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

		if(LPN <= GraphLPN)
		{
			vertexCompletionInformation.NumOfAdjVertices += CopyFunction_InPage(
							devAddr, Test_variable_size_buffer,
							vertexCompletionInformation.NumOfAdjVertices * sizeof(unsigned int),
							vertexCompletionInformation.vertexId);
		} else if(LPN > GraphLPN){
			vertexCompletionInformation.NumOfAdjVertices_weight += CopyFunction_InPage(
							devAddr, Test_variable_size_buffer + 512*1024,
							vertexCompletionInformation.NumOfAdjVertices_weight * sizeof(unsigned int),
							vertexCompletionInformation.vertexId);
		}

		vertexCompletionInformation.numberOfVerticesRemainingToFetch -= 1;

		if (vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0) {
			//			xil_printf("R 1.1.3 %d\r\n", Test_variable_size_buffer);
			if (vertexCompletionInformation.NumOfAdjVertices != 0) {
				*((unsigned int *) Test_variable_size_buffer) = findEdge_inBuffer(Test_variable_size_buffer, Test_variable_size_buffer + 512*1024, vertexCompletionInformation.destEdge, vertexCompletionInformation.NumOfAdjVertices);
				unsigned int numItems = 1;

				if (numItems < (SECTOR_SIZE_FTL / sizeof(unsigned int))) {
					copyFromDeviceToHost(Test_variable_size_buffer,
							vertexCompletionInformation.prp1,
							vertexCompletionInformation.prp0,
							ceil((numItems * sizeof(unsigned int) * 1.0) / 16)
									* 16);
				} else {
					int i = 0;
					for (i = 0;
							i
									< ceil(
											(numItems * sizeof(unsigned int)
													* 1.0) / SECTOR_SIZE_FTL);
							i++) {
						if (i >= 256)
							xil_printf("ERROR! i = %d numItems = %d\r\n", i,
									numItems);
						set_auto_tx_dma(vertexCompletionInformation.cmdSlotTag,
								i,
								Test_variable_size_buffer + i * SECTOR_SIZE_FTL);
					}
					check_auto_tx_dma_done();
				}
			}
			vertexCompletionInformation.flags = 0;
			NVME_COMPLETION nvmeCPL;
			nvmeCPL.dword[0] = 0;
			nvmeCPL.specific = 0x0;
			set_auto_nvme_cpl(vertexCompletionInformation.cmdSlotTag,
					nvmeCPL.specific, nvmeCPL.statusFieldWord);
		}
	} else if(vertexCompletionInformation.flags == 8)
	{
		unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
		if(front == rear)
		{
			xil_printf("ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n", chNo, wayNo);
		}
		unsigned int devAddr = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
		gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front = (front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

		xil_printf("1.1.1 process_completionRequest %d %d %d %d\r\n", vertexCompletionInformation.vertexId, gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].LPN, vertexCompletionInformation.NumOfAdjVertices, vertexCompletionInformation.numberOfVerticesRemainingToFetch);

		vertexCompletionInformation.NumOfAdjVertices += CopyFunction_InPage(devAddr, Test_variable_size_buffer, (vertexCompletionInformation.NumOfAdjVertices + 1) * sizeof(unsigned int), vertexCompletionInformation.vertexId);

		vertexCompletionInformation.numberOfVerticesRemainingToFetch -= 1;

		if(vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0)
		{
			if(vertexCompletionInformation.hopNum == 0 || vertexCompletionInformation.NumOfAdjVertices == 0)
			{

				if (vertexCompletionInformation.NumOfAdjVertices != 0) {
					memcpy((void *)Test_variable_size_buffer, &(vertexCompletionInformation.NumOfAdjVertices), sizeof(unsigned int));
					unsigned int numItems = (vertexCompletionInformation.NumOfAdjVertices+1);
					if (numItems < (SECTOR_SIZE_FTL / sizeof(unsigned int))) {
						copyFromDeviceToHost(Test_variable_size_buffer,
								vertexCompletionInformation.prp1,
								vertexCompletionInformation.prp0,
								ceil((numItems * sizeof(unsigned int) * 1.0) / 16)
										* 16);
					} else {
						int i = 0;
						for (i = 0;
								i
										< ceil(
												(numItems * sizeof(unsigned int)
														* 1.0) / SECTOR_SIZE_FTL);
								i++) {
							if(i >= 256)
								xil_printf("ERROR! i = %d numItems = %d\r\n", i, numItems);
							set_auto_tx_dma(vertexCompletionInformation.cmdSlotTag,
									i,
									Test_variable_size_buffer + i * SECTOR_SIZE_FTL);
						}
						check_auto_tx_dma_done();
					}
				}
				vertexCompletionInformation.flags = 0;
				NVME_COMPLETION nvmeCPL;
				nvmeCPL.dword[0] = 0;
				nvmeCPL.specific = 0x0;
				set_auto_nvme_cpl(vertexCompletionInformation.cmdSlotTag,
						nvmeCPL.specific, nvmeCPL.statusFieldWord);
			}
			else if (vertexCompletionInformation.hopNum == 1) {//Incorrect here, vertexCompletionInformation.vertexId is a shared variable
				unsigned int firstHopNeighbors =
						vertexCompletionInformation.NumOfAdjVertices;
				vertexCompletionInformation.numberOfVerticesRemainingToFetch =
						vertexCompletionInformation.NumOfAdjVertices;
				vertexCompletionInformation.hopNum = 0;
				unsigned int i = 0;
				for (i = 0; i < firstHopNeighbors; i++) {
					vertexCompletionInformation.vertexId =
							((unsigned int *) Test_variable_size_buffer)[i + 1];
					unsigned int gttVidIndex, gttVidNumber;
					binarySearch(gtt1->GTTPointer1, 0, currentGTTIndex_edge,
							vertexCompletionInformation.vertexId, &gttVidIndex,
							&gttVidNumber);
					vertexCompletionInformation.numberOfVerticesRemainingToFetch +=
							(gttVidNumber - 1);
					vertexCompletionInformation.isEdge = 1;
					getEdgesToSSDDram_initiateLoading(vertexCompletionInformation.cmdSlotTag, gttVidIndex,
							gttVidNumber, 0);
				}
			}
		}
	}
	return;
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
}

void process_completionRequest_write(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	if(!(vertexCompletionInformation.flags == 5 || vertexCompletionInformation.flags == 3))
	{
	        return;
	}
	unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
	unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
	if(front == rear)
	{
		xil_printf("ERROR: COMPLETION_QUEUE is empty! chNo = %d wayNo = %d\r\n", chNo, wayNo);
	}
	unsigned int devAddr = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].devAddr;
	unsigned int destPageOffset = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].destPageOffset;
	unsigned int srcPageOffset = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].srcPageOffset;
	unsigned int Number = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].Number;
	unsigned int lowestVertexId = gtlCompletionQueues->completionQueueEntry[chNo][wayNo][front].lowestVertexId;
	gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front = (front + 1) % MAX_COMPLETION_QUEUE_ENTRY;

	if(debug_mode == 1)
		{
		xil_printf("W pcRW %d %d %d %d \r\n", chNo, wayNo, gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front, gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear);
		xil_printf("W pcRW 1.1.1.1 %d %d %d %d %d %d\r\n", devAddr, destPageOffset, srcPageOffset, vertexCompletionInformation.vertexId, lowestVertexId, Number);
		}

	memcpy((void *)(devAddr+destPageOffset), (void *)(Test_variable_size_buffer+srcPageOffset), Number*sizeof(unsigned int));
	unsigned int vertexId = vertexCompletionInformation.vertexId;
	((unsigned int *)devAddr)[(PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3)] = vertexId;
	((unsigned int *)devAddr)[(PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3 + 1)] = destPageOffset;
	((unsigned int *)devAddr)[(PAGE_SIZE/sizeof(unsigned int)) - 2 - ((vertexId - lowestVertexId) * 3 + 2)] = Number;
	if(lowestVertexId == vertexId)
	{
		((unsigned int *)devAddr)[(PAGE_SIZE/sizeof(unsigned int)) - 1] = 1;
	}
	else {
		((unsigned int *)devAddr)[(PAGE_SIZE/sizeof(unsigned int)) - 1] += 1;
	}
	vertexCompletionInformation.numberOfVerticesRemainingToFetch -= Number;

	if(vertexCompletionInformation.numberOfVerticesRemainingToFetch == 0)
	{
		if(vertexCompletionInformation.NumOfAdjVertices == 0 && vertexCompletionInformation.isEdge == 1)
		{
			vertexCompletionInformation.isEdge = 0;
			return;
		}
		if(debug_mode)
			xil_printf("W pcRW 1.1.1.2 vid = %d\r\n", vertexCompletionInformation.vertexId);
		vertexCompletionInformation.flags = 0;
		NVME_COMPLETION nvmeCPL;
		nvmeCPL.dword[0] = 0;
		nvmeCPL.specific = 0x0;
		set_auto_nvme_cpl(vertexCompletionInformation.cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
		reservedReq = 1;
	}
	return;
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
}

int GraphPmRead_completionTest(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, P_BUFFER_REQ_INFO bufCmd, unsigned int Number, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int isWrite)
{
	LOW_LEVEL_REQ_INFO lowLevelCmd;
	unsigned int dieNo = bufCmd->lpn % DIE_NUM;
	unsigned int dieLpn = bufCmd->lpn / DIE_NUM;

	if (pageMap->pmEntry[dieNo][dieLpn].ppn != 0xffffffff)
	{
		lowLevelCmd.rowAddr = pageMap->pmEntry[dieNo][dieLpn].ppn;
		lowLevelCmd.spareDataBuf = SPARE_ADDR;
		lowLevelCmd.devAddr = bufCmd->devAddr;
		lowLevelCmd.cmdSlotTag = bufCmd->cmdSlotTag;
		lowLevelCmd.startDmaIndex = bufCmd->startDmaIndex;
		lowLevelCmd.chNo = dieNo % CHANNEL_NUM;
		lowLevelCmd.wayNo = dieNo / CHANNEL_NUM;
		lowLevelCmd.subReqSect =  bufCmd->subReqSect;
		lowLevelCmd.bufferEntry = bufCmd->bufferEntry;
		lowLevelCmd.request = V2FCommand_ReadPageTrigger;//This request is to fetch the data to page buffer
//		lowLevelCmd.request = V2FCommand_ReadPageTransfer;
#if (TEST_COMPLETION_STATUS == 1)
/*		unsigned int * completionStatusQueue = (unsigned int *)malloc_completionStatus(9 * sizeof(unsigned int));
		*completionStatusQueue = lowLevelCmd.chNo;
		*(completionStatusQueue + 1) = lowLevelCmd.wayNo;
		*(completionStatusQueue + 2) = cmdSlotTag;
		*(completionStatusQueue + 3) = prp0;
		*(completionStatusQueue + 4) = prp1;
		*(completionStatusQueue + 5) = bufCmd->devAddr;
		*(completionStatusQueue + 6) = destPageOffset;
		*(completionStatusQueue + 7) = srcPageOffset;
		*(completionStatusQueue + 8) = Number;
	*/	unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[lowLevelCmd.chNo][lowLevelCmd.wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[lowLevelCmd.chNo][lowLevelCmd.wayNo].rear;
		if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
		{
			xil_printf("ERROR: COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", lowLevelCmd.chNo, lowLevelCmd.wayNo);
		}
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].cmdSlotTag = cmdSlotTag;
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].prp0 = prp0;
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].prp1 = prp1;
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].devAddr = bufCmd->devAddr;
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].destPageOffset = destPageOffset;
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].srcPageOffset = srcPageOffset;
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].Number = Number;
		gtlCompletionQueuePtrs->completionQueuePointer[lowLevelCmd.chNo][lowLevelCmd.wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);
#endif //TEST_COMPLETION_STATUS == 1
		PushToReqQueue(&lowLevelCmd);
/*#if (TEST_COMPLETION_STATUS == 0)
		if(isWrite == 1)
		{
			memcpy((void *)(bufCmd->devAddr + destPageOffset), (void *)(Test_variable_size_buffer + srcPageOffset), Number*sizeof(unsigned int));
		}
//		copyFromDeviceToHost(Test_variable_size_buffer, prp1, prp0, Number * sizeof(unsigned int)+pageOffset);
#endif //TEST_COMPLETION_STATUS == 0
	*/}
	else
	{//This option is called when no page had been written but content is being read from it
		if(isWrite == 0)
		{
			memcpy((void *)(Test_variable_size_buffer + destPageOffset), (void *)(bufCmd->devAddr+srcPageOffset), Number*sizeof(unsigned int));
		}
		else if(isWrite == 1)
		{
			memcpy((void *)(bufCmd->devAddr + destPageOffset), (void *)(Test_variable_size_buffer + srcPageOffset), Number*sizeof(unsigned int));
		}
	}
	return 0;
}

/*This function is broken*/
unsigned int GraphLRUBufRead_completionTest(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int sectorNumber, unsigned int Number, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int isWrite)
{
		BUFFER_REQ_INFO bufferCmd;
		unsigned int tempLpn, hitEntry, dmaIndex, devAddr, dieNo;

		dmaIndex = 0;
		tempLpn = sectorNumber / SECTOR_NUM_PER_PAGE;
//		tempLpn = sectorNumber;

		hitEntry = CheckBufHit(tempLpn);
		if(hitEntry != 0x7fff)
		{
			devAddr = BUFFER_ADDR + hitEntry * BUF_ENTRY_SIZE + (sectorNumber % SECTOR_NUM_PER_PAGE) * SECTOR_SIZE_FTL;
			if(isWrite == 0)
			{
				memcpy((void *)(Test_variable_size_buffer+destPageOffset), (void *)(devAddr+srcPageOffset), Number*sizeof(unsigned int));
				//Need to fill all the details, where to fill the detials, it may be better to keep all the information in the global pointer, this information can be kept one
			}
			else if(isWrite == 1)
			{
				memcpy((void *)(devAddr+destPageOffset), (void *)(Test_variable_size_buffer+srcPageOffset), Number*sizeof(unsigned int));
				bufMap->bufEntry[hitEntry].dirty = 1;
			}
			return 1;
		}
		else
		{
			bufferCmd.bufferEntry =  AllocateBufEntry(tempLpn);
			bufferCmd.lpn = tempLpn;
			bufferCmd.devAddr = (BUFFER_ADDR + bufferCmd.bufferEntry * BUF_ENTRY_SIZE + (sectorNumber % SECTOR_NUM_PER_PAGE) * SECTOR_SIZE_FTL);
			bufferCmd.cmdSlotTag = cmdSlotTag;
			bufferCmd.startDmaIndex = dmaIndex;
			bufferCmd.subReqSect = 1;

			if(isWrite == 0)
			{
				bufMap->bufEntry[bufferCmd.bufferEntry].dirty = 0;
			} else if(isWrite == 1)
			{
				bufMap->bufEntry[bufferCmd.bufferEntry].dirty = 1;
			}

			//link
			dieNo = tempLpn % DIE_NUM;
			if(bufLruList->bufLruEntry[dieNo].head != 0x7fff)
			{
				bufMap->bufEntry[bufferCmd.bufferEntry].prevEntry = 0x7fff;
				bufMap->bufEntry[bufferCmd.bufferEntry].nextEntry = bufLruList->bufLruEntry[dieNo].head;
				bufMap->bufEntry[bufLruList->bufLruEntry[dieNo].head].prevEntry = bufferCmd.bufferEntry;
				bufLruList->bufLruEntry[dieNo].head = bufferCmd.bufferEntry;
			}
			else
			{
				bufMap->bufEntry[bufferCmd.bufferEntry].prevEntry = 0x7fff;
				bufMap->bufEntry[bufferCmd.bufferEntry].nextEntry = 0x7fff;
				bufLruList->bufLruEntry[dieNo].head = bufferCmd.bufferEntry;
				bufLruList->bufLruEntry[dieNo].tail = bufferCmd.bufferEntry;
			}
			bufMap->bufEntry[bufferCmd.bufferEntry].lpn = tempLpn;
			GraphPmRead_completionTest(cmdSlotTag, prp0, prp1, &bufferCmd, Number, destPageOffset, srcPageOffset, isWrite);
			return 0;
		}
}
//#endif //TEST_COMPLETION_STATUS == 1

int GraphPmRead(unsigned int cmdSlotTag, P_BUFFER_REQ_INFO bufCmd)
{
	LOW_LEVEL_REQ_INFO lowLevelCmd;
	unsigned int dieNo = bufCmd->lpn % DIE_NUM;
	unsigned int dieLpn = bufCmd->lpn / DIE_NUM;

	if (pageMap->pmEntry[dieNo][dieLpn].ppn != 0xffffffff)
	{
		lowLevelCmd.rowAddr = pageMap->pmEntry[dieNo][dieLpn].ppn;
		lowLevelCmd.spareDataBuf = SPARE_ADDR;
		lowLevelCmd.devAddr = bufCmd->devAddr;
		lowLevelCmd.cmdSlotTag = bufCmd->cmdSlotTag;
		lowLevelCmd.startDmaIndex = bufCmd->startDmaIndex;
		lowLevelCmd.chNo = dieNo % CHANNEL_NUM;
		lowLevelCmd.wayNo = dieNo / CHANNEL_NUM;
		lowLevelCmd.subReqSect =  bufCmd->subReqSect;
		lowLevelCmd.bufferEntry = bufCmd->bufferEntry;
		lowLevelCmd.request = V2FCommand_ReadPageTrigger;

		unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[lowLevelCmd.chNo][lowLevelCmd.wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[lowLevelCmd.chNo][lowLevelCmd.wayNo].rear;
		if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
		{
			xil_printf("ERROR (GraphPmRead): COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", lowLevelCmd.chNo, lowLevelCmd.wayNo);
		}
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].devAddr = bufCmd->devAddr;
		gtlCompletionQueues->completionQueueEntry[lowLevelCmd.chNo][lowLevelCmd.wayNo][rear].LPN = bufCmd->lpn;
		gtlCompletionQueuePtrs->completionQueuePointer[lowLevelCmd.chNo][lowLevelCmd.wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);

		PushToReqQueue(&lowLevelCmd);
		if(debug_mode == 1)
						xil_printf("E40 vId = %d\r\n", vertexCompletionInformation.vertexId);
		LOW_LEVEL_REQ_INFO lowLevelCmd1;
		lowLevelCmd1.chNo = lowLevelCmd.chNo;
		lowLevelCmd1.wayNo = lowLevelCmd.wayNo;
		lowLevelCmd1.request = V2FCommand_ReadCompletionStatus;
		PushToReqQueue(&lowLevelCmd1);
	}
	else
	{//This option is called when no page had been written but content is being read from it
		xil_printf("ERROR INVALID CONTENT BEING READ!!\r\n");
	}
	reservedReq = 1;//KIRAN: DELETE IT (OR THINK ABOUT IT)
	NAND_PAGE_ACCESS_READ++;
	return 0;
}

void GraphLRUBufRead(unsigned int cmdSlotTag, unsigned int sectorNumber)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	BUFFER_REQ_INFO bufferCmd;
	unsigned int tempLpn, hitEntry, dmaIndex, devAddr, dieNo;

	dmaIndex = 0;
//	tempLpn = sectorNumber / SECTOR_NUM_PER_PAGE;
	tempLpn = sectorNumber;

	if(debug_mode == 1)
		xil_printf("R 1.1 %d \r\n", sectorNumber);

	hitEntry = CheckBufHit(tempLpn);
	if(hitEntry != 0x7fff)
	{
//		devAddr = BUFFER_ADDR + hitEntry * BUF_ENTRY_SIZE + (sectorNumber % SECTOR_NUM_PER_PAGE) * SECTOR_SIZE_FTL;
		devAddr = BUFFER_ADDR + hitEntry * BUF_ENTRY_SIZE;

		unsigned int dieNo = tempLpn % DIE_NUM;
		unsigned int chNo = dieNo % CHANNEL_NUM;
		unsigned int wayNo = dieNo / CHANNEL_NUM;
		unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
		if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
		{
			xil_printf("ERROR (GraphPmRead): COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", chNo, wayNo);
		}
		gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].devAddr = devAddr;
		gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].LPN = tempLpn;
		gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
		process_completionRequest(chNo, wayNo, 0, 1);
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
			//Temporarily GRUBufRead is broken
	}
	else
	{
		bufferCmd.bufferEntry =  AllocateBufEntry(tempLpn);
		bufferCmd.lpn = tempLpn;
//		bufferCmd.devAddr = (BUFFER_ADDR + bufferCmd.bufferEntry * BUF_ENTRY_SIZE + (sectorNumber % SECTOR_NUM_PER_PAGE) * SECTOR_SIZE_FTL);
		bufferCmd.devAddr = (BUFFER_ADDR + bufferCmd.bufferEntry * BUF_ENTRY_SIZE);
		bufferCmd.cmdSlotTag = cmdSlotTag;
		bufferCmd.startDmaIndex = dmaIndex;
		bufferCmd.subReqSect = 4;

		bufMap->bufEntry[bufferCmd.bufferEntry].dirty = 0;

		//link
		dieNo = tempLpn % DIE_NUM;
		if(bufLruList->bufLruEntry[dieNo].head != 0x7fff)
		{
			bufMap->bufEntry[bufferCmd.bufferEntry].prevEntry = 0x7fff;
			bufMap->bufEntry[bufferCmd.bufferEntry].nextEntry = bufLruList->bufLruEntry[dieNo].head;
			bufMap->bufEntry[bufLruList->bufLruEntry[dieNo].head].prevEntry = bufferCmd.bufferEntry;
			bufLruList->bufLruEntry[dieNo].head = bufferCmd.bufferEntry;
		}
		else
		{
			bufMap->bufEntry[bufferCmd.bufferEntry].prevEntry = 0x7fff;
			bufMap->bufEntry[bufferCmd.bufferEntry].nextEntry = 0x7fff;
			bufLruList->bufLruEntry[dieNo].head = bufferCmd.bufferEntry;
			bufLruList->bufLruEntry[dieNo].tail = bufferCmd.bufferEntry;
		}
		bufMap->bufEntry[bufferCmd.bufferEntry].lpn = tempLpn;

		GraphPmRead(cmdSlotTag, &bufferCmd);
	}
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
}
#endif

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER == 1 | GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
void GraphLRUBufWrite(unsigned int curSect, unsigned int SrcMemAddress, unsigned int Len, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int lowestVertexId)
{
#if(GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	BUFFER_REQ_INFO bufferCmd;
	unsigned int tempLpn, hitEntry, devAddr, dieNo;

//	tempLpn = curSect / SECTOR_NUM_PER_PAGE; //tempLpn is the logical page number, there should be a translation from tempLpn to tempPpn
	tempLpn = curSect;

	hitEntry = CheckBufHit(tempLpn);
	if(hitEntry != 0x7fff)//If it is a hit then write it into that buffer only
	{
//		devAddr = (BUFFER_ADDR + hitEntry * BUF_ENTRY_SIZE + (curSect % SECTOR_NUM_PER_PAGE) * SECTOR_SIZE_FTL);
		devAddr = (BUFFER_ADDR + hitEntry * BUF_ENTRY_SIZE);

		dieNo = tempLpn % DIE_NUM;
		unsigned int chNo = dieNo % CHANNEL_NUM;
		unsigned int wayNo = dieNo / CHANNEL_NUM;
		unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
		unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
		if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
		{
			xil_printf("ERROR: COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", chNo, wayNo);
		}
		gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].devAddr = devAddr;
		gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].destPageOffset = destPageOffset;
		gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].srcPageOffset = srcPageOffset;
		gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].Number = Len / sizeof(unsigned int);
		gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].lowestVertexId = lowestVertexId;
		gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);

		if(debug_mode == 1)
		{
		xil_printf("W GLBW %d %d %d %d \r\n", chNo, wayNo, gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front, gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear);
		xil_printf("W GLBW 1.1.0 %d %d\r\n", gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front, gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear);
		}

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
		process_completionRequest_write(chNo, wayNo, 0, 1);
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
		bufMap->bufEntry[hitEntry].dirty = 1;
//		reservedReq = 1;
	}
	else
	{
		unsigned int isLruBufHit;
		bufferCmd.bufferEntry =  GraphAllocateBufEntry(tempLpn, &isLruBufHit);
		bufferCmd.lpn = tempLpn;
//		bufferCmd.devAddr = BUFFER_ADDR + bufferCmd.bufferEntry * BUF_ENTRY_SIZE + (curSect % SECTOR_NUM_PER_PAGE) * SECTOR_SIZE_FTL;
		bufferCmd.devAddr = BUFFER_ADDR + bufferCmd.bufferEntry * BUF_ENTRY_SIZE;
		bufMap->bufEntry[bufferCmd.bufferEntry].dirty = 1;

		//link
		dieNo = tempLpn % DIE_NUM;
		if(isLruBufHit == 1)
		{
			unsigned int chNo = dieNo % CHANNEL_NUM;
			unsigned int wayNo = dieNo / CHANNEL_NUM;
			unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
			unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
			if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
			{
				xil_printf("ERROR: COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", chNo, wayNo);
			}
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].devAddr = bufferCmd.devAddr;
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].destPageOffset = destPageOffset;
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].srcPageOffset = srcPageOffset;
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].Number = Len / sizeof(unsigned int);
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].lowestVertexId = lowestVertexId;
			gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);

if(debug_mode == 1)
	xil_printf("W GLBW %d %d\r\n", gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front, gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear);

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
			process_completionRequest_write(chNo, wayNo, 0, 1);
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
		}
		else if(isLruBufHit == 0)
		{
//			xil_printf("W tempLPN = %d\r\n", tempLpn);
			unsigned int chNo = dieNo % CHANNEL_NUM;
			unsigned int wayNo = dieNo / CHANNEL_NUM;
			unsigned int front = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front;
			unsigned int rear = gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear;
			if(((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY))
			{
				xil_printf("ERROR: COMPLETION_QUEUE is full! chNo = %d wayNo = %d\r\n", chNo, wayNo);
			}
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].devAddr = bufferCmd.devAddr;
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].destPageOffset = destPageOffset;
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].srcPageOffset = srcPageOffset;
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].Number = Len / sizeof(unsigned int);
			gtlCompletionQueues->completionQueueEntry[chNo][wayNo][rear].lowestVertexId = lowestVertexId;
			gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear = ((rear+1)%MAX_COMPLETION_QUEUE_ENTRY);

			if(debug_mode == 1)
			{
			xil_printf("W GLBW 1.1.2 %d %d %d %d %d\r\n", bufferCmd.devAddr, destPageOffset, srcPageOffset, Len / sizeof(unsigned int), lowestVertexId);
			xil_printf("W GLBW.1.3 %d %d\r\n", gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].front, gtlCompletionQueuePtrs->completionQueuePointer[chNo][wayNo].rear);
			}
//			xil_printf("Init WC\r\n");
			LOW_LEVEL_REQ_INFO lowLevelCmd;
			lowLevelCmd.chNo = chNo;
			lowLevelCmd.wayNo = wayNo;
			lowLevelCmd.request = V2FCommand_WriteCompletionStatus;
			PushToReqQueue(&lowLevelCmd);
//			reservedReq = 1;
		}

		if(bufLruList->bufLruEntry[dieNo].head != 0x7fff)
		{
			bufMap->bufEntry[bufferCmd.bufferEntry].prevEntry = 0x7fff;
			bufMap->bufEntry[bufferCmd.bufferEntry].nextEntry = bufLruList->bufLruEntry[dieNo].head;
			bufMap->bufEntry[bufLruList->bufLruEntry[dieNo].head].prevEntry = bufferCmd.bufferEntry;
			bufLruList->bufLruEntry[dieNo].head = bufferCmd.bufferEntry;
		}
		else
		{
			bufMap->bufEntry[bufferCmd.bufferEntry].prevEntry = 0x7fff;
			bufMap->bufEntry[bufferCmd.bufferEntry].nextEntry = 0x7fff;
			bufLruList->bufLruEntry[dieNo].head = bufferCmd.bufferEntry;
			bufLruList->bufLruEntry[dieNo].tail = bufferCmd.bufferEntry;
		}
		bufMap->bufEntry[bufferCmd.bufferEntry].lpn = tempLpn;
	}
	return;
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
}
#endif

/*void * malloc_returnList(unsigned int numOfBytes)
{
	unsigned int Address = GTL_POINTER1_ADDR + currentRetListAddress;
	currentRetListAddress += numOfBytes;
	return (void *)Address;
}

void bufferingPage(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int getNumAdjVertices, struct GTLPointer1 * gtlPointer)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
	if(getNumAdjVertices < (SECTOR_SIZE_FTL/sizeof(unsigned int)))
	{
    	CopyFromHost(Test_variable_size_buffer, prp0, prp1, ceil(getNumAdjVertices*sizeof(unsigned int)*1.0/16)*16);
	}
	else {
		int i=0;
		for(i=0; i < ceil((getNumAdjVertices*sizeof(unsigned int)*1.0) / SECTOR_SIZE_FTL); i++)
		{
			set_auto_rx_dma(cmdSlotTag, i, Test_variable_size_buffer+i*SECTOR_SIZE_FTL);
		}
		check_auto_rx_dma_done();
	}
	int i=0;
    unsigned int valuesRemainingToAdd = getNumAdjVertices;
    while(valuesRemainingToAdd > 0)
    {
    	unsigned int  numOfItemsToCopy;
    	numOfItemsToCopy = valuesRemainingToAdd < numOfFreeElementsAvailableInTheBuffer ? valuesRemainingToAdd : numOfFreeElementsAvailableInTheBuffer;
        unsigned int tempLPN = GraphLPN;
    	GraphLRUBufWrite(tempLPN, Test_variable_size_buffer, numOfItemsToCopy * sizeof(unsigned int), bufferPageTopPointer * sizeof(unsigned int), (getNumAdjVertices - valuesRemainingToAdd)*sizeof(unsigned int));
    	valuesRemainingToAdd = valuesRemainingToAdd - numOfItemsToCopy;
    	if(valuesRemainingToAdd == 0)
    	{
    		if(i == 0)
    		{
    			gtlPointer->LPN = tempLPN;
    			gtlPointer->Offset = bufferPageTopPointer;
    			gtlPointer->Number = numOfItemsToCopy;
    			gtlPointer->direct = 1;
    		}
    		else {
    	    	memcpy((void *)((unsigned int *)GRAPH_BUFFER + i*3), &tempLPN, sizeof(unsigned int));
    	    	memcpy((void *)((unsigned int *)GRAPH_BUFFER + i*3+1), &bufferPageTopPointer, sizeof(unsigned int));
    	    	memcpy((void *)((unsigned int *)GRAPH_BUFFER + i*3+2), &numOfItemsToCopy, sizeof(unsigned int));
    	    	i++;
    	        unsigned int *ReturnList;
    	       	ReturnList = (unsigned int *)malloc_returnList(3*i*sizeof(unsigned int));
    	    	memcpy((void *)(ReturnList), (void *)(GRAPH_BUFFER), 3*i*sizeof(unsigned int));
    	    	gtlPointer->LPN = (unsigned int)ReturnList;
    	    	gtlPointer->Offset = 0;
    	    	gtlPointer->Number = i;
    	    	gtlPointer->direct = 0;
    		}
    		numOfFreeElementsAvailableInTheBuffer = numOfFreeElementsAvailableInTheBuffer - numOfItemsToCopy;
    		bufferPageTopPointer = bufferPageTopPointer + numOfItemsToCopy;
    		if(numOfFreeElementsAvailableInTheBuffer == 0)
    		{
    			GraphLPN++;
    			numOfFreeElementsAvailableInTheBuffer = SECTOR_SIZE_FTL / sizeof(unsigned int);
    			bufferPageTopPointer = 0;
    		}
    		break;
    	}
    	memcpy((void *)((unsigned int *)GRAPH_BUFFER + i*3), &tempLPN, sizeof(unsigned int));
    	memcpy((void *)((unsigned int *)GRAPH_BUFFER + i*3+1), &bufferPageTopPointer, sizeof(unsigned int));
    	memcpy((void *)((unsigned int *)GRAPH_BUFFER + i*3+2), &numOfItemsToCopy, sizeof(unsigned int));
    	i++;
    	numOfFreeElementsAvailableInTheBuffer = numOfFreeElementsAvailableInTheBuffer - numOfItemsToCopy;
    	bufferPageTopPointer = bufferPageTopPointer + numOfItemsToCopy;
    	if(numOfFreeElementsAvailableInTheBuffer == 0)
    	{
    	    			GraphLPN++;
    	    			numOfFreeElementsAvailableInTheBuffer = SECTOR_SIZE_FTL / sizeof(unsigned int);
    	    			bufferPageTopPointer = 0;
    	}
    }
    return;
#endif
}*/
#endif
