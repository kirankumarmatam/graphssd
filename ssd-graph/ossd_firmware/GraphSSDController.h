#ifndef GRAPH_SSD_H_
#define GRAPH_SSD_H_
#include "math.h"
#include "init_ftl.h"
#include <assert.h>
struct GTLPointer1 {
	unsigned int LPN;
	unsigned int Number;
	unsigned int Offset;
	unsigned int direct;
};

struct GTTPointer {
	unsigned int vertexId;
	unsigned int LPN;
};

unsigned int debug_mode;

unsigned int NAND_PAGE_ACCESS_EVICTION;
unsigned int NAND_PAGE_ACCESS_READ;

#define USE_DEFINE_ 1 //Change it in memory_map.h also
#define EDGE_WEIGHT_ 1 //Change it in memory_map.h also

#if (USE_DEFINE_ == 1)
#define MAX_NUM_OF_VERTICES		10000000
#endif

#define GTL_INTERMMEDIATE_VERTEX_PAGE 0
#define GTL_INTERMMEDIATE_VERTEX_BUFFER 0
#define GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO 1
#define TEST_COMPLETION_STATUS 0// Uncomment the corresponding code if you want it // Change it in memory_map.h also
#define GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION 1

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
struct VertexCompletionInformation {
	unsigned int vertexId;
	unsigned int cmdSlotTag;
	unsigned int prp0;
	unsigned int prp1;
	unsigned int numberOfVerticesRemainingToFetch;
	unsigned int isEdge;
	unsigned int destEdge;
	unsigned int NumOfAdjVertices;
	unsigned int NumOfAdjVertices_weight;
	unsigned int flags;
	unsigned int hopNum;
};
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1

struct GTLTable1 {
#if (USE_DEFINE_ == 1)
	struct GTLPointer1	GTLPoiniter1[MAX_NUM_OF_VERTICES];
#endif
#if (USE_DEFINE_ == 0)
	struct GTLPointer1	*GTLPoiniter1;
#endif
};

struct GTTTable1 {
	struct GTTPointer	GTTPointer1[MAX_NUM_OF_VERTICES];
};

struct GTTTable1Weight {
	struct GTTPointer	GTLPoniter1[MAX_NUM_OF_VERTICES];
};

struct CompletionQueueEntry {
	unsigned int cmdSlotTag;
	unsigned int prp0;
	unsigned int prp1;
	unsigned int devAddr;
	unsigned int destPageOffset;
	unsigned int srcPageOffset;
	unsigned int Number;
	unsigned int lowestVertexId;
	unsigned int LPN;
};

struct CompletionQueuePointer {
	unsigned int front;
	unsigned int rear;
};

#define MAX_COMPLETION_QUEUE_ENTRY	8

struct CompletionQueues {
	struct CompletionQueueEntry completionQueueEntry[MAX_CHANNEL_NUM][MAX_WAY_NUM][MAX_COMPLETION_QUEUE_ENTRY];
};

struct CompletionQueuePtrs {
	struct CompletionQueuePointer completionQueuePointer[MAX_CHANNEL_NUM][MAX_WAY_NUM];
};

void *GraphBuffer;

void bufferingPage(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int getNumAdjVertices);
void bufferingPage_weight(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int getNumAdjVertices);
void getEdgeWeight(unsigned int vID, unsigned int cmdSlotTag, unsigned int destEdge, unsigned int prp0, unsigned int prp1);
void getAdjEdgeList(unsigned int vID, unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1);
void getTwoHopAdjEdgeList(unsigned int vID, unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1);
void * malloc_returnList(unsigned int numOfBytes);
void Graph_Initialize(unsigned int maxNumOfVertices);
void getEdgesToSSDDram_calcNumAdjVertices(unsigned int vID, unsigned int *NumberOfAdjVertices);
void getEdgesToSSDDram_initiateLoading(unsigned int cmdSlotTag, unsigned int gttVidIndex, unsigned int gttVidNumber, unsigned int isEdge);
void findEdgeOffset(unsigned int vID, unsigned int destEdge, unsigned int NumberOfAdjVertices, unsigned int *reqLPN, unsigned int *reqOffset);
unsigned int binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number);
unsigned int CopyFunction_InPage(unsigned int devAddr, unsigned int Test_variable_size_buffer, unsigned int currentTopPointer, unsigned int vertexId);
unsigned int findEdge_inBuffer(unsigned int Test_variable_size_buffer, unsigned int Test_variable_size_buffer_weight, unsigned int destEdge, unsigned int NumberOfAdjVertices);
void InitializeGTT(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int numOfEntries);

void process_completionRequest(unsigned int chNo, unsigned int wayNo, unsigned int front, unsigned int isLruBufHit);
void process_completionRequest_write(unsigned int chNo, unsigned int wayNo, unsigned int Front, unsigned int isLruBufHit);
#if (TEST_COMPLETION_STATUS == 1)
unsigned int GraphLRUBufRead_completionTest(unsigned int cmdSlotTag, unsigned int sectorNumber, unsigned int Number, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int isWrite, unsigned int prp1, unsigned int prp0);
#endif //TEST_COMPLETION_STATUS == 1

#if (GTL_INTERMMEDIATE_VERTEX_PAGE == 1)
void GraphLRUBufWrite(unsigned int curSect, unsigned int SrcMemAddress, unsigned int Len, unsigned int pageOffset);
void GraphLRUBufRead(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int sectorNumber, unsigned int Number, unsigned int pageOffset);
#endif

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER == 1 | GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
void GraphLRUBufWrite(unsigned int curSect, unsigned int SrcMemAddress, unsigned int Len, unsigned int destPageOffset, unsigned int srcPageOffset, unsigned int lowestVertexId);
void GraphLRUBufRead(unsigned int cmdSlotTag, unsigned int sectorNumber);
#endif

#endif
