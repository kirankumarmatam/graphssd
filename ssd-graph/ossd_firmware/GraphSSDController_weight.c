#include "GraphSSDController.h"
#include	"memory_map.h"
#include "GraphUtil.h"
#if (GRAPH_SSD_ == 1)
unsigned int GraphLPN;
extern unsigned int Test_variable_size_buffer;
extern unsigned int currentRetListAddress;
extern unsigned int numOfFreeElementsAvailableInTheBuffer;
extern unsigned int bufferPageTopPointer;
extern unsigned int currentGTTIndex_edge;
extern unsigned int currentGTTIndex_weight;
unsigned int numOfFreeElementsAvailableInTheBufferWeight;
unsigned int bufferPageTopPointerWeight;
unsigned int GraphLPNWeight;
extern struct GTLTable1 *gtlTable1;
struct GTLTable1Weight *gtlTable1Weight;
unsigned int currentRetListAddressWeight;

extern struct GTTTable1 *gtt1;
extern struct GTTTable1 *gtt1_weight;

#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
extern struct VertexCompletionInformation vertexCompletionInformation;
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1

void bufferingPage_loadToSSD(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int getNumAdjVertices)
{
	if(getNumAdjVertices == 0) return;
	unsigned int numItems = 2*getNumAdjVertices;
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
	if(numItems < (SECTOR_SIZE_FTL/sizeof(unsigned int)))
	{
    	CopyFromHost(Test_variable_size_buffer, prp0, prp1, ceil(numItems*sizeof(unsigned int)*1.0/16)*16);
	}
	else {
		int i=0;
		for(i=0; i < ceil((numItems*sizeof(unsigned int)*1.0) / SECTOR_SIZE_FTL); i++)
		{
			set_auto_rx_dma(cmdSlotTag, i, Test_variable_size_buffer+i*SECTOR_SIZE_FTL);
		}
		check_auto_rx_dma_done();
	}
#endif
}

void * malloc_returnList(unsigned int numOfBytes)
{
	unsigned int Address = GTL_POINTER1_ADDR + currentRetListAddress;
	currentRetListAddress += numOfBytes;
	return (void *)Address;
}

void * malloc_returnListWeight(unsigned int numOfBytes)
{
	unsigned int Address = GTL_POINTER1_ADDR_WEIGHT + currentRetListAddressWeight;//Set these
	currentRetListAddressWeight += numOfBytes;
	return (void *)Address;
}

void bufferingPage_loadToNAND(unsigned int getNumAdjVertices, unsigned int isEdges)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
	unsigned int valuesRemainingToAdd = getNumAdjVertices;
	unsigned int numOfItemsToCopy;
	{
		if (isEdges == 1) {
			if ((valuesRemainingToAdd + 3)
					> numOfFreeElementsAvailableInTheBuffer) {
				GraphLPN++;
				numOfFreeElementsAvailableInTheBuffer = (PAGE_SIZE / (sizeof(unsigned int))) - 1;
				bufferPageTopPointer = 0;
				currentGTTIndex_edge += 1;
				gtt1->GTTPointer1[currentGTTIndex_edge].vertexId = vertexCompletionInformation.vertexId;
				gtt1->GTTPointer1[currentGTTIndex_edge].LPN = GraphLPN;
				if(debug_mode == 1)
					xil_printf("W bPLTN %d %d %d\r\n",currentGTTIndex_edge, GraphLPN, vertexCompletionInformation.vertexId);
			}
		} else {
			if ((valuesRemainingToAdd + 3)
				> numOfFreeElementsAvailableInTheBufferWeight) {
				GraphLPNWeight++;
				numOfFreeElementsAvailableInTheBufferWeight = (PAGE_SIZE / (sizeof(unsigned int))) - 1;
				bufferPageTopPointerWeight = 0;
				currentGTTIndex_weight += 1;
				gtt1_weight->GTTPointer1[currentGTTIndex_weight].vertexId = vertexCompletionInformation.vertexId;
				gtt1_weight->GTTPointer1[currentGTTIndex_weight].LPN = GraphLPNWeight;
				if(debug_mode == 1)
				xil_printf("W bPLTN %d %d %d\r\n",currentGTTIndex_weight, GraphLPNWeight, vertexCompletionInformation.vertexId);
			}
		}

		if (isEdges == 1) {
			numOfItemsToCopy =
			(valuesRemainingToAdd + 3) < numOfFreeElementsAvailableInTheBuffer ?
					valuesRemainingToAdd :
					numOfFreeElementsAvailableInTheBuffer - 3;
		} else {
			numOfItemsToCopy =
			(valuesRemainingToAdd + 3)
					< numOfFreeElementsAvailableInTheBufferWeight ?
					valuesRemainingToAdd :
					numOfFreeElementsAvailableInTheBufferWeight - 3;
		}
		if(debug_mode == 1)
			xil_printf("W bPLTN 1.1 %d %d %d %d\r\n", valuesRemainingToAdd, numOfFreeElementsAvailableInTheBuffer, bufferPageTopPointer, currentGTTIndex_edge);
		if (isEdges == 1) {
			GraphLRUBufWrite(GraphLPN, Test_variable_size_buffer,
			numOfItemsToCopy * sizeof(unsigned int),
			bufferPageTopPointer * sizeof(unsigned int),
			(getNumAdjVertices - valuesRemainingToAdd) * sizeof(unsigned int), gtt1->GTTPointer1[currentGTTIndex_edge].vertexId);
			if(debug_mode == 1)
				xil_printf("W bPLTN 1.2 %d %d %d %d\r\n", valuesRemainingToAdd, numOfFreeElementsAvailableInTheBuffer, bufferPageTopPointer, currentGTTIndex_edge);
		} else {
			GraphLRUBufWrite(GraphLPNWeight, Test_variable_size_buffer,
			numOfItemsToCopy * sizeof(unsigned int),
			bufferPageTopPointerWeight * sizeof(unsigned int),
			(2 * getNumAdjVertices - valuesRemainingToAdd) * sizeof(unsigned int), gtt1_weight->GTTPointer1[currentGTTIndex_weight].vertexId);
			if(debug_mode == 1)
				xil_printf("W bPLTN 1.2 %d %d %d %d\r\n", valuesRemainingToAdd, numOfFreeElementsAvailableInTheBufferWeight, bufferPageTopPointerWeight, currentGTTIndex_weight);
		}

		valuesRemainingToAdd = valuesRemainingToAdd - numOfItemsToCopy;
		if (isEdges == 1) {
			bufferPageTopPointer += numOfItemsToCopy;
			numOfFreeElementsAvailableInTheBuffer =
					numOfFreeElementsAvailableInTheBuffer - numOfItemsToCopy - 3;
		} else {
			bufferPageTopPointerWeight += numOfItemsToCopy;
			numOfFreeElementsAvailableInTheBufferWeight =
					numOfFreeElementsAvailableInTheBufferWeight - numOfItemsToCopy - 3;
		}
	} while (valuesRemainingToAdd > 0)
	return;
#endif
}

void bufferingPage_weight(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int getNumAdjVertices)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
	bufferingPage_loadToSSD(cmdSlotTag, prp0, prp1, getNumAdjVertices);
	vertexCompletionInformation.flags = 5;
	vertexCompletionInformation.cmdSlotTag = cmdSlotTag;
	vertexCompletionInformation.prp0 = prp0;
	vertexCompletionInformation.prp1 = prp1;
	vertexCompletionInformation.isEdge = 1;
	vertexCompletionInformation.NumOfAdjVertices = getNumAdjVertices;
	vertexCompletionInformation.numberOfVerticesRemainingToFetch = 2*getNumAdjVertices;
//	xil_printf("W bufferingPage_weight %d %d\r\n", vertexCompletionInformation.vertexId, 2*getNumAdjVertices);
	bufferingPage_loadToNAND(getNumAdjVertices, 1);
	bufferingPage_loadToNAND(getNumAdjVertices, 0);
    return;
#endif
}

void bufferingPage(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int getNumAdjVertices)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
	if(debug_mode == 1)
		xil_printf("bP %d\r\n",getNumAdjVertices);
	bufferingPage_loadToSSD(cmdSlotTag, prp0, prp1, getNumAdjVertices);
	vertexCompletionInformation.flags = 3;
	vertexCompletionInformation.cmdSlotTag = cmdSlotTag;
	vertexCompletionInformation.prp0 = prp0;
	vertexCompletionInformation.prp1 = prp1;
	vertexCompletionInformation.isEdge = 0;
	vertexCompletionInformation.NumOfAdjVertices = getNumAdjVertices;
	vertexCompletionInformation.numberOfVerticesRemainingToFetch = getNumAdjVertices;
	if(debug_mode == 1)
		xil_printf("W 1 %d %d\r\n", vertexCompletionInformation.vertexId, getNumAdjVertices);
	bufferingPage_loadToNAND(getNumAdjVertices, 1);
    return;
#endif
}

void getEdgesToSSDDram_calcNumAdjVertices(unsigned int vID, unsigned int *NumberOfAdjVertices)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	*NumberOfAdjVertices = 0;
	if(gtlTable1->GTLPoiniter1[vID].direct == 1)
	{
		*NumberOfAdjVertices = gtlTable1->GTLPoiniter1[vID].Number;
	}
	else {
		unsigned int numPages = 0;
		for(numPages = 0; numPages < gtlTable1->GTLPoiniter1[vID].Number; numPages++)
		{
			*NumberOfAdjVertices += ((unsigned int *)(gtlTable1->GTLPoiniter1[vID].LPN))[numPages*3+2];
		}
	}
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
#endif
}

void getEdgesToSSDDram_initiateLoading(unsigned int cmdSlotTag, unsigned int gttVidIndex, unsigned int gttVidNumber, unsigned int isEdge)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	//Run a for loop for all the page entries
	int i=0;
	for(i = 0; i < gttVidNumber; i++)
	{
		if(isEdge == 0)
			GraphLRUBufRead(cmdSlotTag, gtt1->GTTPointer1[gttVidIndex+i].LPN);
		else if(isEdge == 1)
			GraphLRUBufRead(cmdSlotTag, gtt1_weight->GTTPointer1[gttVidIndex+i].LPN);
	}
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
#endif
	return;
}

void getEdgeWeight(unsigned int vID, unsigned int cmdSlotTag, unsigned int destEdge, unsigned int prp0, unsigned int prp1)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	vertexCompletionInformation.flags = 5;
	vertexCompletionInformation.vertexId = vID;
	vertexCompletionInformation.cmdSlotTag = cmdSlotTag;
	vertexCompletionInformation.prp0 = prp0;
	vertexCompletionInformation.prp1 = prp1;
	vertexCompletionInformation.destEdge = destEdge;
	unsigned int gttVidIndex, gttVidNumber, gttVidIndex_weight, gttVidNumber_weight;
	binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, vID, &gttVidIndex, &gttVidNumber);
	binarySearch( gtt1_weight->GTTPointer1, 0, currentGTTIndex_weight, vID, &gttVidIndex_weight, &gttVidNumber_weight);
	vertexCompletionInformation.numberOfVerticesRemainingToFetch = gttVidNumber + gttVidNumber_weight;
	vertexCompletionInformation.isEdge = 1;
	vertexCompletionInformation.NumOfAdjVertices = 0;
	vertexCompletionInformation.NumOfAdjVertices_weight = 0;
	getEdgesToSSDDram_initiateLoading(cmdSlotTag, gttVidIndex, gttVidNumber, 0);
	getEdgesToSSDDram_initiateLoading(cmdSlotTag, gttVidIndex_weight, gttVidNumber_weight, 1);
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
#endif
	return;
}

void getAdjEdgeList(unsigned int vID, unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	vertexCompletionInformation.flags = 2;
	vertexCompletionInformation.vertexId = vID;
	vertexCompletionInformation.cmdSlotTag = cmdSlotTag;
	vertexCompletionInformation.prp0 = prp0;
	vertexCompletionInformation.prp1 = prp1;
	unsigned int gttVidIndex, gttVidNumber;
	//Check if we need to add right+1 or only right
//	xil_printf("R 1 vID = %d\r\n", vID);
	binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, vID, &gttVidIndex, &gttVidNumber);
//	xil_printf("R 2 %d %d %d \r\n", currentGTTIndex_edge, gttVidIndex, gttVidNumber);
	vertexCompletionInformation.numberOfVerticesRemainingToFetch = gttVidNumber;
	vertexCompletionInformation.isEdge = 1;
	vertexCompletionInformation.NumOfAdjVertices = 0;
	getEdgesToSSDDram_initiateLoading(cmdSlotTag, gttVidIndex, gttVidNumber, 0);
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
#endif
	return;
}

void getTwoHopAdjEdgeList(unsigned int vID, unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
	vertexCompletionInformation.flags = 8;
	vertexCompletionInformation.vertexId = vID;
	vertexCompletionInformation.cmdSlotTag = cmdSlotTag;
	vertexCompletionInformation.prp0 = prp0;
	vertexCompletionInformation.prp1 = prp1;
	unsigned int gttVidIndex, gttVidNumber;
	binarySearch( gtt1->GTTPointer1, 0, currentGTTIndex_edge, vID, &gttVidIndex, &gttVidNumber);
	vertexCompletionInformation.numberOfVerticesRemainingToFetch = gttVidNumber;
	vertexCompletionInformation.isEdge = 1;
	vertexCompletionInformation.NumOfAdjVertices = 0;
	vertexCompletionInformation.hopNum = 1;
	getEdgesToSSDDram_initiateLoading(cmdSlotTag, gttVidIndex, gttVidNumber, 0);
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
#endif
	return;
}

void InitializeGTT(unsigned int cmdSlotTag, unsigned int prp0, unsigned int prp1, unsigned int numOfEntries)
{
	bufferingPage_loadToSSD(cmdSlotTag, prp0, prp1, numOfEntries);
	unsigned int i = 0;
	for(i = 0; i < numOfEntries; i++)
	{
		currentGTTIndex_edge += 1;
		gtt1->GTTPointer1[currentGTTIndex_edge].vertexId = ((unsigned int *)Test_variable_size_buffer)[2*i];
		gtt1->GTTPointer1[currentGTTIndex_edge].LPN = ((unsigned int *)Test_variable_size_buffer)[2*i+1];
	}
//	GraphLPN = ((unsigned int *)Test_variable_size_buffer)[2*numOfEntries+1];
}
#endif
