#include "GraphUtil.h"

#if	(TEST_VARIABLE_SIZE_TRANSFER_ == 1 | GRAPH_SSD_ == 1)
extern unsigned int Test_variable_size_buffer;
#endif

void CopyFromHost(unsigned int AddressToCopy, unsigned int prp0, unsigned int prp1, unsigned int prpLen)
{
	set_direct_rx_dma(AddressToCopy, prp1, prp0, prpLen);
	check_direct_rx_dma_done();
	return;
}

inline unsigned char get_flags_from_cmd_slot_tag(unsigned int cmdSlotTag)
{
        unsigned int cmdAddr = NVME_CMD_SRAM_ADDR + (cmdSlotTag * 64);
        unsigned int cmdDword = IO_READ32(cmdAddr);
        unsigned char flags = (cmdDword & 0xFF00) >> 8;
        return (flags);
}

void copyFromDeviceToHost(unsigned int deviceAddress, unsigned int prp1, unsigned int prp0, unsigned int length)
{
//	memcpy((void *)Test_variable_size_buffer, (void *)deviceAddress, length);
	set_direct_tx_dma(deviceAddress, prp1, prp0, length);
//	set_direct_tx_dma(Test_variable_size_buffer, prp1, prp0, length);
	check_direct_tx_dma_done();
	return;
}

