#ifndef _GraphUTIL_H_
#define _GraphUTIL_H_

#include "nvme/nvme.h"
#include "nvme/host_lld.h"
#include "nvme/io_access.h"
#include "xil_printf.h" // for printf

#define TEST_VARIABLE_SIZE_TRANSFER_ 0
#define TEST_VARIABLE_SIZE_TRANSFER_AUTO_ 1
#define GRAPH_SSD_ 1
#define USE_DEFINE_ 1 //Change it ini GraphSSDController.h also
#define EDGE_WEIGHT_ 1 //Change it ini GraphSSDController.h also
#define TEST_COMPLETION_STATUS 0 //Uncomment corresponding code if you want it // Change it in memory_map.h also

void copyFromDeviceToHost(unsigned int deviceAddress, unsigned int prp1, unsigned int prp0, unsigned int length);
void CopyFromHost(unsigned int AddressToCopy, unsigned int prp0, unsigned int prp1, unsigned int prpLen);
inline unsigned char get_flags_from_cmd_slot_tag(unsigned int cmdSlotTag);

#endif  // _GraphUTIL_H_
