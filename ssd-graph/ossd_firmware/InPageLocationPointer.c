#include "GraphSSDController.h"
#include	"memory_map.h"
#include "GraphUtil.h"

unsigned int CopyFunction_InPage(unsigned int devAddr, unsigned int Test_variable_size_buffer, unsigned int currentTopPointer, unsigned int vertexId)
{
//	xil_printf("R 1.1.1.1 %d %d %d\r\n", devAddr, currentTopPointer, vertexId);
	unsigned int lowestVertexId = ((unsigned int *)devAddr)[(PAGE_SIZE / sizeof(unsigned int)) - 2];
//	xil_printf("R 1.1.1.2 %d \r\n", lowestVertexId);
	unsigned int distance = vertexId - lowestVertexId;
//	xil_printf("R 1.1.1.3 %d \r\n", distance);
	unsigned int offset = ((unsigned int *)devAddr)[(PAGE_SIZE / sizeof(unsigned int)) - (3*distance + 3)];
//	xil_printf("R 1.1.1.4 %d\r\n", offset);
	unsigned int number = ((unsigned int *)devAddr)[(PAGE_SIZE / sizeof(unsigned int)) - (3*distance + 4)];
//	xil_printf("R 1.1.1.5 %d %d %d %d\r\n", lowestVertexId, distance, offset, number);
//	xil_printf("R 1.1.1.6 %d\r\n", Test_variable_size_buffer);
	memcpy((void *)(Test_variable_size_buffer + currentTopPointer), (void *)(devAddr + offset), number * sizeof(unsigned int));
//	xil_printf("R 1.1.1.7 %d %d %d %d\r\n", lowestVertexId, distance, offset, number);
	return number;
}

unsigned int binarySearch(struct GTTPointer *arr, unsigned int l, unsigned int r, unsigned int x, unsigned int *index, unsigned int *number)
{
        while (l < r)
        {
                int m = l + (r-l)/2;
                //xil_printf("l = %d %d m = %d %d r = %d %d\n", l, arr[l].vertexId, m, arr[m].vertexId, r, arr[r].vertexId);

                if (arr[m].vertexId == x) // Check if x is present at mid
                {
                        unsigned int start = m;
                        m = m-1;
                        while((m >= 0) && (arr[m].vertexId == x))
                        {
                                start = m;
                                m = m-1;
                        }
                        m = start;
                        unsigned int count=0;
                        while((m <= r) && (arr[m].vertexId == x))
                        {
                                count++;
                                m++;
                        }
                        *index = start;
                        *number = count;
                        return 0;
                }
                else if (arr[m].vertexId < x) // If x greater, ignore left half
                {
                        if(x < arr[m+1].vertexId)
                        {
                                *index = m;
                                *number = 1;
                                return 0;
                        }
                        else if(x > arr[m+1].vertexId)
                        {
                                l = m+1;
                        }
                        else if(x == arr[m+1].vertexId)
                        {
                                unsigned int start = m+1;
                                m = start;
                                unsigned int count = 0;
                                while((m <= r) && (arr[m].vertexId == x))
                                {
                                        count++;
                                        m++;
                                }
                                *index = start;
                                *number = count;
                                return 0;
                        }
                }
                else if(arr[m].vertexId > x) // If x is smaller, ignore right half
                {
                        r = m;
                }
        }
        if(l == r)
        {
                if(arr[l].vertexId <= x)
                {
                        *index = l;
                        *number = 1;
                        return 0;
                }
                else {
                        return 1;
                }
        }

        // if we reach here, then element was not present
        return 1;
}

unsigned int findEdge_inBuffer(unsigned int Test_variable_size_buffer, unsigned int Test_variable_size_buffer_weight, unsigned int destEdge, unsigned int NumberOfAdjVertices)
{
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
	int i;
	for(i=0; i < NumberOfAdjVertices; i++)
	{
		if(destEdge == ((unsigned int *)Test_variable_size_buffer)[i])
		{
			return ((unsigned int *)Test_variable_size_buffer_weight)[i];
		}
	}
	return -1;
#endif
}
