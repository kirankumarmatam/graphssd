//////////////////////////////////////////////////////////////////////////////////
// nvme_io_cmd.c for Cosmos+ OpenSSD
// Copyright (c) 2016 Hanyang University ENC Lab.
// Contributed by Yong Ho Song <yhsong@enc.hanyang.ac.kr>
//				  Youngjin Jo <yjjo@enc.hanyang.ac.kr>
//				  Sangjin Lee <sjlee@enc.hanyang.ac.kr>
//				  Jaewook Kwak <jwkwak@enc.hanyang.ac.kr>
//
// This file is part of Cosmos+ OpenSSD.
//
// Cosmos+ OpenSSD is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3, or (at your option)
// any later version.
//
// Cosmos+ OpenSSD is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Cosmos+ OpenSSD; see the file COPYING.
// If not, see <http://www.gnu.org/licenses/>.
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// Company: ENC Lab. <http://enc.hanyang.ac.kr>
// Engineer: Sangjin Lee <sjlee@enc.hanyang.ac.kr>
//			 Jaewook Kwak <jwkwak@enc.hanyang.ac.kr>
//
// Project Name: Cosmos+ OpenSSD
// Design Name: Cosmos+ Firmware
// Module Name: NVMe IO Command Handler
// File Name: nvme_io_cmd.c
//
// Version: v1.0.1
//
// Description:
//   - handles NVMe IO command
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
// Revision History:
//
// * v1.0.1
//   - header file for buffer is changed from "ia_lru_buffer.h" to "lru_buffer.h"
//
// * v1.0.0
//   - First draft
//////////////////////////////////////////////////////////////////////////////////


#include "xil_printf.h"
#include "debug.h"
#include "io_access.h"

#include "nvme.h"
#include "host_lld.h"
#include "nvme_io_cmd.h"

#include "../lru_buffer.h"

#include "../GraphSSDController.h"
#include "../GraphUtil.h"
#include "../memory_map.h"

#if	(TEST_VARIABLE_SIZE_TRANSFER_ == 1 | GRAPH_SSD_ == 1)
extern unsigned int Test_variable_size_buffer;
extern struct GTLTable1 *gtlTable1;
extern struct GTLTable1Weight *gtlTable1Weight;
#if (USE_DEFINE_ == 0)
extern unsigned int MAX_NUM_OF_VERTICES;
extern unsigned int GTL_POINTER1_ADDR;
#endif //USE_DEFINE_ == 0
extern unsigned int GraphLPN;
extern unsigned int currentRetListAddress;
extern unsigned int GraphLPNWeight;
extern unsigned int currentRetListAddressWeight;
#endif
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER == 1 | GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
extern unsigned int numOfFreeElementsAvailableInTheBuffer;
extern unsigned int bufferPageTopPointer;
extern unsigned int numOfFreeElementsAvailableInTheBufferWeight;
extern unsigned int bufferPageTopPointerWeight;
extern unsigned int currentGTTIndex_edge;
extern unsigned int currentGTTIndex_weight;
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1)
extern struct VertexCompletionInformation vertexCompletionInformation;
extern struct CompletionQueues *gtlCompletionQueues;
extern struct CompletionQueuePtrs *gtlCompletionQueuePtrs;
#endif //GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO_COMPLETION == 1
#endif

void handle_nvme_io_read(unsigned int cmdSlotTag, NVME_IO_COMMAND *nvmeIOCmd)
{
	IO_READ_COMMAND_DW12 readInfo12;
	//IO_READ_COMMAND_DW13 readInfo13;
	//IO_READ_COMMAND_DW15 readInfo15;
	unsigned int startLba[2];
	unsigned int nlb;

	readInfo12.dword = nvmeIOCmd->dword[12];
	//readInfo13.dword = nvmeIOCmd->dword[13];
	//readInfo15.dword = nvmeIOCmd->dword[15];

	startLba[0] = nvmeIOCmd->dword[10];
	startLba[1] = nvmeIOCmd->dword[11];
	nlb = readInfo12.NLB;

#if (GRAPH_SSD_ == 0)
	ASSERT(startLba[0] < storageCapacity_L && (startLba[1] < STORAGE_CAPACITY_H || startLba[1] == 0));
#endif
	//ASSERT(nlb < MAX_NUM_OF_NLB);
	ASSERT((nvmeIOCmd->PRP1[0] & 0xF) == 0 && (nvmeIOCmd->PRP2[0] & 0xF) == 0); //error
	ASSERT(nvmeIOCmd->PRP1[1] < 0x10 && nvmeIOCmd->PRP2[1] < 0x10);

#if (GRAPH_SSD_ == 1)
	char flags = (nvmeIOCmd->dword[0] << 16) >> 24;
	if(flags == 3)
	{
		getAdjEdgeList(startLba[0], cmdSlotTag, nvmeIOCmd->PRP1[0], nvmeIOCmd->PRP1[1]);
		return;
	}
	else if(flags == 5)
	{
		getEdgeWeight(startLba[0], cmdSlotTag, nvmeIOCmd->dword14, nvmeIOCmd->PRP1[0], nvmeIOCmd->PRP1[1]);
		return;
	}
	else if(flags == 6)
	{
//		xil_printf("UW %d\r\n", nvmeIOCmd->dword15);
		return;
	}
	else if(flags == 7)
	{
		unsigned int retValue = GraphLRUBufRead_completionTest(cmdSlotTag, nvmeIOCmd->PRP1[0], nvmeIOCmd->PRP1[1], startLba[0], 1, 0, 0, 0);
		if(retValue == 1)
		{
			copyFromDeviceToHost(Test_variable_size_buffer, nvmeIOCmd->PRP1[1], nvmeIOCmd->PRP1[0], ceil((sizeof(unsigned int)*1.0)/16)*16);
			NVME_COMPLETION nvmeCPL;
			nvmeCPL.dword[0] = 0;
			nvmeCPL.specific = 0x0;
			set_auto_nvme_cpl(cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
		}
		return;
	}
	else if(flags == 8)
	{
		getTwoHopAdjEdgeList(startLba[0], cmdSlotTag, nvmeIOCmd->PRP1[0], nvmeIOCmd->PRP1[1]);
		return;
	}
	else if(flags == 9)
	{
		unsigned int length = nvmeIOCmd->dword14;
		unsigned int prp[2];
		prp[0] = nvmeIOCmd->PRP1[0];
		prp[1] = nvmeIOCmd->PRP1[1];
//		*((volatile unsigned int *)Test_variable_size_buffer) = 65;
//		memset( (void *)Test_variable_size_buffer, 99, 16);
//#if (TEST_VARIABLE_SIZE_TRANSFER_DIRECT_ == 1)
		unsigned int numItems = length / sizeof(unsigned int);
		if (numItems < (SECTOR_SIZE_FTL / sizeof(unsigned int))) {
							copyFromDeviceToHost(Test_variable_size_buffer,
									prp[1],
									prp[0],
									ceil((numItems * sizeof(unsigned int) * 1.0) / 16)
											* 16);
						} else {
							int i = 0;
							for (i = 0;
									i
											< ceil(
													(numItems * sizeof(unsigned int)
															* 1.0) / SECTOR_SIZE_FTL);
									i++) {
								if(i >= 256)
									xil_printf("ERROR! i = %d numItems = %d\r\n", i, numItems);
								set_auto_tx_dma(cmdSlotTag,
										i,
										Test_variable_size_buffer + i * SECTOR_SIZE_FTL);
							}
							check_auto_tx_dma_done();
						}
//		set_direct_tx_dma(Test_variable_size_buffer, prp[1], prp[0], length);
	//	check_direct_tx_dma_done();
//#endif
/*#if (TEST_VARIABLE_SIZE_TRANSFER_AUTO_ == 1)
		set_auto_tx_dma(cmdSlotTag, 0, Test_variable_size_buffer);
		set_auto_tx_dma(cmdSlotTag, 1, Test_variable_size_buffer+SECTOR_SIZE_FTL);
		check_auto_tx_dma_done();
#endif
	*/	NVME_COMPLETION nvmeCPL;
		nvmeCPL.dword[0] = 0;
		nvmeCPL.specific = 0x0;
		set_auto_nvme_cpl(cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
/*		unsigned int i=0;
		for(i=0; i < 16; i++)
		{
			if(i < 8)
				xil_printf("%d ",*((volatile char *)Test_variable_size_buffer + i));
			else
				xil_printf("%d ",*((volatile char *)Test_variable_size_buffer + 4096 + i));
		}
		xil_printf("\r\n");
	*/  return;
	}
#endif

	HOST_REQ_INFO hostCmd;
	hostCmd.curSect = startLba[0];
	hostCmd.reqSect = nlb + 1;
	hostCmd.cmdSlotTag = cmdSlotTag;

	//For collecting access statistics
	LRUBufRead(&hostCmd);
}

void handle_nvme_io_write(unsigned int cmdSlotTag, NVME_IO_COMMAND *nvmeIOCmd)
{
	IO_READ_COMMAND_DW12 writeInfo12;
	//IO_READ_COMMAND_DW13 writeInfo13;
	//IO_READ_COMMAND_DW15 writeInfo15;
	unsigned int startLba[2];
	unsigned int nlb;

	writeInfo12.dword = nvmeIOCmd->dword[12];
	//writeInfo13.dword = nvmeIOCmd->dword[13];
	//writeInfo15.dword = nvmeIOCmd->dword[15];

	if(writeInfo12.FUA == 1)
		xil_printf("write FUA\r\n");

	startLba[0] = nvmeIOCmd->dword[10];
	startLba[1] = nvmeIOCmd->dword[11];
	nlb = writeInfo12.NLB;

#if (GRAPH_SSD_ == 0)
	ASSERT(startLba[0] < storageCapacity_L && (startLba[1] < STORAGE_CAPACITY_H || startLba[1] == 0));
#endif
	//ASSERT(nlb < MAX_NUM_OF_NLB);
	ASSERT((nvmeIOCmd->PRP1[0] & 0xF) == 0 && (nvmeIOCmd->PRP2[0] & 0xF) == 0);
	ASSERT(nvmeIOCmd->PRP1[1] < 0x10 && nvmeIOCmd->PRP2[1] < 0x10);

#if (GRAPH_SSD_ == 1)
	char flags = (nvmeIOCmd->dword[0] << 16) >> 24;
	if(flags == 2)
	{
		unsigned int vID = startLba[0];

		unsigned int getNumAdjVertices = nvmeIOCmd->dword14;
		vertexCompletionInformation.vertexId = vID;
#if(USE_DEFINE_ == 1)
		bufferingPage(cmdSlotTag, nvmeIOCmd->PRP1[0], nvmeIOCmd->PRP1[1], getNumAdjVertices);
#endif
		return;
	}
	else if(flags == 4)
	{
	GraphLPN = 50-1;// Also initialized in init_ftl.c
	currentRetListAddress = 0;// Also initialized in init_ftl.c
	currentRetListAddressWeight = 0;// Also initialized in init_ftl.c
	GraphLPNWeight = 786407-1;// Also initialized in init_ftl.c
#if (GTL_INTERMMEDIATE_VERTEX_BUFFER == 1 | GTL_INTERMMEDIATE_VERTEX_BUFFER_AUTO == 1)
	numOfFreeElementsAvailableInTheBuffer = 0;
	bufferPageTopPointer = 0;
	numOfFreeElementsAvailableInTheBufferWeight = 0;
	bufferPageTopPointerWeight = 0;
	currentGTTIndex_edge = -1;
	currentGTTIndex_weight = -1;

#endif
	int i,j;
	for(i=0; i < MAX_CHANNEL_NUM; i++)
	{
		for(j=0; j < MAX_WAY_NUM; j++)
		{
			//When (front == rear) then it is empty, when (((rear+1)%MAX_COMPLETION_QUEUE_ENTRY) == (front % MAX_COMPLETION_QUEUE_ENTRY)) then it is full
			(gtlCompletionQueuePtrs->completionQueuePointer[i][j]).front = 0;
			(gtlCompletionQueuePtrs->completionQueuePointer[i][j]).rear = 0;
		}
	}
	vertexCompletionInformation.flags = 0;
	vertexCompletionInformation.vertexId = -1;
	NAND_PAGE_ACCESS_EVICTION = 0;
	NAND_PAGE_ACCESS_READ = 0;
//		Graph_Initialize(startLba[0]);
	}
	else if(flags == 5)
	{
		unsigned int vID = startLba[0];
		unsigned int getNumAdjVertices = nvmeIOCmd->dword14;
		vertexCompletionInformation.vertexId = vID;
		bufferingPage_weight(cmdSlotTag, nvmeIOCmd->PRP1[0], nvmeIOCmd->PRP1[1], getNumAdjVertices);
		return;
	}
	else if(flags == 6)
	{
		xil_printf("NAND_PAGE_ACCESSES_EVICTION = %d\r\n", NAND_PAGE_ACCESS_EVICTION);
		xil_printf("NAND_PAGE_ACCESS_READ = %d\r\n", NAND_PAGE_ACCESS_READ);
		NVME_COMPLETION nvmeCPL;
		nvmeCPL.dword[0] = 0;
		nvmeCPL.specific = 0x0;
		set_auto_nvme_cpl(cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
		return;
	}
	if(flags == 4)
	{
		NVME_COMPLETION nvmeCPL;
		nvmeCPL.dword[0] = 0;
		nvmeCPL.specific = 0x0;
		set_auto_nvme_cpl(cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
		return;
	}
	else if(flags == 7)
	{
		debug_mode = 0;
		int i,j;
		for(i=0; i < MAX_CHANNEL_NUM; i++)
		{
			for(j=0; j < MAX_WAY_NUM; j++)
			{
				(gtlCompletionQueuePtrs->completionQueuePointer[i][j]).front = 0;
				(gtlCompletionQueuePtrs->completionQueuePointer[i][j]).rear = 0;
			}
		}
		NVME_COMPLETION nvmeCPL;
		nvmeCPL.dword[0] = 0;
		nvmeCPL.specific = 0x0;
		set_auto_nvme_cpl(cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
		return;
	}
	else if(flags == 8)
	{
		unsigned int numOfEntries = startLba[0];
		GraphLPN = nvmeIOCmd->dword14;
		InitializeGTT(cmdSlotTag, nvmeIOCmd->PRP1[0], nvmeIOCmd->PRP1[1], numOfEntries);
		NVME_COMPLETION nvmeCPL;
		nvmeCPL.dword[0] = 0;
		nvmeCPL.specific = 0x0;
		set_auto_nvme_cpl(cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
		return;
	}
	else if(flags == 9)
	{
		unsigned int length = nvmeIOCmd->dword14;
		unsigned int prp[2];
		prp[0] = nvmeIOCmd->PRP1[0];
		prp[1] = nvmeIOCmd->PRP1[1];
		bufferingPage_loadToSSD(cmdSlotTag, prp[0], prp[1], length/(sizeof(unsigned int)));
//		set_direct_rx_dma(Test_variable_size_buffer, prp[1], prp[0], length);
//		check_direct_rx_dma_done();
		NVME_COMPLETION nvmeCPL;
		nvmeCPL.dword[0] = 0;
		nvmeCPL.specific = 0x0;
		set_auto_nvme_cpl(cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
/*		unsigned int i=0;
		for(i=0; i < 16; i++)
		{
			if(i < 8)
				xil_printf("%d ",*((volatile char *)Test_variable_size_buffer + i));
			else
				xil_printf("%d ",*((volatile char *)Test_variable_size_buffer + 4096 + i));
		}
		xil_printf("\r\n");
	*/	return;
	}
#endif

	HOST_REQ_INFO hostCmd;
	hostCmd.curSect = startLba[0];
	hostCmd.reqSect = nlb + 1;
	hostCmd.cmdSlotTag = cmdSlotTag;

	LRUBufWrite(&hostCmd);
}

void handle_nvme_io_cmd(NVME_COMMAND *nvmeCmd)
{
	NVME_IO_COMMAND *nvmeIOCmd;
	NVME_COMPLETION nvmeCPL;
	unsigned int opc;

	nvmeIOCmd = (NVME_IO_COMMAND*)nvmeCmd->cmdDword;
	opc = (unsigned int)nvmeIOCmd->OPC;

	switch(opc)
	{
		case IO_NVM_FLUSH:
		{
			xil_printf("IO Flush Command\r\n");
			nvmeCPL.dword[0] = 0;
			nvmeCPL.specific = 0x0;
			set_auto_nvme_cpl(nvmeCmd->cmdSlotTag, nvmeCPL.specific, nvmeCPL.statusFieldWord);
			break;
		}
		case IO_NVM_WRITE:
		{
			//xil_printf("IO Write Command\r\n");
			handle_nvme_io_write(nvmeCmd->cmdSlotTag, nvmeIOCmd);
			break;
		}
		case IO_NVM_READ:
		{
			//xil_printf("IO Read Command\r\n");
			handle_nvme_io_read(nvmeCmd->cmdSlotTag, nvmeIOCmd);
			break;
		}
		default:
		{
			xil_printf("Not Support IO Command OPC: %X\r\n", opc);
			ASSERT(0);
			break;
		}
	}
}

